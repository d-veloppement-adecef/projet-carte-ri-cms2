/* 
 * File:   Eeprom.c
 * Author: LFRAD
 *
 * Created on 26 ao�t 2021, 10:09
 */

#include <p18f8723.h>
#include <stdio.h>
#include <stdlib.h>
#include "RecupCapteur.h"
#include <i2c.h>

signed char EESequRead(unsigned char cControl, unsigned int iAdresse, unsigned char *cTrame, unsigned char cLongueur) { //Lecture dans l'Eeprom numero cControl � l'adresse indiqu�e et stockage dans la trame en entr�e sortie
    unsigned char cADDE;
    unsigned char cAdressL, cAdressH;

    cADDE = (cControl << 1) & 0xFF; //Mise en place de l'octet a envoyer 
    cADDE = cADDE | 0xA0;

    cAdressL = (iAdresse) & 0xFF; //D�coupage de l'adresse en 2 octets
    cAdressH = (iAdresse) >> 8;
    IdleI2C(); // Lancement com I2C
    StartI2C(); // Debut com I2C
    if (PIR2bits.BCLIF) { // test du bus de collision
        PIR2bits.BCLIF = 0;
        return (-1); // return l'erreur du bus de collision
    } else { //Si pas de bus de collision
        if (WriteI2C(cADDE)) {// Debut communication avec l'Eeprom souhait�e
            StopI2C(); //Stop la com si erreur dans l'envoie
            return (-3); // Erreur de collision
        }

        if (!SSPCON2bits.ACKSTAT) { // Si on recoit l'acquitement de l'Eeprom
            if (WriteI2C(cAdressH)) {// Ecriture adresse haute
                StopI2C(); //Stop la com si erreur dans l'envoie
                return (-3); // Erreur de collision
            }

            while (SSPCON2bits.ACKSTAT); //Attente de reception de l'acquitement
            if (!SSPCON2bits.ACKSTAT) {// Si on recoit l'acquitement de l'Eeprom
                if (WriteI2C(cAdressL)) {// Ecriture adresse basse
                    StopI2C(); //Stop la com si erreur dans l'envoie
                    return (-3); // Erreur de collision
                }

                while (SSPCON2bits.ACKSTAT); //Attente de reception de l'acquitement
                if (!SSPCON2bits.ACKSTAT) { // Si on recoit l'acquitement de l'Eeprom
                    RestartI2C(); // Restart de l'I2C
                    if (WriteI2C(cADDE + 1)) {//Demande d'envoie des donn�es de l'Eeprom
                        StopI2C(); //Stop la com si erreur dans l'envoie
                        return (-3); // Erreur de collision
                    }

                    while (SSPCON2bits.ACKSTAT); //Attente de reception de l'acquitement
                    if (!SSPCON2bits.ACKSTAT) {// Si on recoit l'acquitement de l'Eeprom
                        if (getsI2C(cTrame, cLongueur)) {//On r�cup�re les donn�es de l'Eeprom
                            return (-1); // SI r�cup�ration impossible, erreur
                        }
                        NotAckI2C(); // send not ACK condition 
                        StopI2C(); // send STOP condition
                        if (PIR2bits.BCLIF) { // test for bus collision
                            PIR2bits.BCLIF = 0;
                            return (-1); // return with Bus Collision error 
                        }
                    } else {
                        StopI2C();
                        return (-2); // return with Not Ack error
                    }
                } else {
                    StopI2C();
                    return (-2); // return with Not Ack error
                }
            } else {
                StopI2C();
                return (-2); // return with Not Ack error condition   
            }
        } else {
            StopI2C();
            return (-2); // return with Not Ack error
        }
    }
    return (0); // return with no error
}

signed char EEPaWrite(unsigned char cControl, unsigned int iAdresse, unsigned char *cTrame) {
    unsigned char cADDE;
    unsigned char cAdressL, cAdressH;

    cADDE = (cControl << 1) & 0xFF;
    cADDE = cADDE | 0xA0;

    cAdressL = (iAdresse) & 0xFF;
    cAdressH = (iAdresse) >> 8;

    IdleI2C(); // ensure module is idle
    StartI2C(); // initiate START condition
    if (PIR2bits.BCLIF) // test for bus collision
    {
        PIR2bits.BCLIF = 0;
        return (-1); // return with Bus Collision error 
    } else {
        if (WriteI2C(cADDE)) // write 1 byte - R/W bit should be 0
        {
            StopI2C();
            return (-3); // return with write collision error
        }
        IdleI2C();
        if (WriteI2C(cAdressH)) // write word address for EEPROM
        {
            StopI2C();
            return (-3); // set error for write collision
        }
        IdleI2C();
        if (WriteI2C(cAdressL)) // data byte for EEPROM
        {
            StopI2C();
            return (-3); // set error for write collision
        }
        IdleI2C();
        if (putsI2C(cTrame)) {
            StopI2C();
            return (-4); // bus device responded possible error
        }
    }
    StopI2C(); // send STOP condition
    if (PIR2bits.BCLIF) // test for Bus collision
    {
        PIR2bits.BCLIF = 0;
        return (-1); // return with Bus Collision error 
    }
    EEAckPolling(0xA0);
    return (0); // return with no error
}

unsigned int calcul_addmemoire(unsigned char cVoie, unsigned char cCapteur) { //Calcul de l'adresse en fonction de la voie et du capteur
    unsigned int iAdresse;
    iAdresse = 2176 * (cVoie % 30);
    iAdresse = iAdresse + 128 * cCapteur;
    return iAdresse;
}

unsigned char choix_memoire(unsigned char cVoie) { //Calcul de l'Eeprom en fonction de la voie
    unsigned char cMemoire;
    cMemoire = cVoie / 30;
    return cMemoire;
}
