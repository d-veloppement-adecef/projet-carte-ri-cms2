/* 
 * File:   Mesure.c
 * Author: LFRAD
 *
 * Created on 26 ao�t 2021, 09:21
 */

#include <p18f8723.h>
#include <stdio.h>
#include <stdlib.h>
#include "RecupCapteur.h"



unsigned int iComptDebordement; //	compteur de d�bordement du timer1  (de 65535 � 0000)
unsigned int iComptInt2;

unsigned int iTimer1L; // pour sauvegarder les valeurs � l'interruption de INT2 (utilis� dans la mesure de fr�quence)
unsigned int iTimer1H;


unsigned int Mesure_I_Conso(char cMesure, unsigned int iCoeff) {// Acquisition de la valeur CAN du courant de consommation en MAx10
    float Val_Temp_Conso;   //Valeur temporel du courant de conso
    Val_Temp_Conso = 10 * Coefficient * acqui_ana_16_plus(cMesure) * iCoeff / 100; //Acquisition courant conso
    return ((char) Val_Temp_Conso); //Renvoie courant de conso
}

unsigned int Mesure_I_Modul(char cMesure, unsigned int iCoeff) {// Acquisition de la valeur CAN du courant de modulation MA*10
    float Val_Temp_Modul;   //Valeur tempo Courant Modulation
    Val_Temp_Modul = 10 * Coefficient * acqui_ana_16_plus(cMesure) * iCoeff / 100; //Acquisition courant Modulation
    return ((char) Val_Temp_Modul); //Renvoie courant modulation
}

unsigned int Mesure_I_Repos(char cMesure, unsigned int iCoeff) { // Acquisition de la valeur CAN de la tension ligne AVal capteur �A
    float fRepos;   //Valeur tempo courant repos
    fRepos = Coefficient * acqui_ana_16_plus(cMesure) * iCoeff;    //Acquisition courant repos
    return ((int) fRepos);  //Renvoie courant repos
}

unsigned int Mesure_Freq(void) { //Mesure de la fr�quence
    unsigned int iComptTimeOut; //compteur time out
    unsigned int iFreqOut;
    float fFreq;

    // mesure sur 1 p�riode du signal fr�quence capteur par int2(RB2) d�clench�e sur front montant et par le nbre de boucles du timer1 sur 16bits
    // l'horloge du timer1 est celle du quarts de 24Mhz/4= 6Mhz, donc une boucle timer = 1 / (6Mhz) = 0.000000166sec = 0.166�s
    // la valeur de la fr�quence capteur = 1/(0.166�s * val_timer1)
    // valeur d'1 p�riode =   [(iComptDebordement * 65536 + TempH * 256 + TempL) * 0.166]  normalement   iComptDebordement sera �gal � 0 si >0 => erreur

    iComptDebordement = 0x00; // compteur de d�bordement timer1
    iComptInt2 = 0x00;
    fFreq = 0x0000;
    TMR1H = 0x0000; // registre H 8bit du timer 1
    TMR1L = 0x0000; // registre L 16bit du timer 1
    iTimer1L = 0x0000;
    iTimer1H = 0x0000;


    PIR1bits.TMR1IF = 0; // efface le drapeau d'IT
    T1CONbits.T1CKPS0 = 0; // pr�diviseur  1:1
    T1CONbits.T1CKPS1 = 0; // pr�diviseur  1:1
    T1CONbits.RD16 = 0; // timer sur 8bits SINON TROP DE PB
    T1CONbits.TMR1CS = 0; // TIMER1 sur horloge interne (Fosc/4)
    T1CONbits.T1RUN = 0; // source autre que oscillateur du timer 1
    T1CONbits.T1OSCEN = 0; // turn off resistor feeback 
    IPR1 = 0x01; // set Timer1 to high priority	
    PIE1 = 0x01; // enable Timer1 roll-over interrupt
    iComptInt2 = 0;
    INTCON2bits.RBPU = 1; // pull-up port B         � voir  <<<< provoque une erreur sur l'entre de 2V 
    INTCON2bits.INTEDG2 = 0; //INT2 sur front montant   <<<<<
    T1CONbits.TMR1ON = 0; // stop timer 1 

    INTCONbits.GIEH = 1; // Toutes les IT d�masqu�es autoris�es
    INTCONbits.PEIE = 1; // valide ttes les int haute priorit�
    INTCON3bits.INT2IP = 1; // INT2 en haute priorit�

    INTCON3bits.INT2IF = 0; // flag d'interruption int2 � reseter 
    INTCON3bits.INT2IE = 1; // valide interruption INT2

    iComptTimeOut = 0x0000; // compteur time-out

    while (iComptTimeOut < 10000 && iComptInt2 < 21) {
        iComptTimeOut++;
    }

    INTCONbits.GIE = 0; //disable global interrupt  
    INTCONbits.PEIE = 0; //d�valide ttes les int haute priorit�
    INTCON3bits.INT2IE = 0; // arret interruption INT2

    fFreq = (iComptDebordement * 65535 + iTimer1H * 256 + iTimer1L) / 20;
    if (fFreq > 0) {
        fFreq += 0x0015;
        fFreq *= 0xA6; //0xA6 = * 166
        fFreq= 1000000 / fFreq;
        fFreq *= 1000; //x1000
        iFreqOut = (int) fFreq;
    } else {
        iFreqOut = 0x0000;
    }

    T1CONbits.RD16 = 0; // timer sur 8bits
    TMR1H = 0x0000; // registre H 8bit du timer 1
    TMR1L = 0x0000; // registre L 16bit du timer 1

    return iFreqOut;

}

signed int CalculFreq(char cSelecFreq) {    //2 mesures de la fr�quence sur la voie demand�e (paire ou impaire)
    signed int iFreqSortie;
    char cBcl;
    iFreqSortie = 0;
    Sel_Freq = cSelecFreq; //commutateur de fr�quence voie paire 
    for (cBcl = 0; cBcl < 2; cBcl++) {
        iFreqSortie += Mesure_Freq(); //m�morise 1�re valeur de fr�quence (pour tester instabilit�)
    }
    return iFreqSortie;
}

void Calcul_Deb(unsigned int* iDebit) { //Calcul du d�bit
    // Echelle d�bitm�tre 1000 � 1500Hz pour 0L/H � 1000 et 500L/H � 1500Hz

    if (*iDebit >= 0x03E5 && *iDebit <= 0x03E8) { // 3Hz de tol�rance � voir !!
        *iDebit = 0;
    } else if ((*iDebit < 0x03E5) || (*iDebit >= 0x05DC)) { //997 inferieur a 1000 alors on met 9999
        *iDebit = 0x270F; //9999
    } else {
        *iDebit = (*iDebit - 0x03E8); // -1000
    }
}

unsigned int acqui_ana(unsigned char cAdcon) {  //Acquisition d'une valeur analogique
    ADCON0 = cAdcon; // adress + abort la mesure en cours
    _ADON = 1;
    _ADGO = 1;
    while (_ADDONE);
    return ADRES;
}

unsigned int acqui_ana_16_plus(unsigned char cAdcon) {  //Acquisition de 16 valeurs analogiques
    unsigned long lMoyen;
    int iBcl; // Variable usage general
    
    lMoyen = 0;
    for (iBcl = 0; iBcl < 0x10; iBcl++) // 16 MESURES POUR LA MOYENNE
    {
        lMoyen = lMoyen + acqui_ana(cAdcon);
        Delay10KTCYx(1);
    }
    lMoyen >>= 4; // Moyenne pour 16 mesures

    return ((int) lMoyen); // sauvegarde de Long en Int
}

unsigned char Instable(signed int* iFreq) { // test instabilit�
    float fDifF12, fDifF23, fDifF34;
    unsigned char cSortie;

    fDifF12 = *(iFreq) - *(iFreq + 1);
    fDifF12 = abs(fDifF12);

    fDifF23 = *(iFreq + 1) - *(iFreq + 2);
    fDifF23 = abs(fDifF23);

    fDifF34 = *(iFreq + 2) - *(iFreq + 3);
    fDifF34 = abs(fDifF34);

    if (fDifF12 > max_instable || fDifF23 > max_instable || fDifF34 > max_instable) {
        cSortie = 0x80; // instable
    } else {
        cSortie = 0;
    }
    return (cSortie);
}

void mesure_test_cc(unsigned char cImpaire, unsigned char cPaire, char cCourtCircuit) { // test si la voie est en CC
    unsigned char cModifTab[6];
    if ((cCourtCircuit == 0xF1) || (cCourtCircuit == 0xFF)) {
        //VOIE IMPAIRE CC OK
        cModifTab[1] = '0';
        cModifTab[2] = '0';
        cModifTab[3] = cImpaire + 48;
        if (cImpaire >= 10) {
            cModifTab[2] = '1';
            cModifTab[3] = (cImpaire - 10) + 48;
        }
        if (cImpaire >= 20) {
            cModifTab[2] = '2';
            cModifTab[3] = '0';
        }
        cModifTab[4] = '0';
        cModifTab[5] = '0';

        MODIFICATION_TABLE_DEXISTENCE(cModifTab, 6); //mettre 'c' sur la table existence
    }
    if ((cCourtCircuit == 0xF2) || (cCourtCircuit == 0xFF)) {
        cModifTab[1] = '0';
        cModifTab[2] = '0';
        cModifTab[3] = cPaire + 48;
        if (cPaire >= 10) {
            cModifTab[2] = '1';
            cModifTab[3] = (cPaire - 10) + 48;
        }
        if (cPaire >= 20) {
            cModifTab[2] = '2';
            cModifTab[3] = '0';
        }
        cModifTab[4] = '0';
        cModifTab[5] = '0';

        MODIFICATION_TABLE_DEXISTENCE(cModifTab, 7); //mettre 'c' sur la table existence
    }
}