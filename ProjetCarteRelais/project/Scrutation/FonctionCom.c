/* 
 * File:   FonctionCom.c
 * Author: LFRAD
 *
 * Created on 24 septembre 2021, 11:52
 */

#include <p18f8723.h>
#include <stdio.h>
#include <stdlib.h>
#include "RecupCapteur.h"

void DebutCommunication(void) { //Attend un d�but de com de la part de la carte m�re (reste dedans tant qu'il n'y a pas de com)
    unsigned char cTestReception; //Variable envoy� par la carte m�re signifiant une demande de communication
    unsigned char cTestEnvoie; //Variable envoy� par la RI signifiant que la demande est bien pass�e
    unsigned char cCara; //Variable d'erreur de com
    char cCompt; //Compteur d'activation de l'interruption

    cCompt = 0; //Initialisation du compteur
    //    DELAY_SW_250MS();
    //    INTCONbits.TMR0IF = 1;
    do {
        cCara = RecoitTrame(&cTestReception, 1); //Reception d'un caract�re envoy� par la CM
        DELAY_MS(3); //Delay de 3 ms pour s'assurer que la CM est pr�te
        if (cTestReception == '#' && cCara == 'a') { //V�rification que la carte m�re � demand� une communication et que celle ci est valide
            cTestEnvoie = 6; //Caract�re de validation de la communication
            PORTCbits.RC5 = 0; //Desactivation de l'interruption signifiant la disponibilit� de la RI
        } else { //Si la com ne s'est pas faite correctement
            cTestEnvoie = 21; //Caract�re d'envoie signifiant une mauvaise com
            if (INTCONbits.TMR0IF) { //Si la drapeau du timer 0 est � 1
                INTCONbits.TMR0IF = 0; //Remise � 0 du drapeau
                cCompt++; //Incr�mentation du compteur
                DELAY_SW_250MS(); //Lancement delay 250ms non bloquant
                PORTCbits.RC5 = 1; //D�clancement interruption CM
            }
            if (cCompt == 2) { //Si compteur � 2
                cCompt = 0; //Reinitialisation du compteur
                PORTCbits.RC5 = 0; //Desactivation interruption
            }
        }
        if (cCara != 'o') { //Si pas d'erreur de Timeout
            EnvoieTrame(&cTestEnvoie, 1); //Envoie caract�re d'envoie � la cm
        }
    } while (cTestEnvoie != 6); //On continue tant que la comm n'est pas bonne
}

char Communication(unsigned char* cTrame) {
    unsigned char cTestEnvoie;
    unsigned char cCara;
    unsigned char cWatchDog;
    char cSortieFonction;
    cWatchDog = 0;

    do {
        cCara = RecoitTrame(cTrame, 10);
        DELAY_MS(3);
        if (cTrame[0] == 'S' && cTrame[1] == 'c' && cTrame[2] == 'r' && cTrame[3] == 'u' && cCara == 'a') {
            cSortieFonction = 1;
            cTestEnvoie = 6;

        } else if (cTrame[0] == 'T' && cTrame[1] == 'e' && cTrame[2] == 's' && cTrame[3] == 't' && cTrame[4] == 'S' && cTrame[5] == 'c' && cTrame[6] == 'r' && cTrame[7] == 'u' &&
                cTrame[8] == 't' && cTrame[9] == 'a' && cCara == 'a') {
            cSortieFonction = 2;
            cTestEnvoie = 6;

        } else if ((cTrame[0] == 'A' && cTrame[1] == 'c' && cTrame[2] == 'q' && cTrame[3] == 'u' && cTrame[4] == 'i' && cTrame[5] == 's' && cTrame[6] == 'i' && cTrame[7] == 't'
                && cCara == 'a')) {
            cSortieFonction = 3;
            cTestEnvoie = 6;

        } else if (cTrame[0] == 'R' && cTrame[1] == 'e' && cTrame[2] == 'c' && cTrame[3] == 'u' && cTrame[4] == 'p' && cTrame[5] == 'V' && cTrame[6] == 'o' && cTrame[7] == 'i' &&
                cTrame[8] == 'e' && cTrame[9] == 's' && cCara == 'a') {
            cSortieFonction = 4;
            cTestEnvoie = 6;
        } else {
            cSortieFonction = 0;
            cTestEnvoie = 21;

        }
        EnvoieTrame(&cTestEnvoie, 1);
        cWatchDog++;
    } while ((cTestEnvoie != 6) && (cWatchDog < 5));

    return cSortieFonction;
}

void EnvoieDonnees(unsigned char* cTrame, char cLongueur) {
    unsigned char cTestReception;
    char cWatchDog;
    unsigned char cCara;
    cWatchDog = 0; //Initialisation du WatchDog
    do {
        EnvoieTrame(cTrame, cLongueur); //Envoie de la trame contenant les donn�es � la CM par le RS485
        cCara = RecoitTrame(&cTestReception, 1); //Reception du bit de confirmation envoy� par la CM
        cWatchDog++; //Incr�mentation du WatchDog �vitant le bloquage dans la boucle en cas de mauvaise communication
    } while ((cTestReception != 6) && (cWatchDog < 5)); //A faire tant que la CM n'a pas valid� l'envoie ou que le WatchDog n'est pas d�pac�
}