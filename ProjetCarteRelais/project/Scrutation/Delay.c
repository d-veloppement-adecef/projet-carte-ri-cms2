/* 
 * File:   Delay.c
 * Author: LFRAD
 *
 * Created on 26 ao�t 2021, 09:31
 */

#include <p18f8723.h>
#include <stdio.h>
#include <stdlib.h>
#include "RecupCapteur.h"

void DELAY_SW_250MS(void) { //Delay de 250ms non bloquant
    T0CON = 0x84; //Active le Timer 0, prescaler � 32
    TMR0H = 0x48; //Regle le timer pour un d�clanchement � 250ms
    TMR0L = 0xE5;
    INTCONbits.TMR0IE = 0; //Desactive l'interruption du TMR0
    INTCONbits.TMR0IF = 0; //Abaisse le drapeau du TMR0
}

void DELAY_MS(int ms) { //Delay bloquant de X ms
    int iBcl;

    if (ms >= 1000) //Si temps d'attente sup�rieur ou �gal � 1000ms
        ms = 2; //On fait un temps d'attente � 2 ms
    T0CON = 0x88; /* enable TMR0, select instruction clock, prescaler set to 1.1 */
    for (iBcl = 0; iBcl < ms; iBcl++) {
        TMR0H = 0xE8; //Regle le timer pour un d�clanchement �1ms
        TMR0L = 0x90;
        INTCONbits.TMR0IE = 0; //Desactive l'interruption du TMR0
        INTCONbits.TMR0IF = 0; //Abaisse le drapeau du TMR0
        while (!(INTCONbits.TMR0IF)); // On attend la lev� de drapeau
    }
}
