/* 
 * File:   FonctionsScrutation.c
 * Author: LFRAD
 *
 * Created on 23 ao�t 2021, 08:11
 */

#include <p18f8723.h>
#include <stdio.h>
#include <stdlib.h>
#include "RecupCapteur.h"
#include <i2c.h>



unsigned int iCourantRepos1[17];
unsigned int iCourantRepos2[17];
unsigned int iCourantPression1[17];
unsigned int iCourantPression2[17];
unsigned int iCourantConso1[17];
unsigned int iCourantConso2[17];
unsigned char cEtat1[17];
unsigned char cEtat2[17];
unsigned int iCourantModul1[17];
unsigned int iCourantModul2[17];
unsigned int iSeuil1[17]; //34 octets	17 caract�res	  seuil 16+1 tp + deb
unsigned int iSeuil2[17]; //34 octets	17 caract�res	  seuil 16+1 tp + deb

unsigned char envoyerHORLOGE_i2c(unsigned char cNumeroH, unsigned char cADDH, unsigned char cDataH0) {
    StartI2C(); //Lance la communication i2c
    WriteI2C(cNumeroH); //Lance la communication � l'adresse de l'horloge
    WriteI2C(cADDH);
    WriteI2C(cDataH0); //Envoie les donn�es pour regler l'horloge
    StopI2C(); //Arret de l'horloge
}

void MODIFICATION_TABLE_DEXISTENCE(unsigned char* cTrame, char cEtat) { //eff pour effacer (eff=2 alors recopie) alors et etat ligne pour fusible HS ou CC ou surtension 
    unsigned int iAdresse;
    unsigned char cExistence[24];

    cTrame[0] = '#';
    iAdresse = 100 * (cTrame[1] - 48) + 10 * (cTrame[2] - 48)+(cTrame[3] - 48); //VOIE
    iAdresse = 128 * iAdresse + 10000;

    EESequRead(6, iAdresse, cExistence, 24);

    if ((((cEtat == 4) || (cEtat == 5))&&(cExistence[2] != 'c')) || (cEtat == 8)) {
        cExistence[2] = cExistence[21];
    }
    if ((cEtat == 6) || (cEtat == 7)) {
        cExistence[2] = 'c';
    }
    cExistence[23] = '*';
    EEPaWrite(6, iAdresse, cExistence);
    DELAY_MS(10);
}

void Prog_HC595(unsigned char cImpaire, unsigned char cPaire, unsigned char cAlarme, unsigned char cFusible) {
    unsigned char cBcl;
    unsigned char cSortie;
    unsigned char cRegistre1; //registres des HC595 pour commutation des relais voies et alarme et test fusible
    unsigned char cRegistre2;
    unsigned char cRegistre3;
    cRegistre1 = 0b00000000; // relais 1 � 8
    cRegistre2 = 0b00000000; // relais 9 � 16
    cRegistre3 = 0b00000000; // relais 17 � 20 + alarme c�ble + test fusibles voies

    if (cImpaire > 0) {
        switch (cImpaire) {
            case 1:
                cRegistre1 |= 0b00000001; // relais 1
                break;
            case 3:
                cRegistre1 |= 0b00000100; // relais 3
                break;
            case 5:
                cRegistre1 |= 0b00010000; // relais 5
                break;
            case 7:
                cRegistre1 |= 0b01000000; // relais 7
                break;
            case 9:
                cRegistre2 |= 0b00000001; // relais 9
                break;
            case 11:
                cRegistre2 |= 0b00000100; // relais 11
                break;
            case 13:
                cRegistre2 |= 0b00010000; // relais 13
                break;
            case 15:
                cRegistre2 |= 0b01000000; // relais 15
                break;
            case 17:
                cRegistre3 |= 0b00000001; // relais 17
                break;
            case 19:
                cRegistre3 |= 0b00000100; // relais 19
                break;
        }
    }
    // Choix Relais		
    if (cPaire > 0) {
        switch (cPaire) {
            case 2:
                cRegistre1 |= 0b00000010; // relais 2
                break;
            case 4:
                cRegistre1 |= 0b00001000; // relais 4
                break;
            case 6:
                cRegistre1 |= 0b00100000; // relais 6
                break;
            case 8:
                cRegistre1 |= 0b10000000; // relais 8
                break;
            case 10:
                cRegistre2 |= 0b00000010; // relais 10
                break;
            case 12:
                cRegistre2 |= 0b00001000; // relais 12
                break;
            case 14:
                cRegistre2 |= 0b00100000; // relais 14
                break;
            case 16:
                cRegistre2 |= 0b10000000; // relais 16
                break;
            case 18:
                cRegistre3 |= 0b00000010; // relais 18
                break;
            case 20:
                cRegistre3 |= 0b00001000; // relais 20
                break;
        }
    }
    //test si alarme c�ble . si oui mofification du registre 3
    if (cAlarme) {
        cRegistre3 |= 0b00010000; //"ou logique"  pour si alarme c�ble
    }
    // pour TEST si fusion fusible voies . si oui mofification du registre 3
    if (cFusible) {
        cRegistre3 |= 0b00100000; //"ou logique"  pour si test fusion fusible
    }


    Rck_HC595 = 1;


    for (cBcl = 24; cBcl > 0; cBcl--) {// 23= Nb de pins-1
        if (cBcl > 16) {
            cSortie = cRegistre3 & 0x80;
            cRegistre3 <<= 1; //relais de 17 � 24
        } else if (cBcl > 8) {
            cSortie = cRegistre2 & 0x80;
            cRegistre2 <<= 1; //relais de 9 � 16
        } else {
            cSortie = cRegistre1 & 0x80;
            cRegistre1 <<= 1; //relais de 1 � 8
        }

        if (cSortie == 0x80)
            cSortie = 0xFF;
        Data_HC595 = cSortie;
        Clk_HC595 = 1;
        Clk_HC595 = 0;
    }

    Rck_HC595 = 0;
    Rck_HC595 = 1;
    Rst_HC595 = 1; // reset les "shift regitrers" 
}

void Mesure_DEB_TPA(char cCapteurmax, char cVoieImpaire, char cVoiePaire, unsigned char cAlarme, char cFusibleHS) {

    char cBclCapteur;
    char cBcl;

    char cCourtCircuit;

    signed int iFreqPaire[4];
    signed int iFreqImpaire[4];

    float fCumulFreq1; //r�sultat de mesure de fr�quence du TPA voie impaire
    float fCumulFreq2; //r�sultat de mesure de fr�quence du TPA voie paire

    LEDRX = 0;

    Prog_HC595(cVoieImpaire, cVoiePaire, cAlarme, cFusibleHS); //COMMANDE RELAIS !
    cCourtCircuit = 0x00;

    Init_Tab_tpa();

    //config pour 56V VOIE IMPAIRE
    UN_CPT_1 = 1; //VALIDE
    V_SUP1 = 0; //DEVALIDE 
    CPT_ON_1 = 0; //   <======== d�valide 56V SUR VOIE   par s�curit�

    //config pour 56V VOIE PAIRE
    UN_CPT_2 = 1;
    V_SUP2 = 0;
    CPT_ON_2 = 0; //   <======== d�valide 56V SUR VOIE   par s�curit�

    DECH_L1 = 1;
    DECH_L2 = 1;

    envoyerHORLOGE_i2c(0xD0, 0, 0b00000000); //Initialisation horloge

    while (HORLOGE == 0); //attend passage � 0 de RB3, boucle tant que la condition n'est pas arriv�e HORLOGE=PORTBbits.RB3
    while (HORLOGE == 1); //attend passage � 1 de RB3, boucle tant que la condition n'est pas arriv�e HORLOGE=PORTBbits.RB3

    LEDRX = !LEDRX; //Changement �tat LED
    if (cVoieImpaire != 0) { // si voie impaire valid�e
        CPT_ON_1 = 1; //   <======== valide voie impaire de mesure � 56v
    }
    if (cVoiePaire != 0) { //	si voie paire valid�e
        CPT_ON_2 = 1; //   <======== valide voie paire de mesure � 56v
    }

    DECH_L1 = 0; // lib�re la d�charge ligne impaire
    DECH_L2 = 0; // lib�re la d�charge ligne  paire



    for (cBclCapteur = 0; cBclCapteur <= cCapteurmax; cBclCapteur++) { //cBclCapteur=0 POUR LECTURE EN ZERO (DEBITMETRE)
        for (cBcl = 0; cBcl < 4; cBcl++) { //Initialisation du tableau des frequences  
            iFreqPaire[cBcl] = 0;
            iFreqImpaire[cBcl] = 0;
        }

        fCumulFreq1 = 0;
        fCumulFreq2 = 0;

        DELAY_MS(250); //Delay de 250MS
        DELAY_SW_250MS(); // TEMPO2  la Tempo2 se termine � 	while(!(INTCONbits.TMR0IF)); on lance la tempo 250ms au niveau du timer0


        if (cVoieImpaire != 0) { // si voie impaire valid�e
            iCourantConso1[cBclCapteur] = Mesure_I_Conso(IConso1_AN4, Coeff_I_Conso1); //Mesure courant conso
            iCourantConso1[cBclCapteur] = (iCourantConso1[cBclCapteur] + Mesure_I_Conso(IConso1_AN4, Coeff_I_Conso1)) / 2; // moyenne de deux mesure du courant de conso
            iCourantModul1[cBclCapteur] = Mesure_I_Modul(IModul1_AN6, Coeff_I_Modul1); //Mesure courant modulation
        }
        if (cVoiePaire != 0) { // si voie paire valid�e
            iCourantConso2[cBclCapteur] = Mesure_I_Conso(IConso2_AN5, Coeff_I_Conso2); //Mesure courant conso
            iCourantConso2[cBclCapteur] = (iCourantConso2[cBclCapteur] + Mesure_I_Conso(IConso2_AN5, Coeff_I_Conso2)) / 2; // moyenne de deux mesure du courant de conso
            iCourantModul2[cBclCapteur] = Mesure_I_Modul(IModul2_AN7, Coeff_I_Modul2); //Mesure courant de modulation
        }

        //attente fin de tempo
        // TEST CC
        if ((iCourantConso1[cBclCapteur] >= Icons_cc)&&(iCourantConso2[cBclCapteur] >= Icons_cc)) { //Si d�tection court circuit sur 
            Prog_HC595(0, 0, cAlarme, cFusibleHS); //Desactivation des voies
            cCourtCircuit = 0xFF; //Information CC sur les deux voies
        } else if (iCourantConso1[cBclCapteur] >= Icons_cc) { // Si detection court circuit sur voie impaire
            cCourtCircuit = 0xF1; //Information CC sur voie impaire
            Prog_HC595(0, cVoiePaire, cAlarme, cFusibleHS); //Desactivation voie impaire
        } else if (iCourantConso2[cBclCapteur] >= Icons_cc) { //SI detection cour circuit sur voie paire
            cCourtCircuit = 0xF2; //Information CC sur voie paire
            Prog_HC595(cVoieImpaire, 0, cAlarme, cFusibleHS); //Desactivation voie paire
        }

        while (!(INTCONbits.TMR0IF)); // FIN tempo SW 250 ms

        for (cBcl = 0; cBcl < 4; cBcl++) {
            DELAY_MS(60); // Delay de 60ms
            if (cVoieImpaire != 0) { // si voie impaire valid�e
                iFreqImpaire[cBcl] = CalculFreq(0); //m�morise 1�re valeur de fr�quence (pour tester instabilit�)
                fCumulFreq1 += iFreqImpaire[cBcl]; //Additionne toutes les frequences mesur�es dans fCumulFreq1
            }
            //voie paire
            if (cVoiePaire != 0) { // si voie paire valid�e
                iFreqPaire[cBcl] = CalculFreq(1); //m�morise 1�re valeur de fr�quence (pour tester instabilit�)
                fCumulFreq2 += iFreqPaire[cBcl]; //Additionne toutes les frequences mesur�es dans fCumulFreq2
            }
        }
        if (cBclCapteur > 0) { //Si mesure de capteurs
            if (iCourantConso1[cBclCapteur] > i_conso_min) { // Si la conso est pr�sente
                if (cVoieImpaire != 0) { // si voie impaire valid�e
                    cEtat1[cBclCapteur] |= Instable(iFreqImpaire); // v�rification de l'instabilit�
                    iCourantPression1[cBclCapteur] = (int) (fCumulFreq1 / 8); // Stockage de la fr�quence (division de cumulfreq par 8 car 4 * 2mesures)
                }
            } else {
                iCourantPression1[cBclCapteur] = 0; //Sinon, pression � 0
            }

            if (iCourantConso2[cBclCapteur] > i_conso_min) { // Si la conso est pr�sente
                if (cVoiePaire != 0) { // si voie paire valid�e
                    cEtat2[cBclCapteur + 1] |= Instable(iFreqPaire); // v�rification de l'instabilit�
                    iCourantPression2[cBclCapteur] = (int) (fCumulFreq2 / 8); // Stockage de la fr�quence (division de cumulfreq par 8 car 4 * 2mesures)
                }
            } else {
                iCourantPression2[cBclCapteur] = 0; //Sinon, pression � 0
            }
        }
        if (!cBclCapteur) { //Si mesure du debimetre
            Calcul_Deb(&iCourantPression1[0]); //Calcul d�bit impaire
            Calcul_Deb(&iCourantPression2[0]); //Calcul d�bit paire
        }
        // ====> 500mS + 375mS + 375mS ==> 1250mS 
        //Attente de la chute du signal d'horloge pour synchro avec 1.5sec

        while (HORLOGE == 1);
        while (HORLOGE == 0); //attend passage � 0 de RB3, boucle tant que la condition n'est pas arriv�e Synchro1s=PORTBbits.RB3

        LEDRX = !LEDRX; //Changement etat led


        //<=================ICI 1.5SEC================>
        DELAY_MS(150); // delay 150ms

        //#####################			// MESURE le courant de repos VOIE IMPAIRE
        if (cVoieImpaire != 0) // si voie impaire valid�e
        {
            iCourantRepos1[cBclCapteur] = Mesure_I_Repos(IRepos1_AN2, Coeff_I_Repos1); //Mesure courant repos
            iCourantRepos1[cBclCapteur] = (iCourantRepos1[cBclCapteur] + Mesure_I_Repos(IRepos1_AN2, Coeff_I_Repos1)) / 2; //Nouvelle mesure courant repos et moyenne des deux mesures
            if (iCourantRepos1[cBclCapteur] <= 15) { //Si le courant de repos est inferrieur � 15
                iCourantRepos1[cBclCapteur] = 0; //On met les courant de repos � 0
            } // seuil pour �viter d'afficher un courant de repos si rien de branch� 

            iCourantConso1[cBclCapteur] *= 100; // remise � l'echelle du courant de conso pour etre au m�me niveau que le courant de repos
            if (iCourantConso1[cBclCapteur] > iCourantRepos1[cBclCapteur]) { // Si le courant de conso est plus grand que le courant de repos, et donc qu'un capteur est pr�sent
                iCourantConso1[cBclCapteur] -= iCourantRepos1[cBclCapteur]; //On soustrait le courant de repos du courant de conso
                iCourantConso1[cBclCapteur] /= 100; //Remise � l'echelle du courant de conso
            } else {
                iCourantConso1[cBclCapteur] = 0; //Sinon le courant de repos est remis � 0
            }
            iCourantConso1[cBclCapteur] += (iCourantModul1[cBclCapteur] / 2); // On rajourte la moiti� de la modulation au courant de conso
        }

        //#####################			// MESURE le courant de repos VOIE PAIRE

        if (cVoiePaire != 0) // si voie paire valid�e
        {
            iCourantRepos2[cBclCapteur] = Mesure_I_Repos(IRepos2_AN3, Coeff_I_Repos2); //Mesure courant repos
            iCourantRepos2[cBclCapteur] = (iCourantRepos2[cBclCapteur] + Mesure_I_Repos(IRepos2_AN3, Coeff_I_Repos2)) / 2; //Nouvelle mesure courant repos et moyenne des deux mesures
            if (iCourantRepos2[cBclCapteur] <= 15) { //Si le courant de repos est inferrieur � 15
                iCourantRepos2[cBclCapteur] = 0; //On met les courant de repos � 0
            } // seuil pour �viter d'afficher un courant de repos si rien de branch�

            iCourantConso2[cBclCapteur] *= 100; // remise � l'echelle du courant de conso pour etre au m�me niveau que le courant de repos
            if (iCourantConso2[cBclCapteur] > iCourantRepos2[cBclCapteur]) { // Si le courant de conso est plus grand que le courant de repos, et donc qu'un capteur est pr�sent
                iCourantConso2[cBclCapteur] -= iCourantRepos2[cBclCapteur]; // On soustrait le courant de repos du courant de conso
                iCourantConso2[cBclCapteur] /= 100; //Remise � l'echelle du courant de conso
            } else {
                iCourantConso2[cBclCapteur] = 0; //Sinon le courant de repos est remis � 0
            }
            iCourantConso2[cBclCapteur] += (iCourantModul2[cBclCapteur] / 2); // On rajourte la moiti� de la modulation au courant de conso

        }
        //Attente de la remont�e du signal d'horloge pour synchro avec 2sec
        while (HORLOGE == 1); //attend passage � 1 de RB3, boucle tant que la condition n'est pas arriv�e HORLOGE=PORTBbits.RB3
    }


    CPT_ON_1 = 0; //   <======== d�valide 56V SUR VOIE  IMPAIRE
    CPT_ON_2 = 0; //   <======== d�valide 56V SUR VOIE  PAIRE

    DECH_L1 = 0; // d�charge la ligne impaire
    DECH_L2 = 0; // d�charge la ligne	paire

    V_SUP1 = 0;
    V_SUP2 = 0;

    UN_CPT_1 = 0;
    UN_CPT_2 = 0;

    Prog_HC595(0, 0, cAlarme, cFusibleHS); //Desactivation des voies

    mesure_test_cc(cVoieImpaire, cVoiePaire, cCourtCircuit); //ecris CC ou pas 
}

void ScrutationCapteur(char cVoie, char cCapteur, unsigned char* cTrameSortie) { //Scrutation du capteur de la voie choisie
    unsigned char cVoieImpaire; //Numero voie Impaire
    unsigned char cVoiePaire; //Numero voie paire

    envoyerHORLOGE_i2c(0xD0, 0x07, 0b00010000); //Initialisation de l'horloge

    if (cVoie % 2 == 1) { //Si la voie � scruter est impaire
        cVoieImpaire = cVoie; //On donne la valeur de la voie � la voie impaire
        cVoiePaire = 0; //On met la voie paire � 0
    } else { //Sinon (voie � scruter paire)
        cVoieImpaire = 0; //On met la voie impaire � 0
        cVoiePaire = cVoie; //On donne la valeur de la voie � la voie paire
    }

    Mesure_DEB_TPA(cCapteur, cVoieImpaire, cVoiePaire, 0, 0); //Scrutation des voies paires et impaires en parametre jusqu'au capteur en parametre
    if (cVoie % 2 == 1) { //Si la voie scrut�e est impaire
        RECUPERE_TRAME_ET_CAL_ETAT(cEtat1, iCourantPression1[cCapteur], iSeuil1[cCapteur], cCapteur, 0); //RECUPERE LES ELEMENTS FIXE EN MOIRE CONSTIT....SEUIL......ETC //PRECEDENT CYCLE
        EcritureTrameCapteurOeil(cVoie, cCapteur, iCourantModul1[cCapteur], iCourantConso1[cCapteur], iCourantRepos1[cCapteur], cEtat1[cCapteur], iCourantPression1[cCapteur], iSeuil1[cCapteur], cTrameSortie); //On stock les donn�es du capteur de la voie impaire voulue dans la trame de sortie
    } else { //Sinon (Voie scrut�e paire)
        RECUPERE_TRAME_ET_CAL_ETAT(cEtat2, iCourantPression2[cCapteur], iSeuil2[cCapteur], cCapteur, 0); //RECUPERE LES ELEMENTS FIXE EN MOIRE CONSTIT....SEUIL......ETC //PRECEDENT CYCLE
        EcritureTrameCapteurOeil(cVoie, cCapteur, iCourantModul2[cCapteur], iCourantConso2[cCapteur], iCourantRepos2[cCapteur], cEtat2[cCapteur], iCourantPression2[cCapteur], iSeuil2[cCapteur], cTrameSortie); //On stock les donn�es du capteur de la voie paire voulue dans la trame de sortie
    }
}

void ScrutationVoies(void) { //Scrute la carte compl�te et renvoie les trames des capteurs valides
    char cBcl;

    for (cBcl = 1; cBcl < 20; cBcl += 2) { //Pour les 20 voies
        Mesure_DEB_TPA(16, cBcl, cBcl + 1, 0, 0); //On mesure les 16 capteurs des voies
        EcritureTrameVoie(cBcl, cEtat1, iCourantPression1, iSeuil1); //Renvoie des donn�es des voies impaires
        EcritureTrameVoie(cBcl + 1, cEtat2, iCourantPression2, iSeuil2); //Renvoie des donn�es des voies paires
    }
}