/* 
 * File:   InitScrut.c
 * Author: LFRAD
 *
 * Created on 26 ao�t 2021, 09:13
 */

#include <p18f8723.h>
#include <stdio.h>
#include <stdlib.h>
#include "RecupCapteur.h"
#include <i2c.h>

extern unsigned int iCourantPression1[17];
extern unsigned int iCourantPression2[17];
extern unsigned int iCourantRepos1[17];
extern unsigned int iCourantRepos2[17];
extern unsigned int iCourantConso1[17];
extern unsigned int iCourantConso2[17];
extern unsigned char cEtat1[17];
extern unsigned char cEtat2[17];
extern unsigned int iCourantModul1[17];
extern unsigned int iCourantModul2[17];

void init_uc(void) {    //Initialisation de la RI
    TRISAbits.TRISA0 = 1; // AN0   V_ligne_AM1 	mesure tension Amont du capteur voies impaires
    TRISAbits.TRISA1 = 1; // AN1	 V_ligne_AM2 	mesure tension Amont du capteur voies paires
    TRISAbits.TRISA2 = 1; // AN2	 V_ligne_AV1_A 	mesure tension Aval du capteur voies impaires  (apr�s fusible)
    TRISAbits.TRISA3 = 1; // AN3	 V_ligne_AV2_A 	mesure tension Aval du capteur voies paires  (apr�s fusible)
    TRISAbits.TRISA4 = 0; // RX_TX	
    TRISAbits.TRISA5 = 1; // AN4	 I_conso_1		mesure tension(courant de consommation) du capteur voies impaires
    TRISAbits.TRISA6 = 0; //	OSC
    TRISAbits.TRISA7 = 0; //	OSC

    TRISBbits.TRISB0 = 1; //	ALIM_OFF (INT)
    TRISBbits.TRISB1 = 1; //	VAL_RS485_EXT
    TRISBbits.TRISB2 = 1; //	FREQ_TPA
    TRISBbits.TRISB3 = 1; //	HORLOGE
    TRISBbits.TRISB4 = 1; //	VAL_I2C_EXT
    TRISBbits.TRISB5 = 0; //	PGM	}
    TRISBbits.TRISB6 = 0; //	PGC	]> ICSP
    TRISBbits.TRISB7 = 0; //	PGD	}

    TRISCbits.TRISC0 = 0; //	DECH_L1			==> modif piste sur CI
    TRISCbits.TRISC1 = 0; //	OK_I2C_EXT
    TRISCbits.TRISC2 = 0; //	I2C_INTERNE
    TRISCbits.TRISC3 = 1; //	SCL_I2C
    TRISCbits.TRISC4 = 1; //	SDA_I2C
    TRISCbits.TRISC5 = 0; //	OK_RS485_EXT
    TRISCbits.TRISC6 = 0; //	TX_485
    TRISCbits.TRISC7 = 1; //	RX_485

    TRISDbits.TRISD0 = 0; //	D0	
    TRISDbits.TRISD1 = 0; //	D1
    TRISDbits.TRISD2 = 0; //	D2
    TRISDbits.TRISD3 = 0; //	D3
    TRISDbits.TRISD4 = 0; //	D4
    TRISDbits.TRISD5 = 0; //	D5
    TRISDbits.TRISD6 = 0; //	D6
    TRISDbits.TRISD7 = 0; //	D7
    // 1=INPUT    0=OUTPUT
    TRISEbits.TRISE0 = 0; //	RS_DI	
    TRISEbits.TRISE1 = 0; //	R/W	
    TRISEbits.TRISE2 = 0; //	E			
    TRISEbits.TRISE3 = 0; //	Data_Rel
    TRISEbits.TRISE4 = 0; //	Clk_Rel
    TRISEbits.TRISE5 = 0; //	Rck_Rel
    TRISEbits.TRISE6 = 0; //	Rst_Rel
    TRISEbits.TRISE7 = 0; //	Test_Fus


    TRISFbits.TRISF0 = 1; // AN5   I_conso_2		mesure tension(courant de consommation) du capteur voies paires 
    TRISFbits.TRISF1 = 1; // AN6	 I_modul_1		mesure tension(courant de modulation) du capteur voies impaires
    TRISFbits.TRISF2 = 1; // AN7	 I_modul_2		mesure tension(courant de modulation) du capteur voies paires
    TRISFbits.TRISF3 = 1; // AN8   Sel A0 Lecture en entr�e Analogique!!!
    TRISFbits.TRISF4 = 1; // AN9	 Sel A1 Lecture en entr�e Analogique!!!
    TRISFbits.TRISF5 = 1; // AN10  Sel A2 Lecture en entr�e Analogique!!!
    TRISFbits.TRISF6 = 0; // DECH_L1 AN11	 <===  entr�e analogique LIBRE   ==> modif piste sur CI
    TRISFbits.TRISF7 = 0; // DECH_L2

    TRISGbits.TRISG0 = 0; //	Sel_Freq	(0 voie impaire)(1 voie paire)
    TRISGbits.TRISG1 = 0; //	CPT_ON_1
    TRISGbits.TRISG2 = 0; //	UN_CPT_1			
    TRISGbits.TRISG3 = 0; //	CPT_ON_2
    TRISGbits.TRISG4 = 0; //	UN_CPT_2

    TRISHbits.TRISH0 = 0; //  AL_CABLE_ON + LED ALARME
    TRISHbits.TRISH1 = 0; //  /RST
    TRISHbits.TRISH2 = 0; //	LED_TX
    TRISHbits.TRISH3 = 0; //	LED_RX
    TRISHbits.TRISH4 = 1; // AN12  V_48V_1 mesure tension 48V voies impaires  
    TRISHbits.TRISH5 = 1; // AN13	 V_48V_2 mesure tension 48V voies paires 
    TRISHbits.TRISH6 = 1; // AN14  V_ligne_AV1_B 	mesure gain+ tension Aval du capteur voies impaires  (apr�s fusible)
    TRISHbits.TRISH7 = 1; // AN15  V_ligne_AV2_B 	mesure gain+ tension Aval du capteur voies paires  (apr�s fusible)

    TRISJbits.TRISJ0 = 0; //	CS1
    TRISJbits.TRISJ1 = 0; //	CS2
    TRISJbits.TRISJ2 = 0; //	Pt2
    TRISJbits.TRISJ3 = 1; // 	Jumper (entr�e pour TPR)
    TRISJbits.TRISJ4 = 1; //	MODE TEST	
    TRISJbits.TRISJ5 = 0; //	Val_R_I2C
    TRISJbits.TRISJ6 = 0; //	V_SUP_1
    TRISJbits.TRISJ7 = 0; //	V_SUP_2

    // A/D PORT Configuration
    ADCON1 = 0b00000111; // ANALOG: AN0 -> AN7 - (Vref+ = AVDD(5v) et Vref =AVSS(0v) ) 

    ADCON0 = 0; // CAN AU REPOS

    ADCON2bits.ADFM = 1; // FORMAT DU RESULTAT JUSTIFI� A DROITE
    ADCON2bits.ACQT2 = 1; // ACQUISITION = 12 TAD
    ADCON2bits.ACQT1 = 0;
    ADCON2bits.ACQT0 = 1;

    ADCON2bits.ADCS2 = 0; // HORLOGE TAD 1�S POUR F = 32MHz
    ADCON2bits.ADCS1 = 1; // % 32
    ADCON2bits.ADCS0 = 0;

    INTCONbits.PEIE = 0;
    INTCON2bits.RBIP = 0; // interrupt Hight priorit� sur port B
    INTCONbits.RBIE = 0; //valide  interrupt sur port B
    // pour d�tecter coupure alim
    INTCONbits.TMR0IE = 0; // 0 = Disables the TMR0 overflow interrupt    
    INTCONbits.INT0IE = 1; // INT0 External Interrupt Enable bit 1 = Enables the INT0 external interrupt 
    INTCON2bits.INTEDG0 = 0; //External Interrupt 0 Edge Select bit 1 = Interrupt on rising edge 0 = Interrupt on falling edge bit 
    //
    INTCON2bits.INTEDG1 = 1; //pour interruption d�tection demande RS485 sur front montant  (RB1/INT1)

    INTCON3bits.INT1IF = 0; // reinit flag RS485 pour le premier demmarage on autorise pas la com

    INTCON3bits.INT1IE = 1; // valide l'interruption pour RS485 1 pr reactiver

    INTCONbits.GIE = 1;

    TRISHbits.RH0 = 1;
}

void Init_I2C(void) {   //Initialisation de l'I2C

    //here is the I2C setup from the Seeval32 code.
    DDRCbits.RC3 = 1; //Configure SCL as Input
    DDRCbits.RC4 = 1; //Configure SDA as Input
    SSP1STAT = 0x00; //Disable SMBus & Slew Rate Control 80
    SSP1CON1 = 0x28; //Enable MSSP Master  28
    SSP1CON2 = 0x00; //Clear MSSP Conr// 0x3B for 400kHz  (24mhz/((4*4OOkHz))-1) = 14 ou 3B en hexol Bits
    SSP1ADD = 0x3B; //0x0E
}

void Init_RS485(void) { //Initialisation du RS485

    RCONbits.IPEN = 1; // Enable priority levels    Marche sans et avec

    INTCONbits.GIE = 1; //Interruption
    INTCONbits.INT0IE = 1;
    INTCONbits.PEIE = 1; //Interruption
    INTCON2bits.INTEDG0 = 1; //Interruption sur front montant

    TRISBbits.TRISB0 = 1;
    TRISAbits.TRISA4 = 0; //RA4 RX_TX en sortie OU DERE
    PORTAbits.RA4 = 1;
    BAUDCON1bits.BRG16 = 1;
    TXSTA1bits.BRGH = 1; //haute vitesse
    SPBRG1 = SPEED & 0xFF; // Write baudrate to SPBRG1
    SPBRGH1 = SPEED >> 8; // For 16-bit baud rate generation
    TXSTA1bits.SYNC = 0; // Async mode
    RCSTA1bits.SPEN = 1; // Enable serial port

    IPR1bits.RCIP = 1; // Set high priority
    PIE1bits.RCIE = 0; // ne g�n�re pas d'IT pour la liaison s�rie

    RCSTA1bits.RX9 = 0; // r�ception sur 8 bits
    TXSTA1bits.TX9 = 0; // transmission sur 8 bits

    INTCONbits.INT0IF = 0;

    RECEPTION = 0; // interdire reception
    ENVOIE = 0; // autoriser transmission 

}

void Init_Tab_tpa(void) { //reset tableau de mesures des tpa
    int iBcl;
    for (iBcl = 0; iBcl < 17; iBcl++) {
        iCourantPression1[iBcl] = 0; //17 caract�res	
        iCourantRepos1[iBcl] = 0; //17 caract�res	  
        iCourantModul1[iBcl] = 0; //17 caract�res		
        iCourantConso1[iBcl] = 0; //34 caract�res	
        cEtat1[iBcl] = 0; //17 caract�res	

        iCourantPression2[iBcl] = 0; //17 caract�res	
        iCourantRepos2[iBcl] = 0; //17 caract�res	  
        iCourantModul2[iBcl] = 0; //17 caract�res		
        iCourantConso2[iBcl] = 0; //34 caract�res	
        cEtat2[iBcl] = 0; //17 caract�res	
    }

}
