/* 
 * File:   RS485.c
 * Author: LFRAD
 *
 * Created on 26 ao�t 2021, 10:04
 */

#include <p18f8723.h>
#include <stdio.h>
#include <stdlib.h>
#include "RecupCapteur.h"


void EnvoieTrame(unsigned char *cTableau, char cLongueur) { //Envoie trame au RS485
    int iBcl;   //Variable de boucle
    ENVOIE = 1; // autoriser transmission
    for (iBcl = 0; iBcl < cLongueur; iBcl++) {      //Boucle allant du premier caract�re � envoyer au dernier
        TXREG1 = *(cTableau + iBcl);        //Envoie du caract�re point�
        while (FINTRANS == 0); //Attente fin transmission  
    }
    ENVOIE = 0; //Fin de transmission
}

unsigned char RecoitTrame(unsigned char *cTableau, char cLongueur) { //Recoit trame RS485
    char cBcl; //Variable de boucle
    unsigned char cErreur; //Variable d'erreur � renvoyer
    unsigned int iWatchDog; // Chien de garde
    iWatchDog = 0;
    cErreur = 'a'; //Caract�re informant qu'il n'y a pas d'erreur
    RECEPTION = 1; // D�but reception
    for (cBcl = 0; cBcl < cLongueur; cBcl++) { //Boucle allant du premier caract�re � recevoir au dernier
        iWatchDog = 0; //R�initialisation WatchDog
        while ((FINRECEPTION == 0) && (iWatchDog < 20000)) { //Tant qu'aucune r�ception et WatchDog non d�pass�
            iWatchDog++; //Incr�mentation du WatchDog
        }
        *(cTableau + cBcl) = RCREG1; //Lecture du caract�re re�u
        if ((RCSTAbits.OERR) || (iWatchDog >= 20000)) { //Si erreur OERR ou d�passement watchdog
            cErreur = 'o'; //Caract�re d'erreur OERR
            break;  //Fin de lecture
        }
        if (RCSTAbits.FERR) {   //Si erreur FERR
            cErreur = 'f'; //Caract�re d'erreur FERR
            RCREG1; //Vidage tampon de lecture
            break;  //Fin de lecture
        }
    }

    if (cErreur != 'a') { //Si erreur 
        for (cBcl = 0; cBcl < cLongueur; cBcl++) {
            *(cTableau + cBcl) = 0; //On vide le tableau de stockage
        }
    }
    RECEPTION = 0; //Fin de Reception
    return cErreur; //Renvoie de l'erreur
}