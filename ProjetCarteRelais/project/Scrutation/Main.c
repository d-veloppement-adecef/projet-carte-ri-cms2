#define _ADIF	PIR1bits.ADIF	// INTERRUPTION FIN CONVERSION ANALOGIQUE
#define _ADON	ADCON0bits.ADON	// VALIDE LE CAN
#define _ADGO	ADCON0bits.GO	// LANCE ACQUISITION ET CONVERSION
#define _ADDONE	ADCON0bits.DONE	// TEST SI ACQUISITION ET CONVERSION EN COURS
// PIC18F8723 Configuration Bit Settings

// 'C' source line config statements
#include <p18f8723.h>
#include <stdio.h>
#include <stdlib.h>
#include "RecupCapteur.h"
#include <i2c.h>

// CONFIG1H
#pragma config OSC = HS         // Oscillator Selection bits (HS oscillator)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config IESO = OFF       // Internal/External Oscillator Switchover bit (Two-Speed Start-up disabled)

// CONFIG2L
#pragma config PWRT = OFF       // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = SBORDIS  // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only (SBOREN is disabled))
#pragma config BORV = 3         // Brown-out Voltage bits (Minimum setting)

// CONFIG2H
#pragma config WDT = OFF        // Watchdog Timer (WDT disabled (control is placed on the SWDTEN bit))
#pragma config WDTPS = 32768    // Watchdog Timer Postscale Select bits (1:32768)

// CONFIG3L
#pragma config MODE = MC        // Processor Data Memory Mode Select bits (Microcontroller mode)
#pragma config ADDRBW = ADDR20BIT// Address Bus Width Select bits (20-bit Address Bus)
#pragma config DATABW = DATA16BIT// Data Bus Width Select bit (16-bit External Bus mode)
#pragma config WAIT = OFF       // External Bus Data Wait Enable bit (Wait selections are unavailable for table reads and table writes)

// CONFIG3H
#pragma config CCP2MX = PORTC   // CCP2 MUX bit (ECCP2 input/output is multiplexed with RC1)
#pragma config ECCPMX = PORTE   // ECCP MUX bit (ECCP1/3 (P1B/P1C/P3B/P3C) are multiplexed onto RE6, RE5, RE4 and RE3 respectively)
#pragma config LPT1OSC = OFF    // Low-Power Timer1 Oscillator Enable bit (Timer1 configured for higher power operation)
#pragma config MCLRE = ON       // MCLR Pin Enable bit (MCLR pin enabled; RG5 input pin disabled)

// CONFIG4L
#pragma config STVREN = ON      // Stack Full/Underflow Reset Enable bit (Stack full/underflow will cause Reset)
#pragma config LVP = OFF        // Single-Supply ICSP Enable bit (Single-Supply ICSP disabled)
#pragma config BBSIZ = BB2K     // Boot Block Size Select bits (1K word (2 Kbytes) Boot Block size)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled (Legacy mode))

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not code-protected)
#pragma config CP1 = OFF        // Code Protection bit Block 1 (Block 1 (004000-007FFFh) not code-protected)
#pragma config CP2 = OFF        // Code Protection bit Block 2 (Block 2 (008000-00BFFFh) not code-protected)
#pragma config CP3 = OFF        // Code Protection bit Block 3 (Block 3 (00C000-00FFFFh) not code-protected)
#pragma config CP4 = OFF        // Code Protection bit Block 4 (Block 4 (010000-013FFFh) not code-protected)
#pragma config CP5 = OFF        // Code Protection bit Block 5 (Block 5 (014000-017FFFh) not code-protected)
#pragma config CP6 = OFF        // Code Protection bit Block 6 (Block 6 (01BFFF-018000h) not code-protected)
#pragma config CP7 = OFF        // Code Protection bit Block 7 (Block 7 (01C000-01FFFFh) not code-protected)

// CONFIG5H
#pragma config CPB = OFF        // Boot Block Code Protection bit (Boot Block (000000-0007FFh) not code-protected)
#pragma config CPD = OFF        // Data EEPROM Code Protection bit (Data EEPROM not code-protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not write-protected)
#pragma config WRT1 = OFF       // Write Protection bit Block 1 (Block 1 (004000-007FFFh) not write-protected)
#pragma config WRT2 = OFF       // Write Protection bit Block 2 (Block 2 (008000-00BFFFh) not write-protected)
#pragma config WRT3 = OFF       // Write Protection bit Block 3 (Block 3 (00C000-00FFFFh) not write-protected)
#pragma config WRT4 = OFF       // Write Protection bit Block 4 (Block 4 (010000-013FFFh) not write-protected)
#pragma config WRT5 = OFF       // Write Protection bit Block 5 (Block 5 (014000-017FFFh) not write-protected)
#pragma config WRT6 = OFF       // Write Protection bit Block 6 (Block 6 (01BFFF-018000h) not write-protected)
#pragma config WRT7 = OFF       // Write Protection bit Block 7 (Block 7 (01C000-01FFFFh) not write-protected)

// CONFIG6H
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration registers (300000-3000FFh) not write-protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot Block (000000-007FFF, 000FFF or 001FFFh) not write-protected)
#pragma config WRTD = OFF       // Data EEPROM Write Protection bit (Data EEPROM not write-protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection bit Block 1 (Block 1 (004000-007FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR2 = OFF      // Table Read Protection bit Block 2 (Block 2 (008000-00BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR3 = OFF      // Table Read Protection bit Block 3 (Block 3 (00C000-00FFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR4 = OFF      // Table Read Protection bit Block 4 (Block 4 (010000-013FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR5 = OFF      // Table Read Protection bit Block 5 (Block 5 (014000-017FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR6 = OFF      // Table Read Protection bit Block 6 (Block 6 (018000-01BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR7 = OFF      // Table Read Protection bit Block 7 (Block 7 (01C000-01FFFFh) not protected from table reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Boot Block Table Read Protection bit (Boot Block (000000-007FFF, 000FFF or 001FFFh) not protected from table reads executed in other blocks)



extern unsigned int iTimer1L; // pour sauvegarder les valeurs � l'interruption de INT2 (utilis� dans la mesure de fr�quence)
extern unsigned int iTimer1H;
extern unsigned int iComptDebordement; //	compteur de d�bordement du timer1  (de 65535 � 0000)
extern unsigned int iComptInt2;

extern unsigned int iSeuil1[17]; //34 octets	17 caract�res	  seuil 16+1 tp + deb
extern unsigned int iSeuil2[17]; //34 octets	17 caract�res	  seuil 16+1 tp + deb


void Interruption1(void);

#pragma code highVector=0x08			//valeur 0x08 pour interruptions prioritaite 

void highVector(void) {
    _asm GOTO Interruption1 _endasm // on doit �xecuter le code de la fonction interruption
}

#pragma interrupt Interruption1 

void Interruption1(void) {

    //===========TIMER1
    if (PIR1bits.TMR1IF) { // v�rifie que l'IT est Timer1
        iComptDebordement++; // incr�mente le compteur de d�bordement
        PIR1bits.TMR1IF = 0; // efface le flag d'IT du Timer1
    }
    //===========INT0 sur RB0  

    if (INTCONbits.INT0IF == 1) { // v�rifie que l'IT est INT0, origine RB0=0
        INTCONbits.INT0IF = 0; //efface le flag d'IT extern sur RB0
        Reset(); //  <================================= !!!!!!! RESET SOFT DU PIC !!!!!!! 
    }
    if (INTCON3bits.INT2IF == 1) {// v�rifie que l'IT est INT2, origine RB2=2
        if (iComptInt2 == 0) {
            TMR1H = 0;
            TMR1L = 0;
            iComptDebordement = 0;
            T1CONbits.TMR1ON = 1; // timer 1 ON
            PIR1bits.TMR1IF = 0; // 0 = TMR1 register overflowed reset
        }
        if (iComptInt2 >= 20) {
            T1CONbits.TMR1ON = 0; // timer 1 OFF
            iTimer1H = TMR1H;
            iTimer1L = TMR1L;
            INTCON3bits.INT2IE = 0; // arret interruption INT2
        }
        iComptInt2++;
        INTCON3bits.INT2IF = 0; //efface le drapeau d'IT sur RB2
    }
}

void main(void) {

    char cBcl; //Variable de boucle
    unsigned char cNumVoie; //Voie � scruter
    char cNumCapteur; //Capteur max � scruter
    unsigned char cTrameSortie[36]; //Trame d'envoie sur RS485        


    PORTAbits.RA4 = 1;

    RECEPTION = 0; //Reception RS485 d�sactiv�e
    ENVOIE = 0; //Envoie RS485 d�sactiv�e

    Init_I2C(); //Initialisation de l'i2c
    Init_RS485(); //Initialisation RS485
    init_uc(); //Initialisaion Microcontroleur



    INTCON3bits.INT1IE = 0; // devalide l'interruption pour RS485 1 pr reactiver
    for (cBcl = 0; cBcl < 17; cBcl++) { //Initialisation des seuils des capteurs pour test
        iSeuil1[cBcl] = 1000; //Seuil voie impaire
        iSeuil2[cBcl] = 1000; //Seuil voie paire
    }

    cNumVoie = 20; //Choix de la voie � scruter
    cNumCapteur = 1; //Choix du capteur � scruter

    while (1) {
        ScrutationCapteur(cNumVoie, cNumCapteur, cTrameSortie); //Scrutation du capteur de la voie choisie
        EnvoieTrame(cTrameSortie, 36); //Envoie des donn�es r�cup�r�s
        ScrutationVoies(); //Scrutation de toutes les voies de la carte et envoie des capteurs valides
    }
}
