/* 
 * File:   EcritureTrame.c
 * Author: LFRAD
 *
 * Created on 26 ao�t 2021, 09:50
 */

#include <p18f8723.h>
#include <stdio.h>
#include <stdlib.h>
#include "RecupCapteur.h"
#include <string.h>

//unsigned int calcul_CRC(unsigned char far *cTrame, unsigned char cLongueur) { //Calcul du CRC, Fonction d'Eric, pas utilis�
//    unsigned char cBcl, cBcl2;
//    unsigned int iCrc;
//    unsigned char far *cPointeur;
//    iCrc = CRC_START;
//    cPointeur = cTrame;
//    for (cBcl = 0; cBcl < cLongueur; cBcl++) {
//        iCrc ^= (unsigned int) *cPointeur;
//        for (cBcl2 = 0; cBcl2 < 8; cBcl2++) {
//            if (iCrc & 0x8000) {
//                iCrc <<= 1;
//                iCrc ^= CRC_POLY;
//            } else {
//                iCrc <<= 1;
//            }
//        }
//        cPointeur++;
//    }
//    IntToChar(iCrc, cTrame + cLongueur, 5);
//    return iCrc;
//}

void RECUPERE_TRAME_ET_CAL_ETAT(unsigned char* cEtat, unsigned int iPression, unsigned int iSeuil, char cCapteur, char cTypeCarte) { //RECUPERE LES ELEMENTS FIXE EN MOIRE CONSTIT....SEUIL......ETC

    if (*(cEtat + cCapteur) == 0x80) { // Si le capteur est instable 
        *(cEtat + cCapteur) = 50; //Le capteur est sans r�ponse
    } else { //Sinon
        if (iPression > 1700) { //Si le capteur � une pression au dessus de la gamme
            if (cTypeCarte == 'M') //Si r�sistif
            {
                *(cEtat + cCapteur) = 50; //Le capteur est sans r�ponse 
            } else { //Sinon
                *(cEtat + cCapteur) = 40; //Le capteur est hors gamme 
            }

        } else if (iPression >= iSeuil) { //Si le capteur est au dessus du seuil 
            *(cEtat + cCapteur) = 0x00; //Le capteur est bon

        } else if (iPression > 900) { //Si le capteur est en dessous du seuil
            *(cEtat + cCapteur) = 30; //La pression n'est pas bonne, d�clenchement de l'alarme

        } else if (iPression > 0) { //Si le capteur est en dessous de la gamme
            *(cEtat + cCapteur) = 40; //.Le capteur est hors gamme

        } else { //Sinon
            *(cEtat + cCapteur) = 50; //Le capteur est sans r�ponse
        }
    }
}

void TableExistence(unsigned char cVoie, unsigned char* cSortie) { //Lecture de la table d'existance d'une voie
    unsigned int iAdresse;
    iAdresse = 128 * cVoie + 10000; //R�cup�ration de l'adresse de la voie souhait�e
    EESequRead(6, iAdresse, cSortie, 21); //Lecture de la table d'existance
}

void EcritureTrameCapteurOeil(char cVoie, char cCapteur, unsigned int iModul, unsigned int iConso, unsigned int iRepos, unsigned char cEtat, unsigned int iPression, unsigned int iSeuil, unsigned char* cTrame) { //Ecriture de la trame pour oeil sur un capteur
    
    cTrame[0] = 'V';
    IntToChar(cVoie, cTrame + 1, 3); //Ecriture numero de voie
    cTrame[4] = 'C';
    IntToChar(cCapteur, cTrame + 5, 2); //Ecriture numero de capteur
    cTrame[7] = 'M';
    IntToChar(iModul, cTrame + 8, 4); //Ecriture courant module
    cTrame[12] = 'C';
    IntToChar(iConso, cTrame + 13, 4); //Ecriture courant conso
    cTrame[17] = 'R';
    IntToChar(iRepos, cTrame + 18, 4); //Ecriture courant repos
    cTrame[22] = 'E';
    IntToChar(cEtat, cTrame + 23, 2); //Ecriture Etat
    cTrame[25] = 'P';
    IntToChar(iPression, cTrame + 26, 4); //Ecriture pression
    cTrame[30] = 'S';
    IntToChar(iSeuil, cTrame + 31, 4); //Ecriture seuil
    cTrame[35] = 0;
}

void EcritureTrameCapteur(char cVoie, char cCapteur, unsigned char cEtat, unsigned int iPression, unsigned int iSeuil) { //Ecriture d'une trame pour scrutation totale
    unsigned char cTrame[21];
    
    cTrame[0] = 'V';
    IntToChar(cVoie, cTrame + 1, 3); //Ecriture numero de voie
    cTrame[4] = 'C';
    IntToChar(cCapteur, cTrame + 5, 2); //Ecriture numero de capteur
    cTrame[7] = 'E';
    IntToChar(cEtat, cTrame + 8, 2); //Ecriture Etat
    cTrame[10] = 'P';
    IntToChar(iPression, cTrame + 11, 4); //Ecriture pression
    cTrame[15] = 'S';
    IntToChar(iSeuil, cTrame + 16, 4); //Ecriture seuil
    cTrame[20] = 0;

    EnvoieTrame(cTrame, 21);
}

void EcritureTrameVoie(char cVoie, unsigned char* cEtat, unsigned int* iPression, unsigned int* iSeuil) { //Ecriture des trames d'une voie pour une scrutation totale
    char cBcl;

    for (cBcl = 1; cBcl < 17; cBcl++) { //On regarde tous les capteurs de la voie
        if (*(iPression + cBcl) != 0) { //Si la pression du capteur regard� n'est pas nulle (capteur existant)
            RECUPERE_TRAME_ET_CAL_ETAT(cEtat, iPression[cBcl], iSeuil[cBcl], cBcl, 0); //RECUPERE LES ELEMENTS FIXE EN MOIRE CONSTIT....SEUIL......ETC //PRECEDENT CYCLE
            EcritureTrameCapteur(cVoie, cBcl, cEtat[cBcl], iPression[cBcl], iSeuil[cBcl]); //On �crit la trame du capteur
        }
    }
}

void EcritureTrameTot(char cVoie, char cCapteur, unsigned int iPression, unsigned char cEtat, unsigned char* cTrame) { //Ecriture d'une trame type pour la scrutation totale de la carte 
    cTrame[0] = 'V';
    IntToChar(cVoie, cTrame + 1, 2); //Stockage voie
    cTrame[3] = 'C';
    IntToChar(cCapteur, cTrame + 4, 2); //Stockage capteur
    cTrame[6] = 'P';
    IntToChar(iPression, cTrame + 7, 4); //Stockage stockage Etat
    cTrame[11] = 'E';
    IntToChar(cEtat, cTrame + 12, 2); //Stockage stockage pression
}

void IntToChar(unsigned int iValeur, unsigned char *cChaineCar, char cTaille){
    char cTampon[10];
    char cBcl, cDiffLong, cLongTamp;
    
    sprintf(cTampon,"%i",iValeur);
    cLongTamp = strlen(cTampon);
    cDiffLong = cTaille - cLongTamp;
    for(cBcl = 1; cBcl <= cLongTamp; cBcl++){
        cTampon[cTaille - cBcl] = cTampon[cLongTamp - cBcl];
    }
    for(cBcl = 0; cBcl < cDiffLong; cBcl++){
        cTampon[cBcl] = '0';
    }
    for(cBcl = 0; cBcl < cTaille; cBcl++){
        cChaineCar[cBcl] = cTampon[cBcl];
    }
}

void EnvoieVoie(unsigned char cVoie, unsigned char* cEtat, unsigned int* iPression, unsigned int* iSeuil) {
    unsigned char cTestEnvoie;
    unsigned char cTrameVoie[14];
    char cBcl;

    cTestEnvoie = 5;
    for (cBcl = 1; cBcl < 17; cBcl++) { //Renvoies des 16 capteurs de la voie
        if (iPression[cBcl] != 0) { //Si la pression du capteur n'est pas � 0 (capteur existant)
            RECUPERE_TRAME_ET_CAL_ETAT(cEtat, iPression[cBcl], iSeuil[cBcl], cBcl, 0); //RECUPERE LES ELEMENTS FIXE EN MOIRE CONSTIT....SEUIL......ETC //PRECEDENT CYCLE
            EcritureTrameTot(cVoie, cBcl, iPression[cBcl], cEtat[cBcl], cTrameVoie); //On �crit les donn�es � renvoyer dans une trame
            EnvoieDonnees(&cTestEnvoie, 1); //Envoie du caract�re (ENQ) signifiant un envoie de donn�es
            EnvoieDonnees(cTrameVoie, 14); //Renvoie de la trame demand�e par la CM
        }
    }
}