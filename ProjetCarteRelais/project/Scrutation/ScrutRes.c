/* 
 * File:   ScrutRes.c
 * Author: LFRAD
 *
 * Created on 30 septembre 2021, 14:45
 */

#include <p18f8723.h>


#include <stdio.h>
#include <stdlib.h>
#include "RecupCapteur.h"
#define abs(x) ((x) > 0 ? (x) : (-x))


#define COEFF_R1 1
#define COEFF_R2 1

#define COEFFb_R1 1.533
#define COEFFb_R2 1.548

#define COEFFc_R1 3.735
#define COEFFc_R2 3.561

#define OFFSET_R1 -5.173
#define OFFSET_R2 -2.797

#define OFFSETb_R1 -153.3
#define OFFSETb_R2 -154.8

#define OFFSETc_R1 -2535
#define OFFSETc_R2 -2393



// CONFIG1H
#pragma config OSC = HS         // Oscillator Selection bits (HS oscillator)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config IESO = OFF       // Internal/External Oscillator Switchover bit (Two-Speed Start-up disabled)

// CONFIG2L
#pragma config PWRT = OFF       // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = SBORDIS  // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only (SBOREN is disabled))
#pragma config BORV = 3         // Brown-out Voltage bits (Minimum setting)

// CONFIG2H
#pragma config WDT = OFF        // Watchdog Timer (WDT disabled (control is placed on the SWDTEN bit))
#pragma config WDTPS = 32768    // Watchdog Timer Postscale Select bits (1:32768)

// CONFIG3L
#pragma config MODE = MC        // Processor Data Memory Mode Select bits (Microcontroller mode)
#pragma config ADDRBW = ADDR20BIT// Address Bus Width Select bits (20-bit Address Bus)
#pragma config DATABW = DATA16BIT// Data Bus Width Select bit (16-bit External Bus mode)
#pragma config WAIT = OFF       // External Bus Data Wait Enable bit (Wait selections are unavailable for table reads and table writes)

// CONFIG3H
#pragma config CCP2MX = PORTC   // CCP2 MUX bit (ECCP2 input/output is multiplexed with RC1)
#pragma config ECCPMX = PORTE   // ECCP MUX bit (ECCP1/3 (P1B/P1C/P3B/P3C) are multiplexed onto RE6, RE5, RE4 and RE3 respectively)
#pragma config LPT1OSC = OFF    // Low-Power Timer1 Oscillator Enable bit (Timer1 configured for higher power operation)
#pragma config MCLRE = ON       // MCLR Pin Enable bit (MCLR pin enabled; RG5 input pin disabled)

// CONFIG4L
#pragma config STVREN = ON      // Stack Full/Underflow Reset Enable bit (Stack full/underflow will cause Reset)
#pragma config LVP = OFF        // Single-Supply ICSP Enable bit (Single-Supply ICSP disabled)
#pragma config BBSIZ = BB2K     // Boot Block Size Select bits (1K word (2 Kbytes) Boot Block size)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled (Legacy mode))

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not code-protected)
#pragma config CP1 = OFF        // Code Protection bit Block 1 (Block 1 (004000-007FFFh) not code-protected)
#pragma config CP2 = OFF        // Code Protection bit Block 2 (Block 2 (008000-00BFFFh) not code-protected)
#pragma config CP3 = OFF        // Code Protection bit Block 3 (Block 3 (00C000-00FFFFh) not code-protected)
#pragma config CP4 = OFF        // Code Protection bit Block 4 (Block 4 (010000-013FFFh) not code-protected)
#pragma config CP5 = OFF        // Code Protection bit Block 5 (Block 5 (014000-017FFFh) not code-protected)
#pragma config CP6 = OFF        // Code Protection bit Block 6 (Block 6 (01BFFF-018000h) not code-protected)
#pragma config CP7 = OFF        // Code Protection bit Block 7 (Block 7 (01C000-01FFFFh) not code-protected)

// CONFIG5H
#pragma config CPB = OFF        // Boot Block Code Protection bit (Boot Block (000000-0007FFh) not code-protected)
#pragma config CPD = OFF        // Data EEPROM Code Protection bit (Data EEPROM not code-protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not write-protected)
#pragma config WRT1 = OFF       // Write Protection bit Block 1 (Block 1 (004000-007FFFh) not write-protected)
#pragma config WRT2 = OFF       // Write Protection bit Block 2 (Block 2 (008000-00BFFFh) not write-protected)
#pragma config WRT3 = OFF       // Write Protection bit Block 3 (Block 3 (00C000-00FFFFh) not write-protected)
#pragma config WRT4 = OFF       // Write Protection bit Block 4 (Block 4 (010000-013FFFh) not write-protected)
#pragma config WRT5 = OFF       // Write Protection bit Block 5 (Block 5 (014000-017FFFh) not write-protected)
#pragma config WRT6 = OFF       // Write Protection bit Block 6 (Block 6 (01BFFF-018000h) not write-protected)
#pragma config WRT7 = OFF       // Write Protection bit Block 7 (Block 7 (01C000-01FFFFh) not write-protected)

// CONFIG6H
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration registers (300000-3000FFh) not write-protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot Block (000000-007FFF, 000FFF or 001FFFh) not write-protected)
#pragma config WRTD = OFF       // Data EEPROM Write Protection bit (Data EEPROM not write-protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection bit Block 1 (Block 1 (004000-007FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR2 = OFF      // Table Read Protection bit Block 2 (Block 2 (008000-00BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR3 = OFF      // Table Read Protection bit Block 3 (Block 3 (00C000-00FFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR4 = OFF      // Table Read Protection bit Block 4 (Block 4 (010000-013FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR5 = OFF      // Table Read Protection bit Block 5 (Block 5 (014000-017FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR6 = OFF      // Table Read Protection bit Block 6 (Block 6 (018000-01BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR7 = OFF      // Table Read Protection bit Block 7 (Block 7 (01C000-01FFFFh) not protected from table reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Boot Block Table Read Protection bit (Boot Block (000000-007FFF, 000FFF or 001FFFh) not protected from table reads executed in other blocks)





void Mesure_TPR(char fus, char Tab_Voie_1, char Tab_Voie_2);
void DELAY_SEC(int sec);
float Mesure_V_48v(char cMesure, float fCoeff);

float Mesure_V_Ligne_AVB(char cMesure, float fCoeff); // Acquisition de la valeur CAN de la tension ligne AVAL capteur, voies impaires

float Mesure_V_Resistif(char cMesure, int iCoeff);

unsigned int acqui_ana_16(unsigned char adcon);

void InitTimer0(void);

void EcritureTrameResistif(char cVoie, unsigned int iConso, unsigned int iRepos, unsigned char* cTrame);

void main(void) {
    char cBcl;

    Init_I2C();
    Init_RS485();
    init_uc();

    for (cBcl = 1; cBcl < 20; cBcl += 2) {
        Mesure_TPR(0, cBcl, cBcl + 1);
    }

}

void Mesure_TPR(char fus, char Tab_Voie_1, char Tab_Voie_2) // si fus = 1 test pour fusible 	
{
    float fTension48v1;
    float fTension48v2;
    float fTensionAVB1;
    float fTensionAVB2;


    unsigned int iCourantRepos1;
    unsigned int iCourantRepos2;
    float fTensionResistive1; // valeur tension AV_1 pour capteur resistif  sur voies impaires
    float fTensionResistive2; // valeur tension AV_2 pour capteur resistif  sur voies paires
    int iResistanceTP1, iResistanceTP2;

    char cCourtCircuit;

    unsigned int iCourantConso1;
    unsigned int iCourantConso2;

    unsigned int iFreq1, iFreq2;

    unsigned char cTrame[15];

    Prog_HC595(Tab_Voie_1, Tab_Voie_2, 0, 0);

    cCourtCircuit = 0x00;

    //Prog_HC595(1,2,0,FUSIBLEHS); //COMMANDE RELAIS !
    //DEPART INIT
    DECH_L1 = 0;
    V_SUP1 = 0;
    UN_CPT_1 = 0;
    CPT_ON_1 = 0;
    DECH_L2 = 0;
    V_SUP2 = 0;
    UN_CPT_2 = 0;
    CPT_ON_2 = 0;



    DECH_L1 = 1;
    DECH_L2 = 1;


    //config pour 56V VOIE IMPAIRE
    UN_CPT_1 = 1; //VALIDE
    V_SUP1 = 0; //DEVALIDE 
    CPT_ON_1 = 0; //   <======== d�valide 56V SUR VOIE   par s�curit�
    DECH_L1 = 0; // valide d�charge VOIE 
    //config pour 56V VOIE PAIRE
    UN_CPT_2 = 1;
    V_SUP2 = 0;
    CPT_ON_2 = 0; //   <======== d�valide 56V SUR VOIE   par s�curit�
    DECH_L2 = 0; // valide d�charge VOIE 


    DECH_L1 = 1;
    DECH_L2 = 1;


    DELAY_MS(100); //*						
    DECH_L1 = 0;
    DECH_L2 = 0;
    DELAY_MS(100); //*
    CPT_ON_1 = 1; //   <======== valide ligne de mesure 
    CPT_ON_2 = 1; //   <======== valide ligne de mesure 


    if (fus == 0)
        DELAY_SEC(3);


    fTensionResistive1 = Mesure_V_Resistif(IRepos1_AN2, Coeff_Resis1); // valeur = V_Resistif (int)   mesure en aval du capteur
    fTensionResistive2 = Mesure_V_Resistif(IRepos2_AN3, Coeff_Resis2); // valeur = V_Resistif (int)   mesure en aval du capteur

    fTensionAVB1 = Mesure_V_Ligne_AVB(LigneAVB1_AN14, Coeff_Ligne_AVB_1);
    fTensionAVB2 = Mesure_V_Ligne_AVB(LigneAVB2_AN15, Coeff_Ligne_AVB_2);
    
    fTension48v1 = Mesure_V_48v(LigneAVB1_AN14, Coeff_V_48v_1);
    fTension48v2 = Mesure_V_48v(LigneAVB2_AN15, Coeff_V_48v_2);
    
    iCourantConso1 = Mesure_I_Conso(IConso1_AN4, Coeff_I_Conso1);
    iCourantConso2 = Mesure_I_Conso(IConso2_AN5, Coeff_I_Conso2);
    
    iCourantRepos1 = Mesure_I_Repos(IRepos1_AN2, Coeff_I_Repos1);
    iCourantRepos2 = Mesure_I_Repos(IRepos2_AN3, Coeff_I_Repos2);



    // TEST CC
    if (iCourantConso1 >= Icons_cc)// AU DESSU DE 15mA (4000ohms) sur la ligne resistif >100k (0.6mA)
    {
        cCourtCircuit = 0xF1;
        if (iCourantConso2 >= Icons_cc)// AU DESSU DE 15mA (4000ohms) sur la ligne resistif >100k (0.6mA)
        {
            cCourtCircuit = 0xFF;
        }
    }
    if (iCourantConso2 >= Icons_cc)// AU DESSU DE 15mA (4000ohms) sur la ligne resistif >100k (0.6mA)
    {
        cCourtCircuit = 0xF2;
        if (iCourantConso1 >= Icons_cc)// AU DESSU DE 15mA (4000ohms) sur la ligne resistif >100k (0.6mA)
        {
            cCourtCircuit = 0xFF;
        }
    }




    DELAY_MS(100);

    iResistanceTP1 = COEFF_R1 * 1000 * (fTension48v1 - fTensionAVB1) / iCourantRepos1 + OFFSET_R1; //SI INFERIEUR A 500
    if (iResistanceTP1 > 500) {
        iResistanceTP1 = COEFFb_R1 * 1000 * (fTension48v1 - fTensionAVB1) / iCourantRepos1 + OFFSETb_R1; //SI INFERIEUR A 500
    }
    if (iResistanceTP1 > 1000) {
        iResistanceTP1 = COEFFc_R1 * 1000 * (fTension48v1 - fTensionAVB1) / iCourantRepos1 + OFFSETc_R1; //SI INFERIEUR A 500
    }





    iResistanceTP2 = COEFF_R2 * 1000 * (fTension48v2 - fTensionAVB2) / iCourantRepos2 + OFFSET_R2;
    if (iResistanceTP2 > 500) {
        iResistanceTP2 = COEFFb_R2 * 1000 * (fTension48v2 - fTensionAVB2) / iCourantRepos2 + OFFSETb_R2;
    }
    if (iResistanceTP2 > 1000) {
        iResistanceTP2 = COEFFc_R2 * 1000 * (fTension48v2 - fTensionAVB2) / iCourantRepos2 + OFFSETc_R2;
    }


    //20 Tests car 20 paliers en r�sistif


    if (iCourantRepos2 > 612) iResistanceTP2 = 0;
    if (iCourantRepos2 <= 612) iResistanceTP2 = 100;
    if (iCourantRepos2 <= 558) iResistanceTP2 = 110;
    if (iCourantRepos2 <= 504) iResistanceTP2 = 122;
    if (iCourantRepos2 <= 457) iResistanceTP2 = 135;
    if (iCourantRepos2 <= 412) iResistanceTP2 = 150;
    if (iCourantRepos2 <= 374) iResistanceTP2 = 166;
    if (iCourantRepos2 <= 335) iResistanceTP2 = 186;
    if (iCourantRepos2 <= 299) iResistanceTP2 = 208;
    if (iCourantRepos2 <= 270) iResistanceTP2 = 232;
    if (iCourantRepos2 <= 240) iResistanceTP2 = 265;
    if (iCourantRepos2 <= 211) iResistanceTP2 = 301;
    if (iCourantRepos2 <= 186) iResistanceTP2 = 344;
    if (iCourantRepos2 <= 163) iResistanceTP2 = 400;
    if (iCourantRepos2 <= 142) iResistanceTP2 = 468;
    if (iCourantRepos2 <= 121) iResistanceTP2 = 568;
    if (iCourantRepos2 <= 102) iResistanceTP2 = 698;
    if (iCourantRepos2 <= 84) iResistanceTP2 = 898;
    if (iCourantRepos2 <= 67) iResistanceTP2 = 1200;
    if (iCourantRepos2 <= 51) iResistanceTP2 = 1820;
    if (iCourantRepos2 <= 33) iResistanceTP2 = 3820;
    if (iCourantRepos2 <= 28) iResistanceTP2 = 6000;




    if (iCourantRepos1 > 612) iResistanceTP1 = 0;
    if (iCourantRepos1 <= 612) iResistanceTP1 = 100;
    if (iCourantRepos1 <= 558) iResistanceTP1 = 110;
    if (iCourantRepos1 <= 504) iResistanceTP1 = 122;
    if (iCourantRepos1 <= 457) iResistanceTP1 = 135;
    if (iCourantRepos1 <= 412) iResistanceTP1 = 150;
    if (iCourantRepos1 <= 374) iResistanceTP1 = 166;
    if (iCourantRepos1 <= 335) iResistanceTP1 = 186;
    if (iCourantRepos1 <= 299) iResistanceTP1 = 208;
    if (iCourantRepos1 <= 270) iResistanceTP1 = 232;
    if (iCourantRepos1 <= 240) iResistanceTP1 = 265;
    if (iCourantRepos1 <= 211) iResistanceTP1 = 301;
    if (iCourantRepos1 <= 186) iResistanceTP1 = 344;
    if (iCourantRepos1 <= 163) iResistanceTP1 = 400;
    if (iCourantRepos1 <= 142) iResistanceTP1 = 468;
    if (iCourantRepos1 <= 121) iResistanceTP1 = 568;
    if (iCourantRepos1 <= 102) iResistanceTP1 = 698;
    if (iCourantRepos1 <= 84) iResistanceTP1 = 898;
    if (iCourantRepos1 <= 67) iResistanceTP1 = 1200;
    if (iCourantRepos1 <= 51) iResistanceTP1 = 1820;
    if (iCourantRepos1 <= 33) iResistanceTP1 = 3820;
    if (iCourantRepos1 <= 28) iResistanceTP1 = 6000;





    iResistanceTP1 = COEFF_R1*iResistanceTP1;
    iResistanceTP2 = COEFF_R2*iResistanceTP2;






    CPT_ON_1 = 0; //   <======== d�valide ligne de mesure 
    CPT_ON_2 = 0; //   <======== d�valide ligne de mesure 









    // 100K correspond au minimum de valeur d'un capteur r�sistif
    if (fTensionResistive1 < 0) {
        iFreq1 = 8888;
    } else {


        if (iResistanceTP1 < 100) iFreq1 = 0; // <==  ajustements 
        if (iResistanceTP1 >= 100) iFreq1 = 900; //100 vraie valeur
        if (iResistanceTP1 >= 110) iFreq1 = 934; //110 vraie valeur
        if (iResistanceTP1 >= 122) iFreq1 = 969; //122 vraie valeur
        if (iResistanceTP1 >= 135) iFreq1 = 1003;
        if (iResistanceTP1 >= 150) iFreq1 = 1038;
        if (iResistanceTP1 >= 166) iFreq1 = 1072;
        if (iResistanceTP1 >= 186) iFreq1 = 1107;
        if (iResistanceTP1 >= 208) iFreq1 = 1141;
        if (iResistanceTP1 >= 232) iFreq1 = 1176;
        if (iResistanceTP1 >= 265) iFreq1 = 1210;
        if (iResistanceTP1 >= 301) iFreq1 = 1245;
        if (iResistanceTP1 >= 344) iFreq1 = 1279;
        if (iResistanceTP1 >= 400) iFreq1 = 1314;
        if (iResistanceTP1 >= 468) iFreq1 = 1348;
        if (iResistanceTP1 >= 568) iFreq1 = 1383;
        if (iResistanceTP1 >= 698) iFreq1 = 1417;
        if (iResistanceTP1 >= 898) iFreq1 = 1452;
        if (iResistanceTP1 >= 1200) iFreq1 = 1486;
        if (iResistanceTP1 >= 1820) iFreq1 = 1521;
        if (iResistanceTP1 >= 3820) iFreq1 = 1555;
        if (iResistanceTP1 >= 5500) iFreq1 = 9998; //062018 1999
    }



    // 100K correspond au minimum de valeur d'un capteur r�sistif
    if (fTensionResistive2 < 0) {
        iFreq2 = 8888;
    } else {




        if (iResistanceTP2 < 100) iFreq2 = 0; // <==  ajustements 
        if (iResistanceTP2 >= 100) iFreq2 = 900; //100 vraie valeur
        if (iResistanceTP2 >= 110) iFreq2 = 934; //110 vraie valeur
        if (iResistanceTP2 >= 122) iFreq2 = 969; //122 vraie valeur
        if (iResistanceTP2 >= 135) iFreq2 = 1003;
        if (iResistanceTP2 >= 150) iFreq2 = 1038;
        if (iResistanceTP2 >= 166) iFreq2 = 1072;
        if (iResistanceTP2 >= 186) iFreq2 = 1107;
        if (iResistanceTP2 >= 208) iFreq2 = 1141;
        if (iResistanceTP2 >= 232) iFreq2 = 1176;
        if (iResistanceTP2 >= 265) iFreq2 = 1210;
        if (iResistanceTP2 >= 301) iFreq2 = 1245;
        if (iResistanceTP2 >= 344) iFreq2 = 1279;
        if (iResistanceTP2 >= 400) iFreq2 = 1314;
        if (iResistanceTP2 >= 468) iFreq2 = 1348;
        if (iResistanceTP2 >= 568) iFreq2 = 1383;
        if (iResistanceTP2 >= 698) iFreq2 = 1417;
        if (iResistanceTP2 >= 898) iFreq2 = 1452;
        if (iResistanceTP2 >= 1200) iFreq2 = 1486;
        if (iResistanceTP2 >= 1820) iFreq2 = 1521;
        if (iResistanceTP2 >= 3820) iFreq2 = 1555;
        if (iResistanceTP2 >= 5500) iFreq2 = 9998; //1999 062018	


    }

    if (fus == 0) {
        EcritureTrameResistif(Tab_Voie_1, iCourantConso1, iFreq1, cTrame);
        EnvoieTrame(cTrame, 15);

        EcritureTrameResistif(Tab_Voie_2, iCourantConso2, iFreq2, cTrame);
        EnvoieTrame(cTrame, 15);

        mesure_test_cc(Tab_Voie_1, Tab_Voie_2, 1); //ecris CC ou pas 
    }


}

void DELAY_SEC(int sec)
//Prescaler 1:128; TMR0 Preload = 18661; Actual Interrupt Time : 1sec
{
    int iBcl;
    T0CON = 0x86; /* enable TMR0, select instruction clock, prescaler set to 1.128 */
    for (iBcl = 0; iBcl < sec; iBcl++) {
        InitTimer0();
    }
    return;
}

float Mesure_V_Ligne_AVB(char cMesure, float fCoeff){ // Acquisition de la valeur CAN de la tension ligne AMont capteur
    float fLigneAVB;
    fLigneAVB = Coefficient * acqui_ana_16(cMesure) * fCoeff; //(16 mesures)
    return fLigneAVB;
} // fin

// --------- Mesure tension ligne en aval du capteur pour transducteurs r�sistifs----------------

float Mesure_V_Resistif(char cMesure, int iCoeff){ // Acquisition de la valeur CAN de la tension ligne AVal capteur
    float fVResistif;
    fVResistif = Coefficient * acqui_ana_16_plus(cMesure) * iCoeff; //(16 mesures avec tempo entre les mesures)
    return fVResistif;
}

float Mesure_V_48v(char cMesure, float fCoeff) {
    float fTension48v;
    fTension48v = Coefficient * acqui_ana_16(cMesure) * fCoeff; //(16 mesures)
    return fTension48v;
} // fin

/*----------------- acqui_ana_16 -----------------------------------------------*/
unsigned int acqui_ana_16(unsigned char adcon) {
    unsigned long lMoyen;
    int iBcl; // Variable usage general
    
    lMoyen = 0;
    for (iBcl = 0; iBcl < 0x10; iBcl++) // 16 MESURES POUR LA MOYENNE
    {
        lMoyen = lMoyen + acqui_ana(adcon);
    }
    lMoyen >>= 4; // Moyenne pour 16 mesures
    
    return ((int) lMoyen);



}

void InitTimer0(void) {
    TMR0H = 0x48; //Preload = 18661
    TMR0L = 0xE5;
    INTCONbits.TMR0IE = 0;
    INTCONbits.TMR0IF = 0;
    while (!(INTCONbits.TMR0IF)); /* wait until TMR0 rolls over */
}

void EcritureTrameResistif(char cVoie, unsigned int iConso, unsigned int iRepos, unsigned char* cTrame) {
    cTrame[0] = 'V';
    IntToChar(cVoie, cTrame + 1, 3); //Stockage voie
    cTrame[4] = 'C';
    IntToChar(iConso, cTrame + 5, 4); //Stockage courant module
    cTrame[9] = 'R';
    IntToChar(iRepos, cTrame + 10, 4); //Stockage stockage courant conso
    cTrame[14] = 0;
}