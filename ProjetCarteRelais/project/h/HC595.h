
#include <p18f8723.h>

#define TRIS_Data_HC595    TRISEbits.TRISE3 //data     
#define TRIS_Clk_HC595     TRISEbits.TRISE4 //clock   
#define TRIS_Rck_HC595     TRISEbits.TRISE5 //    
#define TRIS_Rst_HC595     TRISEbits.TRISE6 //reset    
#define TRIS_OE_HC595      TRISEbits.TRISE7 //output enable     

#define Data_HC595 		LATEbits.LATE3 
#define Clk_HC595 	 LATEbits.LATE4
#define Rck_HC595 		LATEbits.LATE5 
#define Rst_HC595   	LATEbits.LATE6 
#define OE_HC595   		LATEbits.LATE7 


void Prog_HC595 (unsigned char rel_voies_impaires,unsigned char rel_voies_paires,unsigned char alarme_ca,unsigned char test_fus);






	