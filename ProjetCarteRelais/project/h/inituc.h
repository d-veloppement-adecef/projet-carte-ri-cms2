
#ifndef __INITUC_H

/************************ DEFINE  ***************************/

// LECTURE CLAVIER ( ENTREES DIVERSES POUR FAIRE UN CLAVIER DE MISE � JOUR HORLOGE
#define 	touche_0	PORTJbits.RJ4		//entr�e RJ4 	MODE TEST + voir pour maj horloge	
#define 	RA  	PORTJbits.RJ3		//entr�e RJ3 	Resis ou adresse
#define 	touche_2	PORTJbits.RJ2		//entr�e RJ2	voir pour maj horloge	
#define 	SelA0		PORTFbits.RF3		//entr�e BP3		
#define 	SelA1		PORTFbits.RF4		//entr�e BP4		
#define 	SelA2		PORTFbits.RF5		//entr�e BP5		
#define 	touche_6	PORTFbits.RF6		//entr�e BP6	AN11	SI MIS EN ENTREE LOGIQUE	



#define 	ALIM_OFF		PORTBbits.RB0		//entr�e d�tection coupure alim  (RB0) � mettre sous interruption		
#define		VAL_I2C_EXT		PORTBbits.RB4		//entr�e demande de validation I2C externe  SUR RB4  modif!!!!!
#define		VAL_RS485_EXT	PORTBbits.RB1		//entr�e demande de validation RS485 externe  SUR RB1  modif!!!!!!

#define		I2C_INTERNE		PORTCbits.RC2 //I2C interne ou externe 

#define		LEDTX		PORTHbits.RH2 	//  sortie LED TX	OK_RI
#define		LEDRX		PORTHbits.RH3	//  sortie LED RX	OK_RI
 
#define		Val_R_I2C		PORTJbits.RJ5 		//sortie validation r�sistance de tirage � vdd,sur bus I2C	OK_RI

 
#define		CYCLE		PORTCbits.RC1 		//CYCLE MESURE
//#define		OK_I2C_EXT		LATCbits.LATC1 		//sortie accord I2C externe	OK_RI
#define		OK_RS485_EXT	LATCbits.LATC5 		//sortie accord I2C externe	OK_RI
#define		OK_I2C_INT		LATCbits.LATC2 		//sortie valid I2C interne	OK_RI
#define		Sel_Freq		LATGbits.LATG0 		//sortie s�lection voie fr�quence capteur � mesure (0 voie impaire)(1 voie paire)OK_RI

#define		AL_CABLE	LATHbits.LATH0 	//  sortie validation alarme c�ble	OK_RI
//#define		LED_TX		LATHbits.LATH2 	//  sortie LED TX	OK_RI
//#define		LED_RX		LATHbits.LATH3 	//  sortie LED RX	OK_RI
#define		RX_TX		LATAbits.LATA4 	//  sortie RX_TX	OK_RI


#define		HORLOGE		PORTBbits.RB3 	//	Signal carr� 1Hz de l'horloge	OK_RI


#define		Data_Rel	LATEbits.LATE3 	//	Data_Rel	OK_RI
#define		Clk_Rel		LATEbits.LATE4 	//	Clk_Rel		OK_RI
#define		Rck_Rel		LATEbits.LATE5	//	Rck_Rel		OK_RI
#define		Rst_Rel		LATEbits.LATE6 	//	Rst_Rel		OK_RI


#define		SORTIETest_Fus	PORTEbits.RE7 	//	Test_Fus		OK_RI



#define		CPT_ON_1	LATGbits.LATG1 		// Valid tension sur capteur voies impaires		ok_ri
#define		UN_CPT_1 	LATGbits.LATG2		// choix valeur tension capteur voies impaires		ok_ri				
#define		CPT_ON_2	PORTGbits.RG3 		// Valid tension sur capteur voies paires		ok_ri
#define		UN_CPT_2	LATGbits.LATG4 		// choix valeur tension capteur voies paires		ok_ri
#define		DECH_L1		PORTFbits.RF6		// D�charge ligne capteur voies impaires	ok_ri		
#define		DECH_L2		PORTFbits.RF7		// D�charge ligne capteur voies paires		ok_ri
#define		V_SUP1 	LATJbits.LATJ6 		// Choix valeur tension capteur voies impaires		ok_ri
#define		V_SUP2 	PORTJbits.RJ7		// Choix valeur tension capteur voies paires		ok_ri



/**********  DECLARATION DES SOUS PROGRAMMES ********/

void init_uc (void);  //initialisation du microcontrolleur
void Init_I2C(void);
void Init_RS485(void);
#endif




