#ifndef __eeprom_I2CERIC


unsigned char LIRE_EEPROM(unsigned char ADDE,unsigned int ADDHL,unsigned char *tab,char r);
unsigned char ECRIRE_EEPROMBYTE(unsigned char ADDE,unsigned int ADDHL,unsigned char *tab,char g);
unsigned int calcul_addmemoire (unsigned char *tab);
unsigned char choix_memoire(unsigned char *tab);
unsigned char putstringI2C(unsigned char *wrptr);

#endif
