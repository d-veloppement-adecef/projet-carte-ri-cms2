/*
 * Project: Digital_Clock_V2.0
 * File Name: rtc.h
 * Author: Siddharth Chandrasekaran
 * Created on July 20, 2012, 6:12 PM
 * Visit http://embedjournal.com for more codes.
*/
#ifndef RTC_H
#define RTC_H

#define RTC_ADR 0b11010000
#define I2C_READ 1
#define I2C_WRITE 0

unsigned char envoyerHORLOGE_i2c(unsigned char NumeroH,unsigned char ADDH, unsigned char dataH0);
signed char litHORLOGE_i2c(unsigned char NumeroH,unsigned char AddH); 
unsigned char ECRIRE_HORLOGE(unsigned char zone,unsigned char Time);

unsigned char LIRE_HORLOGE(unsigned char zone);
void creer_trame_horloge (void);
void entrer_trame_horloge (void);

#endif