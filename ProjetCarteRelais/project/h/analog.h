#define _ADIF	PIR1bits.ADIF	// INTERRUPTION FIN CONVERSION ANALOGIQUE
#define _ADON	ADCON0bits.ADON	// VALIDE LE CAN
#define _ADGO	ADCON0bits.GO	// LANCE ACQUISITION ET CONVERSION
#define _ADDONE	ADCON0bits.DONE	// TEST SI ACQUISITION ET CONVERSION EN COURS

unsigned int acqui_ana(unsigned char); // Acquisition de la valeur du CAN a l'adresse specifi�e	
void Mesure_V_48v_1(void); // Mesure 48V pour voies impaires
void Mesure_V_48v_2(void); // Mesure 48V pour voies paires
void Mesure_V_Ligne_AM_1(void); // Acquisition de la valeur CAN de la tension ligne AMont capteur, voies impaires
void Mesure_V_Ligne_AM_2(void); // Acquisition de la valeur CAN de la tension ligne AMont capteur, voies paires
void Mesure_V_Ligne_AVB_1(void); // Acquisition de la valeur CAN de la tension ligne AVAL capteur, voies impaires
void Mesure_V_Ligne_AVB_2(void); // Acquisition de la valeur CAN de la tension ligne AVAL capteur, voies paires


void Mesure_V_Resistif_1(void);					// Acquisition de la valeur CAN de la tension du convertisseur 48V
void Mesure_V_Resistif_2(void);					// Acquisition de la valeur CAN de la tension du convertisseur 48V
void Mesure_I_Modul_1 (void);							// Acquisition de la valeur CAN du courant de modulation
void Mesure_I_Modul_2 (void);							// Acquisition de la valeur CAN du courant de modulation
void Mesure_I_Conso_1(void);								// Acquisition de la valeur CAN du courant de consommation
void Mesure_I_Conso_2(void);								// Acquisition de la valeur CAN du courant de consommation
void Mesure_I_Repos_1(void);								// Acquisition de la valeur CAN du courant de repos
void Mesure_I_Repos_2(void);								// Acquisition de la valeur CAN du courant de repos
unsigned int acqui_ana_16 (unsigned char adcon ); 
unsigned int acqui_ana_16_plus (unsigned char adcon );






