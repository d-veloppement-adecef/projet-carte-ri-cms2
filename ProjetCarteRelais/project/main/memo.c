
#include <delay.h>						/* Defs DELAY functions */
#include <i2c.h>
#include <ftoa.h>						/* Adapt Chaine de caract�re */
#include <memo.h>
#include <eeprom.h>
#include <eeprom_i2cERIC.h>
#include <math.h>
#include <rs485.h>



#pragma udata BIGDATA  //section BIGDATA pour tableau ==> voir modif dans le fichier: 18f8723_bigdata.lkr plac� dans H :"c:\carte_relais\projet\h"; ce fichier remplace 18f8723_g.lkr de mcc18/bin/lkr. 
//Ne pas oubler de le d�clarer dans: Projet/build options.../projet/directories/Include search path et ajouter c:\Carte_Relais\projet\H et d'ajouter le fichier modifi� 18f8723_bigdata.lkr par Projet/Add Files to Projet...
unsigned char Tab_Voie_1[1]; //1 octet VOIE IMPAIRES 1,3,5....99 	  ASCII OU HEX??  => HEXA OU DECIMAL ET CONVERSION EN ASCII JUSTE POUR ECRITURE EN EEPROM
unsigned char Tab_Voie_2[1]; //1 octet VOIE PAIRES   2,4,6...100 	 					IDEM POUR TOUTES LES VALEURS
unsigned char Tab_code_1[17]; //17 octets 	code 0,1,2.....16 	
unsigned char Tab_code_2[17]; //17 octets 	code 0,1,2.....16 	 
unsigned char Tab_Type_Enr[17]; //17 octets  pour m�moriser le type d'enregistrement (A,R,D)
unsigned int Tab_I_Modul_1[17]; //34 octets	17 caract�res	  1 d�bitmetre et 16 TP
unsigned int Tab_I_Modul_2[17]; //34 octets	17 caract�res	  1 d�bitmetre et 16 TP
unsigned int Tab_I_Conso_1[17]; //34 octets	17 caract�res	  1 d�bitmetre et 16 TP
unsigned int Tab_I_Conso_2[17]; //34 octets	17 caract�res	  1 d�bitmetre et 16 TP
unsigned int Tab_I_Repos_1[17]; //34 octets 17 caract�res	  1 d�bitmetre et 16 TP
unsigned int Tab_I_Repos_2[17]; //34 octets 17 caract�res	  1 d�bitmetre et 16 TP

unsigned char Tab_Horodatage[12]; //10 octets	 //10 caract�res	  
unsigned char Tab_Etat_1[17]; //17 octets   (rien=00, int=01, alarme A1= 10, alarme A2=20, alarme A3=30
unsigned char Tab_Etat_2[17]; //17 octets  
unsigned int Tab_Pression_1[17]; //34 octets   en 0 valeur du d�bit  si ffff => pas de d�bitm�tre   !!! METTRE UN CODE SI TP RESISTIF
unsigned int Tab_Pression_2[17]; //34 octets   en 0 valeur du d�bit  si ffff => pas de d�bitm�tre   !!! METTRE UN CODE SI TP RESISTIF
unsigned int Tab_Seuil_1[17]; //34 octets	17 caract�res	  seuil 16+1 tp + deb
unsigned int Tab_Seuil_2[17]; //34 octets	17 caract�res	  seuil 16+1 tp + deb


unsigned char Tab_existance1[24]; //EXISTANCE VOIE IMPAIRE VOIR CARTOGRAPHIE FIN MEMOIRE 4 
unsigned char Tab_existance2[24]; //EXISTANCE VOIE PAIRE
// N�VOIE R ou D ou A
//031D-RDAFRRRRRRRRRRRRR             //VOIE 31 DEFECTUEUSE IMPOSSIBLE MAIS PR EXEMPLE DEBIT=Resistif PRESSION1=DEFECTUEUX  PREESION2=ADRESSABLE PREESION3=EFFACE OOU VIDE PRESSION4=RESISTIF..............

//total 455  0x1C5 max 0x1FE
// retour � defaut section






#pragma udata gpr6
// MEMOIRE la taille #03301R111122223333BRA/4444/22/1COMMENTAIRESICIROB03326111525001020000456781108650888* soit 86octets
unsigned char TRAMEOUT[125];


#pragma udata gpr7 
unsigned char TRAMEIN[125];

#pragma udata gpr8 
unsigned char TMP[125];



#pragma udata gpr4  
unsigned char Tab_Constitution[13];
unsigned char Tab_Commentaire_cable[18];
unsigned char Tab_Distance[10];
unsigned int TMPINT;




#pragma romdata eeprom_data=0xF00000
rom const char Tab_Voie_eep[2]; //2 octets 	 
//rom const int 		Tab_No_Enr_eep[1];					//2 enregistre le num�ro d'enregistrement sur 2 octets
rom const char Tab_Type_Enr_eep[1]; //1 octets  pour m�moriser le type d'enregistrement (TPA, TPR, D�bitm�tre, Groupe, Icra)
rom const char Tab_Horodatage_eep[10]; //10 octets	 //horodatage
rom const int Tab_Pression_eep[17]; //34 octets   en 0 valeur du d�bit  si ffff => pas de d�bitm�tre
rom const int Tab_I_Repos_eep[17]; //34 octets   
rom const char Tab_I_Modul_eep[17]; //17 octets
rom const int Tab_I_Conso_eep[17]; //17 octets
rom const char Tab_Etat_eep[17]; //17 octets   total:   octets


rom const char Constitution[13]; //????octets   mettre des X  ou ? dans les plages NON UTILISEES
rom const char Commentaire_cable[18]; //????octets   mettre des X  ou ? dans les plages NON UTILISEES
rom const char Num_cable[3]; //????octets   mettre des X  ou ? dans les plages NON UTILISEES
rom const char Libre[4]; //????octets   mettre des X  ou ? dans les plages NON UTILISEES
rom const char Distance[10]; //????octets   mettre des X  ou ? dans les plages NON UTILISEES


#pragma romdata		/* Resume allocation of romdata into the default section */



extern char numcarte;

extern unsigned int adr_eeprom;
extern unsigned data_char;
extern unsigned int val_eeprom_i2c;
extern unsigned char recherche_TPR;
extern unsigned char recherche_TPA; // si = 1 => mode recherche automatique des capteurs donc dernier capteur = 16 sur voie impaire et paire

unsigned int Offset_Enr; // adresse i2c de l'enregistrement en lecture

extern unsigned int ad_Der_Enreg; // adresse du dernier enregistrement en EEprom du pic
extern unsigned int Der_Enreg; // valeur de l'adresse m�moire du dernier enregistrement en EEprom du pic

extern unsigned int ad_No_der_enreg; // adresse o� est stock�e, dans le pic, le num�ro du dernier enregistrement 
extern unsigned int No_der_enreg; // valeur du num�ro du dernier enregistrement   entre 0 et 80



unsigned int Offset_Lect;
unsigned int tmp_I, tmp_I2;




unsigned char No_voie; // voie � sauvegarder
unsigned char No_codage; // n� de capteur � sauvegarder
unsigned char No_eeprom; // N� de m�moire pour la sauvegarde


unsigned char toWrite[4]; // MAX 4 OCTETS A ECRIRE EN ASCII

extern unsigned char Memo_autorisee_1; // autorisation de sauvegarde voie impaire
extern unsigned char Memo_autorisee_2; // autorisation de sauvegarde voie paire
extern unsigned char Num_Carte; // num�ro(position sur carte m�re de la carte relais) et pour d�finir n� de voies et sauvegarde
unsigned char Sauvegarde_en_cours;

extern unsigned char DEMANDE_INFOS_CAPTEURS[11];
extern char typedecarte, numdecarte;

extern char FUSIBLEHS, ALARMECABLE;
extern unsigned char BREAKSCRUTATION;




extern unsigned char SAVADDE; //BUG SCRUTATION RESISTIFS SUR ADRESSABLE ACCES MEMOIRE
extern unsigned int SAVADDHL;
extern unsigned int SAVtab;
extern unsigned char SAVr;

extern char FUNCTIONUSE;

extern char NOSAVEOEILSCRUT;


#pragma		code	USER_ROM   // retour � la zone de code*/

void ecriture_trame_i2c(char vv) //choisir voie a traiter ATTENTION CELA CALCUL LETAT AUSSI !!!!!!!!!!!!!!!!!!!!! EN FONCTION DU SEUIL
{// ET RECUPERE LES INFOS FIXE


    unsigned char tpa;
    unsigned char i;
    unsigned char c; //	pour constitution			
    //unsigned int tmpI;
    float tmpf;

    // VOIES IMPAIRES############################################################
    //	*********** CodeVoie  ***********
    //	CODE  de 0 � 16  sachant sue le 0 est le d�bitm�tre 



    DELAY_MS(100); // CAR DES FOIS UNE CARTE PARTAIT EN LIVE !!!!!!!!
    for (i = 0; i <= 16; i++) // il faut faire autant d'enregistrement que de capteurs  A VOIR POUR LES DEBITMETRES !!!!!!!!!!!!!!!
    {
        BREAKSCRUTATION = 0x00;
        if ((vv != 1)&&(vv != 2)) //PROBLEME ON RESET !!!! SCRUTATION AUTOMATIQUE
        {




            BREAKSCRUTATION = 0xFF;
            goto finscrut;

        }

        if (vv == 1) // si autorisation de sauvegarde voie impaire valid�e
        {
            tpa = Tab_code_1[i]; //CODAGE A SAV
            if (tpa > 0) {
                // Sauvegarde 
                No_voie = Tab_Voie_1[0];
                No_codage = Tab_code_1[i]; //A FAIRE 16 SAUVEGARDES	MAX + DEBITMETRE A VOIR !!!!!!!!!!!!!!!!!!	//au sens existence
            }




            //VOIE PAIRE 
            TRAMEOUT[0] = '#';
            IntToChar(No_voie, TMP, 3);
            TRAMEOUT[1] = TMP[0];
            TRAMEOUT[2] = TMP[1];
            TRAMEOUT[3] = TMP[2];
            IntToChar(i, TMP, 2);
            TRAMEOUT[4] = TMP[0];
            TRAMEOUT[5] = TMP[1];

            //	if (No_voie==11)
            //	{			
            //	TRAMEOUT[50]=TMP[0]; //TEST
            //	}
            RECUPERE_TRAME_ET_CAL_ETAT(TRAMEOUT, 1, i); //RECUPERE LES ELEMENTS FIXE EN MOIRE CONSTIT....SEUIL......ETC //PRECEDENT CYCLE


            TRAMEOUT[6] = 'A'; // ADRESSABLE OU RESITIF 

            IntToChar(Tab_I_Modul_1[i], TMP, 4);
            TRAMEOUT[7] = TMP[0];
            TRAMEOUT[8] = TMP[1];
            TRAMEOUT[9] = TMP[2];
            TRAMEOUT[10] = TMP[3];

            IntToChar(Tab_I_Conso_1[i], TMP, 4);
            TRAMEOUT[11] = TMP[0];
            TRAMEOUT[12] = TMP[1];
            TRAMEOUT[13] = TMP[2];
            TRAMEOUT[14] = TMP[3];


            IntToChar(Tab_I_Repos_1[i], TMP, 4);
            TRAMEOUT[15] = TMP[0];
            TRAMEOUT[16] = TMP[1];
            TRAMEOUT[17] = TMP[2];
            TRAMEOUT[18] = TMP[3];


            //CONSTITUTION 

            TRAMEOUT[19] = Tab_Constitution[0]; //info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEOUT,1,i);
            TRAMEOUT[20] = Tab_Constitution[1];
            TRAMEOUT[21] = Tab_Constitution[2];
            TRAMEOUT[22] = Tab_Constitution[3];
            TRAMEOUT[23] = Tab_Constitution[4];
            TRAMEOUT[24] = Tab_Constitution[5];
            TRAMEOUT[25] = Tab_Constitution[6];
            TRAMEOUT[26] = Tab_Constitution[7];
            TRAMEOUT[27] = Tab_Constitution[8];
            TRAMEOUT[28] = Tab_Constitution[9];
            TRAMEOUT[29] = Tab_Constitution[10];
            TRAMEOUT[30] = Tab_Constitution[11];
            TRAMEOUT[31] = Tab_Constitution[12];

            if (recherche_TPA) {
                TRAMEOUT[19] = 'x';
                TRAMEOUT[20] = 'x';
                TRAMEOUT[21] = 'x';
                TRAMEOUT[22] = 'x';
                TRAMEOUT[23] = 'x';
                TRAMEOUT[24] = 'x';
                TRAMEOUT[25] = 'x';
                TRAMEOUT[26] = 'x';
                TRAMEOUT[27] = 'x';
                TRAMEOUT[28] = 'x';
                TRAMEOUT[29] = 'x';
                TRAMEOUT[30] = 'x';
                TRAMEOUT[31] = 'x';
            }

            //COMMENTAIRE CABLE


            TRAMEOUT[32] = Tab_Commentaire_cable[0]; //info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEOUT,1,i);
            TRAMEOUT[33] = Tab_Commentaire_cable[1];
            TRAMEOUT[34] = Tab_Commentaire_cable[2];
            TRAMEOUT[35] = Tab_Commentaire_cable[3];
            TRAMEOUT[36] = Tab_Commentaire_cable[4];
            TRAMEOUT[37] = Tab_Commentaire_cable[5];
            TRAMEOUT[38] = Tab_Commentaire_cable[6];
            TRAMEOUT[39] = Tab_Commentaire_cable[7];
            TRAMEOUT[40] = Tab_Commentaire_cable[8];
            TRAMEOUT[41] = Tab_Commentaire_cable[9];
            TRAMEOUT[42] = Tab_Commentaire_cable[10];
            TRAMEOUT[43] = Tab_Commentaire_cable[11];
            TRAMEOUT[44] = Tab_Commentaire_cable[12];
            TRAMEOUT[45] = Tab_Commentaire_cable[13];
            TRAMEOUT[46] = Tab_Commentaire_cable[14];
            TRAMEOUT[47] = Tab_Commentaire_cable[15];
            TRAMEOUT[48] = Tab_Commentaire_cable[16];
            TRAMEOUT[49] = Tab_Commentaire_cable[17];

            if (recherche_TPA) {
                TRAMEOUT[32] = 'x';
                TRAMEOUT[33] = 'x';
                TRAMEOUT[34] = 'x';
                TRAMEOUT[35] = 'x';
                TRAMEOUT[36] = 'x';
                TRAMEOUT[37] = 'x';
                TRAMEOUT[38] = 'x';
                TRAMEOUT[39] = 'x';
                TRAMEOUT[40] = 'x';
                TRAMEOUT[41] = 'x';
                TRAMEOUT[42] = 'x';
                TRAMEOUT[43] = 'x';
                TRAMEOUT[44] = 'x';
                TRAMEOUT[45] = 'x';
                TRAMEOUT[46] = 'x';
                TRAMEOUT[47] = 'x';
                TRAMEOUT[48] = 'x';
                TRAMEOUT[49] = 'x';
            }






            IntToChar(No_voie, TMP, 3);
            TRAMEOUT[50] = TMP[0];
            TRAMEOUT[51] = TMP[1];
            TRAMEOUT[52] = TMP[2];



            TRAMEOUT[53] = Tab_Horodatage[0]; //Jour
            TRAMEOUT[54] = Tab_Horodatage[1]; //Jour
            TRAMEOUT[55] = Tab_Horodatage[2]; // MOIS
            TRAMEOUT[56] = Tab_Horodatage[3]; //MOIS
            TRAMEOUT[57] = Tab_Horodatage[6]; //HEURE
            TRAMEOUT[58] = Tab_Horodatage[7]; //HEURE
            TRAMEOUT[59] = Tab_Horodatage[8]; //MIN
            TRAMEOUT[60] = Tab_Horodatage[9]; //MIN



            TRAMEOUT[61] = '?';

            //CODAGE
            IntToChar(i, TMP, 2);
            TRAMEOUT[62] = TMP[0];
            TRAMEOUT[63] = TMP[1];

            //POS
            if (i == 0) {
                TMP[0] = '0';
                TMP[1] = '1';
            } else {
                TMP[0] = '0';
                TMP[1] = '2';
            }
            //IntToChar(i+1, TMP,2);
            TRAMEOUT[64] = TMP[0]; //POS
            TRAMEOUT[65] = TMP[1]; //POS




            TRAMEOUT[66] = '?';
            TRAMEOUT[67] = '?';
            TRAMEOUT[68] = '?';
            TRAMEOUT[69] = '?';







            //DISTANCE
            TRAMEOUT[70] = Tab_Distance[0]; //info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEOUT,1,i);
            TRAMEOUT[71] = Tab_Distance[1];
            TRAMEOUT[72] = Tab_Distance[2];
            TRAMEOUT[73] = Tab_Distance[3];
            TRAMEOUT[74] = Tab_Distance[4];

            if (recherche_TPA) {
                TRAMEOUT[70] = '?';
                TRAMEOUT[71] = '?';
                TRAMEOUT[72] = '?';
                TRAMEOUT[73] = '?';
                TRAMEOUT[74] = '?';
            }

            //ETAT

            IntToChar(Tab_Etat_1[i], TMP, 2);
            TRAMEOUT[75] = TMP[0];
            TRAMEOUT[76] = TMP[1];



            //FREQUENCE
            // TEST L'INSTABILITE QD SCRUTATION test la valeur
            //if ((TRAMEOUT[81]=='0')&&(TRAMEOUT[82]=='8')&&(TRAMEOUT[83]=='0')&&(TRAMEOUT[84]<'5')&&(TRAMEOUT[84]>'1'))
            //{
            tmp_I = 0;

            tmp_I2 = TRAMEIN[77];
            tmp_I = tmp_I + 1000 * (tmp_I2 - 48);
            tmp_I2 = TRAMEIN[78];
            tmp_I = tmp_I + 100 * (tmp_I2 - 48);
            tmp_I2 = TRAMEIN[79];
            tmp_I = tmp_I + 10 * (tmp_I2 - 48);
            tmp_I2 = TRAMEIN[80];
            tmp_I = tmp_I + (tmp_I2 - 48);
            if (Tab_Pression_1[i] != 0) {
                if ((tmp_I < (Tab_Pression_1[i] - 20)) || (tmp_I > (Tab_Pression_1[i] + 20)))// il est instable car variation sur 20mbar
                {
                    //TRAMEOUT[75]='5'; //ON MET L'ETAT S/R avec la valeur donc instable  08/03/2018 SUP 
                    //TRAMEOUT[76]='0'; ON LAISSE SI ETAT PRECEDENT = INT 
                } // voir si qd plus le cas letat redevient normal											
            }
            //}// FIN INSTABILITE


            IntToChar(Tab_Pression_1[i], TMP, 4);
            TRAMEOUT[77] = TMP[0];
            TRAMEOUT[78] = TMP[1];
            TRAMEOUT[79] = TMP[2];
            TRAMEOUT[80] = TMP[3];

            IntToChar(Tab_Seuil_1[i], TMP, 4);
            TRAMEOUT[81] = TMP[0];
            TRAMEOUT[82] = TMP[1];
            TRAMEOUT[83] = TMP[2];
            TRAMEOUT[84] = TMP[3];



            if (recherche_TPA) //SEUIL AUTO LORS DE LA SCRUTATION
            {

                Tab_Seuil_1[i] = Tab_Pression_1[i] - 60;
                if (Tab_Seuil_1[i] <= 900)
                    Tab_Seuil_1[i] = 900; //SI LA VALEUR EST INFERIEUR A 900mbar

                IntToChar(Tab_Seuil_1[i], TMP, 4);
                TRAMEOUT[81] = TMP[0];
                TRAMEOUT[82] = TMP[1];
                TRAMEOUT[83] = TMP[2];
                TRAMEOUT[84] = TMP[3];

            }






            //CRC
            TRAMEOUT[85] = '2';
            TRAMEOUT[86] = '2';
            TRAMEOUT[87] = '2';
            TRAMEOUT[88] = '2';
            TRAMEOUT[89] = '2';
            TRAMEOUT[90] = '*';





            remplir_tab_presence(No_voie, 1, i);

        }






        if (vv == 2) // si autorisation de sauvegarde voie paire valid�e
        {

            tpa = Tab_code_2[i];
            if (tpa > 0) {
                // Sauvegarde 
                No_voie = Tab_Voie_2[0];
                No_codage = Tab_code_2[i]; //A FAIRE 16 SAUVEGARDES	MAX 
            }







            //VOIE IMPAIRE 
            TRAMEOUT[0] = '#';
            IntToChar(No_voie, TMP, 3);
            TRAMEOUT[1] = TMP[0];
            TRAMEOUT[2] = TMP[1];
            TRAMEOUT[3] = TMP[2];
            IntToChar(i, TMP, 2);
            TRAMEOUT[4] = TMP[0];
            TRAMEOUT[5] = TMP[1];


            RECUPERE_TRAME_ET_CAL_ETAT(TRAMEOUT, 2, i); //RECUPERE LES ELEMENTS FIXE EN MOIRE CONSTIT....SEUIL......ETC						







            TRAMEOUT[6] = 'A'; // ADRESSABLE OU RESITIF 

            IntToChar(Tab_I_Modul_2[i], TMP, 4);
            TRAMEOUT[7] = TMP[0];
            TRAMEOUT[8] = TMP[1];
            TRAMEOUT[9] = TMP[2];
            TRAMEOUT[10] = TMP[3];

            IntToChar(Tab_I_Conso_2[i], TMP, 4);
            TRAMEOUT[11] = TMP[0];
            TRAMEOUT[12] = TMP[1];
            TRAMEOUT[13] = TMP[2];
            TRAMEOUT[14] = TMP[3];


            IntToChar(Tab_I_Repos_2[i], TMP, 4);
            TRAMEOUT[15] = TMP[0];
            TRAMEOUT[16] = TMP[1];
            TRAMEOUT[17] = TMP[2];
            TRAMEOUT[18] = TMP[3];



            TRAMEOUT[19] = Tab_Constitution[0]; //info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEOUT,1,i);
            TRAMEOUT[20] = Tab_Constitution[1];
            TRAMEOUT[21] = Tab_Constitution[2];
            TRAMEOUT[22] = Tab_Constitution[3];
            TRAMEOUT[23] = Tab_Constitution[4];
            TRAMEOUT[24] = Tab_Constitution[5];
            TRAMEOUT[25] = Tab_Constitution[6];
            TRAMEOUT[26] = Tab_Constitution[7];
            TRAMEOUT[27] = Tab_Constitution[8];
            TRAMEOUT[28] = Tab_Constitution[9];
            TRAMEOUT[29] = Tab_Constitution[10];
            TRAMEOUT[30] = Tab_Constitution[11];
            TRAMEOUT[31] = Tab_Constitution[12];


            if (recherche_TPA) {
                TRAMEOUT[19] = 'x';
                TRAMEOUT[20] = 'x';
                TRAMEOUT[21] = 'x';
                TRAMEOUT[22] = 'x';
                TRAMEOUT[23] = 'x';
                TRAMEOUT[24] = 'x';
                TRAMEOUT[25] = 'x';
                TRAMEOUT[26] = 'x';
                TRAMEOUT[27] = 'x';
                TRAMEOUT[28] = 'x';
                TRAMEOUT[29] = 'x';
                TRAMEOUT[30] = 'x';
                TRAMEOUT[31] = 'x';
            }


            TRAMEOUT[32] = Tab_Commentaire_cable[0]; //info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEOUT,1,i);
            TRAMEOUT[33] = Tab_Commentaire_cable[1];
            TRAMEOUT[34] = Tab_Commentaire_cable[2];
            TRAMEOUT[35] = Tab_Commentaire_cable[3];
            TRAMEOUT[36] = Tab_Commentaire_cable[4];
            TRAMEOUT[37] = Tab_Commentaire_cable[5];
            TRAMEOUT[38] = Tab_Commentaire_cable[6];
            TRAMEOUT[39] = Tab_Commentaire_cable[7];
            TRAMEOUT[40] = Tab_Commentaire_cable[8];
            TRAMEOUT[41] = Tab_Commentaire_cable[9];
            TRAMEOUT[42] = Tab_Commentaire_cable[10];
            TRAMEOUT[43] = Tab_Commentaire_cable[11];
            TRAMEOUT[44] = Tab_Commentaire_cable[12];
            TRAMEOUT[45] = Tab_Commentaire_cable[13];
            TRAMEOUT[46] = Tab_Commentaire_cable[14];
            TRAMEOUT[47] = Tab_Commentaire_cable[15];
            TRAMEOUT[48] = Tab_Commentaire_cable[16];
            TRAMEOUT[49] = Tab_Commentaire_cable[17];


            if (recherche_TPA) {
                TRAMEOUT[32] = 'x';
                TRAMEOUT[33] = 'x';
                TRAMEOUT[34] = 'x';
                TRAMEOUT[35] = 'x';
                TRAMEOUT[36] = 'x';
                TRAMEOUT[37] = 'x';
                TRAMEOUT[38] = 'x';
                TRAMEOUT[39] = 'x';
                TRAMEOUT[40] = 'x';
                TRAMEOUT[41] = 'x';
                TRAMEOUT[42] = 'x';
                TRAMEOUT[43] = 'x';
                TRAMEOUT[44] = 'x';
                TRAMEOUT[45] = 'x';
                TRAMEOUT[46] = 'x';
                TRAMEOUT[47] = 'x';
                TRAMEOUT[48] = 'x';
                TRAMEOUT[49] = 'x';
            }



            IntToChar(No_voie, TMP, 3);
            TRAMEOUT[50] = TMP[0];
            TRAMEOUT[51] = TMP[1];
            TRAMEOUT[52] = TMP[2];



            TRAMEOUT[53] = Tab_Horodatage[0]; //Jour
            TRAMEOUT[54] = Tab_Horodatage[1]; //Jour
            TRAMEOUT[55] = Tab_Horodatage[2]; // MOIS
            TRAMEOUT[56] = Tab_Horodatage[3]; //MOIS
            TRAMEOUT[57] = Tab_Horodatage[6]; //HEURE
            TRAMEOUT[58] = Tab_Horodatage[7]; //HEURE
            TRAMEOUT[59] = Tab_Horodatage[8]; //MIN
            TRAMEOUT[60] = Tab_Horodatage[9]; //MIN


            TRAMEOUT[61] = '?';
            IntToChar(i, TMP, 2);
            TRAMEOUT[62] = TMP[0];
            TRAMEOUT[63] = TMP[1];

            //POS
            if (i == 0) {
                TMP[0] = '0';
                TMP[1] = '1';
            } else {
                TMP[0] = '0';
                TMP[1] = '2';
            }
            //IntToChar(i+1, TMP,2);
            TRAMEOUT[64] = TMP[0]; //POS
            TRAMEOUT[65] = TMP[1]; //POS



            TRAMEOUT[66] = '?';
            TRAMEOUT[67] = '?';
            TRAMEOUT[68] = '?';
            TRAMEOUT[69] = '?';

            //DISTANCE
            TRAMEOUT[70] = Tab_Distance[0]; //info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEOUT,1,i);
            TRAMEOUT[71] = Tab_Distance[1];
            TRAMEOUT[72] = Tab_Distance[2];
            TRAMEOUT[73] = Tab_Distance[3];
            TRAMEOUT[74] = Tab_Distance[4];


            if (recherche_TPA) {
                TRAMEOUT[70] = '?';
                TRAMEOUT[71] = '?';
                TRAMEOUT[72] = '?';
                TRAMEOUT[73] = '?';
                TRAMEOUT[74] = '?';
            }

            //ETAT
            IntToChar(Tab_Etat_2[i], TMP, 2); // voir si etat pr resistif
            TRAMEOUT[75] = TMP[0];
            TRAMEOUT[76] = TMP[1];

            //FREQUENCE


            tmp_I = 0;

            tmp_I2 = TRAMEIN[77];
            tmp_I = tmp_I + 1000 * (tmp_I2 - 48);
            tmp_I2 = TRAMEIN[78];
            tmp_I = tmp_I + 100 * (tmp_I2 - 48);
            tmp_I2 = TRAMEIN[79];
            tmp_I = tmp_I + 10 * (tmp_I2 - 48);
            tmp_I2 = TRAMEIN[80];
            tmp_I = tmp_I + (tmp_I2 - 48);
            if (Tab_Pression_2[i] != 0) {
                if ((tmp_I < (Tab_Pression_2[i] - 20)) || (tmp_I > (Tab_Pression_2[i] + 20)))// il est instable car variation sur 20mbar
                {
                    //	TRAMEOUT[75]='5'; //ON MET L'ETAT S/R avec la valeur donc instable  08/03/2018 SUP
                    //TRAMEOUT[76]='0'; // ON LAISSE LETAT PRECEDENT SI INT
                } // voir si qd plus le cas letat redevient normal											
            }
            //}// FIN INSTABILITE


            IntToChar(Tab_Pression_2[i], TMP, 4);
            TRAMEOUT[77] = TMP[0];
            TRAMEOUT[78] = TMP[1];
            TRAMEOUT[79] = TMP[2];
            TRAMEOUT[80] = TMP[3];





            IntToChar(Tab_Seuil_2[i], TMP, 4);
            TRAMEOUT[81] = TMP[0];
            TRAMEOUT[82] = TMP[1];
            TRAMEOUT[83] = TMP[2];
            TRAMEOUT[84] = TMP[3];




            if (recherche_TPA) {


                Tab_Seuil_2[i] = Tab_Pression_2[i] - 60;
                if (Tab_Seuil_2[i] <= 900)
                    Tab_Seuil_2[i] = 900; //SI VALEUR <=
                IntToChar(Tab_Seuil_2[i], TMP, 4);
                TRAMEOUT[81] = TMP[0];
                TRAMEOUT[82] = TMP[1];
                TRAMEOUT[83] = TMP[2];
                TRAMEOUT[84] = TMP[3];

            }




            //CRC
            TRAMEOUT[85] = '2';
            TRAMEOUT[86] = '2';
            TRAMEOUT[87] = '2';
            TRAMEOUT[88] = '2';
            TRAMEOUT[89] = '2';
            TRAMEOUT[90] = '*';

            remplir_tab_presence(No_voie, 2, i);

        }


        Sauvegarde_en_cours = 1;

        INTCON3bits.INT1IE = 0; // devalide l'interruption pour RS485 1 pr reactiver

        //CODE V
        if (Memo_autorisee_1 || Memo_autorisee_2) //  si on memorise
        {
            if ((NOSAVEOEILSCRUT == 0) || (NOSAVEOEILSCRUT == 2)) //CONTRE ORDRE PAR OEIL OU SCRUTATION VOIE UNIQUE
            {
                if (vv == 2) //Si voie paire
                    ECRIRE_EEPROMBYTE(choix_memoire(TRAMEOUT), calcul_addmemoire(TRAMEOUT), TRAMEOUT, 1); //ECRITURE EN EEPROM seulement codage1
            } else {
                DELAY_MS(100);
            }

            if ((NOSAVEOEILSCRUT == 0) || (NOSAVEOEILSCRUT == 1)) //CONTRE ORDRE PAR OEIL OU SCRUTATION VOIE UNIQUE
            {
                if (vv == 1) //Si voie impaire
                    ECRIRE_EEPROMBYTE(choix_memoire(TRAMEOUT), calcul_addmemoire(TRAMEOUT), TRAMEOUT, 1); //ECRITURE EN EEPROM seulement codage1
            } else {
                DELAY_MS(100);
            }




        }


        DELAY_MS(10);

        LIRE_EEPROM(choix_memoire(TRAMEOUT), calcul_addmemoire(TRAMEOUT), TRAMEIN, 1); //LIRE DANS EEPROM



        DELAY_MS(10);

    } // FIN DU FOR

    //CODE V
    if (vv == 1 && recherche_TPA == 1) {
        if ((NOSAVEOEILSCRUT == 0) || (NOSAVEOEILSCRUT == 1)) //CONTRE ORDRE PAR OEIL OU SCRUTATION VOIE UNIQUE
        {
            ecrire_tab_presence_TPA(1);
        } else {
            DELAY_MS(100);
        }
    }
    if (vv == 2 && recherche_TPA == 1) {
        if ((NOSAVEOEILSCRUT == 0) || (NOSAVEOEILSCRUT == 2)) //CONTRE ORDRE PAR OEIL OU SCRUTATION VOIE UNIQUE
        {
            ecrire_tab_presence_TPA(2);
        } else {
            DELAY_MS(100);
        }

    }

finscrut:


    Sauvegarde_en_cours = 0; // Sauvegarde termin�e
    INTCON3bits.INT1IE = 1; // valide l'interruption pour RS485 1 pr reactiver

}

void ecriture_trame_i2c_tpr(char vv) {

    //unsigned int tmpI;
    //JUSTE LA TRAME EN VOIE ET CODAGE 1 !!!!!!!!!!!!!!!!!!!!!!!


    if (vv == 2) {
        //VOIE IMPAIRE 
        TRAMEOUT[0] = '#';
        IntToChar(Tab_Voie_2[0], TMP, 3);
        TRAMEOUT[1] = TMP[0];
        TRAMEOUT[2] = TMP[1];
        TRAMEOUT[3] = TMP[2];
        IntToChar(1, TMP, 2); // SUR CODAGE 1 !!!!! uniquement
        TRAMEOUT[4] = TMP[0];
        TRAMEOUT[5] = TMP[1];


        RECUPERE_TRAME_ET_CAL_ETAT(TRAMEOUT, 2, 1); //RECUPERE LES ELEMENTS FIXE EN MOIRE CONSTIT....SEUIL......ETC
        TRAMEOUT[6] = 'R'; // ADRESSABLE OU RESITIF 


        TRAMEOUT[7] = '0';
        TRAMEOUT[8] = '0';
        TRAMEOUT[9] = '0';
        TRAMEOUT[10] = '0';

        IntToChar(Tab_I_Conso_2[1], TMP, 4);
        TRAMEOUT[11] = TMP[0];
        TRAMEOUT[12] = TMP[1];
        TRAMEOUT[13] = TMP[2];
        TRAMEOUT[14] = TMP[3];


        IntToChar(Tab_I_Repos_2[1], TMP, 4);
        TRAMEOUT[15] = TMP[0];
        TRAMEOUT[16] = TMP[1];
        TRAMEOUT[17] = TMP[2];
        TRAMEOUT[18] = TMP[3];



        TRAMEOUT[19] = Tab_Constitution[0]; //info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEOUT,1,i);
        TRAMEOUT[20] = Tab_Constitution[1];
        TRAMEOUT[21] = Tab_Constitution[2];
        TRAMEOUT[22] = Tab_Constitution[3];
        TRAMEOUT[23] = Tab_Constitution[4];
        TRAMEOUT[24] = Tab_Constitution[5];
        TRAMEOUT[25] = Tab_Constitution[6];
        TRAMEOUT[26] = Tab_Constitution[7];
        TRAMEOUT[27] = Tab_Constitution[8];
        TRAMEOUT[28] = Tab_Constitution[9];
        TRAMEOUT[29] = Tab_Constitution[10];
        TRAMEOUT[30] = Tab_Constitution[11];
        TRAMEOUT[31] = Tab_Constitution[12];


        if (recherche_TPR) {
            TRAMEOUT[19] = 'x';
            TRAMEOUT[20] = 'x';
            TRAMEOUT[21] = 'x';
            TRAMEOUT[22] = 'x';
            TRAMEOUT[23] = 'x';
            TRAMEOUT[24] = 'x';
            TRAMEOUT[25] = 'x';
            TRAMEOUT[26] = 'x';
            TRAMEOUT[27] = 'x';
            TRAMEOUT[28] = 'x';
            TRAMEOUT[29] = 'x';
            TRAMEOUT[30] = 'x';
            TRAMEOUT[31] = 'x';
        }



        TRAMEOUT[32] = Tab_Commentaire_cable[0]; //info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEOUT,1,i);
        TRAMEOUT[33] = Tab_Commentaire_cable[1];
        TRAMEOUT[34] = Tab_Commentaire_cable[2];
        TRAMEOUT[35] = Tab_Commentaire_cable[3];
        TRAMEOUT[36] = Tab_Commentaire_cable[4];
        TRAMEOUT[37] = Tab_Commentaire_cable[5];
        TRAMEOUT[38] = Tab_Commentaire_cable[6];
        TRAMEOUT[39] = Tab_Commentaire_cable[7];
        TRAMEOUT[40] = Tab_Commentaire_cable[8];
        TRAMEOUT[41] = Tab_Commentaire_cable[9];
        TRAMEOUT[42] = Tab_Commentaire_cable[10];
        TRAMEOUT[43] = Tab_Commentaire_cable[11];
        TRAMEOUT[44] = Tab_Commentaire_cable[12];
        TRAMEOUT[45] = Tab_Commentaire_cable[13];
        TRAMEOUT[46] = Tab_Commentaire_cable[14];
        TRAMEOUT[47] = Tab_Commentaire_cable[15];
        TRAMEOUT[48] = Tab_Commentaire_cable[16];
        TRAMEOUT[49] = Tab_Commentaire_cable[17];


        if (recherche_TPR) {
            TRAMEOUT[32] = 'x';
            TRAMEOUT[33] = 'x';
            TRAMEOUT[34] = 'x';
            TRAMEOUT[35] = 'x';
            TRAMEOUT[36] = 'x';
            TRAMEOUT[37] = 'x';
            TRAMEOUT[38] = 'x';
            TRAMEOUT[39] = 'x';
            TRAMEOUT[40] = 'x';
            TRAMEOUT[41] = 'x';
            TRAMEOUT[42] = 'x';
            TRAMEOUT[43] = 'x';
            TRAMEOUT[44] = 'x';
            TRAMEOUT[45] = 'x';
            TRAMEOUT[46] = 'x';
            TRAMEOUT[47] = 'x';
            TRAMEOUT[48] = 'x';
            TRAMEOUT[49] = 'x';
        }

        IntToChar(No_voie, TMP, 3);
        TRAMEOUT[50] = TMP[0];
        TRAMEOUT[51] = TMP[1];
        TRAMEOUT[52] = TMP[2];



        TRAMEOUT[53] = Tab_Horodatage[0]; //Jour
        TRAMEOUT[54] = Tab_Horodatage[1]; //Jour
        TRAMEOUT[55] = Tab_Horodatage[2]; // MOIS
        TRAMEOUT[56] = Tab_Horodatage[3]; //MOIS
        TRAMEOUT[57] = Tab_Horodatage[6]; //HEURE
        TRAMEOUT[58] = Tab_Horodatage[7]; //HEURE
        TRAMEOUT[59] = Tab_Horodatage[8]; //MIN
        TRAMEOUT[60] = Tab_Horodatage[9]; //MIN


        TRAMEOUT[61] = '?';
        IntToChar(1, TMP, 2);
        TRAMEOUT[62] = TMP[0];
        TRAMEOUT[63] = TMP[1];

        IntToChar(2, TMP, 2);
        TRAMEOUT[64] = TMP[0]; //POS
        TRAMEOUT[65] = TMP[1]; //POS


        TRAMEOUT[66] = '?';
        TRAMEOUT[67] = '?';
        TRAMEOUT[68] = '?';
        TRAMEOUT[69] = '?';



        TRAMEOUT[70] = Tab_Distance[0]; //info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEOUT,1,i);
        TRAMEOUT[71] = Tab_Distance[1];
        TRAMEOUT[72] = Tab_Distance[2];
        TRAMEOUT[73] = Tab_Distance[3];
        TRAMEOUT[74] = Tab_Distance[4];

        if (recherche_TPR) {

            //DISTANCE
            TRAMEOUT[70] = '?';
            TRAMEOUT[71] = '?';
            TRAMEOUT[72] = '?';
            TRAMEOUT[73] = '?';
            TRAMEOUT[74] = '?';
        }



        //ETAT
        IntToChar(Tab_Etat_2[1], TMP, 2);
        TRAMEOUT[75] = TMP[0];
        TRAMEOUT[76] = TMP[1];

        //FREQUENCE

        // TEST L'INSTABILITE QD SCRUTATION test la valeur
        //if ((TRAMEOUT[81]=='0')&&(TRAMEOUT[82]=='8')&&(TRAMEOUT[83]=='0')&&(TRAMEOUT[84]<'5')&&(TRAMEOUT[84]>'1'))
        //{
        tmp_I = 0;

        tmp_I2 = TRAMEIN[77];
        tmp_I = tmp_I + 1000 * (tmp_I2 - 48);
        tmp_I2 = TRAMEIN[78];
        tmp_I = tmp_I + 100 * (tmp_I2 - 48);
        tmp_I2 = TRAMEIN[79];
        tmp_I = tmp_I + 10 * (tmp_I2 - 48);
        tmp_I2 = TRAMEIN[80];
        tmp_I = tmp_I + (tmp_I2 - 48);
        if (Tab_Pression_2[1] != 0) {
            if ((tmp_I < (Tab_Pression_2[1] - 20)) || (tmp_I > (Tab_Pression_2[1] + 20)))// il est instable car variation sur 20mbar
            {
                //		TRAMEOUT[75]='5'; //ON MET L'ETAT S/R avec la valeur donc instable  
                //												TRAMEOUT[76]='0'; //SI ETAT PRECEDENT = INT
            } // voir si qd plus le cas letat redevient normal											
        }
        //}// FIN INSTABILITE




        IntToChar(Tab_Pression_2[1], TMP, 4);
        TRAMEOUT[77] = TMP[0];
        TRAMEOUT[78] = TMP[1];
        TRAMEOUT[79] = TMP[2];
        TRAMEOUT[80] = TMP[3];

        IntToChar(Tab_Seuil_2[1], TMP, 4);
        TRAMEOUT[81] = TMP[0];
        TRAMEOUT[82] = TMP[1];
        TRAMEOUT[83] = TMP[2];
        TRAMEOUT[84] = TMP[3];






        //SEUIL AUTO SI 5 SCRUTATION DEPUIS LORIGINE
        //if ((TRAMEOUT[81]=='0')&&(TRAMEOUT[82]=='8')&&(TRAMEOUT[83]=='0')&&(TRAMEOUT[84]<'5'))
        //{
        //TRAMEOUT[84]=TRAMEOUT[84]+1;
        //}
        //if ((TRAMEOUT[81]=='0')&&(TRAMEOUT[82]=='8')&&(TRAMEOUT[83]=='0')&&(TRAMEOUT[84]=='5')) //SI 0805 mbar seuil alors on regarde la derniere valeur et on retire 60mbar
        //{
        //Tab_Seuil_2[1]=Tab_Pression_2[1]-60;
        //IntToChar(Tab_Seuil_2[1], TMP,4);
        //TRAMEOUT[81]=TMP[0];
        //TRAMEOUT[82]=TMP[1];
        //TRAMEOUT[83]=TMP[2];
        //TRAMEOUT[84]=TMP[3];				
        //}// FIN SEUIL AUTO 






        if (recherche_TPR) {

            Tab_Seuil_2[1] = Tab_Pression_2[1] - 60;
            if (Tab_Seuil_2[1] <= 900)
                Tab_Seuil_2[1] = 900;

            IntToChar(Tab_Seuil_2[1], TMP, 4);
            TRAMEOUT[81] = TMP[0];
            TRAMEOUT[82] = TMP[1];
            TRAMEOUT[83] = TMP[2];
            TRAMEOUT[84] = TMP[3];





            //TRAMEOUT[81]='0';
            //TRAMEOUT[82]='8';  //VALEUR PAR DEFAUT 800mbar
            //TRAMEOUT[83]='0';
            //TRAMEOUT[84]='0';
        }
        //CRC
        TRAMEOUT[85] = '2';
        TRAMEOUT[86] = '2';
        TRAMEOUT[87] = '2';
        TRAMEOUT[88] = '2';
        TRAMEOUT[89] = '2';
        TRAMEOUT[90] = '*';
        //CODE V
        if (recherche_TPR == 1) {
            if ((NOSAVEOEILSCRUT == 0) || (NOSAVEOEILSCRUT == 2)) //CONTRE ORDRE SCRUTATION VOIE UNIQUE
            {
                ecrire_tab_presence_TPR(2);
            } else {
                DELAY_MS(100);
            }

        }

        Sauvegarde_en_cours = 1;

        INTCON3bits.INT1IE = 0; // devalide l'interruption pour RS485 1 pr reactiver

        //CODE V
        if (Memo_autorisee_2) {
            if ((NOSAVEOEILSCRUT == 0) || (NOSAVEOEILSCRUT == 2)) //CONTRE ORDRE PAR OEIL OU SCRUTATION VOIE UNIQUE
            {
                ECRIRE_EEPROMBYTE(choix_memoire(TRAMEOUT), calcul_addmemoire(TRAMEOUT), TRAMEOUT, 1); //ECRITURE EN EEPROM seulement codage1
            } else {
                DELAY_MS(100);
            }

        }


        Sauvegarde_en_cours = 0;

        INTCON3bits.INT1IE = 1; // devalide l'interruption pour RS485 1 pr reactiver










    }



    if (vv == 1) {

        //VOIE IMPAIRE 
        TRAMEOUT[0] = '#';
        IntToChar(Tab_Voie_1[0], TMP, 3);
        TRAMEOUT[1] = TMP[0];
        TRAMEOUT[2] = TMP[1];
        TRAMEOUT[3] = TMP[2];
        IntToChar(1, TMP, 2); // SUR CODAGE 1 !!!!! uniquement
        TRAMEOUT[4] = TMP[0];
        TRAMEOUT[5] = TMP[1];

        RECUPERE_TRAME_ET_CAL_ETAT(TRAMEOUT, 1, 1); //RECUPERE LES ELEMENTS FIXE EN MOIRE CONSTIT....SEUIL......ETC


        TRAMEOUT[6] = 'R'; // ADRESSABLE OU RESITIF 


        TRAMEOUT[7] = '0';
        TRAMEOUT[8] = '0';
        TRAMEOUT[9] = '0';
        TRAMEOUT[10] = '0';

        IntToChar(Tab_I_Conso_1[1], TMP, 4);
        TRAMEOUT[11] = TMP[0];
        TRAMEOUT[12] = TMP[1];
        TRAMEOUT[13] = TMP[2];
        TRAMEOUT[14] = TMP[3];


        IntToChar(Tab_I_Repos_1[1], TMP, 4);
        TRAMEOUT[15] = TMP[0];
        TRAMEOUT[16] = TMP[1];
        TRAMEOUT[17] = TMP[2];
        TRAMEOUT[18] = TMP[3];






        TRAMEOUT[19] = Tab_Constitution[0]; //info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEOUT,1,i);
        TRAMEOUT[20] = Tab_Constitution[1];
        TRAMEOUT[21] = Tab_Constitution[2];
        TRAMEOUT[22] = Tab_Constitution[3];
        TRAMEOUT[23] = Tab_Constitution[4];
        TRAMEOUT[24] = Tab_Constitution[5];
        TRAMEOUT[25] = Tab_Constitution[6];
        TRAMEOUT[26] = Tab_Constitution[7];
        TRAMEOUT[27] = Tab_Constitution[8];
        TRAMEOUT[28] = Tab_Constitution[9];
        TRAMEOUT[29] = Tab_Constitution[10];
        TRAMEOUT[30] = Tab_Constitution[11];
        TRAMEOUT[31] = Tab_Constitution[12];


        if (recherche_TPR) {
            TRAMEOUT[19] = 'x';
            TRAMEOUT[20] = 'x';
            TRAMEOUT[21] = 'x';
            TRAMEOUT[22] = 'x';
            TRAMEOUT[23] = 'x';
            TRAMEOUT[24] = 'x';
            TRAMEOUT[25] = 'x';
            TRAMEOUT[26] = 'x';
            TRAMEOUT[27] = 'x';
            TRAMEOUT[28] = 'x';
            TRAMEOUT[29] = 'x';
            TRAMEOUT[30] = 'x';
            TRAMEOUT[31] = 'x';
        }

        TRAMEOUT[32] = Tab_Commentaire_cable[0]; //info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEOUT,1,i);
        TRAMEOUT[33] = Tab_Commentaire_cable[1];
        TRAMEOUT[34] = Tab_Commentaire_cable[2];
        TRAMEOUT[35] = Tab_Commentaire_cable[3];
        TRAMEOUT[36] = Tab_Commentaire_cable[4];
        TRAMEOUT[37] = Tab_Commentaire_cable[5];
        TRAMEOUT[38] = Tab_Commentaire_cable[6];
        TRAMEOUT[39] = Tab_Commentaire_cable[7];
        TRAMEOUT[40] = Tab_Commentaire_cable[8];
        TRAMEOUT[41] = Tab_Commentaire_cable[9];
        TRAMEOUT[42] = Tab_Commentaire_cable[10];
        TRAMEOUT[43] = Tab_Commentaire_cable[11];
        TRAMEOUT[44] = Tab_Commentaire_cable[12];
        TRAMEOUT[45] = Tab_Commentaire_cable[13];
        TRAMEOUT[46] = Tab_Commentaire_cable[14];
        TRAMEOUT[47] = Tab_Commentaire_cable[15];
        TRAMEOUT[48] = Tab_Commentaire_cable[16];
        TRAMEOUT[49] = Tab_Commentaire_cable[17];


        if (recherche_TPR) {
            TRAMEOUT[32] = 'x';
            TRAMEOUT[33] = 'x';
            TRAMEOUT[34] = 'x';
            TRAMEOUT[35] = 'x';
            TRAMEOUT[36] = 'x';
            TRAMEOUT[37] = 'x';
            TRAMEOUT[38] = 'x';
            TRAMEOUT[39] = 'x';
            TRAMEOUT[40] = 'x';
            TRAMEOUT[41] = 'x';
            TRAMEOUT[42] = 'x';
            TRAMEOUT[43] = 'x';
            TRAMEOUT[44] = 'x';
            TRAMEOUT[45] = 'x';
            TRAMEOUT[46] = 'x';
            TRAMEOUT[47] = 'x';
            TRAMEOUT[48] = 'x';
            TRAMEOUT[49] = 'x';
        }


        IntToChar(No_voie, TMP, 3);
        TRAMEOUT[50] = TMP[0];
        TRAMEOUT[51] = TMP[1];
        TRAMEOUT[52] = TMP[2];



        TRAMEOUT[53] = Tab_Horodatage[0]; //Jour
        TRAMEOUT[54] = Tab_Horodatage[1]; //Jour
        TRAMEOUT[55] = Tab_Horodatage[2]; // MOIS
        TRAMEOUT[56] = Tab_Horodatage[3]; //MOIS
        TRAMEOUT[57] = Tab_Horodatage[6]; //HEURE
        TRAMEOUT[58] = Tab_Horodatage[7]; //HEURE
        TRAMEOUT[59] = Tab_Horodatage[8]; //MIN
        TRAMEOUT[60] = Tab_Horodatage[9]; //MIN


        TRAMEOUT[61] = '?';
        IntToChar(1, TMP, 2);
        TRAMEOUT[62] = TMP[0];
        TRAMEOUT[63] = TMP[1];

        IntToChar(2, TMP, 2);
        TRAMEOUT[64] = TMP[0]; //POS
        TRAMEOUT[65] = TMP[1]; //POS


        TRAMEOUT[66] = '?';
        TRAMEOUT[67] = '?';
        TRAMEOUT[68] = '?';
        TRAMEOUT[69] = '?';








        TRAMEOUT[70] = Tab_Distance[0]; //info recuperer par 	RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEOUT,1,i);
        TRAMEOUT[71] = Tab_Distance[1];
        TRAMEOUT[72] = Tab_Distance[2];
        TRAMEOUT[73] = Tab_Distance[3];
        TRAMEOUT[74] = Tab_Distance[4];



        if (recherche_TPR) {
            //DISTANCE
            TRAMEOUT[70] = '?';
            TRAMEOUT[71] = '?';
            TRAMEOUT[72] = '?';
            TRAMEOUT[73] = '?';
            TRAMEOUT[74] = '?';
        }

        //ETAT
        IntToChar(Tab_Etat_1[1], TMP, 2);
        TRAMEOUT[75] = TMP[0];
        TRAMEOUT[76] = TMP[1];

        //FREQUENCE

        // TEST L'INSTABILITE QD SCRUTATION test la valeur
        //if ((TRAMEOUT[81]=='0')&&(TRAMEOUT[82]=='8')&&(TRAMEOUT[83]=='0')&&(TRAMEOUT[84]<'5')&&(TRAMEOUT[84]>'1'))
        //{
        tmp_I = 0;

        tmp_I2 = TRAMEIN[77];
        tmp_I = tmp_I + 1000 * (tmp_I2 - 48);
        tmp_I2 = TRAMEIN[78];
        tmp_I = tmp_I + 100 * (tmp_I2 - 48);
        tmp_I2 = TRAMEIN[79];
        tmp_I = tmp_I + 10 * (tmp_I2 - 48);
        tmp_I2 = TRAMEIN[80];
        tmp_I = tmp_I + (tmp_I2 - 48);
        if (Tab_Pression_1[1] != 0) {
            //		if ((tmp_I<(Tab_Pression_1[1]-20))||(tmp_I>(Tab_Pression_1[1]+20)))// il est instable car variation sur 20mbar
            {
                //	TRAMEOUT[75]='5'; //ON MET L'ETAT S/R avec la valeur donc instable  
                //	TRAMEOUT[76]='0'; //SI ETAT PRECEDENT = INT
            } // voir si qd plus le cas letat redevient normal											
        }
        //}// FIN INSTABILITE

        IntToChar(Tab_Pression_1[1], TMP, 4);
        TRAMEOUT[77] = TMP[0];
        TRAMEOUT[78] = TMP[1];
        TRAMEOUT[79] = TMP[2];
        TRAMEOUT[80] = TMP[3];

        IntToChar(Tab_Seuil_1[1], TMP, 4);
        TRAMEOUT[81] = TMP[0];
        TRAMEOUT[82] = TMP[1];
        TRAMEOUT[83] = TMP[2];
        TRAMEOUT[84] = TMP[3];




        //	//SEUIL AUTO SI 5 SCRUTATION DEPUIS LORIGINE
        //	if ((TRAMEOUT[81]=='0')&&(TRAMEOUT[82]=='8')&&(TRAMEOUT[83]=='0')&&(TRAMEOUT[84]<'5'))
        //	{
        //	TRAMEOUT[84]=TRAMEOUT[84]+1;
        //	}
        //	if ((TRAMEOUT[81]=='0')&&(TRAMEOUT[82]=='8')&&(TRAMEOUT[83]=='0')&&(TRAMEOUT[84]=='5')) //SI 0805 mbar seuil alors on regarde la derniere valeur et on retire 60mbar
        //	{
        //	Tab_Seuil_1[1]=Tab_Pression_1[1]-60;
        //	IntToChar(Tab_Seuil_1[1], TMP,4);
        //	TRAMEOUT[81]=TMP[0];
        //	TRAMEOUT[82]=TMP[1];
        //	TRAMEOUT[83]=TMP[2];
        //	TRAMEOUT[84]=TMP[3];				
        //	}// FIN SEUIL AUTO 


        if (recherche_TPR) {
            Tab_Seuil_1[1] = Tab_Pression_1[1] - 60;

            if (Tab_Seuil_1[1] <= 900)
                Tab_Seuil_1[1] = 900;
            IntToChar(Tab_Seuil_1[1], TMP, 4);
            TRAMEOUT[81] = TMP[0];
            TRAMEOUT[82] = TMP[1];
            TRAMEOUT[83] = TMP[2];
            TRAMEOUT[84] = TMP[3];
            //	TRAMEOUT[81]='0';
            //	TRAMEOUT[82]='8';  //VALEUR PAR DEFAUT 800mbar
            //	TRAMEOUT[83]='0';
            //	TRAMEOUT[84]='0';
        }

        //CRC
        TRAMEOUT[85] = '2';
        TRAMEOUT[86] = '2';
        TRAMEOUT[87] = '2';
        TRAMEOUT[88] = '2';
        TRAMEOUT[89] = '2';
        TRAMEOUT[90] = '*';

        if (recherche_TPR == 1) {

            //CODE V
            if ((NOSAVEOEILSCRUT == 0) || (NOSAVEOEILSCRUT == 1)) //CONTRE ORDRE SCRUTATION VOIE UNIQUE
            {
                ecrire_tab_presence_TPR(1);
            } else {
                DELAY_MS(100);
            }


        }

        Sauvegarde_en_cours = 1;

        INTCON3bits.INT1IE = 0; // devalide l'interruption pour RS485 1 pr reactiver


        if (Memo_autorisee_1) {
            if ((NOSAVEOEILSCRUT == 0) || (NOSAVEOEILSCRUT == 1)) //CONTRE ORDRE PAR OEIL OU SCRUTATION VOIE UNIQUE
            {
                ECRIRE_EEPROMBYTE(choix_memoire(TRAMEOUT), calcul_addmemoire(TRAMEOUT), TRAMEOUT, 1); //ECRITURE EN EEPROM seulement codage1
            } else {
                DELAY_MS(100);
            }

        }

        Sauvegarde_en_cours = 0;

        INTCON3bits.INT1IE = 1; // devalide l'interruption pour RS485 1 pr reactiver


    }

}

void Init_Tab_tpa(void) //reset tableau de mesures des tpa
{
    int i, j;
    for (i = 0; i <= 16; i++) {

        //			Tab_Code[i]=0;			//17 caract�res	  1 d�bitmetre et 16 TP
        Tab_Pression_1[i] = 0; //17 caract�res	
        Tab_I_Repos_1[i] = 0; //17 caract�res	  
        Tab_I_Modul_1[i] = 0; //17 caract�res		
        Tab_I_Conso_1[i] = 0; //34 caract�res	
        Tab_Etat_1[i] = 0; //17 caract�res	

        Tab_Pression_2[i] = 0; //17 caract�res	
        Tab_I_Repos_2[i] = 0; //17 caract�res	  
        Tab_I_Modul_2[i] = 0; //17 caract�res		
        Tab_I_Conso_2[i] = 0; //34 caract�res	
        Tab_Etat_2[i] = 0; //17 caract�res	

    }

    for (j = 0; j <= 9; j++) //10 caract�res
    {
        Tab_Horodatage[j] = 32; // 32 = espace
    }

}

void remplir_tab_presence(char v, char pouimp, char co) //remplie
{


    Tab_existance2[0] = '#';
    Tab_existance1[0] = '#';

    if (pouimp == 1) {
        Tab_existance1[1] = v;
        Tab_existance1[2] = 'a'; //POUR L'INSTANT VOIE ADRESSABLE

        if (v == 5) {




            v = 5;
        }

        if (Tab_Pression_1[co] == 0) // pas de capteur pression=0
            Tab_existance1[co + 3] = 'F';
        if (Tab_Pression_1[co] != 0) // Il y a une reponse adressable (modulation)
            Tab_existance1[co + 3] = 'A';
        if (Tab_Pression_1[0] == 9999) // pas de debimetre debit =9999
            Tab_existance1[3] = 'F';

        if (Tab_existance1[3] == 'A') //TEST
        {


            Tab_existance2[0] = '#';
        }




        if (co == 16) {
            Tab_existance1[20] = numdecarte + 48;
            Tab_existance1[21] = Tab_existance1[2];
            Tab_existance1[22] = 'x';
            Tab_existance1[23] = '*';



        }


    }

    if (pouimp == 2) {

        Tab_existance2[1] = v;
        Tab_existance2[2] = 'a'; //POUR L'INSTANT VOIE ADRESSABLE



        if (Tab_Pression_2[co] == 0) // pas de capteur pression=0
            Tab_existance2[co + 3] = 'F';
        if (Tab_Pression_2[co] != 0) // Il y a une rfeponse adressable (modulation)
            Tab_existance2[co + 3] = 'A';
        if (Tab_Pression_2[0] == 9999) // pas de debimetre debit =9999
            Tab_existance2[3] = 'F';
        if (co == 16) {
            Tab_existance2[20] = numdecarte + 48;
            Tab_existance2[21] = Tab_existance2[2];
            Tab_existance2[22] = typedecarte; //le type de carte
            Tab_existance2[23] = '*';
        }
    }
}

void ecrire_tab_presence_TPA(char p) //ECRIRE LETAT (au sens resistif presence....) DE LA VOIE ET DES CAPTEURS ICI C JUSTE POUR LA MESURE ADRESSABLE
{
    unsigned int l;
    char i;



    if (p == 1) {
        l = Tab_existance1[1];
        l = 128 * l + 10000;



        if (Memo_autorisee_1)
            ECRIRE_EEPROMBYTE(3, l, Tab_existance1, 4);
    }


    if (p == 2) {
        l = Tab_existance2[1];
        l = 128 * l + 10000;


        if (Memo_autorisee_2)
            ECRIRE_EEPROMBYTE(3, l, Tab_existance2, 4);
    }


    p = 0;
    p = 0;
    p = 0;
    delay_qms(10);
    LIRE_EEPROM(3, l, TRAMEIN, 4); //LIRE DANS EEPROM
    p = 0;
    p = 0;
    p = 0;
}

void ecrire_tab_presence_TPR(char p) //ECRIRE LETAT (au sens resistif presence....) DE LA VOIE ET DES CAPTEURS ICI C JUSTE POUR LA MESURE ADRESSABLE
{
    unsigned int l, e;
    char i, j, arthung;








    arthung = 'a';

    if (p == 1) {
        i = Tab_Voie_1[0];
        l = Tab_Voie_1[0];
        l = 128 * l + 10000;
        FUNCTIONUSE = 'R';
        SAVADDE = 3; //BUG SCRUTATION RESISTIFS SUR ADRESSABLE ACCES MEMOIRE
        SAVADDHL = l;
        //SAVtab=&TMP;
        SAVr = 4;
        LIRE_EEPROM(3, l, TMP, 4); //LIRE DANS EEPROM
        e = Tab_Pression_1[1];
        FUNCTIONUSE = 'N';
    }


    if (p == 2) {
        l = Tab_Voie_2[0];
        i = Tab_Voie_2[0];
        l = 128 * l + 10000;
        FUNCTIONUSE = 'R';
        SAVADDE = 3; //BUG SCRUTATION RESISTIFS SUR ADRESSABLE ACCES MEMOIRE
        SAVADDHL = l;
        //SAVtab=&TMP;
        SAVr = 4;
        LIRE_EEPROM(3, l, TMP, 4); //LIRE DANS EEPROM
        e = Tab_Pression_2[1];
        FUNCTIONUSE = 'N';
    }




    if (TMP[2] == 'a') // Si la ligne est considere comme adressable probable premier test 
    {


        for (j = 3; j < 20; j++) {





            if (TMP[j] == 'F') //si le capteur de codage n est absent 
            {
                arthung = 'r';
                if ((typedecarte == 'E'))//si la carte est adressable la voie devient anormale !!!!!!!!!!!!!! 
                    arthung = 'i';


                //if ((e>1300)&&(e<1400)) //ATTENTION VERIFIER QUE CA N'EST PAS UN ADRESSABLE HS 1300mbar et 1400mbar zone adressable adecef HS 	A VERIFIER TRAVAUX CHRISTIAN			
                //{
                //arthung='i';		
                //}


                if (e >= 1999) //pas de capteur	si la valeur resistif est sup a 1555			
                    arthung = 'v';
            }


            if (TMP[j] == 'A') // SI LE CAPTEUR n a repondu come adressable
            {
                arthung = 'a';
                if (typedecarte == 'M') // SI CARTE RESISTIVE ALORS VOIE ANORMALE !!!!!!!!!!
                    arthung = 'i';


                goto fintest;
            }


            if (TMP[j] == 'D') // SI UN CAPTEUR ADRESSABLE EST HS ALORS LA VOIE EST HS  A REMPLIR PRECEDEMMENT NON ENCORE GERE !!! 
            {
                arthung = 'd';
                goto fintest;
            }


        }
fintest:




        TMP[2] = arthung; // type de ligne

        TMP[20] = numdecarte + 48;
        TMP[21] = arthung;
        TMP[22] = 'x';
        TMP[23] = '*';

        TMP[1] = i;
        TMP[0] = '#';



    }




    if (Memo_autorisee_1 || Memo_autorisee_2) {
        Sauvegarde_en_cours = 1;

        INTCON3bits.INT1IE = 0; // devalide l'interruption pour RS485 1 pr reactiver



        FUNCTIONUSE = 'R';
        SAVADDE = 3; //BUG SCRUTATION RESISTIFS SUR ADRESSABLE ACCES MEMOIRE
        SAVADDHL = l;
        //SAVtab=&TMP;
        SAVr = 4;
        ECRIRE_EEPROMBYTE(3, l, TMP, 4);
        FUNCTIONUSE = 'N';

        re_ecrire_en_memoire(arthung); // on modifie la table dexistence en fonction de la correction

        Sauvegarde_en_cours = 0;

        INTCON3bits.INT1IE = 1; // devalide l'interruption pour RS485 1 pr reactiver
    }









    p = 0;
    p = 0;
    p = 0;
    delay_qms(10);
    FUNCTIONUSE = 'r';
    SAVADDE = 3; //BUG SCRUTATION RESISTIFS SUR ADRESSABLE ACCES MEMOIRE
    SAVADDHL = l;
    //SAVtab=&TMP;
    SAVr = 4;
    LIRE_EEPROM(3, l, TRAMEIN, 4); //LIRE DANS EEPROM
    FUNCTIONUSE = 'N';
    if (TRAMEIN[0] != '#') //BUG voir si diff de #
    {
        p = 0;
        p = 0;
        p = 0;
        p = 0;
        FUNCTIONUSE = 'E';
    }

    if (TRAMEIN[0] == 0) //BUG
    {
        p = 0;
        p = 0;
        p = 0;
        p = 0;
        FUNCTIONUSE = 'E';


    }



    p = 0;
    p = 0;
}

void re_ecrire_en_memoire(char u) // apres les tests adressable et resitif pour changer la trame du capteur #00101R.............................

//SI UN ADRESSABLE DETECTE ALORS RIEN A CHANGER
//SI UN RESISTIF DETECTE ALORS RIEN A CHANGER
//SI i alors A DEFINIR POUR CHOISSIR CE QU'il y aura pour la voie donc il faudra modifier la memoire du capteur
{


    if (u == 'i')
        u = 'i'; // INCERTITUDE VOIR CE QUE L'ON DECIDE

    if (u == 'r') // Si la voie est 'r' on prepare la trame memoire a avoir 'R'
        u = 'R';

    if (u == 'v') {

        Memo_autorisee_2 = 0; //ON ENREGISTRE PAS 
        Memo_autorisee_1 = 0;

    }
    if (u == 'd') {
        Memo_autorisee_1 = 0; //ON ENREGISTRE PAS NON GERE UN CAPTEUR DEFECTUEUX NON ENCORE GERE
        Memo_autorisee_2 = 0;
    }

    if (u == 'a') {
        Memo_autorisee_1 = 0; //ON ENREGISTRE PAS 
        Memo_autorisee_2 = 0;
    }



    TRAMEOUT[6] = u; // ADRESSABLE OU RESITIF 



}

void EFFACER_TABLE_EXISTENCE(void) {

    unsigned int y, k;
    char z;

    k = 1;
    for (z = 1; z < 21; z++) {
        delay_qms(100);
        y = 128 * k + 10000;
        k++;

        TMP[0] = '#';
        TMP[1] = z;
        TMP[2] = 'v';

        TMP[3] = 'F';
        TMP[4] = 'F';
        TMP[5] = 'F';
        TMP[6] = 'F';
        TMP[7] = 'F';
        TMP[8] = 'F';
        TMP[9] = 'F';
        TMP[10] = 'F';
        TMP[11] = 'F';
        TMP[12] = 'F';
        TMP[13] = 'F';
        TMP[14] = 'F';
        TMP[15] = 'F';
        TMP[16] = 'F';
        TMP[17] = 'F';
        TMP[18] = 'F';
        TMP[19] = 'F';


        if (numdecarte == 0)
            TMP[20] = 0;
        if (numdecarte == 1)
            TMP[20] = 1;
        if (numdecarte == 2)
            TMP[20] = 2;
        if (numdecarte == 0)
            TMP[20] = 3;
        if (numdecarte == 0)
            TMP[20] = 4;


        TMP[21] = 'v';

        if (typedecarte == 'M')
            TMP[22] = 'M';
        if (typedecarte == 'E')
            TMP[22] = 'E';


        TMP[23] = '*';
        delay_qms(100);
        ECRIRE_EEPROMBYTE(3, y, TMP, 4);


    }


}

void MODIFICATION_TABLE_DEXISTENCE(unsigned char *v, char eff, char etat) //eff pour effacer (eff=2 alors recopie) alors et etat ligne pour fusible HS ou CC ou surtension 
{

    unsigned int y, z;
    char fa, nbrcapteurs, jjj;



    nbrcapteurs = 0;
    v[0] = '#';
    y = 100 * (v[1] - 48) + 10 * (v[2] - 48)+(v[3] - 48); //VOIE

    y = 128 * y + 10000;

    LIRE_EEPROM(3, y, TMP, 4); //LIRE DANS EEPROM









    z = 10 * (v[4] - 48)+(v[5] - 48); //CODAGE

    if (eff == 2) //RECOPIE
    {
        goto recop;
    }



    if (eff == 0) //CREATION
    {
        TMP[z + 3] = v[6]; //RECOPIE SI R OU A #00101R
        fa = TMP[z + 3];
    }

    if (eff == 1) //EFFACEMENT
    {

        // VOIR voie a 'v' qd il y a que des 'F' 
        for (jjj = 2; jjj < 20; jjj++) //VOIR COMBIEN EXISTENT
        {
            if (TMP[jjj] == 'A') {

                delay_qms(1);
                nbrcapteurs = nbrcapteurs + 1; // ON INCREMENTE
            }
        }
        TMP[z + 3] = 'F'; //CAPTEUR EFFACEE DE LA TABLE DEXISTENCE


        if (nbrcapteurs < 2) {
            TMP[2] = 'v';
            TMP[21] = 'v';
        }


    }

    if (fa == 'A') {
        TMP[2] = 'a';
        TMP[21] = 'a';
    }
    if (fa == 'R') //L'ADRESSABLE NE PEUT QUE EXISTER EN CODAGE1 !!!!!!!!!!!!!!!!!!!!!!
    {
        TMP[2] = 'r';
        TMP[21] = 'r';
    }




recop:



    if (etat == 1) {
        TMP[2] = 'h';
    }

    if (etat == 2) {
        TMP[2] = 'h';
    }

    if ((etat == 4)&&(TMP[2] != 'c')) {
        TMP[2] = TMP[21];
    }

    if ((etat == 5)&&(TMP[2] != 'c')) {
        TMP[2] = TMP[21];

    }

    if (etat == 6) {
        TMP[2] = 'c';

    }


    if (etat == 7) {
        TMP[2] = 'c';
    }


    if (etat == 8) // SCRUTATION POUR INHIBER DEFAUT CC
    {
        TMP[2] = TMP[21];
    }



    ECRIRE_EEPROMBYTE(3, y, TMP, 4);

    delay_qms(10);
    LIRE_EEPROM(3, y, TMP, 4); //LIRE DANS EEPROM pour verifier

    delay_qms(10);

}

char VOIR_TABLE_EXISTENCE_POUR_OEIL(unsigned char *v) {



    unsigned int y, z;
    char fa;

    v[0] = '#';

    y = 100 * (v[1] - 48) + 10 * (v[2] - 48)+(v[3] - 48); //VOIE

    y = 128 * y + 10000;

    LIRE_EEPROM(3, y, TMP, 4); //LIRE DANS EEPROM
    fa = TMP[2];




    return (fa); //Retourne si voie resistive ou adressable



}

void VIDER_MEMOIRE(void) {





    TRAMEOUT[0] = '#';
    TRAMEOUT[1] = 'D';
    TRAMEOUT[2] = 'Q';
    TRAMEOUT[3] = 'Q';
    TRAMEOUT[4] = 'Q';
    TRAMEOUT[5] = 'Q';
    TRAMEOUT[6] = 'Q';
    TRAMEOUT[7] = 'Q';
    TRAMEOUT[8] = 'Q';
    TRAMEOUT[9] = 'Q';
    TRAMEOUT[10] = 'Q';
    TRAMEOUT[11] = 'Q';
    TRAMEOUT[12] = 'Q';
    TRAMEOUT[13] = 'Q';
    TRAMEOUT[14] = 'Q';
    TRAMEOUT[15] = 'Q';
    TRAMEOUT[16] = 'Q';
    TRAMEOUT[17] = 'Q';
    TRAMEOUT[18] = 'Q';
    TRAMEOUT[19] = 'Q';
    TRAMEOUT[20] = 'Q';
    TRAMEOUT[21] = 'Q';
    TRAMEOUT[22] = 'Q';
    TRAMEOUT[23] = 'Q';
    TRAMEOUT[24] = 'Q';
    TRAMEOUT[25] = 'Q';
    TRAMEOUT[26] = 'Q';
    TRAMEOUT[27] = 'Q';
    TRAMEOUT[28] = 'Q';
    TRAMEOUT[29] = 'Q';
    TRAMEOUT[30] = 'Q';
    TRAMEOUT[31] = 'Q';
    TRAMEOUT[32] = 'Q';
    TRAMEOUT[33] = 'Q';
    TRAMEOUT[34] = 'Q';
    TRAMEOUT[35] = 'Q';
    TRAMEOUT[36] = 'Q';
    TRAMEOUT[37] = 'Q';
    TRAMEOUT[38] = 'Q';
    TRAMEOUT[39] = 'Q';
    TRAMEOUT[40] = 'Q';
    TRAMEOUT[41] = 'Q';
    TRAMEOUT[42] = 'Q';
    TRAMEOUT[43] = 'Q';
    TRAMEOUT[44] = 'Q';
    TRAMEOUT[45] = 'Q';
    TRAMEOUT[46] = 'Q';
    TRAMEOUT[47] = 'Q';
    TRAMEOUT[48] = 'Q';
    TRAMEOUT[49] = 'Q';
    TRAMEOUT[50] = 'Q';
    TRAMEOUT[51] = 'Q';
    TRAMEOUT[52] = 'Q';
    TRAMEOUT[53] = 'Q';
    TRAMEOUT[54] = 'Q';
    TRAMEOUT[55] = 'Q';
    TRAMEOUT[56] = 'Q';
    TRAMEOUT[57] = 'Q';
    TRAMEOUT[58] = 'Q';
    TRAMEOUT[59] = 'Q';
    TRAMEOUT[60] = 'Q';
    TRAMEOUT[61] = '?';
    TRAMEOUT[62] = 'Q';
    TRAMEOUT[63] = 'Q';
    TRAMEOUT[64] = 'Q';
    TRAMEOUT[65] = 'Q';
    TRAMEOUT[66] = '?';
    TRAMEOUT[67] = '?';
    TRAMEOUT[68] = '?';
    TRAMEOUT[69] = '?';
    TRAMEOUT[70] = 'Q';
    TRAMEOUT[71] = 'Q';
    TRAMEOUT[72] = 'Q';
    TRAMEOUT[73] = 'Q';
    TRAMEOUT[74] = 'Q';
    TRAMEOUT[75] = 'Q';
    TRAMEOUT[76] = 'Q';
    TRAMEOUT[77] = 'Q';
    TRAMEOUT[78] = 'Q';
    TRAMEOUT[79] = 'Q';
    TRAMEOUT[80] = 'Q';
    TRAMEOUT[81] = 'Q';
    TRAMEOUT[82] = 'Q';
    TRAMEOUT[83] = 'Q';
    TRAMEOUT[84] = 'F';
    TRAMEOUT[85] = 'Q';
    TRAMEOUT[86] = 'Q';
    TRAMEOUT[87] = 'Q';
    TRAMEOUT[88] = 'Q';
    TRAMEOUT[89] = 'Q';
    TRAMEOUT[90] = '*';
    TMPINT = 0;

    for (TMPINT = 0; TMPINT < 40000; TMPINT += 128) //UNE CARTE de 1 a 20 
    {

        DELAY_MS(10);


        ECRIRE_EEPROMBYTE(0, TMPINT, TRAMEOUT, 1); //ECRITURE EN EEPROM

        TRAMEOUT[0] = '#';





        DELAY_MS(10);



        LIRE_EEPROM(0, TMPINT, TRAMEIN, 1); //LIRE DANS EEPROM



















        DELAY_MS(10);











    }





}

void RECUPERE_TRAME_ET_CAL_ETAT(unsigned char *hk, char t, unsigned char y) //RECUPERE LES ELEMENTS FIXE EN MOIRE CONSTIT....SEUIL......ETC
{


    unsigned int jp[4];
    char j;


    LIRE_EEPROM(choix_memoire(hk), calcul_addmemoire(hk), TRAMEIN, 1); //LIRE DANS EEPROM



    ALARMECABLE = 0; //AUCUNE ALARME

    //jp0;
    TMPINT = 0;

    //RECUPERATION DU SEUIL
    if (t == 1) {

        if (y == 2) //TEST
        {
            jp[0] = (TRAMEIN[81] - 48); //TEST 

            jp[0] = (TRAMEIN[81] - 48); //TEST 

            jp[0] = (TRAMEIN[81] - 48); //TEST 

            jp[0] = (TRAMEIN[81] - 48); //TEST 




        }

        //calcul
        jp[0] = (TRAMEIN[81] - 48);
        jp[1] = (TRAMEIN[82] - 48);
        jp[2] = (TRAMEIN[83] - 48);
        jp[3] = (TRAMEIN[84] - 48);

        TMPINT = jp[3] + 10 * jp[2] + 100 * jp[1] + 1000 * jp[0];


        Tab_Seuil_1[y] = TMPINT;



        if (Tab_Etat_1[y] == 0x80)//INSTABLE	 
        {
            Tab_Etat_1[y] = 50; //devient SR 
        }

        //calcul etat
        if (Tab_Etat_1[y] != 0x80)//INSTABLE
        {
            if (Tab_Pression_1[y] >= Tab_Seuil_1[y]) //superieure
                Tab_Etat_1[y] = 0x00; //OK 0 en decimal

            if (Tab_Pression_1[y] < Tab_Seuil_1[y]) //inferieur
            {
                Tab_Etat_1[y] = 0x1E; //ALARME 30 en decimal
                //nbreALARMES=nbreALARMES+1;
                //AL_CABLE=0; // SORTIE ALARME CABLE
            }


            if (Tab_Pression_1[y] == 0) //si Resistif ou Adressable donne 0 l'etat devient S/R
                Tab_Etat_1[y] = 50; //H/G 40 en decimal	




            if ((Tab_Pression_1[y] < 900)&&(Tab_Pression_1[y] > 5)) //entre 5mbar et 800
                Tab_Etat_1[y] = 40; //H/G 40 en decimal	





            if (Tab_Pression_1[y] > 1700) //1555 avant
            {
                Tab_Etat_1[y] = 40; //H/G 40 en decimal	
                if (typedecarte == 'M') //SI 1999 resistif qui a exist�
                {
                    Tab_Etat_1[y] = 50; //H/G 50 en decimal
                }

            }

            if ((y == 0)&&(Tab_Pression_1[0] >= 9000)) //debit absent 
                Tab_Etat_1[y] = 0;



        }


        if ((!recherche_TPA)&&(!recherche_TPR)) {
            if (TRAMEIN[76] == '1') //SI EN INTERVENTION
                Tab_Etat_1[y] = Tab_Etat_1[y] + 1;
        } else {
            if (TRAMEIN[76] == '1') //SI EN INTERVENTION ON ENLEVE INTERVENTION PENDANT LA SCRUTATION
                Tab_Etat_1[y] = Tab_Etat_1[y] - 1;
        }



    }

    if (t == 2) {

        if (y == 10) {

        }



        jp[0] = (TRAMEIN[81] - 48);
        jp[1] = (TRAMEIN[82] - 48);
        jp[2] = (TRAMEIN[83] - 48);
        jp[3] = (TRAMEIN[84] - 48);

        TMPINT = jp[3] + 10 * jp[2] + 100 * jp[1] + 1000 * jp[0];



        Tab_Seuil_2[y] = TMPINT;

        if (Tab_Etat_2[y] == 0x80)//INSTABLE	 
        {
            Tab_Etat_2[y] == 50; //devient SR 
        }


        if (Tab_Etat_2[y] != 0x80)//SI PAS INSTABLE
        {



            if (Tab_Pression_2[y] >= Tab_Seuil_2[y]) //superieure
                Tab_Etat_2[y] = 0x00; //OK

            if (Tab_Pression_2[y] < Tab_Seuil_2[y]) //inferieur
            {
                Tab_Etat_2[y] = 0x1E; //ALARME 30 en decimal 
                //nbreALARMES=nbreALARMES+1;
                //AL_CABLE=0;
            }

            if (Tab_Pression_2[y] == 0) //si Resistif ou Adressable donne 0 l'etat devient S/R
                Tab_Etat_2[y] = 50; //S/R 50 en decimal	



            if ((Tab_Pression_2[y] < 900)&&(Tab_Pression_2[y] > 5)) //entre 5mbar et 800	{
                Tab_Etat_2[y] = 40; //H/G 40 en decimal	




            if (Tab_Pression_2[y] > 1700) {
                Tab_Etat_2[y] = 40; //H/G 40 en decimal	
                if (typedecarte == 'M') //SI 1999 resistif qui a exist�
                {
                    Tab_Etat_2[y] = 50; //H/G 50 en decimal
                }
            }




            if ((y == 0)&&(Tab_Pression_2[0] >= 9000)) //debit absent 
                Tab_Etat_2[y] = 0;


        }



        if ((!recherche_TPA)&&(!recherche_TPR)) {
            if (TRAMEIN[76] == '1') //SI EN INTERVENTION
                Tab_Etat_2[y] = Tab_Etat_2[y] + 1;
        } else {
            if (TRAMEIN[76] == '1') //SI EN INTERVENTION ON ENLEVE INTERVENTION PENDANT LA SCRUTATION
                Tab_Etat_2[y] = Tab_Etat_2[y] - 1;
        }




    }



    for (j = 0; j < 13; j++) {
        Tab_Constitution[j] = TRAMEIN[19 + j];
    }



    for (j = 0; j < 18; j++) {
        Tab_Commentaire_cable[j] = TRAMEIN[32 + j];
    }



    for (j = 0; j < 5; j++) {
        Tab_Distance[j] = TRAMEIN[70 + j];
    }






}