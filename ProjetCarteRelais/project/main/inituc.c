/*
    Initialisation du microcontroleur
*/

/************************ INCLUDE ***************************/
#include <p18f8723.h>          /* C18 compiler processor defs */
#include <inituc.h> 		   /* initialisation defs */
/**************** DECLARATION DES SOUS PROGRAMMES *************/

/***************************************************************
Nom   : void init_uc (void)
Role  : Initialisation du microcontroleur
****************************************************************/
void init_uc (void)
{


// 1=INPUT    0=OUTPUT
				// ENTREES ANALOGIQUES  AUTRES 
	TRISAbits.TRISA0 = 1;	// AN0   V_ligne_AM1 	mesure tension Amont du capteur voies impaires
	TRISAbits.TRISA1 = 1;	// AN1	 V_ligne_AM2 	mesure tension Amont du capteur voies paires
	TRISAbits.TRISA2 = 1;	// AN2	 V_ligne_AV1_A 	mesure tension Aval du capteur voies impaires  (apr�s fusible)
	TRISAbits.TRISA3 = 1;	// AN3	 V_ligne_AV2_A 	mesure tension Aval du capteur voies paires  (apr�s fusible)
	TRISAbits.TRISA4 = 0;	// RX_TX	
	TRISAbits.TRISA5 = 1;	// AN4	 I_conso_1		mesure tension(courant de consommation) du capteur voies impaires
	TRISAbits.TRISA6 = 0;	//	OSC
	TRISAbits.TRISA7 = 0;	//	OSC

	TRISBbits.TRISB0 = 1;	//	ALIM_OFF (INT)
	TRISBbits.TRISB1 = 1;	//	VAL_RS485_EXT
	TRISBbits.TRISB2 = 1;	//	FREQ_TPA
	TRISBbits.TRISB3 = 1;	//	HORLOGE
	TRISBbits.TRISB4 = 1;	//	VAL_I2C_EXT
	TRISBbits.TRISB5 = 0;	//	PGM	}
	TRISBbits.TRISB6 = 0;	//	PGC	]> ICSP
	TRISBbits.TRISB7 = 0;	//	PGD	}

	TRISCbits.TRISC0 = 0;	//	DECH_L1			==> modif piste sur CI
	TRISCbits.TRISC1 = 0;	//	OK_I2C_EXT
	TRISCbits.TRISC2 = 0;	//	I2C_INTERNE
	TRISCbits.TRISC3 = 1;	//	SCL_I2C
	TRISCbits.TRISC4 = 1;	//	SDA_I2C

	TRISDbits.TRISD0 = 0;	//	D0	
	TRISDbits.TRISD1 = 0;	//	D1
	TRISDbits.TRISD2 = 0;	//	D2
	TRISDbits.TRISD3 = 0;	//	D3
	TRISDbits.TRISD4 = 0;	//	D4
	TRISDbits.TRISD5 = 0;	//	D5
	TRISDbits.TRISD6 = 0;	//	D6
	TRISDbits.TRISD7 = 0;	//	D7
														// 1=INPUT    0=OUTPUT
	TRISEbits.TRISE0 = 0;	//	RS_DI	
	TRISEbits.TRISE1 = 0;	//	R/W	
	TRISEbits.TRISE2 = 0;	//	E			
	TRISEbits.TRISE3 = 0;	//	Data_Rel
	TRISEbits.TRISE4 = 0;	//	Clk_Rel
	TRISEbits.TRISE5 = 0;	//	Rck_Rel
	TRISEbits.TRISE6 = 0;	//	Rst_Rel
	TRISEbits.TRISE7 = 0;	//	Test_Fus


	TRISFbits.TRISF0 = 1;	// AN5   I_conso_2		mesure tension(courant de consommation) du capteur voies paires 
	TRISFbits.TRISF1 = 1;	// AN6	 I_modul_1		mesure tension(courant de modulation) du capteur voies impaires
	TRISFbits.TRISF2 = 1;	// AN7	 I_modul_2		mesure tension(courant de modulation) du capteur voies paires
	TRISFbits.TRISF3 = 1;	// AN8   Sel A0 Lecture en entr�e Analogique!!!
	TRISFbits.TRISF4 = 1;	// AN9	 Sel A1 Lecture en entr�e Analogique!!!
	TRISFbits.TRISF5 = 1;	// AN10  Sel A2 Lecture en entr�e Analogique!!!
	TRISFbits.TRISF6 = 0;	// DECH_L1 AN11	 <===  entr�e analogique LIBRE   ==> modif piste sur CI
	TRISFbits.TRISF7 = 0;	// DECH_L2

	TRISGbits.TRISG0 = 0;	//	Sel_Freq	(0 voie impaire)(1 voie paire)
	TRISGbits.TRISG1 = 0;	//	CPT_ON_1
	TRISGbits.TRISG2 = 0;	//	UN_CPT_1			
	TRISGbits.TRISG3 = 0;	//	CPT_ON_2
	TRISGbits.TRISG4 = 0;	//	UN_CPT_2
//	(TRISGbits.TRISG5 = 1;	//	/MCLR)

	TRISHbits.TRISH0 = 0;	//  AL_CABLE_ON + LED ALARME
	TRISHbits.TRISH1 = 0;	//  /RST
	TRISHbits.TRISH2 = 0;	//	LED_TX
	TRISHbits.TRISH3 = 0;	//	LED_RX
	TRISHbits.TRISH4 = 1;	// AN12  V_48V_1 mesure tension 48V voies impaires  
	TRISHbits.TRISH5 = 1;	// AN13	 V_48V_2 mesure tension 48V voies paires 
	TRISHbits.TRISH6 = 1;	// AN14  V_ligne_AV1_B 	mesure gain+ tension Aval du capteur voies impaires  (apr�s fusible)
	TRISHbits.TRISH7 = 1;	// AN15  V_ligne_AV2_B 	mesure gain+ tension Aval du capteur voies paires  (apr�s fusible)

	TRISJbits.TRISJ0 = 0;	//	CS1
	TRISJbits.TRISJ1 = 0;	//	CS2
	TRISJbits.TRISJ2 = 0;	//	Pt2
	TRISJbits.TRISJ3 = 1;	// 	Jumper (entr�e pour TPR)
	TRISJbits.TRISJ4 = 1;	//	MODE TEST	
	TRISJbits.TRISJ5 = 0;	//	Val_R_I2C
	TRISJbits.TRISJ6 = 0;	//	V_SUP_1
	TRISJbits.TRISJ7 = 0;	//	V_SUP_2




	// A/D PORT Configuration
 	ADCON1 = 0b00000111;		// ANALOG: AN0 -> AN7 - (Vref+ = AVDD(5v) et Vref =AVSS(0v) ) 

	ADCON0 = 0 ;// CAN AU REPOS

//	ADCON1bits.VCFG1 = 0 ;	// VDD
//	ADCON1bits.VCFG0 = 0 ;	// VSS

//	ADCON1bits.PCFG3 = 1 ;	// SELECTION DES ENTREES ANALOGIQUES
//	ADCON1bits.PCFG2 = 0 ;	// AN0 a AN6 INCLUS     
//	ADCON1bits.PCFG1 = 0 ;	
//	ADCON1bits.PCFG0 = 0 ;	

	ADCON2bits.ADFM = 1 ;	// FORMAT DU RESULTAT JUSTIFI� A DROITE
	ADCON2bits.ACQT2 = 1 ;	// ACQUISITION = 12 TAD
	ADCON2bits.ACQT1 = 0 ;	
	ADCON2bits.ACQT0 = 1 ;	

	ADCON2bits.ADCS2 = 0 ;	// HORLOGE TAD 1�S POUR F = 32MHz
	ADCON2bits.ADCS1 = 1 ;	// % 32
	ADCON2bits.ADCS0 = 0 ;	


}













/**********************************************************************
 * INIT DU PORT I2C
 *
 * @return void
 **********************************************************************/

void Init_I2C(void)
{
//	TRISCbits.TRISC3 = 1;
//	TRISCbits.TRISC4 = 1;


//	INTCON2bits.RBPU=1;  //PUPPULL
//	OpenI2C(8, 0xC0);



  //here is the I2C setup from the Seeval32 code.
	DDRCbits.RC3 = 1; //Configure SCL as Input
	DDRCbits.RC4 = 1; //Configure SDA as Input
	SSP1STAT = 0x00;   //Disable SMBus & Slew Rate Control 80
	SSP1CON1 = 0x28;  //Enable MSSP Master  28
	SSP1CON2 = 0x00;   //Clear MSSP Conrol Bits
	SSP1ADD  = 0x3B;    // 0x3B for 100kHz  (24mhz/1OOkHz)-1 = 59 ou 3B en hex
}







//////////////////////////////////////////////////////////////////
// fonction permettant d'initialiser le port s�rie				//
//////////////////////////////////////////////////////////////////


void Init_RS485(void)
{

unsigned int SPEED;

TRISCbits.TRISC5 = 0;
TRISCbits.TRISC7 = 1;
TRISCbits.TRISC6 = 0;
 TRISCbits.TRISC5 = 0; //RC5 en sortie	

TRISAbits.TRISA4 = 0; //RA4 RX_TX en sortie OU DERE



  RCSTA1bits.SPEN  = 1;       // Enable serial port
  TXSTA1bits.SYNC  = 0;       // Async mode	
  BAUDCON1bits.BRG16 = 1 ;
  
  TXSTA1bits.BRGH = 1;      //haute vitesse    
  SPEED = 624;      	 		 // set 9600 bauds
  SPBRG1 = SPEED ;     		// Write baudrate to SPBRG1
  SPBRGH1 = SPEED >> 8;		// For 16-bit baud rate generation




  
  IPR1bits.RCIP  = 1;       // Set high priority
  PIR1bits.RCIF = 0;        // met le drapeau d'IT � 0 (plus d'IT)
  PIE1bits.RCIE = 0;        // ne g�n�re pas d'IT pour la liaison s�rie
        
  TXSTA1bits.TXEN = 0;        // transmission inhib�e
  RCSTA1bits.RX9 = 0;         // r�ception sur 8 bits
  TXSTA1bits.TX9 = 0;           // transmission sur 8 bits
  RCSTA1bits.CREN  = 0;       // interdire reception
  RCONbits.IPEN  = 1;       // Enable priority levels 

}

