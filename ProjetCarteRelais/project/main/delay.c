

#include <p18f8723.h>             /* C18 compiler processor defs */
              /* Delays defs */
#include <delay.h>
#include <delays.h>


//	Delay10TCYx(0x3C);  //delai de 10�sec


//***************************************************************************
//Nom   : void DELAY_SEC(int sec)
//Role  : Delay de 1 seconde / Quartz 24Mhz (HS)
//utilise timer0
//***************************************************************************

void DELAY_SEC(int sec)
//Prescaler 1:128; TMR0 Preload = 18661; Actual Interrupt Time : 1sec
{
int i;
T0CON = 0x86; /* enable TMR0, select instruction clock, prescaler set to 1.128 */
for (i = 0; i < sec; i++)
	{
		InitTimer0();
	}
return;
}

//***************************************************************************
//Nom   : void DELAY_MS(int ms)
//Role  : Delay de 1 milliseconde / Quartz 24Mhz (HS)
//utilise timer0
//*****************************************************************************/
void DELAY_MS(int ms)

//Timer0
//Prescaler 1:1; TMR0 Preload = 59536; Actual Interrupt Time : 1 ms
{
int bb; 

//PORTHbits.RH3=0;
if (ms>=1000) //PROBLEME BUG EN SCRUTATION AUTO
ms=2;
T0CON = 0x88; /* enable TMR0, select instruction clock, prescaler set to 1.1 */
for (bb = 0; bb < ms; bb++) 
	{

  TMR0H	 = 0xE8; //Preload = 59536
  TMR0L	 = 0x90;
  INTCONbits.TMR0IE	 = 0;
  INTCONbits.TMR0IF = 0;
  while(!(INTCONbits.TMR0IF)); /* wait until TMR0 rolls over */

	}
//PORTHbits.RH3=1;
return;

}



//***************************************************************************
//Nom   : void DELAY_125mS(int ms)
//Role  : Delay de 125ms  / Quartz 24Mhz (HS)
//utilise timer0
//*****************************************************************************/
void DELAY_125MS(int ms)
//Timer0
//Prescaler 1:16; TMR0 Preload = 18661; Actual Interrupt Time : 125 ms
{
int i;
if (ms>=1000) //PROBLEME BUG EN SCRUTATION AUTO
ms=2;

T0CON = 0x83; /* enable TMR0, select instruction clock, prescaler set to 1.16 */
for (i = 0; i < ms; i++) 
	{
 InitTimer0();
  	}
return;
}

//***************************************************************************
//Nom   : void DELAY_250mS()
//Role  : Delay de 250ms unique / Quartz 24Mhz (HS)
//utilise timer0
//*****************************************************************************/
void DELAY_SW_250MS()
//Timer0
//Prescaler 1:32; TMR0 Preload = 18661; Actual Interrupt Time : 125 ms
{

	T0CON = 0x84; /* enable TMR0, select instruction clock, prescaler set to 1.16 */
  	TMR0H	 = 0x48; //Preload = 18661
  	TMR0L	 = 0xE5;
  	INTCONbits.TMR0IE = 0;
  	INTCONbits.TMR0IF = 0;
return;
}



  
void InitTimer0()
{
  TMR0H	 = 0x48; //Preload = 18661
  TMR0L	 = 0xE5;
  INTCONbits.TMR0IE	 = 0;
  INTCONbits.TMR0IF  = 0;
  while(!(INTCONbits.TMR0IF)); /* wait until TMR0 rolls over */
}

 

 
void delay_qms(char tempo)
{
char j,k,l;
 
for (j=0;j<tempo;j++)
  {
  for (k=0;k<10;k++)
    {
    for (l=0;l<40;l++)
      {
      //__no_operation();
      //__no_operation();
      //__no_operation();
      //__no_operation();
      //__no_operation();
      }
    }
  }  
}  








 
