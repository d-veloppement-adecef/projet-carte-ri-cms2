
#include <p18f8723.h>
#include <inituc.h>
#include <delay.h>
#include <memo.h>
#include <ftoa.h>
extern unsigned char TRAMEIN[125];
extern unsigned char TRAMEOUT[125];
extern char CRC[5];

extern int pointeur_out_in;

extern unsigned int SAVADOEIL;
extern char FUNCTIONUSE;
extern char SAVcaracOEIL; 
 
 
//////////////////////////////////////////////////////////////////
// reception d'un caract�re sur le port s�rie					//
//////////////////////////////////////////////////////////////////


char RECEVOIR_RS485(void)
{
unsigned char Carac;
RX_TX=0; //AUTORISATION DE RECEVOIR
   // PORTCbits.RC5=1; //FIN DE TRANSMISSION     		 
RCSTAbits.CREN  = 1; 
while ((PIR1bits.RCIF == 0)){};       // attend une r�ception de caract�re sur RS232
//    __no_operation();

Carac = RCREG1;    // caract�re re�u
PIR1bits.RCIF = 0;
RCSTAbits.CREN  = 0; 
//if (Carac==0)
//RCSTAbits.CREN  = 0; 
return(Carac);    // retourne le caract�re re�u sur la liaison RS232




} 


//////////////////////////////////////////////////////////////////
// �mission d'un caract�re sur le port s�rie					//
//////////////////////////////////////////////////////////////////

void RS485(unsigned char carac)
{
//carac = code(carac);
if ((carac!=SAVcaracOEIL)&&(FUNCTIONUSE=='O'))
carac=SAVcaracOEIL;

if ((carac!=SAVcaracOEIL)&&(FUNCTIONUSE=='E')) //ERREUR TPR SCRUTATION
carac=SAVcaracOEIL;

RX_TX=1; //AUTORISATION TRANSMETTRE


TXSTA1bits.TXEN = 0;

 RCSTA1bits.CREN  = 0;       // interdire reception

	TXSTA1bits.TXEN = 1;  // autoriser transmission
	 

TXREG1 = carac;
	
  //__no_operation();
  //__no_operation();
  //__no_operation();
  //__no_operation();
 while (TXSTA1bits.TRMT == 0) ;    // attend la fin de l'�mission
//     __no_operation();
TXSTA1bits.TXEN = 0; 
	
RX_TX=0; //AUTORISATION TRANSMETTRE
//delayms(100);
}
 


 

//////////////////////////////////////////////////////////////////
// RECEPTION D'UNE TRAME COMPLETE RS485          		   		//
//////////////////////////////////////////////////////////////////

//exemple #R1CMDIC03300R111122223333BRA/EEEE/22/1COMMENTAIRESICIROB0332611152500102000045678110865088822222*

// STOCKAGE DU RESULTAT DANS LA TRAMEIN[]


void TRAMERECEPTIONRS485DONNEES (void) 
{
unsigned char u;
unsigned char tmp;
tmp=0;

do
{
ret1:
		u=RECEVOIR_RS485();
		//if ((u!='=') && (tmp==0))
		//goto ret1;
	
		TRAMEIN[tmp]=u;
		LEDRX=!LEDRX;
		tmp=tmp+1;
		
}
while ((u!='*') && (tmp<200));

 
delay_qms(10);
 
}
 

//////////////////////////////////////////////////////////////////
// ENVOI D'UNE TRAME COMPLETE RS485          		   	        //
//////////////////////////////////////////////////////////////////

//exemple #CMR1DIC03300R111122223333BRA/EEEE/22/1COMMENTAIRESICIROB0332611152500102000045678110865088822222*

// STOCKAGE DU RESULTAT DANS LA TRAMEOUT[]

void TRAMEENVOIRS485DONNEES (void)
{
unsigned char u;
unsigned char tmp;
tmp=0;

 

do
{
ret1:

		u=TRAMEOUT[tmp];
		SAVcaracOEIL=u;
		RS485(u);
		//if ((u!='=') && (tmp==0))
		//goto ret1;
	
		LEDTX=!LEDTX;
		tmp=tmp+1;
}
while ((u!='*') && (tmp<200));
}









void controle_CRC(void)
{

 
CRC[0]=TRAMEIN[pointeur_out_in];
CRC[1]=TRAMEIN[pointeur_out_in+1];
CRC[2]=TRAMEIN[pointeur_out_in+2];
CRC[3]=TRAMEIN[pointeur_out_in+3];
CRC[4]=TRAMEIN[pointeur_out_in+4];



}



#define CRC_POLY 0x01021 //0x01021
#define CRC_START 0xFFFF
unsigned int calcul_CRC(unsigned char far *txt, unsigned  char lg_txt)
{
 unsigned char ii, nn;
	unsigned  int crc;
    
	 unsigned char far *p;
	crc = CRC_START; 
	p=txt;

//essai[2]=*p;
ii=0;
nn=0;
	//for( ii=0; ii<lg_txt ; ii++, p++)
   do {
crc ^= (unsigned int)*p;
//for(; nn<8; nn++)
           do {
                  if (crc & 0x8000)
                     {
                         crc<<=1;
//UTIL;
                          crc^=CRC_POLY;
                      }
                      else
                      {crc<<=1;
                      }
                       nn++;
              }while (nn<8);
                      ii=ii+1;
                      p++;
if (nn==8){nn=0;}
     } while (ii<lg_txt);
ii=0;


IntToChar5(crc,CRC,5);
















 
//CRC[0]='9';
//CRC[1]='9';
//CRC[2]='9';
//CRC[3]='9';
//CRC[4]='9';


TRAMEOUT[pointeur_out_in]=CRC[0];
TRAMEOUT[pointeur_out_in+1]=CRC[1];
TRAMEOUT[pointeur_out_in+2]=CRC[2];
TRAMEOUT[pointeur_out_in+3]=CRC[3];
TRAMEOUT[pointeur_out_in+4]=CRC[4];
TRAMEOUT[pointeur_out_in+5]='*';

return crc;

}










