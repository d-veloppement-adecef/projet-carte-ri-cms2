
#include <p18f8723.h>
#include <i2c.h>
#include <rtcERIC.h>
#include <delay.h>


extern unsigned char 	Tab_Horodatage[12];		//10 octets	 //10 caract�res	  
extern char HORLOGET[12];

unsigned char envoyerHORLOGE_i2c(unsigned char NumeroH,unsigned char ADDH, unsigned char dataH0)
{
	
 StartI2C();
   WriteI2C(NumeroH);     // adresse de l'horloge temps r�el
   WriteI2C(ADDH);
   WriteI2C(dataH0);
   StopI2C();
   delay_qms(11);
 				
}








signed char litHORLOGE_i2c(unsigned char NumeroH,unsigned char AddH) 
{     


  char Data;
  
  StartI2C();
  WriteI2C(NumeroH&0xFE);        // adresse de la DS1307
  WriteI2C(AddH);     // suivant le choix utilisateur
  RestartI2C();
  WriteI2C(NumeroH);        // on veut lire
  Data = ReadI2C();     // lit la valeur 
  StopI2C();
  return(Data);



} 









unsigned char ECRIRE_HORLOGE(unsigned char zone,unsigned char Time)
{
unsigned char TMP;

if (zone==0)
zone=zone&0x7F; //laisse l'horloge CHbits a 0 secondes tres important si ce bit n'est pas a 0 lhorloge ne marche pas
if (zone==1)
zone=zone&0x7F; //laisse le bit7 a zero minutes
if (zone==2)
zone=zone&0x3F;//met a zero les bits 6 et 7 pour le mode 24h  
if (zone==3)
zone=zone&0x07; // laisse ts les bits sauf 0 1 2 a 0 
if (zone==4)
zone=zone&0x3F; // met a 0 les bits 7 et 6 date 
if (zone==5)
zone=zone&0x1F; // met a 0 les bits 7 et 6 et 5 mois
if (zone==6)
zone=zone;
if (zone==7)
zone=zone; //registre control ex: 00010000 =) oscillation de 1hz out





TMP=envoyerHORLOGE_i2c(0xD0,zone,Time);
return(TMP);
}





unsigned char LIRE_HORLOGE(unsigned char zone)
{
unsigned char TMP;
TMP=litHORLOGE_i2c(0xD1,zone); 


if (zone==0)
TMP=TMP&0x7F; // supprimle le bit 7 seconde
if (zone==1)
TMP=TMP&0x7F; //laisse le bit7 a zero minutes
if (zone==2)
TMP=TMP&0x3F;//met a zero les bits 6 et 7 pour le mode 24h  
if (zone==3)
TMP=TMP&0x07; // laisse ts les bits sauf 0 1 2 a 0 
if (zone==4)
TMP=TMP&0x3F; // met a 0 les bits 7 et 6 date 
if (zone==5)
TMP=TMP&0x1F; // met a 0 les bits 7 et 6 et 5 mois
if (zone==6)
TMP=TMP;










return (TMP);
}





void creer_trame_horloge (void)
{

char HEURE,MINUTE,SECONDE,MOIS,JOUR,ANNEE,DATE;


ANNEE=LIRE_HORLOGE(6);
delay_qms(10);
MOIS=LIRE_HORLOGE(5);
delay_qms(10);
DATE=LIRE_HORLOGE(4);
delay_qms(10);
JOUR=LIRE_HORLOGE(3);
delay_qms(10);
HEURE=LIRE_HORLOGE(2);
delay_qms(10);
MINUTE=LIRE_HORLOGE(1);
delay_qms(10);
SECONDE=LIRE_HORLOGE(0);

Tab_Horodatage[1] = (DATE & 0x0F) + 48;
   Tab_Horodatage[0] = ((DATE & 0x30) >> 4) + 48;      // conversion BCD => ASCII
 
   Tab_Horodatage[3] = (MOIS & 0x0F) + 48;  
  Tab_Horodatage[2] = ((MOIS & 0x10) >> 4) + 48;

 Tab_Horodatage[5] = (SECONDE & 0x0F) + 48;
     Tab_Horodatage[4] = ((SECONDE & 0x70) >> 4) + 48;
 
 Tab_Horodatage[7] = (HEURE & 0x0F) + 48; 
 Tab_Horodatage[6] = ((HEURE & 0x30) >> 4) + 48;      // conversion BCD => ASCII
  

Tab_Horodatage[9] = (MINUTE& 0x0F) + 48;
   Tab_Horodatage[8] = ((MINUTE & 0x70) >> 4) + 48;

   

 Tab_Horodatage[11] = (ANNEE & 0x0F) + 48;
   Tab_Horodatage[10] = ((ANNEE & 0xF0) >> 4) + 48;
  


}


void entrer_trame_horloge (void)
{
char HEURE,MINUTE,SECONDE,MOIS,JOUR,ANNEE,DATE;
//ASCII->BCD
HEURE=(HORLOGET[0]-48)<<4; 
HEURE=HEURE+ (HORLOGET[1]-48) ;
MINUTE=(HORLOGET[2]-48)<<4; 
MINUTE=MINUTE+ (HORLOGET[3]-48) ;
SECONDE=(HORLOGET[4]-48)<<4; 
SECONDE=SECONDE+ (HORLOGET[5]-48) ;
DATE=(HORLOGET[6]-48)<<4;
DATE=DATE+ (HORLOGET[7]-48) ;
MOIS=(HORLOGET[8]-48)<<4; 
MOIS=MOIS+ (HORLOGET[9]-48) ;
ANNEE=(HORLOGET[10]-48)<<4;
ANNEE=ANNEE + (HORLOGET[11]-48);





ECRIRE_HORLOGE(2,HEURE);
ECRIRE_HORLOGE(1,MINUTE);
ECRIRE_HORLOGE(0,SECONDE);
ECRIRE_HORLOGE(4,DATE);
ECRIRE_HORLOGE(5,MOIS);
ECRIRE_HORLOGE(6,ANNEE);

}