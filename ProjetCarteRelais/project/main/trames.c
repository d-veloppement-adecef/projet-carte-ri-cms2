
#include <delay.h>						/* Defs DELAY functions */
#include <i2c.h>
#include <eeprom_i2cERIC.h>
#include <rs485.h>
#include <inituc.h>
#include <ftoa.h> 
#include <trames.h>
#include <memo.h>
#include <HC595.h> //CIRCUITS COMMANDE RELAIS 
#include <mesures.h>
#include <eeprom.h> 
#include <rtcERIC.h>

extern char FUNCTIONUSE;
extern char SAVMEMOEIL;
extern unsigned int SAVADOEIL;
extern unsigned char voie_valide_1; // si voie_valide_1	= 0 => pas de mesure,  si voie_valide_1	= 1 => mesure et sauvegarde
extern unsigned char voie_valide_2; // si voie_valide_2	= 0 => pas de mesure,  si voie_valide_2	= 1 => mesure et sauvegarde
extern unsigned char Tab_Voie_1[1]; //1 octet VOIE IMPAIRES 1,3,5....99 	  ASCII OU HEX??  => HEXA OU DECIMAL ET CONVERSION EN ASCII JUSTE POUR ECRITURE EN EEPROM
extern unsigned char Tab_Voie_2[1]; //1 octet VOIE PAIRES   2,4,6...100 	 					IDEM POUR TOUTES LES VALEURS

extern char FUSIBLEHS, ALARMECABLE;
extern char typedecarte, numdecarte;

extern unsigned char alarme_caM;
extern unsigned char TRAMEOUT[125];
extern unsigned char TRAMEIN[125];
extern unsigned char TMP[125];



extern unsigned char Tab_Horodatage[12];
extern unsigned char dernier_TPA_1, dernier_TPA_2;
extern unsigned char recherche_TPR;
extern unsigned char recherche_TPA;

char VOIECOD[5];
char TYPE[1];
char CMOD[4];
char CCON[4];
char CREP[4];
char CONSTITUTION[13];
char COMMENTAIRE[20];
char CABLE[3];
char JJ[2];
char MM[2];
char HH[2];
char mm[2];
char i[1];
char COD[2];
char POS[2];
char iiii[4];
char DISTANCE[5];
char ETAT[2];
char VALEUR[4];
char SEUIL[4];


////// TRAMES IDENTIQUES
char START[1];
char STOP[1];
char CRC[5];

char ID1[2];
char ID2[2];
unsigned char FONCTION[3];



////// TRAMES ERREUR
char TYPE_ERREUR[2];
unsigned char INFORMATION_ERREUR[20];



int pointeur_out_in;
char TYPECOMMANDE; //POUR FONCTION DIC - /
unsigned char SAVTAB[25];
char temoinfinvoieDIC;


unsigned char DEMANDE_INFOS_CAPTEURS[11];
char HORLOGET[12];



extern unsigned char SAVADDE; //BUG SCRUTATION RESISTIFS SUR ADRESSABLE ACCES MEMOIRE
extern unsigned int SAVADDHL;
extern unsigned int SAVtab;
extern unsigned char SAVr;
extern char FUNCTIONUSE;
extern char NOSAVEOEILSCRUT;
extern char SCRUT;
////////////////////////////////////////////////////////////////////////////////////////
// UNIQUEMENT POUR CARTE RI RECOIT demande de la CM exemple CM->RI demande les informations des capteurs
// 

//suivant la fonction obtenu tab , et apres une interruption sur INT0
// 
////////////////////////////////////////////////////////////////////////////////////////

void RECUPERATION_DES_TRAMES(unsigned char *tab) {
    int t;
    int y;
    int funct;
    char g, k[20];
    float h;
    unsigned int iii;
    unsigned int ttt;
    unsigned int rrr;




    ID1[0] = tab[1];
    ID1[1] = tab[2];

    ID2[0] = tab[3];
    ID2[1] = tab[4];

    FONCTION[0] = tab[5];
    FONCTION[1] = tab[6];
    FONCTION[2] = tab[7];


    funct = (FONCTION[0] << 16)*2; // max 0-255 valeur decimal du code ascii de chaque lettre ex ERR E=69 R=82 E=82 
    //methode 69*2 + 82 + 82 = 302
    funct = funct + (FONCTION[1] << 16);
    funct = funct + (FONCTION[2] << 16);






    switch (funct) //analyse de la fonction


 {


        case 300: //FONCTION OEIL DETECTEE
            pointeur_out_in = 13; // LIEU DU CRC	
            controle_CRC();
            FONCTION[0] = 'O';
            FONCTION[1] = 'E';
            FONCTION[2] = 'I';

            TRAMEOUT[0] = START[0];
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            DELAY_MS(50);
            DELAY_MS(50);
            DELAY_MS(50);
            DELAY_MS(50);
            DELAY_MS(50);
            DELAY_MS(50);
            DELAY_MS(50);
            //LE FLAG CM RS485 DOIT RESTER A 1	
            for (t = 0; t < 18; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            //PAS DERREUR
            INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur pour carte Rx
            INFORMATION_ERREUR[1] = '0';
            ENVOIE_DES_TRAMES(0, 302, INFORMATION_ERREUR); //ENVOIE LES DONNEES DU CAPTEUR

            INTCON3bits.INT1IE = 0; // devalide l'interruption pour RS485 1 pr reactiver

            DEMANDE_INFOS_CAPTEURS[1] = TRAMEIN[8];
            DEMANDE_INFOS_CAPTEURS[2] = TRAMEIN[9];
            DEMANDE_INFOS_CAPTEURS[3] = TRAMEIN[10];
            DEMANDE_INFOS_CAPTEURS[4] = TRAMEIN[11];
            DEMANDE_INFOS_CAPTEURS[5] = TRAMEIN[12]; // QUEL CAPTEUR

            g = VOIR_TABLE_EXISTENCE_POUR_OEIL(DEMANDE_INFOS_CAPTEURS); //TMP variable globale contient lexistence de la voie

            for (t = 0; t < 21; t++) {
                k[t] = TMP[t];
            }

            t = TMP[1] / 2; //VOIE PAIRE OU IMPAIRE  ENTIER
            h = TMP[1];
            h = h / 2 - t; //FLOTTANT
            if (h > 0) // IMPAIRE
            {
                NOSAVEOEILSCRUT = 1; //CONTRE ORDRE DE SAV POUR VOIE PAIRE CAR DIF DE 2 
                voie_valide_2 = 0;
                voie_valide_1 = 1;
                Tab_Voie_1[0] = TMP[1];
                Tab_Voie_2[0] = TMP[1] + 1;
            }
            if (h == 0) // PAIRE
            {
                NOSAVEOEILSCRUT = 2; ////CONTRE ORDRE DE SAV POUR VOIE IMPAIRE CAR DIF DE 1
                voie_valide_1 = 0;
                voie_valide_2 = 1;
                Tab_Voie_2[0] = TMP[1];
                Tab_Voie_1[0] = TMP[1] - 1;


            }

            DEMANDE_INFOS_CAPTEURS[0] = DEMANDE_INFOS_CAPTEURS[1];
            DEMANDE_INFOS_CAPTEURS[1] = DEMANDE_INFOS_CAPTEURS[2];
            DEMANDE_INFOS_CAPTEURS[2] = DEMANDE_INFOS_CAPTEURS[3];
            DEMANDE_INFOS_CAPTEURS[3] = DEMANDE_INFOS_CAPTEURS[4];
            DEMANDE_INFOS_CAPTEURS[4] = DEMANDE_INFOS_CAPTEURS[5];



            //PAIRES
            if (DEMANDE_INFOS_CAPTEURS[2] == '2')
                Tab_Voie_1[0] = 0;
            if (DEMANDE_INFOS_CAPTEURS[2] == '4')
                Tab_Voie_1[0] = 0;
            if (DEMANDE_INFOS_CAPTEURS[2] == '6')
                Tab_Voie_1[0] = 0;
            if (DEMANDE_INFOS_CAPTEURS[2] == '8')
                Tab_Voie_1[0] = 0;
            if (DEMANDE_INFOS_CAPTEURS[2] == '0')
                Tab_Voie_1[0] = 0;

            //IMPAIRES
            if (DEMANDE_INFOS_CAPTEURS[2] == '1')
                Tab_Voie_2[0] = 0;
            if (DEMANDE_INFOS_CAPTEURS[2] == '3')
                Tab_Voie_2[0] = 0;
            if (DEMANDE_INFOS_CAPTEURS[2] == '5')
                Tab_Voie_2[0] = 0;
            if (DEMANDE_INFOS_CAPTEURS[2] == '7')
                Tab_Voie_2[0] = 0;
            if (DEMANDE_INFOS_CAPTEURS[2] == '9')
                Tab_Voie_2[0] = 0;



            do{

                Prog_HC595(Tab_Voie_1[0], Tab_Voie_2[0], alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !		
                //	Prog_HC595(1,2,0,FUSIBLEHS); //COMMANDE RELAIS !


                //Voir si on active la memorisation

                if ((g == 'r') || (g == 'v') || (g == 'c') || (g == 'h')) {
                    recherche_TPR = 0;
                    DELAY_SEC(2);
                    Mesure_TPR(0);
                    DELAY_SEC(3);
                }

                if (g == 'a') {
                    for (t = 3; t <= 19; t++) {
                        if (k[t] == 'A') //si il y a un A 
                        {
                            dernier_TPA_2 = t - 2;
                            dernier_TPA_1 = t - 2;
                        }

                    }

                    recherche_TPA = 0;
                    Mesure_DEB_TPA();
                }


                //16022018

                TMP[0] = '#';
                TMP[1] = DEMANDE_INFOS_CAPTEURS[0];
                TMP[2] = DEMANDE_INFOS_CAPTEURS[1];
                TMP[3] = DEMANDE_INFOS_CAPTEURS[2];
                TMP[4] = DEMANDE_INFOS_CAPTEURS[3];
                TMP[5] = DEMANDE_INFOS_CAPTEURS[4];

                //	DELAY_SEC(10);

                TMP[6] = 0; //TOUJOURS MEMOIRE 1 soit 0 

                //CALCUL ZONE MEMOIRE UNIQUEMENT POUR OEIL PB fonction calcul_addmemoire ???????,,,,
                ttt = 0;
                iii = TMP[3];
                iii = iii - 48;
                ttt = ttt + 1 * iii;
                iii = TMP[2];
                iii = iii - 48;
                ttt = ttt + 10 * iii;
                iii = TMP[1];
                iii = iii - 48;
                ttt = ttt + 100 * iii;


                rrr = 2176 * ttt;


                ttt = 0;
                iii = TMP[5] - 48;
                ttt = 1 * iii;
                iii = TMP[4] - 48;
                ttt = ttt + 10 * iii;
                iii = ttt;

                ttt = rrr + 128 * iii;
                SAVMEMOEIL = 0; //SAV BUG
                SAVADOEIL = ttt;
                FUNCTIONUSE = 'O';

                delay_qms(10);

                LIRE_EEPROM(0, ttt, TRAMEIN, 1); //LIRE DANS EEPROM

                FUNCTIONUSE = 'N';

                FONCTION[0] = 'O';
                FONCTION[1] = 'E';
                FONCTION[2] = 'I';

                TRAMEOUT[0] = START[0];
                TRAMEOUT[1] = ID1[0];
                TRAMEOUT[2] = ID1[1];
                TRAMEOUT[3] = ID2[0];
                TRAMEOUT[4] = ID2[1];
                TRAMEOUT[5] = FONCTION[0];
                TRAMEOUT[6] = FONCTION[1];
                TRAMEOUT[7] = FONCTION[2];






                TRANSFORMER_TRAME_MEM_EN_COM();
                pointeur_out_in = 92; // SAUVEGARDE POINTEUR DU TABLEAU
                calcul_CRC(TRAMEOUT, pointeur_out_in - 1); // CALCUL LE CRC ET LE RAJOUTE AU TABLEAU TRAMEOUT[]
                TRAMEOUT[pointeur_out_in + 5] = STOP[0]; // RAJOUT LE BYTE DE STOP 

                if (TRAMEIN[0] == 0xFF) //BUG INCONNU
                {
                    TRAMEOUT[8] = '*';
                }
                FUNCTIONUSE = 'O';
                TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE 
                FUNCTIONUSE = 'N';



            } while (PORTBbits.RB1);
            NOSAVEOEILSCRUT = 0;
            INTCON3bits.INT1IE = 1; // devalide l'interruption pour RS485 1 pr reactiver

            break;












        case 304: //EMANDE INFOS DU TABLEAU EXISTENCE
            pointeur_out_in = 13; // LIEU DU CRC	
            controle_CRC();
            WriteEEPROM(0x50, 0xEE); //LE CYCLE REPART ON PLACE LEPPROMM COMME CELA JUSQUA VOIE 19 20
            CYCLE = 0;
            FONCTION[0] = 'T';
            FONCTION[1] = 'E';
            FONCTION[2] = 'C';
            TRAMEOUT[0] = START[0];
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];



            ENVOIE_DES_TRAMES(0, 304, TMP); //ENVOIE LES DONNEES DU CAPTEUR TMP car il n'y a pas doption

            break;



        case 305: //FONCTION REGLAGE DE L'HORLOGE DETECTEE

            FONCTION[0] = 'H';
            FONCTION[1] = 'O';
            FONCTION[2] = 'R';

            TRAMEOUT[0] = START[0];
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];

            HORLOGET[0] = TRAMEIN[8];
            HORLOGET[1] = TRAMEIN[9];
            HORLOGET[2] = TRAMEIN[10];
            HORLOGET[3] = TRAMEIN[11];
            HORLOGET[4] = TRAMEIN[12];
            HORLOGET[5] = TRAMEIN[13];
            HORLOGET[6] = TRAMEIN[14];
            HORLOGET[7] = TRAMEIN[15];
            HORLOGET[8] = TRAMEIN[16];
            HORLOGET[9] = TRAMEIN[17];
            HORLOGET[10] = TRAMEIN[18];
            HORLOGET[11] = TRAMEIN[19];


            entrer_trame_horloge(); //ECRITURE 
            DELAY_SEC(1);
            creer_trame_horloge(); //VERIFICATION

            TRAMEOUT[8] = Tab_Horodatage[6]; //h
            TRAMEOUT[9] = Tab_Horodatage[7]; //h
            TRAMEOUT[10] = Tab_Horodatage[8]; //mn
            TRAMEOUT[11] = Tab_Horodatage[9]; //mn
            TRAMEOUT[12] = Tab_Horodatage[4]; //s
            TRAMEOUT[13] = Tab_Horodatage[5]; //s
            TRAMEOUT[14] = Tab_Horodatage[0]; //J
            TRAMEOUT[15] = Tab_Horodatage[1]; //J
            TRAMEOUT[16] = Tab_Horodatage[2]; //M
            TRAMEOUT[17] = Tab_Horodatage[3]; //M
            TRAMEOUT[18] = Tab_Horodatage[10]; //A
            TRAMEOUT[19] = Tab_Horodatage[11]; //A






            pointeur_out_in = 20; // SAUVEGARDE POINTEUR DU TABLEAU	
            calcul_CRC(TRAMEOUT, pointeur_out_in - 1);

            TRAMEENVOIRS485DONNEES(); //ENVOYER LA TRAME 			






            break;


        case 301: //FORMATAGE MEMOIRE EXISTENCE 'FOR'			
	
            EFFACER_TABLE_EXISTENCE();
            delay_qms(10); //ON ATTEND

            for (t = 0; t < 18; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            //PAS DERREUR
            INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur pour carte Rx
            INFORMATION_ERREUR[1] = '0';
            ENVOIE_DES_TRAMES(0, 302, INFORMATION_ERREUR); //ENVOIE LES DONNEES DU CAPTEUR

            break;


        case 285: //FONCTION CREATION D'UN CAPTEUR DETECTEE

            pointeur_out_in = 29; // LIEU DU CRC	
            controle_CRC();


            recuperer_trame_memoire(0);
            ECRIRE_EEPROMBYTE(choix_memoire(TRAMEOUT), calcul_addmemoire(TRAMEOUT), TRAMEOUT, 1); //ECRITURE EN EEPROM





            FONCTION[0] = 'C';
            FONCTION[1] = 'R';
            FONCTION[2] = 'E';

            TRAMEOUT[0] = START[0];
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            DEMANDE_INFOS_CAPTEURS[0] = '#';
            DEMANDE_INFOS_CAPTEURS[1] = TRAMEIN[8];
            DEMANDE_INFOS_CAPTEURS[2] = TRAMEIN[9];
            DEMANDE_INFOS_CAPTEURS[3] = TRAMEIN[10];
            DEMANDE_INFOS_CAPTEURS[4] = TRAMEIN[11];
            DEMANDE_INFOS_CAPTEURS[5] = TRAMEIN[12]; // QUEL CAPTEUR
            DEMANDE_INFOS_CAPTEURS[6] = TRAMEIN[13]; // QUEL TYPE

            if (TRAMEIN[92] != 0) //SI il y a 02222 sur le CRC  alors ne pas sav lexistence du capteur SD creation pour SAV le commentaire cable
                MODIFICATION_TABLE_DEXISTENCE(DEMANDE_INFOS_CAPTEURS, 0, 0);



            //ICI ON LIT LE CODAGE 0 ICI LE COMMENTAIRE CABLE
            DEMANDE_INFOS_CAPTEURS[4] = '0';
            DEMANDE_INFOS_CAPTEURS[5] = '0';
            LIRE_EEPROM(choix_memoire(DEMANDE_INFOS_CAPTEURS), calcul_addmemoire(DEMANDE_INFOS_CAPTEURS), TRAMEIN, 1); //LIRE DANS EEPROM CODAGE 00 ET STOCKAGE DANS TRAMEIN
            for (t = 0; t < 18; t++) {
                TRAMEIN[t + 32] = COMMENTAIRE[t];
            }


            ECRIRE_EEPROMBYTE(choix_memoire(DEMANDE_INFOS_CAPTEURS), calcul_addmemoire(DEMANDE_INFOS_CAPTEURS), TRAMEIN, 1); //ECRITURE EN EEPROM DU COMMENTAIRE CABLE
            LIRE_EEPROM(choix_memoire(DEMANDE_INFOS_CAPTEURS), calcul_addmemoire(DEMANDE_INFOS_CAPTEURS), TRAMEOUT, 1); //LIRE DANS EEPROM CODAGE 00 ET STOCKAGE DANS TRAMEIN





            for (t = 0; t < 18; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            //PAS DERREUR
            INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur pour carte Rx
            INFORMATION_ERREUR[1] = '0';
            ENVOIE_DES_TRAMES(0, 302, INFORMATION_ERREUR); //ENVOIE LES DONNEES DU CAPTEUR







            break;




        case 278: //FONCTION EFFACEMENT D'UN CAPTEUR DETECTEE

            pointeur_out_in = 29; // LIEU DU CRC	
            controle_CRC();


            recuperer_trame_memoire(0);

            FONCTION[0] = 'E';
            FONCTION[1] = 'F';
            FONCTION[2] = 'F';

            TRAMEOUT[0] = START[0];
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            DEMANDE_INFOS_CAPTEURS[0] = '#';
            DEMANDE_INFOS_CAPTEURS[1] = TRAMEIN[8];
            DEMANDE_INFOS_CAPTEURS[2] = TRAMEIN[9];
            DEMANDE_INFOS_CAPTEURS[3] = TRAMEIN[10];
            DEMANDE_INFOS_CAPTEURS[4] = TRAMEIN[11];
            DEMANDE_INFOS_CAPTEURS[5] = TRAMEIN[12]; // QUEL CAPTEUR



            MODIFICATION_TABLE_DEXISTENCE(DEMANDE_INFOS_CAPTEURS, 1, 0);

            delay_qms(10); //ON ATTEND

            for (t = 0; t < 18; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }
            //PAS DERREUR
            INFORMATION_ERREUR[0] = '0'; //TT EST BON donc utilisation type d'erreur pour carte Rx
            INFORMATION_ERREUR[1] = '0';
            ENVOIE_DES_TRAMES(0, 302, INFORMATION_ERREUR); //ENVOIE LES DONNEES DU CAPTEUR







            break;














        case 302: //TRAME ERREUR DECTECTEE

            funct = 0;
            TYPE_ERREUR[0] = tab[8];
            TYPE_ERREUR[1] = tab[9];


            funct = (TYPE_ERREUR[1]) - 48;
            funct = funct + 10 * (TYPE_ERREUR[0] - 48);
            //funct = funct + (TYPE_ERREUR[1]);		
            t = 0;
            for (t = 0; t < 18; t++) {
                INFORMATION_ERREUR[t] = tab[t + 10];
            }

            pointeur_out_in = 27;
            controle_CRC();


            switch (funct) //ACTION A FAIRE POUR CHAQUE ERREUR ......

 {
                case 0: // OK CA C EST BIEN PASSEE

                    delay_qms(100);
                    break;


                default:


                    break;


            }

            break;


        case 276: //FONCTION DEMANDE INFO SUR UN CAPTEUR DETECTEE (OBSELETE)

            pointeur_out_in = 13; // LIEU DU CRC	
            controle_CRC();

            FONCTION[0] = 'D';
            FONCTION[1] = 'I';
            FONCTION[2] = 'C';
            TRAMEOUT[0] = START[0];
            TRAMEOUT[1] = ID2[0];
            TRAMEOUT[2] = ID2[1];
            TRAMEOUT[3] = ID1[0];
            TRAMEOUT[4] = ID1[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];








            DEMANDE_INFOS_CAPTEURS[0] = TRAMEIN[8];
            DEMANDE_INFOS_CAPTEURS[1] = TRAMEIN[9];
            DEMANDE_INFOS_CAPTEURS[2] = TRAMEIN[10];
            DEMANDE_INFOS_CAPTEURS[3] = TRAMEIN[11];
            DEMANDE_INFOS_CAPTEURS[4] = TRAMEIN[12]; // QUEL CAPTEUR

            DEMANDE_INFOS_CAPTEURS[5] = TRAMEIN[13];
            DEMANDE_INFOS_CAPTEURS[6] = TRAMEIN[14];
            DEMANDE_INFOS_CAPTEURS[7] = TRAMEIN[15];
            DEMANDE_INFOS_CAPTEURS[8] = TRAMEIN[16];
            DEMANDE_INFOS_CAPTEURS[9] = TRAMEIN[17];
            DEMANDE_INFOS_CAPTEURS[10] = TRAMEIN[18]; // JUSQU'A QUEL CAPTEUR


            ENVOIE_DES_TRAMES(0, 276, DEMANDE_INFOS_CAPTEURS); //ENVOIE LES DONNEES DU CAPTEUR












            break;

        case 315: //SCRUTATION 'S 'C 'R
            INTCON3bits.INT1IF = 0; //INTERDIT COMPLETEMENT RB1 ??????????????
            INTCON3bits.INT1IE = 0; // devalide l'interruption pour RS485 1 pr reactiver voir a deplacer sur routine trame.C QD ACTION
            pointeur_out_in = 11; // LIEU DU CRC	
            controle_CRC();
            delay_qms(100);
            SCRUT = 0xFF;
            for (t = 0; t < 18; t++) {
                INFORMATION_ERREUR[t] = 'x';
            }



            //OK_RS485_EXT=1; //debut de scrutation				
            ENVOIE_DES_TRAMES(0, 302, INFORMATION_ERREUR); //ENVOIE LES DONNEES DU CAPTEUR

            if ((TRAMEIN[8] == 'F') && (TRAMEIN[9] == 'F')) {
                //void interrogation_voie (char voices)

                VIDER_MEMOIRE(); //A EFFACER		

                interrogation_voies();

            } else {

                interrogation_voie(TRAMEIN);
            }// INTERROG ENVOI '$' ET RESET 







            //OK_RS485_EXT=0; //fin de scrutation
            NOSAVEOEILSCRUT = 0; //CONTRE ORDRE DE SAV AU REPOS
            INTCON3bits.INT1IE = 1; // devalide l'interruption pour RS485 1 pr reactiver voir a deplacer sur routine trame.C QD ACTION



        default:

            break;



    }

}

void ENVOIE_DES_TRAMES(char R, int fonctionRS, unsigned char *tab)
 {

    int t;
    int funct;
    int voieDEM, codageDEM;
    int voieDEMFINAL, codageDEMFINAL;
    unsigned int l;
    char i, finaldetect, UNSEUL;



    ID1[0] = 'R';
    if (numdecarte == 0)
        ID1[1] = '1'; // CARTE 1
    if (numdecarte == 1)
        ID1[1] = '2'; // CARTE 2
    if (numdecarte == 2)
        ID1[1] = '3'; // CARTE 3
    if (numdecarte == 3)
        ID1[1] = '4'; // CARTE 4
    if (numdecarte == 4)
        ID1[1] = '5'; // CARTE 5











    switch (R) //analyse de la fonction
 {

        case 0: // CM
            ID2[0] = 'C';
            ID2[1] = 'M';
            break;


        case 1: // R1
            ID2[0] = 'R';
            ID2[1] = '1';
            break;
        case 2: // R2
            ID2[0] = 'R';
            ID2[1] = '2';
            break;
        case 3: // R3
            ID2[0] = 'R';
            ID2[1] = '3';
            break;
        case 4: // R4
            ID2[0] = 'R';
            ID2[1] = '4';
            break;
        case 5: // R5
            ID2[0] = 'R';
            ID2[1] = '5';
            break;

        case 6: // R5
            ID2[0] = 'A';
            ID2[1] = 'U';
            break;

        case 7: // IO
            ID2[0] = 'I';
            ID2[1] = 'O';
            break;

        default:


            break;

    }

    switch (fonctionRS) {
        
        case 302: // ERREUR


            FONCTION[0] = 'E';
            FONCTION[1] = 'R';
            FONCTION[2] = 'R';
            TRAMEOUT[0] = START[0];
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];

            for (t = 8; t < 26; t++) {
                TRAMEOUT[t] = tab[t - 8];
            }

            pointeur_out_in = 26;
            calcul_CRC(TRAMEOUT, pointeur_out_in - 1); //A COMPLETER
            TRAMEOUT[pointeur_out_in + 5] = STOP[0];

            TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE 

            funct = 0;

            break;




        case 300: // OEIL ENV



            TMP[0] = '#';
            TMP[1] = DEMANDE_INFOS_CAPTEURS[0];
            TMP[2] = DEMANDE_INFOS_CAPTEURS[1];
            TMP[3] = DEMANDE_INFOS_CAPTEURS[2];
            TMP[4] = DEMANDE_INFOS_CAPTEURS[3];
            TMP[5] = DEMANDE_INFOS_CAPTEURS[4];


            LIRE_EEPROM(choix_memoire(TMP), calcul_addmemoire(TMP), TRAMEIN, 1); //LIRE DANS EEPROM



            FONCTION[0] = 'O';
            FONCTION[1] = 'E';
            FONCTION[2] = 'I';

            TRAMEOUT[0] = START[0];
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];



            TRANSFORMER_TRAME_MEM_EN_COM();
            pointeur_out_in = 92; // SAUVEGARDE POINTEUR DU TABLEAU
            calcul_CRC(TRAMEOUT, pointeur_out_in - 1); // CALCUL LE CRC ET LE RAJOUTE AU TABLEAU TRAMEOUT[]
            TRAMEOUT[pointeur_out_in + 5] = STOP[0]; // RAJOUT LE BYTE DE STOP 


            TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE 


            break;

        case 304: //ENVOI TABLEAU EXISTENCE



            FONCTION[0] = 'T';
            FONCTION[1] = 'E';
            FONCTION[2] = 'C';

            TRAMEOUT[0] = START[0];
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];


            DELAY_MS(200);

            // DEBUT DE SUPER TRAME
            RS485('%'); //% et $

            for (i = 1; i <= 20; i++)// les 20 voies
            {

                // VOIE IMPAIRE
                delay_qms(10);
                l = 128 * i + 10000;
                FUNCTIONUSE = 'T';
                SAVADDE = 3; //BUG SCRUTATION RESISTIFS SUR ADRESSABLE ACCES MEMOIRE
                SAVADDHL = l;
                SAVr = 4;
                LIRE_EEPROM(3, l, TRAMEIN, 4); //LIRE DANS EEPROM lexistance de toutes le voies
                FUNCTIONUSE = 'N';
                //#R1CM02vFFFFFFFFFFFFFFFFF0vx99999*  le x sera E pour carte adressable ou M carte resistive


                if (numdecarte == 0) //REMISE A JOUR DU NUMERO DE CARTE QD CHANGEMENT SLOT
                    TRAMEIN[20] = '0';
                if (numdecarte == 1)
                    TRAMEIN[20] = '1';
                if (numdecarte == 2)
                    TRAMEIN[20] = '2';
                if (numdecarte == 3)
                    TRAMEIN[20] = '3';
                if (numdecarte == 4)
                    TRAMEIN[20] = '4';

                if (typedecarte == 'M')
                    TRAMEIN[22] = 'M';
                if (typedecarte == 'E')
                    TRAMEIN[22] = 'E';
                if (TRAMEIN[1] >= 10) {
                    TRAMEOUT[5] = '1';
                    TRAMEOUT[6] = TRAMEIN[1] - 10 + 48;
                }
                if (TRAMEIN[1] < 10) {
                    TRAMEOUT[5] = '0';
                    TRAMEOUT[6] = TRAMEIN[1] + 48;
                }
                if (TRAMEIN[1] > 19) {
                    TRAMEOUT[5] = '2';
                    TRAMEOUT[6] = '0';
                }


                TRAMEOUT[7] = TRAMEIN[2];
                TRAMEOUT[8] = TRAMEIN[3];
                TRAMEOUT[9] = TRAMEIN[4];
                TRAMEOUT[10] = TRAMEIN[5];
                TRAMEOUT[11] = TRAMEIN[6];
                TRAMEOUT[12] = TRAMEIN[7];
                TRAMEOUT[13] = TRAMEIN[8];
                TRAMEOUT[14] = TRAMEIN[9];
                TRAMEOUT[15] = TRAMEIN[10];
                TRAMEOUT[16] = TRAMEIN[11];
                TRAMEOUT[17] = TRAMEIN[12];
                TRAMEOUT[18] = TRAMEIN[13];
                TRAMEOUT[19] = TRAMEIN[14];
                TRAMEOUT[20] = TRAMEIN[15];
                TRAMEOUT[21] = TRAMEIN[16];
                TRAMEOUT[22] = TRAMEIN[17];
                TRAMEOUT[23] = TRAMEIN[18];
                TRAMEOUT[24] = TRAMEIN[19];
                TRAMEOUT[25] = TRAMEIN[20];
                TRAMEOUT[26] = TRAMEIN[21];
                TRAMEOUT[27] = TRAMEIN[22];

                DELAY_MS(50);

                pointeur_out_in = 28;
                calcul_CRC(TRAMEOUT, pointeur_out_in - 1); //A COMPLETER
                TRAMEOUT[pointeur_out_in + 5] = STOP[0];

                TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE 


            }

            RS485('$'); //FIN DE SUPERTRAME
            RS485('$'); //FIN DE SUPERTRAME
            RS485('$'); //FIN DE SUPERTRAME
            DELAY_MS(100);
            RS485('$'); //FIN DE SUPERTRAME
            RS485('$'); //FIN DE SUPERTRAME
            RS485('$'); //FIN DE SUPERTRAME

            funct = 0;




            break;

        case 276:

            temoinfinvoieDIC = 0;
            finaldetect = 0;
            voieDEM = (100 * (DEMANDE_INFOS_CAPTEURS[0] - 48))+(10 * (DEMANDE_INFOS_CAPTEURS[1] - 48))+(DEMANDE_INFOS_CAPTEURS[2] - 48);
            codageDEM = (10 * (DEMANDE_INFOS_CAPTEURS[3] - 48))+(DEMANDE_INFOS_CAPTEURS[4] - 48);
            voieDEMFINAL = (100 * (DEMANDE_INFOS_CAPTEURS[6] - 48))+(10 * (DEMANDE_INFOS_CAPTEURS[7] - 48))+(DEMANDE_INFOS_CAPTEURS[8] - 48);
            codageDEMFINAL = (10 * (DEMANDE_INFOS_CAPTEURS[9] - 48))+(DEMANDE_INFOS_CAPTEURS[10] - 48);
            TYPECOMMANDE = DEMANDE_INFOS_CAPTEURS[5];


            //ERREUR DE TRAME PLAGE VOIE
            if (voieDEM > voieDEMFINAL) {
                DELAY_MS(100); // ATTENDRE
                RS485('%'); //% et $
                DELAY_MS(100); // ATTENDRe
                RS485('#'); //% et $
                RS485('*'); //% et $
                goto erreurDIC;
            }
            if (voieDEM > 20) {
                DELAY_MS(100); // ATTENDRE
                RS485('%'); //% et $
                DELAY_MS(100); // ATTENDRe
                RS485('#'); //% et $
                RS485('*'); //% et $
                goto erreurDIC;
            }
            if (voieDEMFINAL > 20) {
                DELAY_MS(100); // ATTENDRE
                RS485('%'); //% et $
                DELAY_MS(100); // ATTENDRe
                RS485('#'); //% et $
                RS485('*'); //% et $
                goto erreurDIC;
            }
            
            UNSEUL = 0;
            if ((voieDEM == voieDEMFINAL)&&(codageDEM == codageDEMFINAL))
                UNSEUL = 1;

            DELAY_MS(100); // ATTENDRE
            // DEBUT DE SUPER TRAME
            RS485('%'); //% et $
            DELAY_MS(50); // ATTENDRE				

            ////////////////JUSTE CEUX QUI EXISTENT

refaire:

            l = 128 * voieDEM + 10000;
            LIRE_EEPROM(3, l, SAVTAB, 4); //LIRE DANS EEPROM lexistance de toutes le voies

CONTINUER:

            TMP[0] = '#';
            TMP[1] = DEMANDE_INFOS_CAPTEURS[0];
            TMP[2] = DEMANDE_INFOS_CAPTEURS[1];
            TMP[3] = DEMANDE_INFOS_CAPTEURS[2];
            TMP[4] = DEMANDE_INFOS_CAPTEURS[3];
            TMP[5] = DEMANDE_INFOS_CAPTEURS[4];

            if (TYPECOMMANDE == '/') {

                if (voieDEM > 20)
                    goto erreurDIC;



                if (SAVTAB[2] == 'r') //Voie RESISTIVE
                {
                    if (codageDEM == 0) //envoyer le codage 0
                        goto continuer0;
                    if (codageDEM == 1) //envoyer le codage 1
                        goto continuer0;
                }

                if (SAVTAB[2] == 'a') //Voie ADRESSABLE	
                {
                    if (codageDEM == 0) //envoyer le codage 0
                        goto continuer0;
                    if (SAVTAB[codageDEM + 3] == 'A') //envoyer le codage 
                        goto continuer0;
                }

                if (SAVTAB[2] == 'i') //VOIE INCIDENTE
                {
                    goto continuer0; // ENVOYER TOUT
                }


                if (SAVTAB[2] == 'c') //VOIE CC
                {
                    goto continuer0; // ENVOYER TOUT	
                }

                codageDEM = codageDEM + 1;

                if (codageDEM == 17) {
                    codageDEM = 0;
                    voieDEM = voieDEM + 1;
                }


                IntToChar(voieDEM, TMP, 3);
                DEMANDE_INFOS_CAPTEURS[0] = TMP[0];
                DEMANDE_INFOS_CAPTEURS[1] = TMP[1];
                DEMANDE_INFOS_CAPTEURS[2] = TMP[2];



                IntToChar(codageDEM, TMP, 2);
                DEMANDE_INFOS_CAPTEURS[3] = TMP[0];
                DEMANDE_INFOS_CAPTEURS[4] = TMP[1];


                if ((voieDEM < 21))
                    goto refaire;
                else
                    goto erreurDIC;
            }
continuer0:


            LIRE_EEPROM(choix_memoire(TMP), calcul_addmemoire(TMP), TRAMEIN, 1); //LIRE DANS EEPROM
            FONCTION[0] = 'D';
            FONCTION[1] = 'I';
            FONCTION[2] = 'C';

            TRAMEOUT[0] = START[0];
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = FONCTION[0];
            TRAMEOUT[6] = FONCTION[1];
            TRAMEOUT[7] = FONCTION[2];



            TRANSFORMER_TRAME_MEM_EN_COM(); //le pointeur est memoris� dans ce sous prg

            calcul_CRC(TRAMEOUT, pointeur_out_in - 1); // CALCUL LE CRC ET LE RAJOUTE AU TABLEAU TRAMEOUT[]
            TRAMEOUT[pointeur_out_in + 5] = STOP[0]; // RAJOUT LE BYTE DE STOP 

            DELAY_MS(20); // ATTENDRE CAR LA CARTE MERE ECRIT DANS EEPROOM 5ms
            TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE 	
            if (SCRUT == 0xFF)
                LATHbits.LATH0 = !LATHbits.LATH0; //SIGNALISATION EXTERNE			

            TRAMERECEPTIONRS485DONNEES();
            if ((TRAMEIN[8] == 'A') && (TRAMEIN[9] == 'A')) //ERRAAxxxxx stop
                goto ARRETERDIC;
            if ((TRAMEIN[9] == 'A') && (TRAMEIN[10] == 'A')) //ERRAAxxxxx stop bug inconnu //   '.'#CMR3ERRAA au lieu de #CMR3ERRAA
                goto ARRETERDIC;

            codageDEM = codageDEM + 1;

            if (codageDEM == 17) {
                codageDEM = 0;
                voieDEM = voieDEM + 1;
                temoinfinvoieDIC = 1;
            }
            IntToChar(voieDEM, TMP, 3);
            DEMANDE_INFOS_CAPTEURS[0] = TMP[0];
            DEMANDE_INFOS_CAPTEURS[1] = TMP[1];
            DEMANDE_INFOS_CAPTEURS[2] = TMP[2];

            IntToChar(codageDEM, TMP, 2);
            DEMANDE_INFOS_CAPTEURS[3] = TMP[0];
            DEMANDE_INFOS_CAPTEURS[4] = TMP[1];

            if (UNSEUL == 0) {
                if (finaldetect == 0) {
                    if ((codageDEM != codageDEMFINAL) || (voieDEM != voieDEMFINAL)) //SI PAS FIN
                    {
                        if (TYPECOMMANDE == '/') {
                            if (temoinfinvoieDIC == 1) {
                                temoinfinvoieDIC = 0;
                                goto refaire;
                            }
                        }
                        goto CONTINUER;
                    }

                    finaldetect = 1;

                    if (TYPECOMMANDE == '/') {
                        if ((codageDEM == codageDEMFINAL)&&(voieDEM == voieDEMFINAL)) //FAIRE ENCORE UN
                            temoinfinvoieDIC = 1;
                        if (temoinfinvoieDIC == 1) {
                            temoinfinvoieDIC = 0;
                            goto refaire;
                        }
                    } else {
                        goto CONTINUER;
                    }
                }
            }
erreurDIC:
            DELAY_MS(100); // ATTENDRE CAR LA CARTE MERE ECRIT DANS EEPROOM 5ms
            RS485('$'); //FIN DE SUPERTRAME
            RS485('$'); //FIN DE SUPERTRAME
            RS485('$'); //FIN DE SUPERTRAME
            DELAY_MS(100); // ATTENDRE CAR LA CARTE MERE ECRIT DANS EEPROOM 5ms
            RS485('$'); //FIN DE SUPERTRAME
            RS485('$'); //FIN DE SUPERTRAME
            RS485('$'); //FIN DE SUPERTRAME

ARRETERDIC:
            SCRUT = 0;
            funct = 0;
            break;

        default:


            TRAMEOUT[0] = '#';
            TRAMEOUT[1] = ID1[0];
            TRAMEOUT[2] = ID1[1];
            TRAMEOUT[3] = ID2[0];
            TRAMEOUT[4] = ID2[1];
            TRAMEOUT[5] = 'E';
            TRAMEOUT[6] = 'E';
            TRAMEOUT[7] = 'E';
            TRAMEOUT[8] = '*';
            TRAMEENVOIRS485DONNEES(); // ENVOI A LA CARTE CONCERNEE 
            Reset();
            funct = 0;

            break;

    }
}


void TRANSFORMER_TRAME_MEM_EN_COM(void) {

    int t;
    int u;
    u = 8;
    TRAMEOUT[0] = '#';


    VOIECOD[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    VOIECOD[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    VOIECOD[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    VOIECOD[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    VOIECOD[4] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    //ecrire dans I2C

    TYPE[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    CMOD[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    CMOD[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    CMOD[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    CMOD[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    CCON[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    CCON[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    CCON[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    CCON[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;



    CREP[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    CREP[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    CREP[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    CREP[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;

    t = 0;
    for (t = 0; t < 13; t++) {
        CONSTITUTION[t] = TRAMEIN[u - 7];
        TRAMEOUT[u] = TRAMEIN[u - 7];
        u = u + 1;

    }

    t = 0;
    for (t = 0; t < 18; t++) {
        COMMENTAIRE[t] = TRAMEIN[u - 7];
        TRAMEOUT[u] = TRAMEIN[u - 7];
        u = u + 1;
    }



    CABLE[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    CABLE[1] == TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    CABLE[2] == TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;



    JJ[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    JJ[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    MM[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    MM[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    HH[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    HH[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    mm[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    mm[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;



    i[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;



    COD[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    COD[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    POS[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    POS[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    iiii[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    iiii[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    iiii[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    iiii[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    DISTANCE[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    DISTANCE[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    DISTANCE[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    DISTANCE[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    DISTANCE[4] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    ETAT[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    ETAT[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    VALEUR[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    VALEUR[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    VALEUR[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    VALEUR[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    SEUIL[0] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    SEUIL[1] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    SEUIL[2] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    SEUIL[3] = TRAMEIN[u - 7];
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;

    TRAMEOUT[u] = '*';

    pointeur_out_in = u;


}

void recuperer_trame_memoire(char w) { //RECUPERE TRAME MET DANS LES VARIABLES PUIS ECRIT DANS L'EPROOM
 
    int t;
    int u;

    u = 8; // pr un TRAMOUT MEMOIRE 

    TRAMEOUT[0] = '#';


    VOIECOD[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    VOIECOD[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    VOIECOD[2] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    VOIECOD[3] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    VOIECOD[4] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;


    //ecrire dans I2C

    TYPE[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;


    CMOD[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    CMOD[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    CMOD[2] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    CMOD[3] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;


    CCON[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    CCON[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    CCON[2] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    CCON[3] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;



    CREP[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    CREP[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    CREP[2] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    CREP[3] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;





    t = 0;
    for (t = 0; t < 13; t++) {
        CONSTITUTION[t] = TRAMEIN[u];
        TRAMEOUT[u - 7] = TRAMEIN[u];
        u = u + 1;

    }

    t = 0;
    for (t = 0; t < 18; t++) {
        COMMENTAIRE[t] = TRAMEIN[u];
        TRAMEOUT[u - 7] = TRAMEIN[u];
        u = u + 1;
    }



    CABLE[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    CABLE[1] == TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    CABLE[2] == TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;



    JJ[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    JJ[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;


    MM[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    MM[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;


    HH[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    HH[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;


    mm[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    mm[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;



    i[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;



    COD[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    COD[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;




    POS[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    POS[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;



    iiii[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    iiii[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    iiii[2] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    iiii[3] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;




    DISTANCE[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    DISTANCE[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    DISTANCE[2] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    DISTANCE[3] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    DISTANCE[4] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;



    ETAT[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    ETAT[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;


    VALEUR[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    VALEUR[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    VALEUR[2] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    VALEUR[3] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;


    SEUIL[0] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    SEUIL[1] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    SEUIL[2] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;
    SEUIL[3] = TRAMEIN[u];
    TRAMEOUT[u - 7] = TRAMEIN[u];
    u = u + 1;

    TRAMEOUT[u - 7] = '*';

    //ecriture ou pas
    if (w == 1)
        ECRIRE_EEPROMBYTE(choix_memoire(TRAMEOUT), calcul_addmemoire(TRAMEOUT), TRAMEOUT, 1); //ECRITURE EN EEPROM

    pointeur_out_in = u;
}