/*
PROGRAMME pour CARTE RELAIS
V 1

 */

/****************CONFIGURATION BITS ************************/
#pragma list P=PIC18F8723

#pragma config OSC = HS			// OSCILLATOR
#pragma config FCMEN = OFF		// Fail Safe Clock Monitor
#pragma config IESO = OFF 		// Internal External Osc. Switch Disabled
#pragma config PWRT = OFF		// Power Up Timer Disabled
#pragma config BOREN = OFF		// Brown-out Reset
//#pragma config BORV = 3		    // Brown-out  voltage bits
#pragma config WDT = OFF			// WATCHDOG Enabled
#pragma config WDTPS = 1024		// Watchdog Prescaler  512 -> 2,048sec
#pragma config MCLRE = ON		// MCLR Pin Enable
#pragma config LPT1OSC = OFF	// Low Power Timer
//#pragma config CCP2MX = PORTE	// CCP2 MUX bit
#pragma config STVREN = OFF 	// Stack Full Reset 
#pragma config LVP = OFF		// Low Voltage Programming Disabled
#pragma config BBSIZ = BB2K		//Boot Block Size
#pragma config XINST = OFF		//Extended Instruction Set Enable bit !!!MODIF CG!!!
#pragma config DEBUG = OFF		//DEBUG Disabled

#pragma config CP0 = OFF		// Code protection bit Block 0 (ON->protected / OFF->not protected) 
#pragma config CP1 = OFF		// Code protection bit Block 1 (ON->protected / OFF->not protected) 
#pragma config CP2 = OFF		// Code protection bit Block 2 (ON->protected / OFF->not protected) 
#pragma config CP3 = OFF		// Code protection bit Block 3 (ON->protected / OFF->not protected) 
#pragma config CP4 = OFF		// Code protection bit Block 4 (ON->protected / OFF->not protected) 
#pragma config CP5 = OFF		// Code protection bit Block 5 (ON->protected / OFF->not protected) 
#pragma config CP6 = OFF		// Code protection bit Block 6 (ON->protected / OFF->not protected) 
#pragma config CP7 = OFF		// Code protection bit Block 7 (ON->protected / OFF->not protected)

#pragma config CPB = OFF		// Boot Block Code Protection bit (ON->protected / OFF->not protected) 
#pragma config CPD = OFF		// Data EEPROM Code Protection bit (ON->protected / OFF->not protected) 

#pragma config WRT0 = OFF		// Write Protection bit Block 0 (ON->protected / OFF->not protected) 
#pragma config WRT1 = OFF		// Write Protection bit Block 1 (ON->protected / OFF->not protected) 
#pragma config WRT2 = OFF		// Write Protection bit Block 2 (ON->protected / OFF->not protected) 
#pragma config WRT3 = OFF		// Write Protection bit Block 3 (ON->protected / OFF->not protected) 
#pragma config WRT4 = OFF		// Write Protection bit Block 4 (ON->protected / OFF->not protected) 
#pragma config WRT5 = OFF		// Write Protection bit Block 5 (ON->protected / OFF->not protected) 
#pragma config WRT6 = OFF		// Write Protection bit Block 6 (ON->protected / OFF->not protected) 
#pragma config WRT7 = OFF		// Write Protection bit Block 7 (ON->protected / OFF->not protected) 

#pragma config WRTB = OFF		// Boot Block Write Protection bit (ON->protected / OFF->not protected) 
#pragma config WRTC = OFF		// Configuration Register Write Protection bit (ON->protected / OFF->not protected) 
#pragma config WRTD = OFF		// Data EEPROM Write Protection bit (ON->protected / OFF->not protected) 

#pragma config EBTR0 = OFF		// Table Read Protection bit Block 0(ON->protected / OFF->not protected) 
#pragma config EBTR1 = OFF		// Table Read Protection bit Block 1(ON->protected / OFF->not protected) 
#pragma config EBTR2 = OFF		// Table Read Protection bit Block 2(ON->protected / OFF->not protected) 
#pragma config EBTR3 = OFF		// Table Read Protection bit Block 3(ON->protected / OFF->not protected) 
#pragma config EBTR4 = OFF		// Table Read Protection bit Block 4(ON->protected / OFF->not protected) 
#pragma config EBTR5 = OFF		// Table Read Protection bit Block 5(ON->protected / OFF->not protected) 
#pragma config EBTR6 = OFF		// Table Read Protection bit Block 6(ON->protected / OFF->not protected) 
#pragma config EBTR7 = OFF		// Table Read Protection bit Block 7(ON->protected / OFF->not protected) 

#pragma config EBTRB = OFF		// Boot Block Table Read Protection bit (ON->protected / OFF->not protected) 


/************************ INCLUDE ***************************/
#include <p18f8723.h>					/* Defs PIC 18F8723 */
#include <inituc.h>                     /* Initialisation defs */
#include <rtcERIC.h>
#include <eeprom.h> 
#include <mesures.h>
#include <usart.h>
#include <HC595.h> //CIRCUITS COMMANDE RELAIS 
#include <delay.h>
#include <memo.h>
#include <rs485.h>
#include <eeprom_i2cERIC.h>
#include <i2c.h>
#include <analog.h>
#include <trames.h>

/**********************************************************************
 * Fonction prototype
 **********************************************************************/

//void main (void);
void Init_Tab_tpa(void); // A VOIR
void voir_qui_existe(void);
void interrogation_voies(void);
void mesure_test_fusible(void);
void Detection_Num_de_carte(void);
void Detection_Type_de_carte(void);
void mesure_test_cc(unsigned char n, unsigned char i, char SCRU);
void interrogation_voie(char *voices);

void FAIRE_ALARME(void);
/**********************************************************************/





/**********************************************************************
 * Fonction extern.
 **********************************************************************/

//extern void Rd_eeprom_i2c_str ( unsigned int adr_eeprom,  char *string, unsigned int long_string );
//extern void Wr_eeprom_i2c_char( unsigned int adr_eeprom,  char data_char);
//extern unsigned char Rd_eeprom_i2c_char( unsigned int adr_eeprom);
//extern unsigned int Rd_eeprom_i2c_int( unsigned int adr_eeprom);



unsigned char Boucle_TP;

//#########################  NE PAS SUPPRIMER POUR CARTE RELAIS #############################
extern unsigned int Timer1L; // pour sauvegarder les valeurs � l'interruption de INT2 (utilis� dans la mesure de fr�quence)
extern unsigned int Timer1H;
unsigned int t1ov_cnt; //	compteur de d�bordement du timer1  (de 65535 � 0000)
unsigned int cpt_int2;

extern char HORLOGET[12]; //TEST HORLOGE
extern unsigned char Tab_Horodatage[12];

//########################################################################################### 
//unsigned char	valid_sens;   // validation de l'affichage et du r�glage de la sensibilit� dans la lecture des TPR
extern unsigned char Tab_existance2[24], Tab_existance1[24]; // a effacer pour test
extern unsigned char dernier_TPA_1, dernier_TPA_2;
extern unsigned char TRAMEOUT[125];
extern unsigned char TRAMEIN[125];
extern unsigned char Tab_Voie_1[1]; //1 octet VOIE IMPAIRES 1,3,5....99 	  ASCII OU HEX??  => HEXA OU DECIMAL ET CONVERSION EN ASCII JUSTE POUR ECRITURE EN EEPROM
extern unsigned char Tab_Voie_2[1]; //1 octet VOIE PAIRES   2,4,6...100 	 					IDEM POUR TOUTES LES VALEURS
extern char START[1], STOP[1];
extern unsigned char dernier_TPA_2, dernier_TPA_1; // contient le num�ro du dernier capteur Adressable (de 0�16), de la voie paire, � mesurer
extern unsigned char recherche_TPR;
extern unsigned char recherche_TPA; // si = 1 => mode recherche automatique des capteurs donc dernier capteur = 16 sur voie impaire et paire
extern unsigned char voie_valide_1; // si voie_valide_1	= 0 => pas de mesure,  si voie_valide_1	= 1 => mesure et sauvegarde
extern unsigned char voie_valide_2; // si voie_valide_2	= 0 => pas de mesure,  si voie_valide_2	= 1 => mesure et sauvegarde


// parametres stock�s en eeprom du pic
extern unsigned char val1TP; // valid l'arr�t de scrutation si 1 TP
extern unsigned char boucleTPR; // valid lecture en boucle des TPR
extern unsigned char boucleDEB; // valid lecture en boucle du d�bit



extern unsigned int I_Conso_1;
extern unsigned int I_Conso_2;


unsigned char rel_voies_impaires;
unsigned char rel_voies_paires;
unsigned char alarme_caM;
unsigned char test_fus;
unsigned int Info_reset; // si � 5555 (par exemple) il y eu reset soft d� � une interruption pour tranfert trames

//unsigned int Offset_Enr; // adresse de l'enregistrement en lecture

unsigned int ad_No_der_enreg = 0x050; // adresse o� est stock�e, dans le pic, le num�ro du dernier enregistrement 
unsigned int No_der_enreg; // valeur du num�ro du dernier enregistrement   entre 0 et 80

unsigned int ad_Der_Enreg = 0x052; // adresse de l'eeprom du pic, de la valeur de l'adresse en i2c de l'enregistrment corespondant
unsigned int Der_Enreg; // valeur de l'adresse m�moire i2c du dernier enregistrement en EEprom

unsigned int Mem_Num_Ligne; //valeur num�ro de ligne TPA pour r�affichage derni�res mesures TPA (num_ligne effac�e par une mesure debit ou groupe)
unsigned int ad_Mem_Num_Ligne = 0x054; // son adresse en eeprom du pic0x54

unsigned int sensibilite_TPA; // variable pour fixer le niveau de d�tection des TPA dans la lecture des TPR (d�clar�e dans main.c)
unsigned int ad_sensibilite_TPA = 0x10; // son adresse en eeprom du pic

unsigned int adr_mesureTPA = 0x7E00; // adresse de l'eeprom ext (I2C) pour sauvegarder les mesures en cours et r�affichables des TPA

char typedecarte, numdecarte;
int RF1, RF2;
char FUSIBLEHS, ALARMECABLE;
char voie;

char nbreALARMES;
char SCRUT;
char COURTCIRCUIT;
char voie_av_reset;
char BREAKSCRUTATION;

char SAVMEMOEIL; //BUG OEIL RESISTIF
unsigned int SAVADOEIL;
char FUNCTIONUSE;
char SAVcaracOEIL;
char NOSAVEOEILSCRUT; //indique 0 si on SAV tt indique 1 si on SAV voie impaire indique 2 si SAV voie paire

unsigned char SAVADDE; //BUG SCRUTATION RESISTIFS SUR ADRESSABLE ACCES MEMOIRE
unsigned int SAVADDHL;
unsigned int SAVtab;
unsigned char SAVr;

// Coefficients de correction
extern unsigned int Coeff_I_Modul1, Coeff_I_Modul2; // pour ajuster I_Modul (coeffx100) exemple 153 => coeff 1.53
extern unsigned int Coeff_I_Conso1, Coeff_I_Conso2; // pour ajuster I_Modul (coeffx100) exemple 153 => coeff 1.53
extern unsigned int Coeff_I_Repos1, Coeff_I_Repos2; // pour ajuster I_Modul (coeffx100) exemple 153 => coeff 1.53;   	// pour ajuster I_Modul (coeffx100) exemple 153 => coeff 1.53
extern unsigned int Coeff_Resis1, Coeff_Resis2;

//#define Icons_cc 15 	// courant CC

#pragma		code	USER_ROM   // retour � la zone de code


#pragma interrupt Interruption2 

void Interruption2(void) {
    INTCONbits.GIE = 0;


}


//________________________________________FIN  INTERRUPTIONS__________________________________________

/********************************* PROGRAMME PRINCIPAL *******************************/


void main(void) {


    //initialisations
    // TEST POUR CALCUL NUMERO ET ADRESSE EEPROM
    extern unsigned char No_voie;


    init_uc(); // ****Initialisation du microcontroleur****

    Detection_Type_de_carte();
    Detection_Num_de_carte();


    alarme_caM = 0; // //ETEINDRE ALARME
    AL_CABLE = 1; //ETEINDRE ALARME



    DECH_L1 = 0;
    V_SUP1 = 0;
    UN_CPT_1 = 0;
    CPT_ON_1 = 0;
    DECH_L2 = 0;
    V_SUP2 = 0;
    UN_CPT_2 = 0;
    CPT_ON_2 = 0;
    SORTIETest_Fus = 1;
    SORTIETest_Fus = 0;








    // Coefficients de correction par defaut
    Coeff_I_Modul1 = 294; // pour ajuster I_Modul (coeffx100) exemple 153 => coeff 1.53
    Coeff_I_Modul2 = 294; // pour ajuster I_Modul (coeffx100) exemple 153 => coeff 1.53
    Coeff_I_Conso1 = 458; // pour ajuster I_Modul (coeffx100) exemple 153 => coeff 1.53
    Coeff_I_Conso2 = 458; // pour ajuster I_Modul (coeffx100) exemple 153 => coeff 1.53
    Coeff_I_Repos1 = 157; // pour ajuster I_Modul (coeffx1) exemple 153 => coeff 153
    Coeff_I_Repos2 = 157; // pour ajuster I_Modul (coeffx1) exemple 153 => coeff 153
    Coeff_Resis1 = 110; // pour ajuster I_Modul (coeffx1000) exemple 0.153 => coeff 153
    Coeff_Resis2 = 110; // pour ajuster I_Modul (coeffx1000) exemple 0.153 => coeff 153


    START[0] = '#';
    STOP[0] = '*';


    Init_I2C();
    Init_RS485();

    //	OK_I2C_EXT = 0;  // d�valide accord transmission I2C
    Val_R_I2C = 0; // ON VALIDE LES RESISTANCES DE TIRAGES SCL ET SDA
    I2C_INTERNE = 0; //ON LAISSE LE BUS I2C A L'INTERIEUR DE LA CARTE RI

    if (ReadEEPROM(0x50) == 0xCC) //MET LA SORTIE FIN CYCLE A 1 OU 0
        CYCLE = 1;
    else
        CYCLE = 0;
    if (CYCLE == 1)
        CYCLE = 1;
    //time:
    DELAY_MS(400);
    Timer1L = LIRE_HORLOGE(0x00);
    Timer1L = Timer1L & 0x0080; //CH

    Timer1H = LIRE_HORLOGE(0x07);
    if (Timer1H != 0x0010)
        ECRIRE_HORLOGE(0x07, 0b00010000); // ECRIRE AVANT PR MARCHE
    //REINIT

    NOSAVEOEILSCRUT = 0; //CONTRE ORDRE OEIL ET SCRUTATION VOIE UNIQUE A ZERO DONC PAS DE CONTRE ORDRE


    TRAMEOUT[0] = '#';
    TRAMEOUT[1] = '0';
    TRAMEOUT[2] = '0';
    TRAMEOUT[3] = '2';
    TRAMEOUT[4] = '0';
    TRAMEOUT[5] = '0';
    TRAMEOUT[6] = 'R';
    TRAMEOUT[7] = '1';
    TRAMEOUT[8] = '1';
    TRAMEOUT[9] = '1';
    TRAMEOUT[10] = '1';
    TRAMEOUT[11] = '2';
    TRAMEOUT[12] = '2';
    TRAMEOUT[13] = '2';
    TRAMEOUT[14] = '2';
    TRAMEOUT[15] = '0';
    TRAMEOUT[16] = '0';
    TRAMEOUT[17] = '0';
    TRAMEOUT[18] = '0';
    TRAMEOUT[19] = 'B';
    TRAMEOUT[20] = 'R';
    TRAMEOUT[21] = 'A';
    TRAMEOUT[22] = '/';
    TRAMEOUT[23] = '8';
    TRAMEOUT[24] = '8';
    TRAMEOUT[25] = '8';
    TRAMEOUT[26] = '8';
    TRAMEOUT[27] = '/';
    TRAMEOUT[28] = '2';
    TRAMEOUT[29] = '2';
    TRAMEOUT[30] = '/';
    TRAMEOUT[31] = '1';
    TRAMEOUT[32] = 'C';
    TRAMEOUT[33] = 'O';
    TRAMEOUT[34] = 'M';
    TRAMEOUT[35] = 'M';
    TRAMEOUT[36] = 'E';
    TRAMEOUT[37] = 'N';
    TRAMEOUT[38] = 'T';
    TRAMEOUT[39] = 'A';
    TRAMEOUT[40] = 'I';
    TRAMEOUT[41] = 'R';
    TRAMEOUT[42] = 'E';
    TRAMEOUT[43] = 'S';
    TRAMEOUT[44] = 'I';
    TRAMEOUT[45] = 'C';
    TRAMEOUT[46] = 'I';
    TRAMEOUT[47] = 'R';
    TRAMEOUT[48] = 'O';
    TRAMEOUT[49] = 'B';
    TRAMEOUT[50] = '9';
    TRAMEOUT[51] = '9';
    TRAMEOUT[52] = '3';
    TRAMEOUT[53] = '2';
    TRAMEOUT[54] = '6';
    TRAMEOUT[55] = '1';
    TRAMEOUT[56] = '1';
    TRAMEOUT[57] = '1';
    TRAMEOUT[58] = '5';
    TRAMEOUT[59] = '2';
    TRAMEOUT[60] = '5';
    TRAMEOUT[61] = '0';
    TRAMEOUT[62] = '0';
    TRAMEOUT[63] = '1';
    TRAMEOUT[64] = '0';
    TRAMEOUT[65] = '2';
    TRAMEOUT[66] = '0';
    TRAMEOUT[67] = '0';
    TRAMEOUT[68] = '0';
    TRAMEOUT[69] = '0';
    TRAMEOUT[70] = '4';
    TRAMEOUT[71] = '5';
    TRAMEOUT[72] = '6';
    TRAMEOUT[73] = '7';
    TRAMEOUT[74] = '8';
    TRAMEOUT[75] = '0';
    TRAMEOUT[76] = '1';
    TRAMEOUT[77] = '0';
    TRAMEOUT[78] = '8';
    TRAMEOUT[79] = '6';
    TRAMEOUT[80] = '5';
    TRAMEOUT[81] = '0';
    TRAMEOUT[82] = '8';
    TRAMEOUT[83] = '8';
    TRAMEOUT[84] = '8';
    TRAMEOUT[85] = '2';
    TRAMEOUT[86] = '2';
    TRAMEOUT[87] = '7';
    TRAMEOUT[88] = '6';
    TRAMEOUT[89] = '7';
    TRAMEOUT[90] = '*';


    LEDTX = 1;

    //VOIR ALARME EN MEMOIRE
    No_voie = ReadEEPROM(0x20);


    if (ReadEEPROM(0x20) == '9') {
        AL_CABLE = 0; //ALLUMER ALARME
        alarme_caM = 1;
    }
    if (ReadEEPROM(0x20) != '9') {
        AL_CABLE = 1; //OFF ALARME
        alarme_caM = 0; // //ETEINDRE ALARME
    }
    mesure_test_fusible(); // TEST LE FUSIBLE


    FAIRE_ALARME();


    INTCONbits.GIE = 0;
    INTCONbits.PEIE = 0;
    INTCON2bits.RBIP = 0; // interrupt Hight priorit� sur port B
    INTCONbits.RBIE = 0; //valide  interrupt sur port B
    // pour d�tecter coupure alim
    INTCONbits.TMR0IE = 0; // 0 = Disables the TMR0 overflow interrupt    
    INTCONbits.INT0IE = 1; // INT0 External Interrupt Enable bit 1 = Enables the INT0 external interrupt 
    INTCON2bits.INTEDG0 = 0; //External Interrupt 0 Edge Select bit 1 = Interrupt on rising edge 0 = Interrupt on falling edge bit 
    //
    OK_RS485_EXT = 0; // d�valide accord transmission RS485  
    INTCON2bits.INTEDG1 = 1; //pour interruption d�tection demande RS485 sur front montant  (RB1/INT1)

    INTCON3bits.INT1IF = 0; // reinit flag RS485 pour le premier demmarage on autorise pas la com

    INTCON3bits.INT1IE = 1; // valide l'interruption pour RS485 1 pr reactiver



    INTCONbits.GIE = 1;

    No_voie = 0;

    Tab_Voie_1[0] = 0; //sup
    Tab_Voie_2[0] = 0; //sup
    Prog_HC595(Tab_Voie_1[0], Tab_Voie_2[0], alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !


    DELAY_SEC(6);



    BREAKSCRUTATION = ReadEEPROM(0x80);
    if (BREAKSCRUTATION == 0xAA) {
        
        Info_reset = 0;

        RS485('('); // FIN DE SCRUTATION
        RS485('(');
        RS485('(');

        DELAY_MS(300);
        RS485('('); // FIN DE SCRUTATION
        RS485('(');
        RS485('(');


        BREAKSCRUTATION = 0x00;
        WriteEEPROM(0x80, BREAKSCRUTATION);
    }



    FAIRE_ALARME();


    voir_qui_existe();

    if (HORLOGE == 0)
        //LEDTX=0;
        if (HORLOGE == 1)
            //LEDTX=1;



            Reset(); //  <================================= !!!!!!! RESET SOFT DU PIC !!!!!!! 



    while (1);





} // fin MAIN()

void voir_qui_existe(void) {
    char i, j, tpr1, tpa1, tpr2, tpa2, ccc;
    unsigned int l;

    //for (i=0;i<13;i++)
    //Tab_Horodatage[i]='1';


    i = 0;
    recherche_TPA = 0;
    recherche_TPR = 0;
    ccc = 0;

    if (Info_reset == 5555) // pour indiquer qu'il faut reprendre les mesures interrompues par l'int
    {

        Info_reset = 0;
        voie_av_reset = ReadEEPROM(0x60);
        i = voie_av_reset; //ON RECUPERE ET ON REPREND ICI
        goto apresreset;
        //charge valeurs de reprise_mesures (stock�es en ram dans l'interruption avant le reset)
        // rel_voies_impaires,rel_voies_paires,alarme_ca,test_fus, type TPA ou TPR....etc
        //
    }


    for (i = 1; i < 20; i += 2) {

apresreset:
        //if (i=1)
        //CYCLE=0;
        if (i == 19)
            WriteEEPROM(0x50, 0xCC); //LE CYCLE EST TERMINE ON PLACE LEPPROMM COMME CELA JUSQUA INTERROGATION TEC DE LA CM


        voie_av_reset = i;
        WriteEEPROM(0x60, voie_av_reset);
        tpa1 = 0;
        tpr1 = 0;
        tpa2 = 0;
        tpr2 = 0;
        Tab_Voie_1[0] = i;
        Tab_Voie_2[0] = i + 1;

        // VOIE IMPAIRE
        delay_qms(10);
        l = 128 * i + 10000;
        LIRE_EEPROM(3, l, TRAMEIN, 4); //LIRE DANS EEPROM lexistance de toutes le voies


        if (TRAMEIN[2] == 'c') //CC
        {
            ccc = 1;
            tpr1 = 0;
            tpa1 = 0;
            tpr1 = 0;
            dernier_TPA_1 = 0;
            Tab_Voie_1[0] = 0;
            //tpa2=0;
            //tpr2=0;
            goto ccc1;
        }


        if (TRAMEIN[2] == 'r') {
            tpr1 = 1;
        }
        if (TRAMEIN[2] == 'i') {
            tpr1 = 0;
            tpa1 = 0;
            tpr1 = 0;
            dernier_TPA_1 = 0;
            //Tab_Voie_1[0]=0;
            //tpa2=0;
            //tpr2=0;
            goto ccc1;

        }
        if (TRAMEIN[2] == 'v') {
            tpa1 = 0;
            voie_valide_1 = 0;
            tpr1 = 0;
            dernier_TPA_1 = 0;
        }
        if (TRAMEIN[2] == 'a') {
            dernier_TPA_1 = 0;
            for (j = 3; j < 20; j++) {
                if (TRAMEIN[j] == 'A') //si il y a un A 
                {
                    dernier_TPA_1 = j - 2;
                    voie_valide_1 = 1;
                    tpa1 = 1;
                }
            }

        }






ccc1:

        // VOIE PAIRE
        delay_qms(10);
        l = 128 * (i + 1) + 10000;
        LIRE_EEPROM(3, l, TRAMEOUT, 4); //LIRE DANS EEPROM lexistance de toutes le voies


        if (TRAMEOUT[2] == 'c') //CC
        {
            //tpr1=0;
            //tpa1=0;
            //tpr1=0;
            tpa2 = 0;
            tpr2 = 0;
            ccc = 1;
            dernier_TPA_2 = 0;
            Tab_Voie_2[0] = 0;
            goto ccc;
        }
        if (TRAMEOUT[2] == 'r') {
            tpr2 = 1;
        }
        if (TRAMEOUT[2] == 'i') {
            tpa2 = 0;
            tpr2 = 0;
            dernier_TPA_2 = 0;

            goto ccc;
        }
        if (TRAMEOUT[2] == 'v') {
            tpa2 = 0;
            voie_valide_2 = 0;
            tpr2 = 0;
            dernier_TPA_2 = 0;
        }
        if (TRAMEOUT[2] == 'a') {
            dernier_TPA_2 = 0;
            for (j = 3; j < 20; j++) {
                if (TRAMEOUT[j] == 'A') //si il y a un A 
                {
                    dernier_TPA_2 = j - 2;
                    voie_valide_2 = 1;
                    tpa2 = 1;
                }
            }

        }


        if ((TRAMEOUT[2] == 'v')&&(TRAMEIN[2] == 'a')) //A cause de voie_valide_x le temps de mesure est d�cal� pour la mesure d'un adressable
            voie_valide_2 = 1;

        if ((TRAMEOUT[2] == 'a')&&(TRAMEIN[2] == 'v'))
            voie_valide_1 = 1;

ccc:


        if (tpr1 == 1 && (tpr2 == 0 && tpa2 == 0)) //SI RESITIF MAIS RIEN VOIE IMPAIRE
        {
            Prog_HC595(Tab_Voie_1[0], Tab_Voie_2[0], alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !

            tpr2 = 1;
            goto testTP;
        }

        if (tpr2 == 1 && (tpr1 == 0 && tpa1 == 0)) //SI RESITIF MAIS RIEN VOIE PAIRE
        {
            Prog_HC595(Tab_Voie_1[0], Tab_Voie_2[0], alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !
            tpr1 = 1;
            goto testTP;
        }

        if (tpa1 == 1 && (tpr2 == 0 && tpa2 == 0)) //SI ADRESSABLE MAIS RIEN VOIE IMPAIRE
        {
            Prog_HC595(Tab_Voie_1[0], Tab_Voie_2[0], alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !

            tpa2 = 1;
            goto testTP;
        }
        if (tpa2 == 1 && (tpr1 == 0 && tpa1 == 0)) //SI ADRESSABLE MAIS RIEN VOIE PAIRE
        {
            Prog_HC595(Tab_Voie_1[0], Tab_Voie_2[0], alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !

            tpa1 = 1;
            goto testTP;
        }

testTP:


        if (tpr1 == 1 && tpr2 == 1) {
            Prog_HC595(Tab_Voie_1[0], Tab_Voie_2[0], alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !

            Mesure_TPR(0);
        } else {

            // PROBLEME SUR LES PAIRES DE VOIES
        }





        if (tpa1 == 1 && tpa2 == 1) {
            Prog_HC595(Tab_Voie_1[0], Tab_Voie_2[0], alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !	

            Mesure_DEB_TPA();
            //pour le ADRESSABLE LE TEST CC EST LA




        } else {
            // PROBLEME SUR LES PAIRES DE VOIES

        }








    } // FIN FOR



    FAIRE_ALARME();
    Reset(); //  <================================= !!!!!!! RESET SOFT DU PIC !!!!!!! 

}

void interrogation_voie(char *voices) {

    char voie;

    recherche_TPA = 1; // ON ACTIVE LA RECHERCHE
    voie_valide_2 = 1;
    voie_valide_1 = 1;



    voie = 10 * (voices[8] - 48)+(voices[9] - 48);


    if (voie == 0) {
        voie = 20;
    }

    if ((voices[10] == '7')&&(voices[11] == '7')) {
        Tab_Voie_1[0] = 0;
        Tab_Voie_2[0] = 0;
        if ((voie == 1) || (voie == 3) || (voie == 5) || (voie == 7) || (voie == 9) || (voie == 11) || (voie == 13) || (voie == 15) || (voie == 17) || (voie == 19)) {
            Tab_Voie_1[0] = voie;
            NOSAVEOEILSCRUT = 1; //CONTRE ORDRE DE SAV POUR VOIE PAIRE
        }
        if ((voie == 2) || (voie == 4) || (voie == 6) || (voie == 8) || (voie == 10) || (voie == 12) || (voie == 14) || (voie == 16) || (voie == 18) || (voie == 20)) {
            Tab_Voie_2[0] = voie;
            NOSAVEOEILSCRUT = 2; //CONTRE ORDRE DE SAV POUR VOIE IMPAIRE
        }
    } else {
        Tab_Voie_1[0] = voie;
        Tab_Voie_2[0] = voie + 1;
    }
    Prog_HC595(Tab_Voie_1[0], Tab_Voie_2[0], alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !
    Mesure_DEB_TPA();


    DELAY_MS(300);
    DELAY_MS(300);
    recherche_TPR = 1; // ON ACTIVE LA RECHERCHE
    Prog_HC595(Tab_Voie_1[0], Tab_Voie_2[0], alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !
    Mesure_TPR(0);



    recherche_TPR = 0;
    recherche_TPA = 0;
    DELAY_MS(300);
    if (FUNCTIONUSE == 'N') {
        RS485('$'); // FIN DE SCRUTATION
        RS485('$');
        RS485('$');


        DELAY_MS(300);
        RS485('$'); // FIN DE SCRUTATION
        RS485('$');
        RS485('$');

    }
    if (FUNCTIONUSE == 'E') {
        SAVcaracOEIL = '7'; //ICI UTILISATION LORS DE LA SCRUTATION
        RS485('7'); // FIN DE SCRUTATION
        RS485('7');
        RS485('7');


        DELAY_MS(300);
        RS485('7'); // FIN DE SCRUTATION
        RS485('7');
        RS485('7');
        FUNCTIONUSE == 'N';

    }

    Reset(); //  <================================= !!!!!!! RESET SOFT DU PIC !!!!!!! 
}

void interrogation_voies(void) {




    recherche_TPA = 1; // ON ACTIVE LA RECHERCHE
    voie_valide_2 = 1;
    voie_valide_1 = 1;
    BREAKSCRUTATION = 0x00;
    for (voie = 1; voie <= 20; voie += 2) //UNE CARTE de 1 a 20 
    {

        if (BREAKSCRUTATION == 0xFF)
            goto FINSCRUTATION;


        recherche_TPR = 0;
        recherche_TPA = 1; // ON ACTIVE LA RECHERCHE
        Tab_Voie_1[0] = voie;
        Tab_Voie_2[0] = voie + 1;
        Prog_HC595(Tab_Voie_1[0], Tab_Voie_2[0], alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !
        Mesure_DEB_TPA();
        DELAY_MS(100);
        recherche_TPA = 0;
        recherche_TPR = 1; // ON ACTIVE LA RECHERCHE
        Prog_HC595(Tab_Voie_1[0], Tab_Voie_2[0], alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !
        Mesure_TPR(0);


        //Reset();

    }


FINSCRUTATION:
    if (BREAKSCRUTATION != 0xFF) {
        RS485('$'); // FIN DE SCRUTATION
        RS485('$');
        RS485('$');

        DELAY_MS(300);
        RS485('$'); // FIN DE SCRUTATION
        RS485('$');
        RS485('$');

    } else {
        BREAKSCRUTATION = 0xAA;


        WriteEEPROM(0x80, BREAKSCRUTATION);

        DELAY_MS(300);
        RS485('2'); // FIN DE SCRUTATION
        RS485('2');
        RS485('2');

    }


    Reset(); //  <================================= !!!!!!! RESET SOFT DU PIC !!!!!!! 
}

void Detection_Type_de_carte(void) {

    if (RA == 1)
        typedecarte = 'E'; //ELECTRIQUE ADRESSABLE

    if (RA == 0)
        typedecarte = 'M'; //MECANIQUE RESISTANCE 

    //typedecarte='M';

}

void Detection_Num_de_carte(void) {
    //RF3,RF4,RF5

    if ((SelA0 == 0)&&(SelA1 == 0)&&(SelA2 == 0))
        numdecarte = 0; //carte relais (0)1,2,3,4,(4)5 

    if ((SelA0 == 1)&&(SelA1 == 0)&&(SelA2 == 0))
        numdecarte = 1; //carte relais (0)1,2,3,4,(4)5 

    if ((SelA0 == 0)&&(SelA1 == 1)&&(SelA2 == 0))
        numdecarte = 2; //carte relais (0)1,2,3,4,(4)5  

    if ((SelA0 == 1)&&(SelA1 == 1)&&(SelA2 == 0))
        numdecarte = 3; //carte relais (0)1,2,3,4,(4)5 

    if ((SelA0 == 0)&&(SelA1 == 0)&&(SelA2 == 1))
        numdecarte = 4; //carte relais (0)1,2,3,4,(4)5 


    //numdecarte=2;








}

void mesure_test_fusible(void) {

    char voie;

    Tab_Voie_1[0] = 0; //sup 
    Tab_Voie_2[0] = 0; //sup
    Prog_HC595(Tab_Voie_1[0], Tab_Voie_2[0], alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !
    SORTIETest_Fus = 1;
    Mesure_TPR(1);

    SORTIETest_Fus = 0;
    if (RF1 < 100) {
        FUSIBLEHS = 0;
        for (voie = 1; voie <= 20; voie += 2) //UNE CARTE de 1 a 20 
        {




            TRAMEOUT[1] = '0';
            TRAMEOUT[2] = '0';
            TRAMEOUT[3] = voie + 48;
            if (voie >= 10) {
                TRAMEOUT[2] = '1';
                TRAMEOUT[3] = (voie - 10) + 48;
            }
            if (voie >= 20) {
                TRAMEOUT[2] = '2';
                TRAMEOUT[3] = '0';
            }

            TRAMEOUT[4] = '0';
            TRAMEOUT[5] = '0';

            MODIFICATION_TABLE_DEXISTENCE(TRAMEOUT, 2, 4);
        }

    } else {
        FUSIBLEHS = 1;

        for (voie = 1; voie <= 20; voie += 2) //UNE CARTE de 1 a 20 
        {




            TRAMEOUT[1] = '0';
            TRAMEOUT[2] = '0';
            TRAMEOUT[3] = voie + 48;
            if (voie >= 10) {
                TRAMEOUT[2] = '1';
                TRAMEOUT[3] = (voie - 10) + 48;
            }
            if (voie >= 20) {
                TRAMEOUT[2] = '2';
                TRAMEOUT[3] = '0';
            }

            TRAMEOUT[4] = '0';
            TRAMEOUT[5] = '0';

            MODIFICATION_TABLE_DEXISTENCE(TRAMEOUT, 2, 1);
        }






        // NOK
    }


    if (RF2 < 100) {
        FUSIBLEHS = 0;
        for (voie = 2; voie <= 20; voie += 2) //UNE CARTE de 1 a 20 
        {




            TRAMEOUT[1] = '0';
            TRAMEOUT[2] = '0';
            TRAMEOUT[3] = voie + 48;
            if (voie >= 10) {
                TRAMEOUT[2] = '1';
                TRAMEOUT[3] = (voie - 10) + 48;
            }
            if (voie >= 20) {
                TRAMEOUT[2] = '2';
                TRAMEOUT[3] = '0';
            }

            TRAMEOUT[4] = '0';
            TRAMEOUT[5] = '0';

            MODIFICATION_TABLE_DEXISTENCE(TRAMEOUT, 2, 5);
        }























    } else {
        FUSIBLEHS = 1;


        for (voie = 2; voie <= 20; voie += 2) //UNE CARTE de 1 a 20 
        {




            TRAMEOUT[1] = '0';
            TRAMEOUT[2] = '0';
            TRAMEOUT[3] = voie + 48;
            if (voie >= 10) {
                TRAMEOUT[2] = '1';
                TRAMEOUT[3] = (voie - 10) + 48;
            }
            if (voie >= 20) {
                TRAMEOUT[2] = '2';
                TRAMEOUT[3] = '0';
            }

            TRAMEOUT[4] = '0';
            TRAMEOUT[5] = '0';

            MODIFICATION_TABLE_DEXISTENCE(TRAMEOUT, 2, 2);
        }














        // NOK
    }

}

void mesure_test_cc(unsigned char n, unsigned char i, char SCRU) // test si la voie est en CC
{

    if ((COURTCIRCUIT == 0xF1) || (COURTCIRCUIT == 0xFF)) {
        //VOIE IMPAIRE CC OK



        TRAMEOUT[1] = '0';
        TRAMEOUT[2] = '0';
        TRAMEOUT[3] = n + 48;
        if (n >= 10) {
            TRAMEOUT[2] = '1';
            TRAMEOUT[3] = (n - 10) + 48;
        }
        if (n >= 20) {
            TRAMEOUT[2] = '2';
            TRAMEOUT[3] = '0';
        }

        TRAMEOUT[4] = '0';
        TRAMEOUT[5] = '0';

        MODIFICATION_TABLE_DEXISTENCE(TRAMEOUT, 2, 6); //mettre 'c' sur la table existence

    }


    if ((COURTCIRCUIT == 0xF2) || (COURTCIRCUIT == 0xFF)) {



        TRAMEOUT[1] = '0';
        TRAMEOUT[2] = '0';
        TRAMEOUT[3] = i + 48;
        if (i >= 10) {
            TRAMEOUT[2] = '1';
            TRAMEOUT[3] = (i - 10) + 48;
        }
        if (i >= 20) {
            TRAMEOUT[2] = '2';
            TRAMEOUT[3] = '0';
        }

        TRAMEOUT[4] = '0';
        TRAMEOUT[5] = '0';

        MODIFICATION_TABLE_DEXISTENCE(TRAMEOUT, 2, 7); //mettre 'c' sur la table existence

    }


}

void FAIRE_ALARME(void) {
    char opp, opp1, etatC;
    unsigned int ipp;
    nbreALARMES = 0;





    //AL_CABLE=0;

    for (opp = 1; opp < 21; opp++) {


        ipp = 128 * opp + 10000;
        LIRE_EEPROM(3, ipp, TRAMEIN, 4); //LIRE DANS EEPROM lexistance de toutes le voies 



        //	TMP[0]='#';
        //	TMP[1]=DEMANDE_INFOS_CAPTEURS[0];
        //	TMP[2]=DEMANDE_INFOS_CAPTEURS[1];
        //	TMP[3]=DEMANDE_INFOS_CAPTEURS[2];
        //	TMP[4]=DEMANDE_INFOS_CAPTEURS[3];
        //	TMP[5]=DEMANDE_INFOS_CAPTEURS[4];
        if (typedecarte == 'E') {
            for (opp1 = 3; opp1 <= 19; opp1++) {
                if (TRAMEIN[opp1] == 'A') {
                    TRAMEOUT[1] = '0';
                    TRAMEOUT[2] = '0';
                    TRAMEOUT[3] = opp + 48;
                    if (opp >= 10) {
                        TRAMEOUT[2] = '1';
                        TRAMEOUT[3] = (opp - 10) + 48;
                    }
                    if (opp >= 20) {
                        TRAMEOUT[2] = '2';
                        TRAMEOUT[3] = '0';
                    }

                    if ((opp1 - 3) < 10) {
                        TRAMEOUT[4] = '0';
                        TRAMEOUT[5] = (opp1 - 3) + 48;
                    }
                    if ((opp1 - 3) >= 10) {
                        TRAMEOUT[4] = '1';
                        TRAMEOUT[5] = (opp1 - 13) + 48;
                    }


                    LIRE_EEPROM(choix_memoire(TRAMEOUT), calcul_addmemoire(TRAMEOUT), TRAMEOUT, 1); //LIRE DANS EEPROM
                    if (((TRAMEOUT[75] == '3') &&(TRAMEOUT[76] == '0')) || ((TRAMEOUT[75] == '5') &&(TRAMEOUT[76] == '0')) || ((TRAMEOUT[75] == '4') &&(TRAMEOUT[76] == '0'))) //ALARME S/R ET H/G
                        nbreALARMES++; //COMPTER LES ALARMES



                }

            } // FIN ADRESSABLE LES 16 capteurs ou 17!!!	
        } // fin adressable

        if (typedecarte == 'M') {
            opp1 = 2; //VOIE resistive 
            if (TRAMEIN[opp1] == 'r') // JUSTE EN CODAGE UN A VOIR NORMALEMENT
            {
                TRAMEOUT[1] = '0';
                TRAMEOUT[2] = '0';
                TRAMEOUT[3] = opp + 48;
                if (opp >= 10) {
                    TRAMEOUT[2] = '1';
                    TRAMEOUT[3] = (opp - 10) + 48;
                }
                if (opp >= 20) {
                    TRAMEOUT[2] = '2';
                    TRAMEOUT[3] = '0';
                }


                TRAMEOUT[4] = '0';
                TRAMEOUT[5] = '1';



                LIRE_EEPROM(choix_memoire(TRAMEOUT), calcul_addmemoire(TRAMEOUT), TRAMEOUT, 1); //LIRE DANS EEPROM
                if (((TRAMEOUT[75] == '3') &&(TRAMEOUT[76] == '0')) || ((TRAMEOUT[75] == '5') &&(TRAMEOUT[76] == '0')) || ((TRAMEOUT[75] == '4') &&(TRAMEOUT[76] == '0'))) //SI N/R ET PAS INT SI ALARME ET PAS INT SI H/G ET PAS INT
                    nbreALARMES++; //COMPTER LES ALARMES


            }

        }


    }







    if (nbreALARMES > 0) {
        alarme_caM = 1;
        AL_CABLE = 0; //ALLUMER ALARME
        WriteEEPROM(0x20, '9');

    } else {
        alarme_caM = 0; // //ETEINDRE ALARME
        AL_CABLE = 1; //ETIENDRE ALARME
        WriteEEPROM(0x20, '1');
    }

}


//________________________________________INTERRUPTIONS__________________________________________

/* fonction interruptions */
void Interruption1(void); // code de l'interruption
//void Interruption2(void);				// code de l'interruption

#pragma code highVector=0x08			//valeur 0x08 pour interruptions prioritaite 

void highVector(void) {
    _asm GOTO Interruption1 _endasm // on doit �xecuter le code de la fonction interruption
}


// ************************
// ****  Interruptions ****
// ************************
#pragma interrupt Interruption1 

void Interruption1(void) {
    unsigned char sauv1;
    unsigned char sauv2;
    char dummy;
    char buffer_test[2];

    sauv1 = PRODL; // sauvegarde le contenu des registres de calcul
    sauv2 = PRODH;

    //===========TIMER1
    if (PIR1bits.TMR1IF) // v�rifie que l'IT est Timer1
    {
        t1ov_cnt++; // incr�mente le compteur de d�bordement
        PIR1bits.TMR1IF = 0; // efface le flag d'IT du Timer1
    }

    //===========INT0 sur RB0  

    if (INTCONbits.INT0IF == 1) // v�rifie que l'IT est INT0, origine RB0=0		
    {
        // =====> ALIM OFF
        // peut-�tre faire comme pour un reset

        Info_reset = 5555; // pour indiquer qu'il faut reprendre les mesures interrompues par l'int0
        //	Sauvegarde en eeprom ou RAM? pour red�marrage apr�s reset
        INTCONbits.INT0IF = 0; //efface le flag d'IT extern sur RB0

        Reset(); //  <================================= !!!!!!! RESET SOFT DU PIC !!!!!!! 

    }



    //===========INT1 sur RB1   POUR DETECTION DEMANDE RS485

    //  IL FAUDRA CONFIGURER: INTCON2bits.INTEDG1 = 0 si d�tection sur front descendant,  ou = 1 si d�tection sur front montant
    //   et INTCON3bits.INT1IE = 1
    if ((INTCON3bits.INT1IF == 1)&&(INTCON3bits.INT1IE == 1)) // v�rifie que l'IT est INT1, origine RB1= 0�1 ou 1�0 � voir le sens		
    {
        // Sauvegarde de l'action en cours , si lecture TPA ou TPR, quelles voies, �criture m�moire... etc
        // si mesure de fr�quence capteur en cours voir si pas de probl�me avec INT2
        LEDTX = 0;
        INTCON2bits.RBPU = 1; // pull-up port B         � voir  <<<< inhibe une erreur sur l'entre de 2V et bloque les int1 ??
        INTCON3bits.INT1IF = 0; //reset le flag d'IT sur INT1 (RB1)
        TRAMERECEPTIONRS485DONNEES();
        LEDTX = 1;
        RECUPERATION_DES_TRAMES(TRAMEIN); //LA TRAMEOUT EST PRETTE A ETRE ENVOYEE
        Info_reset = 5555; // pour indiquer qu'il faut reprendre les mesures interrompues par l'INT1

        INTCON3bits.INT1IF = 0; //reset le flag d'IT sur INT1 (RB1)



        Reset(); //  <================================= !!!!!!! RESET SOFT DU PIC !!!!!!! 

    }


    //===========INT2 sur RB2  pour mesure fr�quence du MCTA
    if (INTCON3bits.INT2IF == 1) // v�rifie que l'IT est INT2, origine RB2=2		
    {
        if (cpt_int2 == 0) {
            TMR1H = 0;
            TMR1L = 0;
            t1ov_cnt = 0;
            T1CONbits.TMR1ON = 1; // timer 1 ON
            PIR1bits.TMR1IF = 0; // 0 = TMR1 register overflowed reset
        }

        if (cpt_int2 >= 20) {
            T1CONbits.TMR1ON = 0; // timer 1 OFF
            Timer1H = TMR1H;
            Timer1L = TMR1L;
            INTCON3bits.INT2IE = 0; // arret interruption INT2
        }
        cpt_int2++;
        INTCON3bits.INT2IF = 0; //efface le drapeau d'IT sur RB2
    }

    PRODL = sauv1; // on restaure les registres de calcul
    PRODH = sauv2;
}

#pragma		code
/* End of File : main.c */