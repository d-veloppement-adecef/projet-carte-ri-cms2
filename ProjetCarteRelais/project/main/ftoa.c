
/* FONCTION POUR CONVERTIR UN FLOAT EN STRING */

/************************ INCLUDE *****************************/
#include <p18f8723.h>

/**************** DECLARATION DES SOUS PROGRAMMES *************/



//////////////////////////////////////////////
// fonction de conversion INT => CHAR
void IntToChar(signed int value, unsigned char *chaine,int Precision)
{
signed int Mil, Cent, Diz, Unit;
int count = 0;

// initialisation des variables
Mil = 0; Cent = 0; Diz = 0; Unit = 0;
/*
if (value < 0) // si c'est un nombre n�gatif
    {
    value = value * (-1);
    *chaine = 45; // signe '-'
    count = 1;  // c'est un nombre n�gatif
    }
else // si c'est un nombre positif
    {
    count = 1;
    *chaine = 32; // code ASCII du signe espace ' '
    }
*/
// si la valeur n'est pas nulle
if (value != 0)
    {
    if (Precision >= 4) // si l'utilisateur d�sire les milliers
         {
         // conversion des milliers
         Mil = value / 1000;
         if (Mil != 0)
               {
               *(chaine+count) = Mil + 48;
               if (*(chaine+count) < 48)
                     *(chaine+count) = 48;
               if (*(chaine+count) > 57)
                     *(chaine+count) = 57;
               }
         else
               *(chaine+count) = 48;
         count++;
         }

    if (Precision >= 3) // si l'utilisateur d�sire les centaines
        {
        // conversion des centaines
        Cent = value - (Mil * 1000);
        Cent = Cent / 100;
        if (Cent != 0)
            {
            *(chaine+count) = Cent + 48;
            if (*(chaine+count) < 48)
                  *(chaine+count) = 48;
            if (*(chaine+count) > 57)
                  *(chaine+count) = 57;
            }
        else
            *(chaine+count) = 48;
            count++;
      }

    if (Precision >= 2) // si l'utilisateur d�sire les dizaines
         {
         // conversion des dizaines
         Diz = value - (Mil * 1000) - (Cent * 100);
         Diz = Diz / 10;
         if (Diz != 0)
            {
            *(chaine+count) = Diz + 48;
            if (*(chaine+count) < 48)
                  *(chaine+count) = 48;
            if (*(chaine+count) > 57)
                  *(chaine+count) = 57;
            }
         else
            *(chaine+count) = 48;
         count++;
         }

    // conversion unit�s
    Unit = value - (Mil * 1000) - (Cent * 100) - (Diz * 10);
    *(chaine+count) = Unit + 48;
    if (*(chaine+count) < 48)       // limites : 0 et 9
            *(chaine+count) = 48;
    if (*(chaine+count) > 57)
            *(chaine+count) = 57;
    }
else // if (value == 0)
    {
    //*(chaine) = 32; // ecrit un espace devant le nombre
    for (Mil=0;Mil<Precision;Mil++)   // inscription de '0' dans toute la chaine
          *(chaine+Mil) = 48;
    }

} // fin de la fonction de conversion INT => CHAR




///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////// CONVERSION UNSIGNED INT TO CHAR///////////////////////////////////////////////////////////////////////////////////////// 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void IntToChar5(unsigned int value, char *chaine,char Precision){
unsigned int Mil, Cent, Diz, Unit, DMil;
int count = 0;

// initialisation des variables
Mil = 0; Cent = 0; Diz = 0; Unit = 0;DMil=0;
/*
if (value < 0) // si c'est un nombre n�gatif
    {
    value = value * (-1);
    *chaine = 45; // signe '-'
    count = 1;  // c'est un nombre n�gatif
    }
else // si c'est un nombre positif
    {
    count = 1;
    *chaine = 32; // code ASCII du signe espace ' '
    }
*/
// si la valeur n'est pas nulle
if (value != 0)
    {
if (Precision >= 5) // si l'utilisateur d�sire les milliers
         {
         // conversion des milliers
         DMil = value / 10000;
         if (DMil != 0)
               {
               *(chaine+count) = DMil + 48;
               if (*(chaine+count) < 48)
                     *(chaine+count) = 48;
               if (*(chaine+count) > 57)
                     *(chaine+count) = 57;
               }
         else
               *(chaine+count) = 48;
         count++;
         }
    if (Precision >= 4) // si l'utilisateur d�sire les milliers
         {
         // conversion des milliers
         Mil = value-(DMil*10000);
         Mil = Mil / 1000;
         if (Mil != 0)
               {
               *(chaine+count) = Mil + 48;
               if (*(chaine+count) < 48)
                     *(chaine+count) = 48;
               if (*(chaine+count) > 57)
                     *(chaine+count) = 57;
               }
         else
               *(chaine+count) = 48;
         count++;
         }

    if (Precision >= 3) // si l'utilisateur d�sire les centaines
        {
        // conversion des centaines
        Cent = value - (Mil * 1000)-(DMil*10000);
        Cent = Cent / 100;
        if (Cent != 0)
            {
            *(chaine+count) = Cent + 48;
            if (*(chaine+count) < 48)
                  *(chaine+count) = 48;
            if (*(chaine+count) > 57)
                  *(chaine+count) = 57;
            }
        else
            *(chaine+count) = 48;
            count++;
      }

    if (Precision >= 2) // si l'utilisateur d�sire les dizaines
         {
         // conversion des dizaines
         Diz = value - (Mil * 1000) - (Cent * 100)-(DMil*10000);
         Diz = Diz / 10;
         if (Diz != 0)
            {
            *(chaine+count) = Diz + 48;
            if (*(chaine+count) < 48)
                  *(chaine+count) = 48;
            if (*(chaine+count) > 57)
                  *(chaine+count) = 57;
            }
         else
            *(chaine+count) = 48;
         count++;
         }

    // conversion unit�s
    Unit = value - (Mil * 1000) - (Cent * 100) - (Diz * 10)-(DMil*10000);
    *(chaine+count) = Unit + 48;
    if (*(chaine+count) < 48)       // limites : 0 et 9
            *(chaine+count) = 48;
    if (*(chaine+count) > 57)
            *(chaine+count) = 57;
    }
else // if (value == 0)
    {
    //*(chaine) = 32; // ecrit un espace devant le nombre
    for (Mil=0;Mil<Precision;Mil++)   // inscription de '0' dans toute la chaine
          *(chaine+Mil) = 48;
    }

} // fin de la fonction de conversion INT => CHAR

