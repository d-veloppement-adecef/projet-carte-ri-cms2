
#include <p18f8723.h>					/* Defs PIC 18F8723 */
#include <mesures.h>
#include <inituc.h>
#include <delay.h>						/* Defs DELAY functions */
#include <ftoa.h>						/* Adapt Chaine de caract�re */
#include <rtcERIC.h>
#include <analog.h>
#include <math.h>
#include <delay.h>						/* Defs DELAY functions */
#include <memo.h>
#include <delay.h>
#include <delays.h>
#include <HC595.h>






//const rom unsigned int Table_R[20];
//const rom unsigned int TableFreq[20];


unsigned char Nb_Capteurs_max; //16 � voir 
unsigned char Nb_TPA_ok; // indique le nb de capteurs qui ont r�pondus lors d'une interrogation  

//extern unsigned char Num_Ligne;		//num�ro ligne en cours
//extern unsigned char Der_Ligne;   	//derniere ligne interrog�e
extern unsigned int I_Conso_1; //courant conso capteurs voies impaires
extern unsigned int I_Conso_2; //courant conso capteurs voies paires
extern unsigned int I_Modul_1; //courant modul capteurs voies impaires
extern unsigned int I_Modul_2; //courant modul capteurs voies paires
extern unsigned int I_Repos_1;
extern unsigned int I_Repos_2;


extern float V_Ligne_AVB_Calc_1; // TENSION SUR CAPTEUR
extern float V_Ligne_AVB_Calc_2; // TENSION SUR CAPTEUR
extern float V_Ligne_AM_Calc_2; // TENSION SUR CAPTEUR
extern float V_Ligne_AM_Calc_1; // TENSION SUR CAPTEUR
extern float V_Resistif_1; // TENSION AUX BORNES DE LA RESISTANCE DE MESURE voies impaires
extern float V_Resistif_2; // TENSION AUX BORNES DE LA RESISTANCE DE MESURE voies paires
extern float V_48_Calc;
extern float V_48v_Calc_2;
extern float V_48v_Calc_1;

extern int RF1, RF2;

//extern unsigned char Tab_Code[17];
extern unsigned int Tab_Pression_1[17];
extern unsigned int Tab_Pression_2[17];
extern unsigned int Tab_I_Repos_1[17];
extern unsigned int Tab_I_Repos_2[17];
extern unsigned int Tab_I_Modul_1[17];
extern unsigned int Tab_I_Modul_2[17];
extern unsigned int Tab_I_Conso_1[17];
extern unsigned int Tab_I_Conso_2[17];
extern unsigned char Tab_Etat_1[17];
extern unsigned char Tab_Etat_2[17];


extern unsigned char Tab_Horodatage[10]; //10 octets	 //10 caract�res	  

//extern unsigned char Boucle_TP;

extern unsigned char Tab_code_1[17]; //17 octets 	code 0,1,2.....16 	
extern unsigned char Tab_code_2[17]; //17 octets 	code 0,1,2.....16 

extern char COURTCIRCUIT;
extern char FUSIBLEHS;
extern unsigned char alarme_caM;
extern unsigned int sensibilite_TPA; // variable pour fixer le niveau de d�tection des TPA dans la lecture des PTR (d�clar�e dans main.c)
// elle est stock�e en eeprom pic @0x056   � lire au d�marrage
extern char typedecarte;
//extern unsigned char F_aff_sauve_tpa; 	// flag pour m�mo affichage: Sauvegarde ? (sauvegarde des mesures pour TP adressables)
//extern unsigned char F_sauve_mes_tpa; 	// flag pour m�mo sauvegarde valid�e des mesures des tp adressables
//extern unsigned char F_mode_scrut; 		// flag pour signaler d�but et fin de scrutation
//extern unsigned char F_it_chenillard; 	// flag pour chenillard
//extern unsigned char F_sup3mes_tpa;	 	// flag pour indiquer si ecran complet ou r�duit et > � 3 mesures

unsigned int Timer1L;
unsigned int Timer1H;

float tempF1;
float tempF2;
float tempF3;
float tempF4;
float Cumul_Freq_1; //r�sultat de mesure de fr�quence du TPA voie impaire
float Cumul_Freq_2; //r�sultat de mesure de fr�quence du TPA voie paire

unsigned int Freq1_1;
unsigned int Freq2_1;
unsigned int Freq3_1;
unsigned int Freq4_1;
unsigned int Freq1_2;
unsigned int Freq2_2;
unsigned int Freq3_2;
unsigned int Freq4_2;

unsigned int Freq_TPA_1;
unsigned int Freq_TPA_2;

unsigned int Freq_A;

unsigned int Freq_R1, Freq_R2; // valeur des TP r�sistifs en mbar ou Hz !!

unsigned int debit_cable; // D�bit mesur� en cours


unsigned char fin_mesures_1; // si 1 indique que l'interrogation est termin�e sur voie impaire
unsigned char fin_mesures_2; // si 1 indique que l'interrogation est termin�e sur voie impaire
//(c'est pour valider l'analyse des courants pour indiquer les d�fauts et le affichages inverses



extern unsigned int t1ov_cnt;
extern unsigned int cpt_int2;
unsigned char val1TP; // valid l'arr�t de scrutation si 1 TP

extern unsigned char BREAKSCRUTATION;
extern char FUNCTIONUSE;


#define i_conso_min 	20 		// = 2.0mA =>seuil mini de d�tection d'un courant capteur (i_conso est x10) (20)
#define i_repos_min 	150 	// = 150�A =>seuil pout indiquer qu'un seul capteur sur la ligne      A VOIR!!!!!!!!!!!
#define def_i_conso 	80 		// defaut si > 8.0 mA
#define def_i_modul  	40 		// defaut si > 4.0 mA
#define def_i_repos  	150 	// defaut si > 150 �A
#define def_pression_B  800 	// defaut si < 800 mbar
#define def_pression_H  1800 	// defaut si > 1800 mbar

#define Icons_cc 100 //cc DOUBLE DECLARATION 


#define V_Ligne_Max	5	// tension max acceptable sur la ligne d'interrogation capteurs
#define max_instable 3  // valeur max d'�cart fr�quence (Hz) pour signaler une instabilit� du capteur

//#define		Impuls_TPA	PORTBbits.RB2		// entr�e pour d�tecter si capteur adressable lors d'une interro de TP r�sistifs
/* Resume allocation of romdata into the default section */


unsigned char voie_valide_1; // si voie_valide_1	= 0 => pas de mesure,  si voie_valide_1	= 1 => mesure et sauvegarde
unsigned char voie_valide_2; // si voie_valide_2	= 0 => pas de mesure,  si voie_valide_2	= 1 => mesure et sauvegarde

unsigned char dernier_TPA_1; // contient le num�ro du dernier capteur Adressable (de 0�16), de la voie impaire, � mesurer
unsigned char dernier_TPA_2; // contient le num�ro du dernier capteur Adressable (de 0�16), de la voie paire, � mesurer
unsigned char recherche_TPA; // si = 1 => mode recherche automatique des capteurs donc dernier capteur = 16 sur voie impaire et paire
unsigned char recherche_TPR;
unsigned char Memo_autorisee_1; // autorisation de sauvegarde voie impaire
unsigned char Memo_autorisee_2; // autorisation de sauvegarde voie paire
extern unsigned char Tab_Voie_1[1]; //1 octet VOIE IMPAIRES 1,3,5....99 	  ASCII OU HEX??  => HEXA OU DECIMAL ET CONVERSION EN ASCII JUSTE POUR ECRITURE EN EEPROM
extern unsigned char Tab_Voie_2[1]; //1 octet VOIE PAIRES   2,4,6...100 	 					IDEM POUR TOUTES LES VALEURS




#pragma		code	USER_ROM   // retour � la zone de code*/


// ######################### MESURE DEBITMETRE ET TPA SUR 2 VOIES (IMPAIRE et PAIRE suivante)###############

void Mesure_DEB_TPA(void) {
    unsigned int TEST; // pour tester le calcul debit cable

    int tpa;
    int scrut_TP; //indicateur r�ponse d'un tpa, seulement dans le cas ou le courant de repos est < 150�A (pour stopper la sructation d�s que mesur�, si un seul tpa sur la ligne)
    int nb; // variable compteur

    unsigned int I_Conso1_1; // pour faire moyenne sur 2 mesures, voies impaires
    unsigned int I_Conso2_1; //
    unsigned int I_Conso1_2; // pour faire moyenne sur 2 mesures, voies paires
    unsigned int I_Conso2_2; //

    unsigned int I_Repos1_1; // pour faire moyenne sur 2 mesures, voies impaires
    unsigned int I_Repos2_1; //
    unsigned int I_Repos1_2; // pour faire moyenne sur 2 mesures, voies paires
    unsigned int I_Repos2_2; //




    // POUR ERIC
    //voie_valide_1=1;
    //voie_valide_2=1; //ON REGARDE LES VOIES PAIRES ET IMPAIRES
    //recherche_TPA=1;

    LEDRX = 0;
    Memo_autorisee_1 = 1;
    Memo_autorisee_2 = 1;
    //
    COURTCIRCUIT = 0x00;
    Freq_A = 0x0000;
    Freq1_1 = 0x0000;
    Freq2_1 = 0x0000;
    Freq3_1 = 0x0000;
    Freq4_1 = 0x0000;
    Freq1_2 = 0x0000;
    Freq2_2 = 0x0000;
    Freq3_2 = 0x0000;
    Freq4_2 = 0x0000;

    Cumul_Freq_1 = 0x0000; // pour moyenne
    Cumul_Freq_2 = 0x0000; // pour moyenne

    I_Conso1_1 = 0x0000;
    I_Conso2_1 = 0x0000;
    I_Conso1_2 = 0x0000;
    I_Conso2_2 = 0x0000;
    I_Conso_1 = 0;
    I_Conso_2 = 0;
    Init_Tab_tpa();

    Nb_TPA_ok = 0; // indique le nb de capteurs trouv�s en interrogation
    fin_mesures_1 = 0; // si 1 indique que l'interrogation est termin�e  
    fin_mesures_2 = 0; // si 1 indique que l'interrogation est termin�e  



    if (recherche_TPA) //si mode recherche automatique des tp 
    {
        Nb_Capteurs_max = 17; //16 tp + 1 d�bitm�tre en 0 mettre 17
        for (nb = 0; nb <= 16; nb++) //TOUS LES CODAGES
        {
            Tab_code_1[nb] = 1;
            Tab_code_2[nb] = 1;
        }   
    } else {
        if (dernier_TPA_1 >= dernier_TPA_2) {
            Nb_Capteurs_max = dernier_TPA_1; //VOIR AVEC TABLEAU EXISTANCE
        } else {
            Nb_Capteurs_max = dernier_TPA_2;
        }
    }


    tempF1 = 0x0000;
    tempF2 = 0x0000;
    tempF3 = 0x0000;
    tempF4 = 0x0000;







    //DEPART INIT
    DECH_L1 = 0;
    V_SUP1 = 0;
    UN_CPT_1 = 0;
    CPT_ON_1 = 0;
    DECH_L2 = 0;
    V_SUP2 = 0;
    UN_CPT_2 = 0;
    CPT_ON_2 = 0;





    //config pour 56V VOIE IMPAIRE
    UN_CPT_1 = 1; //VALIDE
    V_SUP1 = 0; //DEVALIDE 



    CPT_ON_1 = 0; //   <======== d�valide 56V SUR VOIE   par s�curit�
    DECH_L1 = 0; // valide d�charge VOIE 

    //config pour 56V VOIE PAIRE
    UN_CPT_2 = 1;
    V_SUP2 = 0;

    CPT_ON_2 = 0; //   <======== d�valide 56V SUR VOIE   par s�curit�
    DECH_L2 = 0; // valide d�charge VOIE 




    tpa = 0;
    scrut_TP = 1; // tant que  = 1 => scrutation


    DECH_L1 = 1;
    DECH_L2 = 1;


    //			b_ModeScrut=1;
    // Par s�curit�, contr�le de la tension ligne avant interrogation
    //	Mesure_Ligne_AM();  // s�curit� pr�sence tension sur ligne , � voir si n�cessaire

    // ajouter test du 48V

    // synchronisation de l'horloge de 1hz du ci clock DS1307 pour cadencer la lecture des capteurs ttes les 2sec
    /*
                    set_time(0x07,0x00);  // adresse= 0x07 valeur 0x00 pour d�valider SQR/OUT  � 1Hz
                DELAY_125MS(1);
                    set_time(0x07,0x10);  // adresse= 0x07 valeur 0x10 pour valider SQR/OUT  � 1Hz
                DELAY_125MS(1);		//  
     */
    creer_trame_horloge();

    while (HORLOGE == 0); //attend passage � 0 de RB3, boucle tant que la condition n'est pas arriv�e HORLOGE=PORTBbits.RB3


    while (HORLOGE == 1); //attend passage � 1 de RB3, boucle tant que la condition n'est pas arriv�e HORLOGE=PORTBbits.RB3
    //LEDTX=!LEDTX;



    LEDRX = !LEDRX;
    //===>	0Ms
    if (voie_valide_1 == 1) { // si voie impaire valid�e
        CPT_ON_1 = 1; //   <======== valide voie impaire de mesure � 56v
    }
    if (voie_valide_2 == 1) { //	si voie paire valid�e
        CPT_ON_2 = 1; //   <======== valide voie paire de mesure � 56v
    }


    DECH_L1 = 0; // lib�re la d�charge ligne impaire
    DECH_L2 = 0; // lib�re la d�charge ligne  paire

    Delay10KTCYx(10); //peut-�tre � ajuster


    for (tpa = 0; tpa < Nb_Capteurs_max; tpa++) //tpa=0 POUR LECTURE EN ZERO (DEBITMETRE)
        //	for (tpa=1; tpa < Nb_Capteurs_max ;tpa++)
    {

        if (recherche_TPA)
            LATHbits.LATH0 = !LATHbits.LATH0; //SIGNALISATION EXTERNE



        LEDRX = !LEDRX;
        Freq_A = 0;
        Freq1_1 = 0x0000;
        Freq2_1 = 0x0000;
        Freq3_1 = 0x0000;
        Freq4_1 = 0x0000;
        Freq1_2 = 0x0000;
        Freq2_2 = 0x0000;
        Freq3_2 = 0x0000;
        Freq4_2 = 0x0000;
        tempF1 = 0;
        tempF2 = 0;
        tempF3 = 0;
        tempF4 = 0;
        Cumul_Freq_1 = 0;
        Cumul_Freq_2 = 0;

        // ====> 0mS MESURE DE LA CONSO DU CAPTEUR PENDANT 500ms

        DELAY_125MS(2); //Tempo1  250ms!!!!!!!!!!!!!!!!!!!! on stop a 250ms (AAA)

        DELAY_SW_250MS(); // TEMPO2  la Tempo2 se termine � 	while(!(INTCONbits.TMR0IF)); on lance la tempo 250ms au niveau du timer0

        Delay10KTCYx(10); //<== voir s'il faut plus de tempo  pour mesurer un peu plus tard la conso  


        if (voie_valide_1 == 1) { // si voie impaire valid�e
            Mesure_I_Conso_1();
            I_Conso1_1 = I_Conso_1;
        }
        if (voie_valide_2 == 1) { // si voie paire valid�e
            Mesure_I_Conso_2();
            I_Conso1_2 = I_Conso_2;
        }

        Delay10KTCYx(10);

        if (voie_valide_1 == 1) { // si voie impaire valid�e
            Mesure_I_Conso_1();
            I_Conso2_1 = I_Conso_1;
            I_Conso_1 = (I_Conso1_1 + I_Conso2_1) / 2; // moyenne 
            Mesure_I_Modul_1();
        }
        if (voie_valide_2 == 1) { // si voie impaire valid�e
            Mesure_I_Conso_2();
            I_Conso2_2 = I_Conso_2;
            I_Conso_2 = (I_Conso1_2 + I_Conso2_2) / 2; // moyenne 
            Mesure_I_Modul_2();
        }

        //attente fin de tempo
        // TEST CC
        if (I_Conso_1 >= Icons_cc)// AU DESSU DE 15mA (4000ohms) sur la ligne resistif >100k (0.6mA)
        {
            COURTCIRCUIT = 0xF1;
            Prog_HC595(0, Tab_Voie_2[0], alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !
            if (I_Conso_2 >= Icons_cc)// AU DESSU DE 15mA (4000ohms) sur la ligne resistif >100k (0.6mA)
            {
                Prog_HC595(0, 0, alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !
                COURTCIRCUIT = 0xFF;
            }
        }
        if (I_Conso_2 >= Icons_cc)// AU DESSU DE 15mA (4000ohms) sur la ligne resistif >100k (0.6mA)
        {
            COURTCIRCUIT = 0xF2;
            Prog_HC595(Tab_Voie_1[0], 0, alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !
            if (I_Conso_1 >= Icons_cc)// AU DESSU DE 15mA (4000ohms) sur la ligne resistif >100k (0.6mA)
            {
                Prog_HC595(0, 0, alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !
                COURTCIRCUIT = 0xFF;
            }
        }




        while (!(INTCONbits.TMR0IF)); // FIN de TEMPO2      wait until TMR0 rolls over  FIN (BBB)

        // ====> 500mS
        if (recherche_TPA)
            LATHbits.LATH0 = !LATHbits.LATH0; //SIGNALISATION EXTERNE

        LEDRX = !LEDRX;


        //				Sel_Freq=0;  //(0 voie impaire)
        //				Sel_Freq=1;  //(1 voie paire)











        //####### Mesure Fr�quence de r�ponse des capteurs = 4 fois 2 mesures de fr�q sur 20 p�riodes #######

        DELAY_MS(60); // 125ms -5 ms ajustement pour avoir 625ms pour lancer la premiere mesure de frequence (CCC)

        LEDRX = !LEDRX;

        //voie impaire
        if (voie_valide_1 == 1) { // si voie impaire valid�e
            Sel_Freq = 0; //commutateur de fr�quence voie impaire
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); // mesure de la fr�quence 	
                Freq1_1 += Freq_A; //m�morise 1�re valeur de fr�quence (pour tester instabilit�)

            }
        }
        //voie paire
        if (voie_valide_2 == 1) { // si voie paire valid�e
            Sel_Freq = 1; //commutateur de fr�quence voie paire 
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); // mesure de la fr�quence 	
                Freq1_2 += Freq_A; //m�morise 1�re valeur de fr�quence (pour tester instabilit�)
            }
        }


        LEDRX = !LEDRX;
        DELAY_MS(60); // 125ms
        LEDRX = !LEDRX;
        //voie impaire
        if (voie_valide_1 == 1) { // si voie impaire valid�e
            Sel_Freq = 0; //commutateur de fr�quence voie impaire
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); // mesure de la fr�quence 	
                Freq2_1 += Freq_A; //m�morise 1�re valeur de fr�quence (pour tester instabilit�)
            }
        }
        //voie paire
        if (voie_valide_2 == 1) { // si voie paire valid�e
            Sel_Freq = 1; //commutateur de fr�quence voie paire 
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); // mesure de la fr�quence 	
                Freq2_2 += Freq_A; //m�morise 1�re valeur de fr�quence (pour tester instabilit�)
            }
        }

        LEDRX = !LEDRX;
        DELAY_MS(60); // 125ms
        LEDRX = !LEDRX;
        //voie impaire
        if (voie_valide_1 == 1) { // si voie impaire valid�e
            Sel_Freq = 0; //commutateur de fr�quence voie impaire
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); // mesure de la fr�quence 	
                Freq3_1 += Freq_A; //m�morise 1�re valeur de fr�quence (pour tester instabilit�)
            }
        }
        //voie paire
        if (voie_valide_2 == 1) { // si voie paire valid�e
            Sel_Freq = 1; //commutateur de fr�quence voie paire 
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); // mesure de la fr�quence 	
                Freq3_2 += Freq_A; //m�morise 1�re valeur de fr�quence (pour tester instabilit�)
            }
        }

        LEDRX = !LEDRX;
        DELAY_MS(60); // 125ms   
        LEDRX = !LEDRX;
        //voie impaire
        if (voie_valide_1 == 1) { // si voie impaire valid�e
            Sel_Freq = 0; //commutateur de fr�quence voie impaire
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); // mesure de la fr�quence 	
                Freq4_1 += Freq_A; //m�morise 1�re valeur de fr�quence (pour tester instabilit�)
            }
        }
        //voie paire
        if (voie_valide_2 == 1) { // si voie paire valid�e
            Sel_Freq = 1; //commutateur de fr�quence voie paire 
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); // mesure de la fr�quence 	
                Freq4_2 += Freq_A; //m�morise 1�re valeur de fr�quence (pour tester instabilit�)
            }
        }

        LEDRX = !LEDRX;



        if (tpa == 2) {





            LEDRX = !LEDRX;




        }



        if (I_Conso_1 > i_conso_min) // soit I_Conso mini >= 2.0mA pour led verte => pr�sence capteur   (i_conso est x10)
        {

            if (voie_valide_1 == 1) { // si voie impaire valid�e
                Instable_1(tpa); // v�rification de l'instabilit�
                Freq_TPA_1 = (int) (Cumul_Freq_1 / 8); // 8 => 4 x 2 mesures
            }


            //    			Nb_TPA_ok;  // indique le nb de capteurs trouv�s en interrogation@@@@
        } else {
            Freq_TPA_1 = 0;
        }






        if (I_Conso_2 > i_conso_min) // soit I_Conso mini >= 2.0mA pour led verte => pr�sence capteur   (i_conso est x10)
        {

            if (voie_valide_2 == 1) { // si voie paire valid�e
                Instable_2(tpa); // v�rification de l'instabilit�
                Freq_TPA_2 = (int) (Cumul_Freq_2 / 8); // 8 => 4 x 2 mesures
            }
            //    			Nb_TPA_ok;  // indique le nb de capteurs trouv�s en interrogation@@@@
        } else {
            Freq_TPA_2 = 0;
        }


        if (tpa > 0) {
            Tab_Pression_1[tpa] = Freq_TPA_1;
            Tab_Pression_2[tpa] = Freq_TPA_2; //1 = OFFSET POUR VOIE PAIRE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        }
        //CALCUL DEBIT
        if (!tpa) {
            Calcul_Deb(Tab_Pression_1[0]);
            TEST = debit_cable;
            Tab_Pression_1[0] = debit_cable;

            Calcul_Deb(Tab_Pression_2[0]);
            TEST = debit_cable;
            Tab_Pression_2[0] = debit_cable;
        }
        // ====> 500mS + 375mS + 375mS ==> 1250mS 
        //Attente de la chute du signal d'horloge pour synchro avec 1.5sec





        if (HORLOGE == 1) {
            while (HORLOGE == 1);
            while (HORLOGE == 0); //attend passage � 0 de RB3, boucle tant que la condition n'est pas arriv�e Synchro1s=PORTBbits.RB3
        }

        while (HORLOGE == 0); //attend passage � 0 de RB3, boucle tant que la condition n'est pas arriv�e Synchro1s=PORTBbits.RB3



        LEDRX = !LEDRX;


        //<=================ICI 1.5SEC================>
        DELAY_MS(150); // 1 x 150ms  peut-�tre � ajuster (LLL)

        // ====> 1500+150= 1650mS

        //LEDRX=!LEDRX;

        //#####################			// MESURE le courant de repos VOIE IMPAIRE
        if (voie_valide_1) // si voie paire valid�e
        {
            Mesure_I_Repos_1();
            I_Repos1_1 = I_Repos_1;
            DELAY_MS(5);
            Mesure_I_Repos_1();
            I_Repos2_1 = I_Repos_1;
            I_Repos_1 = (I_Repos1_1 + I_Repos2_1) / 2;
            if (I_Repos_1 <= 15) {
                I_Repos_1 = 0;
            } // seuil pour �viter d'afficher un courant de repos si rien de branch� 
            // car sinon il y a un minimum de 12�A d� � l'offset de l'ampli

            Tab_I_Repos_1[tpa] = I_Repos_1; //I_Repos est en (�A)	  
            /////				Tab_I_Repos[tpa+1]=I_Repos_2;			    	//I_Repos est en (�A)	  
            Tab_I_Modul_1[tpa] = I_Modul_1; //I_Modul est en (mA x10)

            //######  Retranche le courant de repos du courant de modulation
            //				I_Conso_1/=2;   		// moyenne d�ja faite
            I_Conso_1 *= 100; // met � la m�me �chelle que i_repos   (i_conso est x10)
            if (I_Conso_1 > I_Repos_1) { // pour ne pas prendre en compte si pas de capteur
                I_Conso_1 -= I_Repos_1; // retranche I_Repos de I_Conso  (i_conso est x10)
            } else {
                I_Conso_1 = 0;
            }
            I_Conso_1 /= 100; // remet � l'�chelle pour i_conso
            //######
            I_Conso_1 += (I_Modul_1 / 2); // TEST  ON RAJOUTE LA MOITI� DE LA MODULATION
            Tab_I_Conso_1[tpa] = I_Conso_1; //I_Conso en (mA x10) 


            ////////////////////////// voir arr�t si 1 capteur � r�pondu ##########################################################

            ////			if (((I_Repos_1<i_repos_min) && tpa>=1) && (Tab_I_Conso_1[tpa]> i_conso_min) && (val1TP))  // il n'y a qu'un capteur et il a r�pondu  (!!si val1TP=1 la boucle s'arrete si capteur a r�pondu)
            ////				{
            ////				scrut_TP = 0;
            ////				}
        } //FIN de if(voie_valide_1)

        //#####################			// MESURE le courant de repos VOIE PAIRE



        if (voie_valide_2 == 1) // si voie paire valid�e
        {
            Mesure_I_Repos_2();
            I_Repos1_2 = I_Repos_2;
            DELAY_MS(5);
            Mesure_I_Repos_2();
            I_Repos2_2 = I_Repos_2;
            I_Repos_2 = (I_Repos1_2 + I_Repos2_2) / 2;
            if (I_Repos_2 <= 15) {
                I_Repos_2 = 0;
            } // seuil pour �viter d'afficher un courant de repos si rien de branch� 
            // car sinon il y a un minimum de 12�A d� � l'offset de l'ampli

            Tab_I_Repos_2[tpa] = I_Repos_2; //I_Repos est en (�A)	  
            /////				Tab_I_Repos[tpa+1]=I_Repos_2;			    	//I_Repos est en (�A)	  
            Tab_I_Modul_2[tpa] = I_Modul_2; //I_Modul est en (mA x10)




            //######  Retranche le courant de repos du courant de modulation
            //				I_Conso_1/=2;   		// moyenne d�ja faite
            I_Conso_2 *= 100; // met � la m�me �chelle que i_repos   (i_conso est x10)
            if (I_Conso_2 > I_Repos_2) { // pour ne pas prendre en compte si pas de capteur
                I_Conso_2 -= I_Repos_2; // retranche I_Repos de I_Conso  (i_conso est x10)
            } else {
                I_Conso_2 = 0;
            }
            I_Conso_2 = I_Conso_2 / 100; // remet � l'�chelle pour i_conso
            //######
            I_Conso_2 += (I_Modul_2 / 2); // TEST  ON RAJOUTE LA MOITI� DE LA MODULATION
            Tab_I_Conso_2[tpa] = I_Conso_2; //I_Conso en (mA x10)   1 OFFSET POUR VOIE PAIRE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!




            ////////////////////////// voir arr�t si 1 capteur � r�pondu ##########################################################


            ////			if (((I_Repos_2<i_repos_min) && tpa>=2) && (Tab_I_Conso_2[tpa]> i_conso_min) && (val1TP))  // il n'y a qu'un capteur et il a r�pondu  (!!si val1TP=1 la boucle s'arrete si capteur a r�pondu)
            ////				{
            ////				scrut_TP = 0;
            ////				}

        } //FIN de if(voie_valide_1==1)






        //Attente de la remont�e du signal d'horloge pour synchro avec 2sec
        while (HORLOGE == 1); //attend passage � 1 de RB3, boucle tant que la condition n'est pas arriv�e HORLOGE=PORTBbits.RB3






        //FIN DE BOUCLE MESURE CAPTEURS PCPG  ou groupe
        if (scrut_TP == 0) // A REVOIR!!!!!!!!!!!!!!!!!!!!!!!!!!
        {
            Nb_Capteurs_max = tpa; // utilis� en mode scrolling  -1
            fin_mesures_1 = 1; // A REVOIR!!!!!!!!!!!!!!!!!!!!!!!!!!
            fin_mesures_2 = 1; // A REVOIR!!!!!!!!!!!!!!!!!!!!!!!!!!
            break; //<======!!!!!
        }
    } // FIN DE :	for(tpa=0; tpa < Nb_Capteurs_max ;tpa++)

    CPT_ON_1 = 0; //   <======== d�valide 56V SUR VOIE  IMPAIRE
    CPT_ON_2 = 0; //   <======== d�valide 56V SUR VOIE  PAIRE

    delay_qms(100);

    DECH_L1 = 1; // d�charge la ligne impaire
    DECH_L2 = 1; // d�charge la ligne	paire

    delay_qms(100);
    DECH_L1 = 0; // d�charge la ligne impaire
    DECH_L2 = 0; // d�charge la ligne	paire

    V_SUP1 = 0;
    UN_CPT_1 = 0;
    V_SUP2 = 0;
    UN_CPT_2 = 0;

    Prog_HC595(0, 0, alarme_caM, FUSIBLEHS); //COMMANDE RELAIS !

    //	ICI SAUVEGARDES EN EEPROM DU PIC ou 24LC256 A VOIR!!!!!!!!!!!!!!!!!!!
    if (recherche_TPA) {
        // sauvegarde position dernier capteur sur voie impaire
        //variable:dernier_TPA_1
        // sauvegarde position dernier capteur sur voie paire
        //variable:dernier_TPA_2
    }



    //	ICI SAUVEGARDES EN EEPROM 24LC256  A VOIR!!!!!!!!!!!!!!!!!!!

    if (voie_valide_1 == 1) // si voie impaire valid�e
    {
        Memo_autorisee_1 = 1;
        Memo_autorisee_2 = 0;
        ecriture_trame_i2c(1);
    }

    if (BREAKSCRUTATION == 0xFF)
        goto BREAK;

    if (voie_valide_2 == 1) // si voie paire valid�e
    {
        Memo_autorisee_1 = 0;
        Memo_autorisee_2 = 1;
        ecriture_trame_i2c(2);
    }

    if (BREAKSCRUTATION == 0xFF)
        goto BREAK;

BREAK:
    mesure_test_cc(Tab_Voie_1[0], Tab_Voie_2[0], 1); //ecris CC ou pas 


}

void Mesure_Freq() // ne pas modifier critique!!!
{
    unsigned int cpt_to; //compteur time out

    // mesure sur 1 p�riode du signal fr�quence capteur par int2(RB2) d�clench�e sur front montant et par le nbre de boucles du timer1 sur 16bits
    // l'horloge du timer1 est celle du quarts de 24Mhz/4= 6Mhz, donc une boucle timer = 1 / (6Mhz) = 0.000000166sec = 0.166�s
    // la valeur de la fr�quence capteur = 1/(0.166�s * val_timer1)
    // valeur d'1 p�riode =   [(t1ov_cnt * 65536 + TempH * 256 + TempL) * 0.166]  normalement   t1ov_cnt sera �gal � 0 si >0 => erreur

    t1ov_cnt = 0x00; // compteur de d�bordement timer1
    cpt_int2 = 0x00;
    Freq_A = 0x0000; // fr�quence de capteur
    tempF1 = 0x0000;
    tempF2 = 0x0000;
    tempF3 = 0x0000;
    tempF4 = 0x0000;
    //				Freq1=0;
    //				Freq2=0;
    //				Freq3=0;
    //				Freq4=0;
    TMR1H = 0x0000; // registre H 8bit du timer 1
    TMR1L = 0x0000; // registre L 16bit du timer 1
    Timer1L = 0x0000;
    Timer1H = 0x0000;


    PIR1bits.TMR1IF = 0; // efface le drapeau d'IT
    T1CONbits.T1CKPS0 = 0; // pr�diviseur  1:1
    T1CONbits.T1CKPS1 = 0; // pr�diviseur  1:1
    T1CONbits.RD16 = 0; // timer sur 8bits SINON TROP DE PB
    T1CONbits.TMR1CS = 0; // TIMER1 sur horloge interne (Fosc/4)
    T1CONbits.T1RUN = 0; // source autre que oscillateur du timer 1
    T1CONbits.T1OSCEN = 0; // turn off resistor feeback 
    //   			RCONbits.IPEN = 1; 	//enable priority interrupt   => � priori pas besoin d'int du timer1 sauf si roll-over en cas d'absence de fr�quence
    //pour pouvoir stopper le compteur
    IPR1 = 0x01; // set Timer1 to high priority	
    PIE1 = 0x01; // enable Timer1 roll-over interrupt
    //			INTCON = 0xC0;		//GIE=1 PEIE=1 enable global and peripheral interrupts


    
    // CONFIG POUR INT2(RB2)
    cpt_int2 = 0;
    INTCONbits.RBIE = 0; //d�valide  interrupt sur port B 
    //			INTCONbits.RBIF = 0;	//RESET  interrupt 	FLAG sur port B  
    INTCON2bits.RBPU = 1; // pull-up port B         � voir  <<<< provoque une erreur sur l'entre de 2V 
    INTCON2bits.INTEDG2 = 0; //INT2 sur front montant   <<<<<
    INTCON2bits.RBIP = 0; // interrupt Hight priorit� sur port B   <<<<<
    T1CONbits.TMR1ON = 0; // stop timer 1 


    //INTCON3bits.INT1IF=0; //INTERDIT RB1 ??????????????

    INTCONbits.GIEH = 1; // Toutes les IT d�masqu�es autoris�es
    INTCONbits.PEIE = 1; // valide ttes les int haute priorit�
    INTCON3bits.INT2IP = 1; // INT2 en haute priorit�

    INTCON3bits.INT2IF = 0; // flag d'interruption int2 � reseter 
    INTCON3bits.INT2IE = 1; // valide interruption INT2

    // time out en cas de non r�ponse il faut cpt_to <500(~800Hz) et > 240(1827Hz)
    //		Si <240 f < 800Hz => valeur 800
    //		Si >500 f > 1800Hz => valeur 1800

    cpt_to = 0x0000; // compteur time-out

    //			while (cpt_to<500 && cpt_int2<2)
    while (cpt_to < 10000 && cpt_int2 < 21) {
        cpt_to++;
    }
    /*			if (cpt_to < 240 || cpt_to > 500 || cpt_int2<2)
                        {
                    Freq = 0;
                    return;
                        }   */



    //			while (cpt_int2<2);
    INTCONbits.GIE = 0; //disable global interrupt  
    INTCONbits.PEIE = 0; //d�valide ttes les int haute priorit�
    INTCON3bits.INT2IE = 0; // arret interruption INT2
    //			while(1);




    //			tempF1 = (Timer1H * 256 + Timer1L);
    tempF1 = (Timer1H * 256 + Timer1L);
    tempF1 = (tempF1 + t1ov_cnt * 65535) / 20;
    if (tempF1 > 0) {
        //			if (tempF1 >= 0x0001)  //0x0A
        //						{  
        tempF1 += 0x0015;
        //						}	//  corrige le temps de retard du aux instructions de d�marrage du timer1 

        tempF2 = tempF1 * 0xA6; //0xA6 = * 166
        tempF3 = 1 / (tempF2 / 1000000);
        tempF4 = tempF3 * 1000; //x1000
        Freq_A = (int) tempF4;

        if (!Sel_Freq) { //commutateur de fr�quence voie impaire
            Cumul_Freq_1 += tempF4;
        }
        if (Sel_Freq) { //commutateur de fr�quence voie paire
            Cumul_Freq_2 += tempF4;
        }
    } else {
        Freq_A = 0x0000;
    }

    T1CONbits.RD16 = 0; // timer sur 8bits
    TMR1H = 0x0000; // registre H 8bit du timer 1
    TMR1L = 0x0000; // registre L 16bit du timer 1

}

void Instable_1(int tpa) // test instabilit�
{
    if (Freq1_1 >= Freq2_1) {
        tempF1 = Freq1_1 - Freq2_1;
    } else {
        tempF1 = Freq2_1 - Freq1_1;
    }

    //---------

    if (Freq2_1 >= Freq3_1) {
        tempF2 = Freq2_1 - Freq3_1;
    } else {
        tempF2 = Freq3_1 - Freq2_1;
    }

    //---------

    if (Freq3_1 >= Freq4_1) {
        tempF3 = Freq3_1 - Freq4_1;
    } else {
        tempF3 = Freq4_1 - Freq3_1;
    }

    if (tempF1 > max_instable || tempF2 > max_instable || tempF3 > max_instable) {
        Tab_Etat_1[tpa] |= 0x80; // instable
    }

}

void Instable_2(int tpa) // test instabilit�
{
    if (Freq1_2 >= Freq2_2) {
        tempF1 = Freq1_2 - Freq2_2;
    } else {
        tempF1 = Freq2_2 - Freq1_2;
    }

    //---------

    if (Freq2_2 >= Freq3_2) {
        tempF2 = Freq2_2 - Freq3_2;
    } else {
        tempF2 = Freq3_2 - Freq2_2;
    }

    //---------

    if (Freq3_2 >= Freq4_2) {
        tempF3 = Freq3_2 - Freq4_2;
    } else {
        tempF3 = Freq4_2 - Freq3_2;
    }

    if (tempF1 > max_instable || tempF2 > max_instable || tempF3 > max_instable) {
        Tab_Etat_2[tpa + 1] |= 0x80; // instable    1 OFFSET POUR VOIE PAIRE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }

}

//########################################FAIRE MESURE POUR VOIES PAIRES


// MESURE TPR VOIES IMPAIRES ou PAIRES  ?  � voir si les 2 voies sont mesur�es en m�me temps ou l'une apr�s l'autre

void Mesure_TPR(char fus) // si fus = 1 test pour fusible 	
{
    //int RTP_table;
    int Seuil_R;
    int R_TP1, R_TP2;



    float R_1K = 0.95; // 0.95 en Kohm
    float I_1k;
    //float Gain = 10.09;     //avec r88=100k et r89=11k







    float COEFF_R1, COEFF_R2;
    float OFFSET_R1, OFFSET_R2;

    float COEFFb_R1, COEFFb_R2;
    float OFFSETb_R1, OFFSETb_R2;

    float COEFFc_R1, COEFFc_R2;
    float OFFSETc_R1, OFFSETc_R2;



    double F;



    float I_offset_Ampli = 12; //12

    //init variables
    //mesure V_resistif
    // mesure V_48v_AM  en amont du capteur r�sistif
    // calcul de la valeur de la tension en fonction de V48AM,Vresistif,R1K,....
    // lecture table pour connaitre la position
    // lecture table2 pour avoir la valeur de fr�quence en fonction de la position





    COURTCIRCUIT = 0x00;

    COEFF_R1 = 1;
    COEFF_R2 = 1; //0.985507246;
    ;

    OFFSET_R2 = -2.797;
    OFFSET_R1 = -5.173;


    COEFFb_R1 = 1.533;
    COEFFb_R2 = 1.548;

    OFFSETb_R2 = -154.8;
    OFFSETb_R1 = -153.3;


    COEFFc_R1 = 3.735;
    COEFFc_R2 = 3.561;

    OFFSETc_R2 = -2393.0;
    OFFSETc_R1 = -2535.0;





    //goto test;




    Memo_autorisee_1 = 1;
    Memo_autorisee_2 = 1;

    //Prog_HC595(1,2,0,FUSIBLEHS); //COMMANDE RELAIS !
    creer_trame_horloge();
    //DEPART INIT
    DECH_L1 = 0;
    V_SUP1 = 0;
    UN_CPT_1 = 0;
    CPT_ON_1 = 0;
    DECH_L2 = 0;
    V_SUP2 = 0;
    UN_CPT_2 = 0;
    CPT_ON_2 = 0;



    DECH_L1 = 1;
    DECH_L2 = 1;


    //config pour 56V VOIE IMPAIRE
    UN_CPT_1 = 1; //VALIDE
    V_SUP1 = 0; //DEVALIDE 
    CPT_ON_1 = 0; //   <======== d�valide 56V SUR VOIE   par s�curit�
    DECH_L1 = 0; // valide d�charge VOIE 
    //config pour 56V VOIE PAIRE
    UN_CPT_2 = 1;
    V_SUP2 = 0;
    CPT_ON_2 = 0; //   <======== d�valide 56V SUR VOIE   par s�curit�
    DECH_L2 = 0; // valide d�charge VOIE 


    DECH_L1 = 1;
    DECH_L2 = 1;


    DELAY_MS(100); //*
    //				DELAY_SEC (1);		//							
    DECH_L1 = 0;
    DECH_L2 = 0;
    DELAY_MS(100); //*
    CPT_ON_1 = 1; //   <======== valide ligne de mesure 
    CPT_ON_2 = 1; //   <======== valide ligne de mesure 


    if (fus == 0)
        DELAY_SEC(3);


    Mesure_V_Ligne_AM_1();

    Mesure_V_Resistif_1(); // valeur = V_Resistif (int)   mesure en aval du capteur
    Mesure_V_Ligne_AM_1();


    Mesure_V_Resistif_2(); // valeur = V_Resistif (int)   mesure en aval du capteur
    Mesure_V_Ligne_AM_2();



    Mesure_V_Ligne_AVB_2();
    Mesure_V_Ligne_AVB_1();
    Mesure_V_48v_1();
    Mesure_V_48v_2();
    Mesure_I_Modul_1(); // Acquisition de la valeur CAN du courant de modulation
    Mesure_I_Modul_2(); // Acquisition de la valeur CAN du courant de modulation
    Mesure_I_Conso_1(); // Acquisition de la valeur CAN du courant de consommation
    Mesure_I_Conso_2(); // Acquisition de la valeur CAN du courant de consommation
    Mesure_I_Repos_1(); // Acquisition de la valeur CAN du courant de repos
    Mesure_I_Repos_2();

    Tab_I_Conso_2[1] = I_Conso_2;
    Tab_I_Conso_1[1] = I_Conso_1;


    // TEST CC
    if (I_Conso_1 >= Icons_cc)// AU DESSU DE 15mA (4000ohms) sur la ligne resistif >100k (0.6mA)
    {
        COURTCIRCUIT = 0xF1;
        if (I_Conso_2 >= Icons_cc)// AU DESSU DE 15mA (4000ohms) sur la ligne resistif >100k (0.6mA)
        {
            COURTCIRCUIT = 0xFF;
        }
    }
    if (I_Conso_2 >= Icons_cc)// AU DESSU DE 15mA (4000ohms) sur la ligne resistif >100k (0.6mA)
    {
        COURTCIRCUIT = 0xF2;
        if (I_Conso_1 >= Icons_cc)// AU DESSU DE 15mA (4000ohms) sur la ligne resistif >100k (0.6mA)
        {
            COURTCIRCUIT = 0xFF;
        }
    }




    DELAY_MS(100);


    //R_TP2=(127809.1343*(pow (V_Resistif_2,-1.158273627)));
    //if (V_Resistif_2<46.4172345) //mesure sur 1500K
    //R_TP2=(928775.6718*(pow (V_Resistif_2,-1.675079084)));



    //R_TP1=(127809.1343*(pow (V_Resistif_1,-1.158273627)));
    //if (V_Resistif_1<46.4172345) //mesure sur 1500K
    //R_TP1=(928775.6718*(pow (V_Resistif_1,-1.675079084)));




    R_TP1 = COEFF_R1 * 1000 * (V_48v_Calc_1 - V_Ligne_AVB_Calc_1) / I_Repos_1 + OFFSET_R1; //SI INFERIEUR A 500
    if (R_TP1 > 500) {
        R_TP1 = COEFFb_R1 * 1000 * (V_48v_Calc_1 - V_Ligne_AVB_Calc_1) / I_Repos_1 + OFFSETb_R1; //SI INFERIEUR A 500
    }
    if (R_TP1 > 1000) {
        R_TP1 = COEFFc_R1 * 1000 * (V_48v_Calc_1 - V_Ligne_AVB_Calc_1) / I_Repos_1 + OFFSETc_R1; //SI INFERIEUR A 500
    }





    R_TP2 = COEFF_R2 * 1000 * (V_48v_Calc_2 - V_Ligne_AVB_Calc_2) / I_Repos_2 + OFFSET_R2;
    if (R_TP2 > 500) {
        R_TP2 = COEFFb_R2 * 1000 * (V_48v_Calc_2 - V_Ligne_AVB_Calc_2) / I_Repos_2 + OFFSETb_R2;
    }
    if (R_TP2 > 1000) {
        R_TP2 = COEFFc_R2 * 1000 * (V_48v_Calc_2 - V_Ligne_AVB_Calc_2) / I_Repos_2 + OFFSETc_R2;
    }


//20 Tests car 20 paliers en r�sistif


    if (I_Repos_2 > 612) R_TP2 = 0;
    if (I_Repos_2 <= 612) R_TP2 = 100;
    if (I_Repos_2 <= 558) R_TP2 = 110;
    if (I_Repos_2 <= 504) R_TP2 = 122;
    if (I_Repos_2 <= 457) R_TP2 = 135;
    if (I_Repos_2 <= 412) R_TP2 = 150;
    if (I_Repos_2 <= 374) R_TP2 = 166;
    if (I_Repos_2 <= 335) R_TP2 = 186;
    if (I_Repos_2 <= 299) R_TP2 = 208;
    if (I_Repos_2 <= 270) R_TP2 = 232;
    if (I_Repos_2 <= 240) R_TP2 = 265;
    if (I_Repos_2 <= 211) R_TP2 = 301;
    if (I_Repos_2 <= 186) R_TP2 = 344;
    if (I_Repos_2 <= 163) R_TP2 = 400;
    if (I_Repos_2 <= 142) R_TP2 = 468;
    if (I_Repos_2 <= 121) R_TP2 = 568;
    if (I_Repos_2 <= 102) R_TP2 = 698;
    if (I_Repos_2 <= 84) R_TP2 = 898;
    if (I_Repos_2 <= 67) R_TP2 = 1200;
    if (I_Repos_2 <= 51) R_TP2 = 1820;
    if (I_Repos_2 <= 33) R_TP2 = 3820;
    if (I_Repos_2 <= 28) R_TP2 = 6000;




    if (I_Repos_1 > 612) R_TP1 = 0;
    if (I_Repos_1 <= 612) R_TP1 = 100;
    if (I_Repos_1 <= 558) R_TP1 = 110;
    if (I_Repos_1 <= 504) R_TP1 = 122;
    if (I_Repos_1 <= 457) R_TP1 = 135;
    if (I_Repos_1 <= 412) R_TP1 = 150;
    if (I_Repos_1 <= 374) R_TP1 = 166;
    if (I_Repos_1 <= 335) R_TP1 = 186;
    if (I_Repos_1 <= 299) R_TP1 = 208;
    if (I_Repos_1 <= 270) R_TP1 = 232;
    if (I_Repos_1 <= 240) R_TP1 = 265;
    if (I_Repos_1 <= 211) R_TP1 = 301;
    if (I_Repos_1 <= 186) R_TP1 = 344;
    if (I_Repos_1 <= 163) R_TP1 = 400;
    if (I_Repos_1 <= 142) R_TP1 = 468;
    if (I_Repos_1 <= 121) R_TP1 = 568;
    if (I_Repos_1 <= 102) R_TP1 = 698;
    if (I_Repos_1 <= 84) R_TP1 = 898;
    if (I_Repos_1 <= 67) R_TP1 = 1200;
    if (I_Repos_1 <= 51) R_TP1 = 1820;
    if (I_Repos_1 <= 33) R_TP1 = 3820;
    if (I_Repos_1 <= 28) R_TP1 = 6000;





    R_TP1 = COEFF_R1*R_TP1;
    R_TP2 = COEFF_R2*R_TP2;






    Tab_I_Repos_2[1] = I_Repos_2;

    Tab_I_Conso_2[1] = I_Conso_2;


    Tab_I_Repos_1[1] = I_Repos_1;

    Tab_I_Conso_1[1] = I_Conso_1;




    CPT_ON_1 = 0; //   <======== d�valide ligne de mesure 
    CPT_ON_2 = 0; //   <======== d�valide ligne de mesure 









    // 100K correspond au minimum de valeur d'un capteur r�sistif
    if (V_Resistif_1 < 0) {
        Freq_R1 = 8888;
    } else {


        //	V_Resistif_1 = (V_Resistif_1/ref_56V)*V_Ligne_AM_Calc_1; // SERT A READAPTER SUR LE 56V de ref de la carte A PARTIR DU 560K comme reference




        if (R_TP1 < 100) Freq_R1 = 0; // <==  ajustements 
        if (R_TP1 >= 100) Freq_R1 = 900; //100 vraie valeur
        if (R_TP1 >= 110) Freq_R1 = 934; //110 vraie valeur
        if (R_TP1 >= 122) Freq_R1 = 969; //122 vraie valeur
        if (R_TP1 >= 135) Freq_R1 = 1003;
        if (R_TP1 >= 150) Freq_R1 = 1038;
        if (R_TP1 >= 166) Freq_R1 = 1072;
        if (R_TP1 >= 186) Freq_R1 = 1107;
        if (R_TP1 >= 208) Freq_R1 = 1141;
        if (R_TP1 >= 232) Freq_R1 = 1176;
        if (R_TP1 >= 265) Freq_R1 = 1210;
        if (R_TP1 >= 301) Freq_R1 = 1245;
        if (R_TP1 >= 344) Freq_R1 = 1279;
        if (R_TP1 >= 400) Freq_R1 = 1314;
        if (R_TP1 >= 468) Freq_R1 = 1348;
        if (R_TP1 >= 568) Freq_R1 = 1383;
        if (R_TP1 >= 698) Freq_R1 = 1417;
        if (R_TP1 >= 898) Freq_R1 = 1452;
        if (R_TP1 >= 1200) Freq_R1 = 1486;
        if (R_TP1 >= 1820) Freq_R1 = 1521;
        if (R_TP1 >= 3820) Freq_R1 = 1555;
        if (R_TP1 >= 5500) Freq_R1 = 9998; //062018 1999

        Tab_Pression_1[1] = Freq_R1;

    }



    // 100K correspond au minimum de valeur d'un capteur r�sistif
    if (V_Resistif_2 < 0) {
        Freq_R2 = 8888;
    } else {


        //	V_Resistif_1 = (V_Resistif_1/ref_56V)*V_Ligne_AM_Calc_1; // SERT A READAPTER SUR LE 56V de ref de la carte A PARTIR DU 560K comme reference




        if (R_TP2 < 100) Freq_R2 = 0; // <==  ajustements 
        if (R_TP2 >= 100) Freq_R2 = 900; //100 vraie valeur
        if (R_TP2 >= 110) Freq_R2 = 934; //110 vraie valeur
        if (R_TP2 >= 122) Freq_R2 = 969; //122 vraie valeur
        if (R_TP2 >= 135) Freq_R2 = 1003;
        if (R_TP2 >= 150) Freq_R2 = 1038;
        if (R_TP2 >= 166) Freq_R2 = 1072;
        if (R_TP2 >= 186) Freq_R2 = 1107;
        if (R_TP2 >= 208) Freq_R2 = 1141;
        if (R_TP2 >= 232) Freq_R2 = 1176;
        if (R_TP2 >= 265) Freq_R2 = 1210;
        if (R_TP2 >= 301) Freq_R2 = 1245;
        if (R_TP2 >= 344) Freq_R2 = 1279;
        if (R_TP2 >= 400) Freq_R2 = 1314;
        if (R_TP2 >= 468) Freq_R2 = 1348;
        if (R_TP2 >= 568) Freq_R2 = 1383;
        if (R_TP2 >= 698) Freq_R2 = 1417;
        if (R_TP2 >= 898) Freq_R2 = 1452;
        if (R_TP2 >= 1200) Freq_R2 = 1486;
        if (R_TP2 >= 1820) Freq_R2 = 1521;
        if (R_TP2 >= 3820) Freq_R2 = 1555;
        if (R_TP2 >= 5500) Freq_R2 = 9998; //1999 062018	
        Tab_Pression_2[1] = Freq_R2;


    }



test:


    //CODE V
    //if ((fus==0)&&(FUNCTIONUSE!='E')) // SI pour test du fusible !!
    if (fus == 0) {
        Memo_autorisee_1 = 1;
        ecriture_trame_i2c_tpr(1);
        Memo_autorisee_2 = 1;
        ecriture_trame_i2c_tpr(2);

        mesure_test_cc(Tab_Voie_1[0], Tab_Voie_2[0], 1); //ecris CC ou pas 
    }

    else {
        RF1 = R_TP1;
        RF2 = R_TP2;


    }

}

void Calcul_Deb(unsigned int deb_temp) {


    // Echelle d�bitm�tre 1000 � 1500Hz pour 0L/H � 1000 et 500L/H � 1500Hz

    if (deb_temp >= 0x03E5 && deb_temp <= 0x03E8) // 3Hz de tol�rance � voir !!
    {
        deb_temp = 0x03E8; //1000
    } else if (deb_temp < 0x03E5) //997 inferieur a 1000 alors on met 9999
    {
        deb_temp = 0x270F; //9999
        //					Pres_Debit = 0; //d�bitm�tre absent
    }



    // ICI ELIMINATION DES FREQUENCES PARASITES > 1500 HZ  RAJOUT
    if (deb_temp >= 0x05DC) {
        deb_temp = 0x270F; //9999
    }










    if (deb_temp == 0x270F) //9999 si pas dans la plage
    {
        debit_cable = 0x270F; //pour indiquer que pas de r�ponse    (VALEUR 9999)!!!!!!!!!!!!!!!!!!!!!!!
    } else {
        debit_cable = (deb_temp - 0x03E8); // -1000

        //					return debit_cable;
    }
}