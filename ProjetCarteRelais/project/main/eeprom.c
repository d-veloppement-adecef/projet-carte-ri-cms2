// eeprom.c
// GESTION DE L'EEPROM

#include <stdio.h>
#include <string.h>
#include <p18f8723.h>
#include <stdlib.h>
#include <delays.h> 
#include <eeprom.h> 


/******************** WriteEEPROM ****************************************************/
void WriteEEPROM(unsigned char ad,unsigned char data) 
{ /* Ecrit un seul octet dans l'eeprom de sauvegarde du PIC18 */
static unsigned char GIE_Status; 
EEADR = ad;  			//EEPROM memory location : LSB
//EEADRH = ad >> 8;  		//EEPROM memory location : MSB
EEADRH=0;
EEDATA = data;     		//Data to be writen  
EECON1bits.EEPGD=0;    	//Enable EEPROM write 
EECON1bits.CFGS=0;   	//Enable EEPROM write 
EECON1bits.WREN = 1;  	//Enable EEPROM write 
GIE_Status = INTCONbits.GIE; //Save global interrupt enable bit 
INTCONbits.GIE=0;   	//Disable global interrupts 
EECON2 = 0x55;    		//Required sequence to start write cycle 
EECON2 = 0xAA;     		//Required sequence to start write cycle 
EECON1bits.WR = 1;    	//Required sequence to start write cycle 
INTCONbits.GIE=GIE_Status;  //Restore the original global interrupt status 
while(EECON1bits.WR);   //Wait for completion of write sequence 
PIR2bits.EEIF = 0;    	//Disable EEPROM write // EEprom Interruption flag bit 
EECON1bits.WREN = 0;  	//Disable EEPROM write 
} // fin WriteEEPROM

/******************** ReadEEPROM ****************************************************/
unsigned char ReadEEPROM(unsigned char ad) 
{ /* Lit un seul octet dans l'eeprom de sauvegarde du PIC18 */
unsigned int memory_data; 
EEADR = ad;  			//EEPROM memory location : LSB
//EEADRH = ad >> 8;  		//EEPROM memory location : MSB 
EEADRH=0;
EECON1bits.EEPGD = 0;   //Enable read sequence 
EECON1bits.CFGS = 0;   	//Enable read sequence 
EECON1bits.RD = 1;   	//Enable read sequence 
Delay1TCY( ); 			//Delay to ensure read is completed 
Delay1TCY( ); 			// ~ NOP
return (EEDATA) ; 		// memory_data         
} // fin ReadEEPROM
