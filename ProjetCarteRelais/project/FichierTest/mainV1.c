
// PIC18F8723 Configuration Bit Settings

// 'C' source line config statements
#include <p18f8723.h>


#include <stdio.h>
#include <stdlib.h>

// CONFIG1H
#pragma config OSC = HS         // Oscillator Selection bits (HS oscillator)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config IESO = OFF       // Internal/External Oscillator Switchover bit (Two-Speed Start-up disabled)
 
// CONFIG2L
#pragma config PWRT = OFF       // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = SBORDIS  // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only (SBOREN is disabled))
#pragma config BORV = 3         // Brown-out Voltage bits (Minimum setting)

// CONFIG2H
#pragma config WDT = OFF        // Watchdog Timer (WDT disabled (control is placed on the SWDTEN bit))
#pragma config WDTPS = 32768    // Watchdog Timer Postscale Select bits (1:32768)

// CONFIG3L
#pragma config MODE = MC        // Processor Data Memory Mode Select bits (Microcontroller mode)
#pragma config ADDRBW = ADDR20BIT// Address Bus Width Select bits (20-bit Address Bus)
#pragma config DATABW = DATA16BIT// Data Bus Width Select bit (16-bit External Bus mode)
#pragma config WAIT = OFF       // External Bus Data Wait Enable bit (Wait selections are unavailable for table reads and table writes)

// CONFIG3H
#pragma config CCP2MX = PORTC   // CCP2 MUX bit (ECCP2 input/output is multiplexed with RC1)
#pragma config ECCPMX = PORTE   // ECCP MUX bit (ECCP1/3 (P1B/P1C/P3B/P3C) are multiplexed onto RE6, RE5, RE4 and RE3 respectively)
#pragma config LPT1OSC = OFF    // Low-Power Timer1 Oscillator Enable bit (Timer1 configured for higher power operation)
#pragma config MCLRE = ON       // MCLR Pin Enable bit (MCLR pin enabled; RG5 input pin disabled)

// CONFIG4L
#pragma config STVREN = ON      // Stack Full/Underflow Reset Enable bit (Stack full/underflow will cause Reset)
#pragma config LVP = OFF        // Single-Supply ICSP Enable bit (Single-Supply ICSP disabled)
#pragma config BBSIZ = BB2K     // Boot Block Size Select bits (1K word (2 Kbytes) Boot Block size)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled (Legacy mode))

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not code-protected)
#pragma config CP1 = OFF        // Code Protection bit Block 1 (Block 1 (004000-007FFFh) not code-protected)
#pragma config CP2 = OFF        // Code Protection bit Block 2 (Block 2 (008000-00BFFFh) not code-protected)
#pragma config CP3 = OFF        // Code Protection bit Block 3 (Block 3 (00C000-00FFFFh) not code-protected)
#pragma config CP4 = OFF        // Code Protection bit Block 4 (Block 4 (010000-013FFFh) not code-protected)
#pragma config CP5 = OFF        // Code Protection bit Block 5 (Block 5 (014000-017FFFh) not code-protected)
#pragma config CP6 = OFF        // Code Protection bit Block 6 (Block 6 (01BFFF-018000h) not code-protected)
#pragma config CP7 = OFF        // Code Protection bit Block 7 (Block 7 (01C000-01FFFFh) not code-protected)

// CONFIG5H
#pragma config CPB = OFF        // Boot Block Code Protection bit (Boot Block (000000-0007FFh) not code-protected)
#pragma config CPD = OFF        // Data EEPROM Code Protection bit (Data EEPROM not code-protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not write-protected)
#pragma config WRT1 = OFF       // Write Protection bit Block 1 (Block 1 (004000-007FFFh) not write-protected)
#pragma config WRT2 = OFF       // Write Protection bit Block 2 (Block 2 (008000-00BFFFh) not write-protected)
#pragma config WRT3 = OFF       // Write Protection bit Block 3 (Block 3 (00C000-00FFFFh) not write-protected)
#pragma config WRT4 = OFF       // Write Protection bit Block 4 (Block 4 (010000-013FFFh) not write-protected)
#pragma config WRT5 = OFF       // Write Protection bit Block 5 (Block 5 (014000-017FFFh) not write-protected)
#pragma config WRT6 = OFF       // Write Protection bit Block 6 (Block 6 (01BFFF-018000h) not write-protected)
#pragma config WRT7 = OFF       // Write Protection bit Block 7 (Block 7 (01C000-01FFFFh) not write-protected)

// CONFIG6H
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration registers (300000-3000FFh) not write-protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot Block (000000-007FFF, 000FFF or 001FFFh) not write-protected)
#pragma config WRTD = OFF       // Data EEPROM Write Protection bit (Data EEPROM not write-protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection bit Block 0 (Block 0 (000800, 001000 or 002000-003FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection bit Block 1 (Block 1 (004000-007FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR2 = OFF      // Table Read Protection bit Block 2 (Block 2 (008000-00BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR3 = OFF      // Table Read Protection bit Block 3 (Block 3 (00C000-00FFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR4 = OFF      // Table Read Protection bit Block 4 (Block 4 (010000-013FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR5 = OFF      // Table Read Protection bit Block 5 (Block 5 (014000-017FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR6 = OFF      // Table Read Protection bit Block 6 (Block 6 (018000-01BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR7 = OFF      // Table Read Protection bit Block 7 (Block 7 (01C000-01FFFFh) not protected from table reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Boot Block Table Read Protection bit (Boot Block (000000-007FFF, 000FFF or 001FFFh) not protected from table reads executed in other blocks)




#define SPEED 51 // 624 set 9600 bauds 311 pour 19230bauds  208 pour 28800 Bauds 38 pour 38400 vitesse moyenne
#define ENVOIE TXSTA1bits.TXEN 
#define RECEPTION RCSTA1bits.CREN
#define FINTRANS TXSTA1bits.TRMT
#define FINRECEPTION PIR1bits.RCIF

void EnvoieTrame(unsigned char *cTableau, char cLongueur);
void RecoitTrame(unsigned char *cTableau, char cLongueur);


void main(void) {
        
    unsigned char cTrame[10];
    unsigned char cTestEnvoie;
    
    TRISAbits.TRISA4 = 0; //RA4 RX_TX en sortie OU DERE
    PORTAbits.RA4 = 1;
    BAUDCON1bits.BRG16 = 1;
    TXSTA1bits.BRGH = 1; //haute vitesse //Passer � 0 a vitesse moyenne
    SPBRG1 = SPEED & 0xFF; // Write baudrate to SPBRG1
    SPBRGH1 = SPEED >> 8; // For 16-bit baud rate generation
    TXSTA1bits.SYNC = 0; // Async mode
    RCSTA1bits.SPEN = 1; // Enable serial port
    
    IPR1bits.RCIP = 1; // Set high priority
    PIE1bits.RCIE = 0; // ne g�n�re pas d'IT pour la liaison s�rie
    
    RCSTA1bits.RX9 = 0; // r�ception sur 8 bits
    TXSTA1bits.TX9 = 0; // transmission sur 8 bits
    RCONbits.IPEN = 1; // Enable priority levels
    RECEPTION = 0;
    ENVOIE = 0; // transmission inhib�e

    
    while(1){
        cTestEnvoie = 0;
        RecoitTrame(&cTestEnvoie, 1);
        if(cTestEnvoie == '#'){
            cTestEnvoie = '*';
            EnvoieTrame(&cTestEnvoie, 1);
            RecoitTrame(cTrame,10);
            if(cTrame[0] == 'T' && cTrame[1] == 'e' && cTrame[2] == 's' && cTrame[3] == 't' && cTrame[4] == 'E' && cTrame[5] == 'n' && cTrame[6] == 'v' && cTrame[7] == 'o' && 
        cTrame[8] == 'i' && cTrame[9] == 'e'){
                cTestEnvoie = 'v';
            }else{
                cTestEnvoie = 'x';
            }
            EnvoieTrame(&cTestEnvoie, 1);
            cTrame[0] = 0;
            cTrame[1] = 0;
            cTrame[2] = 0;
            cTrame[3] = 0;
            cTrame[4] = 0;
            cTrame[5] = 0;
            cTrame[6] = 0;
            cTrame[7] = 0;
            cTrame[8] = 0;
            cTrame[9] = 0;
        }
    }
}

void EnvoieTrame(unsigned char *cTableau, char cLongueur){
    int iBcl;
    ENVOIE = 1; // autoriser transmission
    for(iBcl = 0; iBcl < cLongueur; iBcl++){
        TXREG1 = *(cTableau + iBcl);
        while (FINTRANS == 0);   //Attente fin transmission  
    }
    ENVOIE = 0;
}


void RecoitTrame(unsigned char *cTableau, char cLongueur){
    int iBcl;
    RECEPTION = 1; // reception
    for(iBcl = 0; iBcl < cLongueur; iBcl++){
        while (FINRECEPTION == 0);
        *(cTableau + iBcl) = RCREG1; 
    }

    RECEPTION = 0;
}

