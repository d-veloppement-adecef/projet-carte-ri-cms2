#line 1 "../project/Scrutation/RecupCapteur.c"
#line 1 "../project/Scrutation/RecupCapteur.c"
#line 2 "../project/Scrutation/RecupCapteur.c"
#line 3 "../project/Scrutation/RecupCapteur.c"
#line 4 "../project/Scrutation/RecupCapteur.c"
#line 5 "../project/Scrutation/RecupCapteur.c"



#line 1 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"

#line 5 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"
 


#line 9 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"

extern volatile near unsigned char       SSP2CON2;
extern volatile near union {
  struct {
    unsigned SEN:1;
    unsigned RSEN:1;
    unsigned PEN:1;
    unsigned RCEN:1;
    unsigned ACKEN:1;
    unsigned ACKDT:1;
    unsigned ACKSTAT:1;
    unsigned GCEN:1;
  };
  struct {
    unsigned SEN2:1;
    unsigned RSEN2:1;
    unsigned PEN2:1;
    unsigned RCEN2:1;
    unsigned ACKEN2:1;
    unsigned ACKDT2:1;
    unsigned ACKSTAT2:1;
    unsigned GCEN2:1;
  };
} SSP2CON2bits;
extern volatile near unsigned char       SSP2CON1;
extern volatile near union {
  struct {
    unsigned SSPM:4;
    unsigned CKP:1;
    unsigned SSPEN:1;
    unsigned SSPOV:1;
    unsigned WCOL:1;
  };
  struct {
    unsigned SSPM0:1;
    unsigned SSPM1:1;
    unsigned SSPM2:1;
    unsigned SSPM3:1;
  };
  struct {
    unsigned SSPM02:1;
    unsigned SSPM12:1;
    unsigned SSPM22:1;
    unsigned SSPM32:1;
    unsigned CKP2:1;
    unsigned SSPEN2:1;
    unsigned SSPOV2:1;
    unsigned WCOL2:1;
  };
} SSP2CON1bits;
extern volatile near unsigned char       SSP2STAT;
extern volatile near union {
  struct {
    unsigned BF:1;
    unsigned UA:1;
    unsigned R_NOT_W:1;
    unsigned S:1;
    unsigned P:1;
    unsigned D_NOT_A:1;
    unsigned CKE:1;
    unsigned SMP:1;
  };
  struct {
    unsigned :2;
    unsigned R_W:1;
    unsigned :2;
    unsigned D_A:1;
  };
  struct {
    unsigned :2;
    unsigned I2C_READ:1;
    unsigned I2C_START:1;
    unsigned I2C_STOP:1;
    unsigned I2C_DAT:1;
  };
  struct {
    unsigned :2;
    unsigned NOT_W:1;
    unsigned :2;
    unsigned NOT_A:1;
  };
  struct {
    unsigned :2;
    unsigned NOT_WRITE:1;
    unsigned :2;
    unsigned NOT_ADDRESS:1;
  };
  struct {
    unsigned :2;
    unsigned READ_WRITE:1;
    unsigned :2;
    unsigned DATA_ADDRESS:1;
  };
  struct {
    unsigned :2;
    unsigned R:1;
    unsigned :2;
    unsigned D:1;
  };
  struct {
    unsigned BF2:1;
    unsigned UA2:1;
    unsigned RW2:1;
    unsigned START2:1;
    unsigned STOP2:1;
    unsigned DA2:1;
    unsigned CKE2:1;
    unsigned SMP2:1;
  };
} SSP2STATbits;
extern volatile near unsigned char       SSP2ADD;
extern volatile near unsigned char       SSP2BUF;
extern volatile near unsigned char       ECCP2DEL;
extern volatile near union {
  struct {
    unsigned P2DC:7;
    unsigned P2RSEN:1;
  };
  struct {
    unsigned P2DC0:1;
    unsigned P2DC1:1;
    unsigned P2DC2:1;
    unsigned P2DC3:1;
    unsigned P2DC4:1;
    unsigned P2DC5:1;
    unsigned P2DC6:1;
  };
  struct {
    unsigned PDC0:1;
    unsigned PDC1:1;
    unsigned PDC2:1;
    unsigned PDC3:1;
    unsigned PDC4:1;
    unsigned PDC5:1;
    unsigned PDC6:1;
    unsigned PRSEN:1;
  };
} ECCP2DELbits;
extern volatile near unsigned char       ECCP2AS;
extern volatile near union {
  struct {
    unsigned PSS2BD:2;
    unsigned PSS2AC:2;
    unsigned ECCP2AS:3;
    unsigned ECCP2ASE:1;
  };
  struct {
    unsigned PSS2BD0:1;
    unsigned PSS2BD1:1;
    unsigned PSS2AC0:1;
    unsigned PSS2AC1:1;
    unsigned ECCP2AS0:1;
    unsigned ECCP2AS1:1;
    unsigned ECCP2AS2:1;
  };
  struct {
    unsigned PSSBD0:1;
    unsigned PSSBD1:1;
    unsigned PSSAC0:1;
    unsigned PSSAC1:1;
    unsigned ECCPAS0:1;
    unsigned ECCPAS1:1;
    unsigned ECCPAS2:1;
    unsigned ECCPASE:1;
  };
} ECCP2ASbits;
extern volatile near unsigned char       ECCP3DEL;
extern volatile near union {
  struct {
    unsigned P3DC:7;
    unsigned P3RSEN:1;
  };
  struct {
    unsigned P3DC0:1;
    unsigned P3DC1:1;
    unsigned P3DC2:1;
    unsigned P3DC3:1;
    unsigned P3DC4:1;
    unsigned P3DC5:1;
    unsigned P3DC6:1;
  };
  struct {
    unsigned PDC0:1;
    unsigned PDC1:1;
    unsigned PDC2:1;
    unsigned PDC3:1;
    unsigned PDC4:1;
    unsigned PDC5:1;
    unsigned PDC6:1;
    unsigned PRSEN:1;
  };
} ECCP3DELbits;
extern volatile near unsigned char       ECCP3AS;
extern volatile near union {
  struct {
    unsigned PSS3BD:2;
    unsigned PSS3AC:2;
    unsigned ECCP3AS:3;
    unsigned ECCP3ASE:1;
  };
  struct {
    unsigned PSS3BD0:1;
    unsigned PSS3BD1:1;
    unsigned PSS3AC0:1;
    unsigned PSS3AC1:1;
    unsigned ECCP3AS0:1;
    unsigned ECCP3AS1:1;
    unsigned ECCP3AS2:1;
  };
  struct {
    unsigned PSSBD0:1;
    unsigned PSSBD1:1;
    unsigned PSSAC0:1;
    unsigned PSSAC1:1;
    unsigned ECCPAS0:1;
    unsigned ECCPAS1:1;
    unsigned ECCPAS2:1;
    unsigned ECCPASE:1;
  };
} ECCP3ASbits;
extern volatile near unsigned char       RCSTA2;
extern volatile near union {
  struct {
    unsigned RX9D:1;
    unsigned OERR:1;
    unsigned FERR:1;
    unsigned ADDEN:1;
    unsigned CREN:1;
    unsigned SREN:1;
    unsigned RX9:1;
    unsigned SPEN:1;
  };
  struct {
    unsigned RCD8:1;
    unsigned :5;
    unsigned RC9:1;
  };
  struct {
    unsigned :6;
    unsigned NOT_RC8:1;
  };
  struct {
    unsigned :6;
    unsigned RC8_9:1;
  };
  struct {
    unsigned RX9D2:1;
    unsigned OERR2:1;
    unsigned FERR2:1;
    unsigned ADDEN2:1;
    unsigned CREN2:1;
    unsigned SREN2:1;
    unsigned RX92:1;
    unsigned SPEN2:1;
  };
} RCSTA2bits;
extern volatile near unsigned char       TXSTA2;
extern volatile near union {
  struct {
    unsigned TX9D:1;
    unsigned TRMT:1;
    unsigned BRGH:1;
    unsigned SENDB:1;
    unsigned SYNC:1;
    unsigned TXEN:1;
    unsigned TX9:1;
    unsigned CSRC:1;
  };
  struct {
    unsigned TXD8:1;
    unsigned :5;
    unsigned TX8_9:1;
  };
  struct {
    unsigned :6;
    unsigned NOT_TX8:1;
  };
  struct {
    unsigned TX9D2:1;
    unsigned TRMT2:1;
    unsigned BRGH2:1;
    unsigned SENDB2:1;
    unsigned SYNC2:1;
    unsigned TXEN2:1;
    unsigned TX92:1;
    unsigned CSRC2:1;
  };
} TXSTA2bits;
extern volatile near unsigned char       TXREG2;
extern volatile near unsigned char       RCREG2;
extern volatile near unsigned char       SPBRG2;
extern volatile near unsigned char       CCP5CON;
extern volatile near union {
  struct {
    unsigned CCP5M:4;
    unsigned DC5B:2;
  };
  struct {
    unsigned CCP5M0:1;
    unsigned CCP5M1:1;
    unsigned CCP5M2:1;
    unsigned CCP5M3:1;
    unsigned DCCP5Y:1;
    unsigned DCCP5X:1;
  };
  struct {
    unsigned :4;
    unsigned DC5B0:1;
    unsigned DC5B1:1;
  };
} CCP5CONbits;
extern volatile near unsigned            CCPR5;
extern volatile near unsigned char       CCPR5L;
extern volatile near unsigned char       CCPR5H;
extern volatile near unsigned char       CCP4CON;
extern volatile near union {
  struct {
    unsigned CCP4M:4;
    unsigned DC4B:2;
  };
  struct {
    unsigned CCP4M0:1;
    unsigned CCP4M1:1;
    unsigned CCP4M2:1;
    unsigned CCP4M3:1;
    unsigned DCCP4Y:1;
    unsigned DCCP4X:1;
  };
  struct {
    unsigned :4;
    unsigned DC4B0:1;
    unsigned DC4B1:1;
  };
} CCP4CONbits;
extern volatile near unsigned            CCPR4;
extern volatile near unsigned char       CCPR4L;
extern volatile near unsigned char       CCPR4H;
extern volatile near unsigned char       T4CON;
extern volatile near union {
  struct {
    unsigned T4CKPS:2;
    unsigned TMR4ON:1;
    unsigned TOUTPS:4;
  };
  struct {
    unsigned T4CKPS0:1;
    unsigned T4CKPS1:1;
    unsigned :1;
    unsigned T4OUTPS0:1;
    unsigned T4OUTPS1:1;
    unsigned T4OUTPS2:1;
    unsigned T4OUTPS3:1;
  };
} T4CONbits;
extern volatile near unsigned char       PR4;
extern volatile near unsigned char       TMR4;
extern volatile near unsigned char       ECCP1DEL;
extern volatile near union {
  struct {
    unsigned P1DC:7;
    unsigned P1RSEN:1;
  };
  struct {
    unsigned P1DC0:1;
    unsigned P1DC1:1;
    unsigned P1DC2:1;
    unsigned P1DC3:1;
    unsigned P1DC4:1;
    unsigned P1DC5:1;
    unsigned P1DC6:1;
  };
  struct {
    unsigned PDC0:1;
    unsigned PDC1:1;
    unsigned PDC2:1;
    unsigned PDC3:1;
    unsigned PDC4:1;
    unsigned PDC5:1;
    unsigned PDC6:1;
    unsigned PRSEN:1;
  };
} ECCP1DELbits;
extern volatile near unsigned char       BAUDCON2;
extern volatile near union {
  struct {
    unsigned ABDEN:1;
    unsigned WUE:1;
    unsigned :1;
    unsigned BRG16:1;
    unsigned SCKP:1;
    unsigned :1;
    unsigned RCIDL:1;
    unsigned ABDOVF:1;
  };
  struct {
    unsigned :6;
    unsigned RCMT:1;
  };
  struct {
    unsigned ABDEN2:1;
    unsigned WUE2:1;
    unsigned :1;
    unsigned BRG162:1;
    unsigned SCKP2:1;
    unsigned :1;
    unsigned RCIDL2:1;
    unsigned ABDOVF2:1;
  };
} BAUDCON2bits;
extern volatile near unsigned char       SPBRGH2;
extern volatile near unsigned char       BAUDCON;
extern volatile near union {
  struct {
    unsigned ABDEN:1;
    unsigned WUE:1;
    unsigned :1;
    unsigned BRG16:1;
    unsigned SCKP:1;
    unsigned :1;
    unsigned RCIDL:1;
    unsigned ABDOVF:1;
  };
  struct {
    unsigned :6;
    unsigned RCMT:1;
  };
  struct {
    unsigned ABDEN1:1;
    unsigned WUE1:1;
    unsigned :1;
    unsigned BRG161:1;
    unsigned SCKP1:1;
    unsigned :1;
    unsigned RCIDL1:1;
    unsigned ABDOVF1:1;
  };
} BAUDCONbits;
extern volatile near unsigned char       BAUDCON1;
extern volatile near union {
  struct {
    unsigned ABDEN:1;
    unsigned WUE:1;
    unsigned :1;
    unsigned BRG16:1;
    unsigned SCKP:1;
    unsigned :1;
    unsigned RCIDL:1;
    unsigned ABDOVF:1;
  };
  struct {
    unsigned :6;
    unsigned RCMT:1;
  };
  struct {
    unsigned ABDEN1:1;
    unsigned WUE1:1;
    unsigned :1;
    unsigned BRG161:1;
    unsigned SCKP1:1;
    unsigned :1;
    unsigned RCIDL1:1;
    unsigned ABDOVF1:1;
  };
} BAUDCON1bits;
extern volatile near unsigned char       SPBRGH;
extern volatile near unsigned char       SPBRGH1;
extern volatile near unsigned char       PORTA;
extern volatile near union {
  struct {
    unsigned RA0:1;
    unsigned RA1:1;
    unsigned RA2:1;
    unsigned RA3:1;
    unsigned RA4:1;
    unsigned RA5:1;
    unsigned RA6:1;
    unsigned RA7:1;
  };
  struct {
    unsigned :2;
    unsigned VREFM:1;
    unsigned VREFP:1;
    unsigned T0CKI:1;
    unsigned LVDIN:1;
  };
  struct {
    unsigned AN0:1;
    unsigned AN1:1;
    unsigned AN2:1;
    unsigned AN3:1;
    unsigned :1;
    unsigned AN4:1;
  };
  struct {
    unsigned :5;
    unsigned HLVDIN:1;
  };
} PORTAbits;
extern volatile near unsigned char       PORTB;
extern volatile near union {
  struct {
    unsigned RB0:1;
    unsigned RB1:1;
    unsigned RB2:1;
    unsigned RB3:1;
    unsigned RB4:1;
    unsigned RB5:1;
    unsigned RB6:1;
    unsigned RB7:1;
  };
  struct {
    unsigned INT0:1;
    unsigned INT1:1;
    unsigned INT2:1;
    unsigned INT3:1;
    unsigned KBI0:1;
    unsigned KBI1:1;
    unsigned KBI2:1;
    unsigned KBI3:1;
  };
  struct {
    unsigned FLT0:1;
    unsigned :2;
    unsigned ECCP2:1;
  };
  struct {
    unsigned :3;
    unsigned CCP2:1;
  };
  struct {
    unsigned :3;
    unsigned P2A:1;
  };
} PORTBbits;
extern volatile near unsigned char       PORTC;
extern volatile near union {
  struct {
    unsigned RC0:1;
    unsigned RC1:1;
    unsigned RC2:1;
    unsigned RC3:1;
    unsigned RC4:1;
    unsigned RC5:1;
    unsigned RC6:1;
    unsigned RC7:1;
  };
  struct {
    unsigned T1OSO:1;
    unsigned T1OSI:1;
    unsigned ECCP1:1;
    unsigned SCK:1;
    unsigned SDI:1;
    unsigned SDO:1;
    unsigned TX:1;
    unsigned RX:1;
  };
  struct {
    unsigned T13CKI:1;
    unsigned ECCP2:1;
    unsigned :1;
    unsigned SCL:1;
    unsigned SDA:1;
    unsigned :1;
    unsigned CK:1;
    unsigned DT:1;
  };
  struct {
    unsigned :1;
    unsigned CCP2:1;
    unsigned CCP1:1;
    unsigned SCL1:1;
    unsigned SDA1:1;
    unsigned :1;
    unsigned CK1:1;
    unsigned DT1:1;
  };
  struct {
    unsigned :1;
    unsigned P2A:1;
    unsigned P1A:1;
    unsigned SCK1:1;
    unsigned SDI1:1;
    unsigned SDO1:1;
    unsigned TX1:1;
    unsigned RX1:1;
  };
} PORTCbits;
extern volatile near unsigned char       PORTD;
extern volatile near union {
  struct {
    unsigned RD0:1;
    unsigned RD1:1;
    unsigned RD2:1;
    unsigned RD3:1;
    unsigned RD4:1;
    unsigned RD5:1;
    unsigned RD6:1;
    unsigned RD7:1;
  };
  struct {
    unsigned PSP0:1;
    unsigned PSP1:1;
    unsigned PSP2:1;
    unsigned PSP3:1;
    unsigned PSP4:1;
    unsigned PSP5:1;
    unsigned PSP6:1;
    unsigned PSP7:1;
  };
  struct {
    unsigned AD0:1;
    unsigned AD1:1;
    unsigned AD2:1;
    unsigned AD3:1;
    unsigned AD4:1;
    unsigned AD5:1;
    unsigned AD6:1;
    unsigned AD7:1;
  };
  struct {
    unsigned :5;
    unsigned SDA2:1;
    unsigned SCL2:1;
    unsigned SS2:1;
  };
  struct {
    unsigned :4;
    unsigned SDO2:1;
    unsigned SDI2:1;
    unsigned SCK2:1;
    unsigned NOT_SS2:1;
  };
} PORTDbits;
extern volatile near unsigned char       PORTE;
extern volatile near union {
  struct {
    unsigned RE0:1;
    unsigned RE1:1;
    unsigned RE2:1;
    unsigned RE3:1;
    unsigned RE4:1;
    unsigned RE5:1;
    unsigned RE6:1;
    unsigned RE7:1;
  };
  struct {
    unsigned RD:1;
    unsigned WR:1;
    unsigned CS:1;
    unsigned :4;
    unsigned ECCP2:1;
  };
  struct {
    unsigned NOT_RD:1;
    unsigned NOT_WR:1;
    unsigned NOT_CS:1;
    unsigned :4;
    unsigned CCP2:1;
  };
  struct {
    unsigned AD8:1;
    unsigned AD9:1;
    unsigned AD10:1;
    unsigned AD11:1;
    unsigned AD12:1;
    unsigned AD13:1;
    unsigned AD14:1;
    unsigned AD15:1;
  };
  struct {
    unsigned P2D:1;
    unsigned P2C:1;
    unsigned P2B:1;
    unsigned P3C:1;
    unsigned P3B:1;
    unsigned P1C:1;
    unsigned P1B:1;
    unsigned P2A:1;
  };
} PORTEbits;
extern volatile near unsigned char       PORTF;
extern volatile near union {
  struct {
    unsigned RF0:1;
    unsigned RF1:1;
    unsigned RF2:1;
    unsigned RF3:1;
    unsigned RF4:1;
    unsigned RF5:1;
    unsigned RF6:1;
    unsigned RF7:1;
  };
  struct {
    unsigned AN5:1;
    unsigned AN6:1;
    unsigned AN7:1;
    unsigned AN8:1;
    unsigned AN9:1;
    unsigned AN10:1;
    unsigned AN11:1;
    unsigned SS1:1;
  };
  struct {
    unsigned :1;
    unsigned C2OUT:1;
    unsigned C1OUT:1;
    unsigned :2;
    unsigned CVREF:1;
    unsigned :1;
    unsigned NOT_SS1:1;
  };
} PORTFbits;
extern volatile near unsigned char       PORTG;
extern volatile near union {
  struct {
    unsigned RG0:1;
    unsigned RG1:1;
    unsigned RG2:1;
    unsigned RG3:1;
    unsigned RG4:1;
    unsigned RG5:1;
  };
  struct {
    unsigned ECCP3:1;
    unsigned TX2:1;
    unsigned RX2:1;
    unsigned CCP4:1;
    unsigned CCP5:1;
    unsigned MCLR:1;
  };
  struct {
    unsigned P3A:1;
    unsigned CK2:1;
    unsigned DT2:1;
    unsigned P3D:1;
    unsigned P1D:1;
    unsigned NOT_MCLR:1;
  };
  struct {
    unsigned CCP3:1;
  };
} PORTGbits;
extern volatile near unsigned char       PORTH;
extern volatile near union {
  struct {
    unsigned RH0:1;
    unsigned RH1:1;
    unsigned RH2:1;
    unsigned RH3:1;
    unsigned RH4:1;
    unsigned RH5:1;
    unsigned RH6:1;
    unsigned RH7:1;
  };
  struct {
    unsigned A16:1;
    unsigned A17:1;
    unsigned A18:1;
    unsigned A19:1;
    unsigned AN12:1;
    unsigned AN13:1;
    unsigned AN14:1;
    unsigned AN15:1;
  };
  struct {
    unsigned :4;
    unsigned P3C:1;
    unsigned P3B:1;
    unsigned P1C:1;
    unsigned P1B:1;
  };
} PORTHbits;
extern volatile near unsigned char       PORTJ;
extern volatile near union {
  struct {
    unsigned RJ0:1;
    unsigned RJ1:1;
    unsigned RJ2:1;
    unsigned RJ3:1;
    unsigned RJ4:1;
    unsigned RJ5:1;
    unsigned RJ6:1;
    unsigned RJ7:1;
  };
  struct {
    unsigned ALE:1;
    unsigned OE:1;
    unsigned WRL:1;
    unsigned WRH:1;
    unsigned BA0:1;
    unsigned CE:1;
    unsigned LB:1;
    unsigned UB:1;
  };
  struct {
    unsigned :1;
    unsigned NOT_OE:1;
    unsigned NOT_WRL:1;
    unsigned NOT_WRH:1;
    unsigned :1;
    unsigned NOT_CE:1;
    unsigned NOT_LB:1;
    unsigned NOT_UB:1;
  };
} PORTJbits;
extern volatile near unsigned char       LATA;
extern volatile near struct {
  unsigned LATA0:1;
  unsigned LATA1:1;
  unsigned LATA2:1;
  unsigned LATA3:1;
  unsigned LATA4:1;
  unsigned LATA5:1;
  unsigned LATA6:1;
  unsigned LATA7:1;
} LATAbits;
extern volatile near unsigned char       LATB;
extern volatile near struct {
  unsigned LATB0:1;
  unsigned LATB1:1;
  unsigned LATB2:1;
  unsigned LATB3:1;
  unsigned LATB4:1;
  unsigned LATB5:1;
  unsigned LATB6:1;
  unsigned LATB7:1;
} LATBbits;
extern volatile near unsigned char       LATC;
extern volatile near struct {
  unsigned LATC0:1;
  unsigned LATC1:1;
  unsigned LATC2:1;
  unsigned LATC3:1;
  unsigned LATC4:1;
  unsigned LATC5:1;
  unsigned LATC6:1;
  unsigned LATC7:1;
} LATCbits;
extern volatile near unsigned char       LATD;
extern volatile near struct {
  unsigned LATD0:1;
  unsigned LATD1:1;
  unsigned LATD2:1;
  unsigned LATD3:1;
  unsigned LATD4:1;
  unsigned LATD5:1;
  unsigned LATD6:1;
  unsigned LATD7:1;
} LATDbits;
extern volatile near unsigned char       LATE;
extern volatile near struct {
  unsigned LATE0:1;
  unsigned LATE1:1;
  unsigned LATE2:1;
  unsigned LATE3:1;
  unsigned LATE4:1;
  unsigned LATE5:1;
  unsigned LATE6:1;
  unsigned LATE7:1;
} LATEbits;
extern volatile near unsigned char       LATF;
extern volatile near struct {
  unsigned LATF0:1;
  unsigned LATF1:1;
  unsigned LATF2:1;
  unsigned LATF3:1;
  unsigned LATF4:1;
  unsigned LATF5:1;
  unsigned LATF6:1;
  unsigned LATF7:1;
} LATFbits;
extern volatile near unsigned char       LATG;
extern volatile near struct {
  unsigned LATG0:1;
  unsigned LATG1:1;
  unsigned LATG2:1;
  unsigned LATG3:1;
  unsigned LATG4:1;
  unsigned LATG5:1;
} LATGbits;
extern volatile near unsigned char       LATH;
extern volatile near struct {
  unsigned LATH0:1;
  unsigned LATH1:1;
  unsigned LATH2:1;
  unsigned LATH3:1;
  unsigned LATH4:1;
  unsigned LATH5:1;
  unsigned LATH6:1;
  unsigned LATH7:1;
} LATHbits;
extern volatile near unsigned char       LATJ;
extern volatile near struct {
  unsigned LATJ0:1;
  unsigned LATJ1:1;
  unsigned LATJ2:1;
  unsigned LATJ3:1;
  unsigned LATJ4:1;
  unsigned LATJ5:1;
  unsigned LATJ6:1;
  unsigned LATJ7:1;
} LATJbits;
extern volatile near unsigned char       DDRA;
extern volatile near union {
  struct {
    unsigned TRISA0:1;
    unsigned TRISA1:1;
    unsigned TRISA2:1;
    unsigned TRISA3:1;
    unsigned TRISA4:1;
    unsigned TRISA5:1;
    unsigned TRISA6:1;
    unsigned TRISA7:1;
  };
  struct {
    unsigned RA0:1;
    unsigned RA1:1;
    unsigned RA2:1;
    unsigned RA3:1;
    unsigned RA4:1;
    unsigned RA5:1;
    unsigned RA6:1;
    unsigned RA7:1;
  };
} DDRAbits;
extern volatile near unsigned char       TRISA;
extern volatile near union {
  struct {
    unsigned TRISA0:1;
    unsigned TRISA1:1;
    unsigned TRISA2:1;
    unsigned TRISA3:1;
    unsigned TRISA4:1;
    unsigned TRISA5:1;
    unsigned TRISA6:1;
    unsigned TRISA7:1;
  };
  struct {
    unsigned RA0:1;
    unsigned RA1:1;
    unsigned RA2:1;
    unsigned RA3:1;
    unsigned RA4:1;
    unsigned RA5:1;
    unsigned RA6:1;
    unsigned RA7:1;
  };
} TRISAbits;
extern volatile near unsigned char       DDRB;
extern volatile near union {
  struct {
    unsigned TRISB0:1;
    unsigned TRISB1:1;
    unsigned TRISB2:1;
    unsigned TRISB3:1;
    unsigned TRISB4:1;
    unsigned TRISB5:1;
    unsigned TRISB6:1;
    unsigned TRISB7:1;
  };
  struct {
    unsigned RB0:1;
    unsigned RB1:1;
    unsigned RB2:1;
    unsigned RB3:1;
    unsigned RB4:1;
    unsigned RB5:1;
    unsigned RB6:1;
    unsigned RB7:1;
  };
} DDRBbits;
extern volatile near unsigned char       TRISB;
extern volatile near union {
  struct {
    unsigned TRISB0:1;
    unsigned TRISB1:1;
    unsigned TRISB2:1;
    unsigned TRISB3:1;
    unsigned TRISB4:1;
    unsigned TRISB5:1;
    unsigned TRISB6:1;
    unsigned TRISB7:1;
  };
  struct {
    unsigned RB0:1;
    unsigned RB1:1;
    unsigned RB2:1;
    unsigned RB3:1;
    unsigned RB4:1;
    unsigned RB5:1;
    unsigned RB6:1;
    unsigned RB7:1;
  };
} TRISBbits;
extern volatile near unsigned char       DDRC;
extern volatile near union {
  struct {
    unsigned TRISC0:1;
    unsigned TRISC1:1;
    unsigned TRISC2:1;
    unsigned TRISC3:1;
    unsigned TRISC4:1;
    unsigned TRISC5:1;
    unsigned TRISC6:1;
    unsigned TRISC7:1;
  };
  struct {
    unsigned RC0:1;
    unsigned RC1:1;
    unsigned RC2:1;
    unsigned RC3:1;
    unsigned RC4:1;
    unsigned RC5:1;
    unsigned RC6:1;
    unsigned RC7:1;
  };
} DDRCbits;
extern volatile near unsigned char       TRISC;
extern volatile near union {
  struct {
    unsigned TRISC0:1;
    unsigned TRISC1:1;
    unsigned TRISC2:1;
    unsigned TRISC3:1;
    unsigned TRISC4:1;
    unsigned TRISC5:1;
    unsigned TRISC6:1;
    unsigned TRISC7:1;
  };
  struct {
    unsigned RC0:1;
    unsigned RC1:1;
    unsigned RC2:1;
    unsigned RC3:1;
    unsigned RC4:1;
    unsigned RC5:1;
    unsigned RC6:1;
    unsigned RC7:1;
  };
} TRISCbits;
extern volatile near unsigned char       DDRD;
extern volatile near union {
  struct {
    unsigned TRISD0:1;
    unsigned TRISD1:1;
    unsigned TRISD2:1;
    unsigned TRISD3:1;
    unsigned TRISD4:1;
    unsigned TRISD5:1;
    unsigned TRISD6:1;
    unsigned TRISD7:1;
  };
  struct {
    unsigned RD0:1;
    unsigned RD1:1;
    unsigned RD2:1;
    unsigned RD3:1;
    unsigned RD4:1;
    unsigned RD5:1;
    unsigned RD6:1;
    unsigned RD7:1;
  };
} DDRDbits;
extern volatile near unsigned char       TRISD;
extern volatile near union {
  struct {
    unsigned TRISD0:1;
    unsigned TRISD1:1;
    unsigned TRISD2:1;
    unsigned TRISD3:1;
    unsigned TRISD4:1;
    unsigned TRISD5:1;
    unsigned TRISD6:1;
    unsigned TRISD7:1;
  };
  struct {
    unsigned RD0:1;
    unsigned RD1:1;
    unsigned RD2:1;
    unsigned RD3:1;
    unsigned RD4:1;
    unsigned RD5:1;
    unsigned RD6:1;
    unsigned RD7:1;
  };
} TRISDbits;
extern volatile near unsigned char       DDRE;
extern volatile near union {
  struct {
    unsigned TRISE0:1;
    unsigned TRISE1:1;
    unsigned TRISE2:1;
    unsigned TRISE3:1;
    unsigned TRISE4:1;
    unsigned TRISE5:1;
    unsigned TRISE6:1;
    unsigned TRISE7:1;
  };
  struct {
    unsigned RE0:1;
    unsigned RE1:1;
    unsigned RE2:1;
    unsigned RE3:1;
    unsigned RE4:1;
    unsigned RE5:1;
    unsigned RE6:1;
    unsigned RE7:1;
  };
} DDREbits;
extern volatile near unsigned char       TRISE;
extern volatile near union {
  struct {
    unsigned TRISE0:1;
    unsigned TRISE1:1;
    unsigned TRISE2:1;
    unsigned TRISE3:1;
    unsigned TRISE4:1;
    unsigned TRISE5:1;
    unsigned TRISE6:1;
    unsigned TRISE7:1;
  };
  struct {
    unsigned RE0:1;
    unsigned RE1:1;
    unsigned RE2:1;
    unsigned RE3:1;
    unsigned RE4:1;
    unsigned RE5:1;
    unsigned RE6:1;
    unsigned RE7:1;
  };
} TRISEbits;
extern volatile near unsigned char       DDRF;
extern volatile near union {
  struct {
    unsigned TRISF0:1;
    unsigned TRISF1:1;
    unsigned TRISF2:1;
    unsigned TRISF3:1;
    unsigned TRISF4:1;
    unsigned TRISF5:1;
    unsigned TRISF6:1;
    unsigned TRISF7:1;
  };
  struct {
    unsigned RF0:1;
    unsigned RF1:1;
    unsigned RF2:1;
    unsigned RF3:1;
    unsigned RF4:1;
    unsigned RF5:1;
    unsigned RF6:1;
    unsigned RF7:1;
  };
} DDRFbits;
extern volatile near unsigned char       TRISF;
extern volatile near union {
  struct {
    unsigned TRISF0:1;
    unsigned TRISF1:1;
    unsigned TRISF2:1;
    unsigned TRISF3:1;
    unsigned TRISF4:1;
    unsigned TRISF5:1;
    unsigned TRISF6:1;
    unsigned TRISF7:1;
  };
  struct {
    unsigned RF0:1;
    unsigned RF1:1;
    unsigned RF2:1;
    unsigned RF3:1;
    unsigned RF4:1;
    unsigned RF5:1;
    unsigned RF6:1;
    unsigned RF7:1;
  };
} TRISFbits;
extern volatile near unsigned char       DDRG;
extern volatile near union {
  struct {
    unsigned TRISG0:1;
    unsigned TRISG1:1;
    unsigned TRISG2:1;
    unsigned TRISG3:1;
    unsigned TRISG4:1;
  };
  struct {
    unsigned RG0:1;
    unsigned RG1:1;
    unsigned RG2:1;
    unsigned RG3:1;
    unsigned RG4:1;
  };
} DDRGbits;
extern volatile near unsigned char       TRISG;
extern volatile near union {
  struct {
    unsigned TRISG0:1;
    unsigned TRISG1:1;
    unsigned TRISG2:1;
    unsigned TRISG3:1;
    unsigned TRISG4:1;
  };
  struct {
    unsigned RG0:1;
    unsigned RG1:1;
    unsigned RG2:1;
    unsigned RG3:1;
    unsigned RG4:1;
  };
} TRISGbits;
extern volatile near unsigned char       DDRH;
extern volatile near union {
  struct {
    unsigned TRISH0:1;
    unsigned TRISH1:1;
    unsigned TRISH2:1;
    unsigned TRISH3:1;
    unsigned TRISH4:1;
    unsigned TRISH5:1;
    unsigned TRISH6:1;
    unsigned TRISH7:1;
  };
  struct {
    unsigned RH0:1;
    unsigned RH1:1;
    unsigned RH2:1;
    unsigned RH3:1;
    unsigned RH4:1;
    unsigned RH5:1;
    unsigned RH6:1;
    unsigned RH7:1;
  };
} DDRHbits;
extern volatile near unsigned char       TRISH;
extern volatile near union {
  struct {
    unsigned TRISH0:1;
    unsigned TRISH1:1;
    unsigned TRISH2:1;
    unsigned TRISH3:1;
    unsigned TRISH4:1;
    unsigned TRISH5:1;
    unsigned TRISH6:1;
    unsigned TRISH7:1;
  };
  struct {
    unsigned RH0:1;
    unsigned RH1:1;
    unsigned RH2:1;
    unsigned RH3:1;
    unsigned RH4:1;
    unsigned RH5:1;
    unsigned RH6:1;
    unsigned RH7:1;
  };
} TRISHbits;
extern volatile near unsigned char       DDRJ;
extern volatile near union {
  struct {
    unsigned TRISJ0:1;
    unsigned TRISJ1:1;
    unsigned TRISJ2:1;
    unsigned TRISJ3:1;
    unsigned TRISJ4:1;
    unsigned TRISJ5:1;
    unsigned TRISJ6:1;
    unsigned TRISJ7:1;
  };
  struct {
    unsigned RJ0:1;
    unsigned RJ1:1;
    unsigned RJ2:1;
    unsigned RJ3:1;
    unsigned RJ4:1;
    unsigned RJ5:1;
    unsigned RJ6:1;
    unsigned RJ7:1;
  };
} DDRJbits;
extern volatile near unsigned char       TRISJ;
extern volatile near union {
  struct {
    unsigned TRISJ0:1;
    unsigned TRISJ1:1;
    unsigned TRISJ2:1;
    unsigned TRISJ3:1;
    unsigned TRISJ4:1;
    unsigned TRISJ5:1;
    unsigned TRISJ6:1;
    unsigned TRISJ7:1;
  };
  struct {
    unsigned RJ0:1;
    unsigned RJ1:1;
    unsigned RJ2:1;
    unsigned RJ3:1;
    unsigned RJ4:1;
    unsigned RJ5:1;
    unsigned RJ6:1;
    unsigned RJ7:1;
  };
} TRISJbits;
extern volatile near unsigned char       OSCTUNE;
extern volatile near union {
  struct {
    unsigned TUN:5;
    unsigned :1;
    unsigned PLLEN:1;
    unsigned INTSRC:1;
  };
  struct {
    unsigned TUN0:1;
    unsigned TUN1:1;
    unsigned TUN2:1;
    unsigned TUN3:1;
    unsigned TUN4:1;
  };
} OSCTUNEbits;
extern volatile near unsigned char       MEMCON;
extern volatile near union {
  struct {
    unsigned WM:2;
    unsigned :2;
    unsigned WAIT:2;
    unsigned :1;
    unsigned EBDIS:1;
  };
  struct {
    unsigned WM0:1;
    unsigned WM1:1;
    unsigned :2;
    unsigned WAIT0:1;
    unsigned WAIT1:1;
  };
} MEMCONbits;
extern volatile near unsigned char       PIE1;
extern volatile near union {
  struct {
    unsigned TMR1IE:1;
    unsigned TMR2IE:1;
    unsigned CCP1IE:1;
    unsigned SSP1IE:1;
    unsigned TX1IE:1;
    unsigned RC1IE:1;
    unsigned ADIE:1;
    unsigned PSPIE:1;
  };
  struct {
    unsigned :3;
    unsigned SSPIE:1;
    unsigned TXIE:1;
    unsigned RCIE:1;
  };
} PIE1bits;
extern volatile near unsigned char       PIR1;
extern volatile near union {
  struct {
    unsigned TMR1IF:1;
    unsigned TMR2IF:1;
    unsigned CCP1IF:1;
    unsigned SSP1IF:1;
    unsigned TX1IF:1;
    unsigned RC1IF:1;
    unsigned ADIF:1;
    unsigned PSPIF:1;
  };
  struct {
    unsigned :3;
    unsigned SSPIF:1;
    unsigned TXIF:1;
    unsigned RCIF:1;
  };
} PIR1bits;
extern volatile near unsigned char       IPR1;
extern volatile near union {
  struct {
    unsigned TMR1IP:1;
    unsigned TMR2IP:1;
    unsigned CCP1IP:1;
    unsigned SSP1IP:1;
    unsigned TX1IP:1;
    unsigned RC1IP:1;
    unsigned ADIP:1;
    unsigned PSPIP:1;
  };
  struct {
    unsigned :3;
    unsigned SSPIP:1;
    unsigned TXIP:1;
    unsigned RCIP:1;
  };
} IPR1bits;
extern volatile near unsigned char       PIE2;
extern volatile near union {
  struct {
    unsigned CCP2IE:1;
    unsigned TMR3IE:1;
    unsigned HLVDIE:1;
    unsigned BCL1IE:1;
    unsigned EEIE:1;
    unsigned :1;
    unsigned CMIE:1;
    unsigned OSCFIE:1;
  };
  struct {
    unsigned :2;
    unsigned LVDIE:1;
    unsigned BCLIE:1;
  };
} PIE2bits;
extern volatile near unsigned char       PIR2;
extern volatile near union {
  struct {
    unsigned CCP2IF:1;
    unsigned TMR3IF:1;
    unsigned HLVDIF:1;
    unsigned BCL1IF:1;
    unsigned EEIF:1;
    unsigned :1;
    unsigned CMIF:1;
    unsigned OSCFIF:1;
  };
  struct {
    unsigned :2;
    unsigned LVDIF:1;
    unsigned BCLIF:1;
  };
} PIR2bits;
extern volatile near unsigned char       IPR2;
extern volatile near union {
  struct {
    unsigned CCP2IP:1;
    unsigned TMR3IP:1;
    unsigned HLVDIP:1;
    unsigned BCL1IP:1;
    unsigned EEIP:1;
    unsigned :1;
    unsigned CMIP:1;
    unsigned OSCFIP:1;
  };
  struct {
    unsigned :2;
    unsigned LVDIP:1;
    unsigned BCLIP:1;
  };
} IPR2bits;
extern volatile near unsigned char       PIE3;
extern volatile near struct {
  unsigned CCP3IE:1;
  unsigned CCP4IE:1;
  unsigned CCP5IE:1;
  unsigned TMR4IE:1;
  unsigned TX2IE:1;
  unsigned RC2IE:1;
  unsigned BCL2IE:1;
  unsigned SSP2IE:1;
} PIE3bits;
extern volatile near unsigned char       PIR3;
extern volatile near struct {
  unsigned CCP3IF:1;
  unsigned CCP4IF:1;
  unsigned CCP5IF:1;
  unsigned TMR4IF:1;
  unsigned TX2IF:1;
  unsigned RC2IF:1;
  unsigned BCL2IF:1;
  unsigned SSP2IF:1;
} PIR3bits;
extern volatile near unsigned char       IPR3;
extern volatile near struct {
  unsigned CCP3IP:1;
  unsigned CCP4IP:1;
  unsigned CCP5IP:1;
  unsigned TMR4IP:1;
  unsigned TX2IP:1;
  unsigned RC2IP:1;
  unsigned BCL2IP:1;
  unsigned SSP2IP:1;
} IPR3bits;
extern volatile near unsigned char       EECON1;
extern volatile near struct {
  unsigned RD:1;
  unsigned WR:1;
  unsigned WREN:1;
  unsigned WRERR:1;
  unsigned FREE:1;
  unsigned :1;
  unsigned CFGS:1;
  unsigned EEPGD:1;
} EECON1bits;
extern volatile near unsigned char       EECON2;
extern volatile near unsigned char       EEDATA;
extern volatile near unsigned char       EEADR;
extern volatile near unsigned char       EEADRH;
extern volatile near unsigned char       RCSTA;
extern volatile near union {
  struct {
    unsigned RX9D:1;
    unsigned OERR:1;
    unsigned FERR:1;
    unsigned ADDEN:1;
    unsigned CREN:1;
    unsigned SREN:1;
    unsigned RX9:1;
    unsigned SPEN:1;
  };
  struct {
    unsigned RCD8:1;
    unsigned :5;
    unsigned RC9:1;
  };
  struct {
    unsigned :6;
    unsigned NOT_RC8:1;
  };
  struct {
    unsigned :6;
    unsigned RC8_9:1;
  };
  struct {
    unsigned RX9D1:1;
    unsigned OERR1:1;
    unsigned FERR1:1;
    unsigned ADDEN1:1;
    unsigned CREN1:1;
    unsigned SREN1:1;
    unsigned RX91:1;
    unsigned SPEN1:1;
  };
} RCSTAbits;
extern volatile near unsigned char       RCSTA1;
extern volatile near union {
  struct {
    unsigned RX9D:1;
    unsigned OERR:1;
    unsigned FERR:1;
    unsigned ADDEN:1;
    unsigned CREN:1;
    unsigned SREN:1;
    unsigned RX9:1;
    unsigned SPEN:1;
  };
  struct {
    unsigned RCD8:1;
    unsigned :5;
    unsigned RC9:1;
  };
  struct {
    unsigned :6;
    unsigned NOT_RC8:1;
  };
  struct {
    unsigned :6;
    unsigned RC8_9:1;
  };
  struct {
    unsigned RX9D1:1;
    unsigned OERR1:1;
    unsigned FERR1:1;
    unsigned ADDEN1:1;
    unsigned CREN1:1;
    unsigned SREN1:1;
    unsigned RX91:1;
    unsigned SPEN1:1;
  };
} RCSTA1bits;
extern volatile near unsigned char       TXSTA;
extern volatile near union {
  struct {
    unsigned TX9D:1;
    unsigned TRMT:1;
    unsigned BRGH:1;
    unsigned SENDB:1;
    unsigned SYNC:1;
    unsigned TXEN:1;
    unsigned TX9:1;
    unsigned CSRC:1;
  };
  struct {
    unsigned TXD8:1;
    unsigned :5;
    unsigned TX8_9:1;
  };
  struct {
    unsigned :6;
    unsigned NOT_TX8:1;
  };
  struct {
    unsigned TX9D1:1;
    unsigned TRMT1:1;
    unsigned BRGH1:1;
    unsigned SENDB1:1;
    unsigned SYNC1:1;
    unsigned TXEN1:1;
    unsigned TX91:1;
    unsigned CSRC1:1;
  };
} TXSTAbits;
extern volatile near unsigned char       TXSTA1;
extern volatile near union {
  struct {
    unsigned TX9D:1;
    unsigned TRMT:1;
    unsigned BRGH:1;
    unsigned SENDB:1;
    unsigned SYNC:1;
    unsigned TXEN:1;
    unsigned TX9:1;
    unsigned CSRC:1;
  };
  struct {
    unsigned TXD8:1;
    unsigned :5;
    unsigned TX8_9:1;
  };
  struct {
    unsigned :6;
    unsigned NOT_TX8:1;
  };
  struct {
    unsigned TX9D1:1;
    unsigned TRMT1:1;
    unsigned BRGH1:1;
    unsigned SENDB1:1;
    unsigned SYNC1:1;
    unsigned TXEN1:1;
    unsigned TX91:1;
    unsigned CSRC1:1;
  };
} TXSTA1bits;
extern volatile near unsigned char       TXREG;
extern volatile near unsigned char       TXREG1;
extern volatile near unsigned char       RCREG;
extern volatile near unsigned char       RCREG1;
extern volatile near unsigned char       SPBRG;
extern volatile near unsigned char       SPBRG1;
extern volatile near unsigned char       PSPCON;
extern volatile near struct {
  unsigned :4;
  unsigned PSPMODE:1;
  unsigned IBOV:1;
  unsigned OBF:1;
  unsigned IBF:1;
} PSPCONbits;
extern volatile near unsigned char       T3CON;
extern volatile near union {
  struct {
    unsigned TMR3ON:1;
    unsigned TMR3CS:1;
    unsigned NOT_T3SYNC:1;
    unsigned T3CCP1:1;
    unsigned T3CKPS:2;
    unsigned T3CCP2:1;
    unsigned RD16:1;
  };
  struct {
    unsigned :2;
    unsigned T3SYNC:1;
    unsigned :1;
    unsigned T3CKPS0:1;
    unsigned T3CKPS1:1;
  };
  struct {
    unsigned :2;
    unsigned T3INSYNC:1;
  };
} T3CONbits;
extern volatile near unsigned char       TMR3L;
extern volatile near unsigned char       TMR3H;
extern volatile near unsigned char       CMCON;
extern volatile near union {
  struct {
    unsigned CM:3;
    unsigned CIS:1;
    unsigned C1INV:1;
    unsigned C2INV:1;
    unsigned C1OUT:1;
    unsigned C2OUT:1;
  };
  struct {
    unsigned CM0:1;
    unsigned CM1:1;
    unsigned CM2:1;
  };
} CMCONbits;
extern volatile near unsigned char       CVRCON;
extern volatile near union {
  struct {
    unsigned CVR:4;
    unsigned CVRSS:1;
    unsigned CVRR:1;
    unsigned CVROE:1;
    unsigned CVREN:1;
  };
  struct {
    unsigned CVR0:1;
    unsigned CVR1:1;
    unsigned CVR2:1;
    unsigned CVR3:1;
  };
} CVRCONbits;
extern volatile near unsigned char       ECCP1AS;
extern volatile near union {
  struct {
    unsigned PSS1BD:2;
    unsigned PSS1AC:2;
    unsigned ECCP1AS:3;
    unsigned ECCP1ASE:1;
  };
  struct {
    unsigned PSS1BD0:1;
    unsigned PSS1BD1:1;
    unsigned PSS1AC0:1;
    unsigned PSS1AC1:1;
    unsigned ECCP1AS0:1;
    unsigned ECCP1AS1:1;
    unsigned ECCP1AS2:1;
  };
  struct {
    unsigned PSSBD0:1;
    unsigned PSSBD1:1;
    unsigned PSSAC0:1;
    unsigned PSSAC1:1;
    unsigned ECCPAS0:1;
    unsigned ECCPAS1:1;
    unsigned ECCPAS2:1;
    unsigned ECCPASE:1;
  };
} ECCP1ASbits;
extern volatile near unsigned char       CCP3CON;
extern volatile near union {
  struct {
    unsigned CCP3M:4;
    unsigned DC3B:2;
    unsigned P3M:2;
  };
  struct {
    unsigned CCP3M0:1;
    unsigned CCP3M1:1;
    unsigned CCP3M2:1;
    unsigned CCP3M3:1;
    unsigned DC3B0:1;
    unsigned DC3B1:1;
    unsigned P3M0:1;
    unsigned P3M1:1;
  };
  struct {
    unsigned :4;
    unsigned CCP3Y:1;
    unsigned CCP3X:1;
  };
} CCP3CONbits;
extern volatile near unsigned char       ECCP3CON;
extern volatile near union {
  struct {
    unsigned CCP3M:4;
    unsigned DC3B:2;
    unsigned P3M:2;
  };
  struct {
    unsigned CCP3M0:1;
    unsigned CCP3M1:1;
    unsigned CCP3M2:1;
    unsigned CCP3M3:1;
    unsigned DC3B0:1;
    unsigned DC3B1:1;
    unsigned P3M0:1;
    unsigned P3M1:1;
  };
  struct {
    unsigned :4;
    unsigned CCP3Y:1;
    unsigned CCP3X:1;
  };
} ECCP3CONbits;
extern volatile near unsigned            CCPR3;
extern volatile near unsigned char       CCPR3L;
extern volatile near unsigned char       CCPR3H;
extern volatile near unsigned char       CCP2CON;
extern volatile near union {
  struct {
    unsigned CCP2M:4;
    unsigned DC2B:2;
    unsigned P2M:2;
  };
  struct {
    unsigned CCP2M0:1;
    unsigned CCP2M1:1;
    unsigned CCP2M2:1;
    unsigned CCP2M3:1;
    unsigned DC2B0:1;
    unsigned DC2B1:1;
    unsigned P2M0:1;
    unsigned P2M1:1;
  };
  struct {
    unsigned :4;
    unsigned CCP2Y:1;
    unsigned CCP2X:1;
  };
} CCP2CONbits;
extern volatile near unsigned char       ECCP2CON;
extern volatile near union {
  struct {
    unsigned CCP2M:4;
    unsigned DC2B:2;
    unsigned P2M:2;
  };
  struct {
    unsigned CCP2M0:1;
    unsigned CCP2M1:1;
    unsigned CCP2M2:1;
    unsigned CCP2M3:1;
    unsigned DC2B0:1;
    unsigned DC2B1:1;
    unsigned P2M0:1;
    unsigned P2M1:1;
  };
  struct {
    unsigned :4;
    unsigned CCP2Y:1;
    unsigned CCP2X:1;
  };
} ECCP2CONbits;
extern volatile near unsigned            CCPR2;
extern volatile near unsigned char       CCPR2L;
extern volatile near unsigned char       CCPR2H;
extern volatile near unsigned char       CCP1CON;
extern volatile near union {
  struct {
    unsigned CCP1M:4;
    unsigned DC1B:2;
    unsigned P1M:2;
  };
  struct {
    unsigned CCP1M0:1;
    unsigned CCP1M1:1;
    unsigned CCP1M2:1;
    unsigned CCP1M3:1;
    unsigned DC1B0:1;
    unsigned DC1B1:1;
    unsigned P1M0:1;
    unsigned P1M1:1;
  };
  struct {
    unsigned :4;
    unsigned CCP1Y:1;
    unsigned CCP1X:1;
  };
} CCP1CONbits;
extern volatile near unsigned char       ECCP1CON;
extern volatile near union {
  struct {
    unsigned CCP1M:4;
    unsigned DC1B:2;
    unsigned P1M:2;
  };
  struct {
    unsigned CCP1M0:1;
    unsigned CCP1M1:1;
    unsigned CCP1M2:1;
    unsigned CCP1M3:1;
    unsigned DC1B0:1;
    unsigned DC1B1:1;
    unsigned P1M0:1;
    unsigned P1M1:1;
  };
  struct {
    unsigned :4;
    unsigned CCP1Y:1;
    unsigned CCP1X:1;
  };
} ECCP1CONbits;
extern volatile near unsigned            CCPR1;
extern volatile near unsigned char       CCPR1L;
extern volatile near unsigned char       CCPR1H;
extern volatile near unsigned char       ADCON2;
extern volatile near union {
  struct {
    unsigned ADCS:3;
    unsigned ACQT:3;
    unsigned :1;
    unsigned ADFM:1;
  };
  struct {
    unsigned ADCS0:1;
    unsigned ADCS1:1;
    unsigned ADCS2:1;
    unsigned ACQT0:1;
    unsigned ACQT1:1;
    unsigned ACQT2:1;
  };
} ADCON2bits;
extern volatile near unsigned char       ADCON1;
extern volatile near union {
  struct {
    unsigned PCFG:4;
    unsigned VCFG:2;
  };
  struct {
    unsigned PCFG0:1;
    unsigned PCFG1:1;
    unsigned PCFG2:1;
    unsigned PCFG3:1;
    unsigned VCFG0:1;
    unsigned VCFG1:1;
  };
} ADCON1bits;
extern volatile near unsigned char       ADCON0;
extern volatile near union {
  struct {
    unsigned ADON:1;
    unsigned GO_NOT_DONE:1;
    unsigned CHS:4;
  };
  struct {
    unsigned :1;
    unsigned DONE:1;
  };
  struct {
    unsigned :1;
    unsigned GO_DONE:1;
    unsigned CHS0:1;
    unsigned CHS1:1;
    unsigned CHS2:1;
    unsigned CHS3:1;
  };
  struct {
    unsigned :1;
    unsigned GO:1;
  };
  struct {
    unsigned :1;
    unsigned NOT_DONE:1;
  };
} ADCON0bits;
extern volatile near unsigned            ADRES;
extern volatile near unsigned char       ADRESL;
extern volatile near unsigned char       ADRESH;
extern volatile near unsigned char       SSP1CON2;
extern volatile near struct {
  unsigned SEN:1;
  unsigned RSEN:1;
  unsigned PEN:1;
  unsigned RCEN:1;
  unsigned ACKEN:1;
  unsigned ACKDT:1;
  unsigned ACKSTAT:1;
  unsigned GCEN:1;
} SSP1CON2bits;
extern volatile near unsigned char       SSPCON2;
extern volatile near struct {
  unsigned SEN:1;
  unsigned RSEN:1;
  unsigned PEN:1;
  unsigned RCEN:1;
  unsigned ACKEN:1;
  unsigned ACKDT:1;
  unsigned ACKSTAT:1;
  unsigned GCEN:1;
} SSPCON2bits;
extern volatile near unsigned char       SSP1CON1;
extern volatile near union {
  struct {
    unsigned SSPM:4;
    unsigned CKP:1;
    unsigned SSPEN:1;
    unsigned SSPOV:1;
    unsigned WCOL:1;
  };
  struct {
    unsigned SSPM0:1;
    unsigned SSPM1:1;
    unsigned SSPM2:1;
    unsigned SSPM3:1;
  };
} SSP1CON1bits;
extern volatile near unsigned char       SSPCON1;
extern volatile near union {
  struct {
    unsigned SSPM:4;
    unsigned CKP:1;
    unsigned SSPEN:1;
    unsigned SSPOV:1;
    unsigned WCOL:1;
  };
  struct {
    unsigned SSPM0:1;
    unsigned SSPM1:1;
    unsigned SSPM2:1;
    unsigned SSPM3:1;
  };
} SSPCON1bits;
extern volatile near unsigned char       SSP1STAT;
extern volatile near union {
  struct {
    unsigned BF:1;
    unsigned UA:1;
    unsigned R_NOT_W:1;
    unsigned S:1;
    unsigned P:1;
    unsigned D_NOT_A:1;
    unsigned CKE:1;
    unsigned SMP:1;
  };
  struct {
    unsigned :2;
    unsigned R_W:1;
    unsigned :2;
    unsigned D_A:1;
  };
  struct {
    unsigned :2;
    unsigned I2C_READ:1;
    unsigned I2C_START:1;
    unsigned I2C_STOP:1;
    unsigned I2C_DAT:1;
  };
  struct {
    unsigned :2;
    unsigned NOT_W:1;
    unsigned :2;
    unsigned NOT_A:1;
  };
  struct {
    unsigned :2;
    unsigned NOT_WRITE:1;
    unsigned :2;
    unsigned NOT_ADDRESS:1;
  };
  struct {
    unsigned :2;
    unsigned READ_WRITE:1;
    unsigned :2;
    unsigned DATA_ADDRESS:1;
  };
  struct {
    unsigned :2;
    unsigned R:1;
    unsigned :2;
    unsigned D:1;
  };
} SSP1STATbits;
extern volatile near unsigned char       SSPSTAT;
extern volatile near union {
  struct {
    unsigned BF:1;
    unsigned UA:1;
    unsigned R_NOT_W:1;
    unsigned S:1;
    unsigned P:1;
    unsigned D_NOT_A:1;
    unsigned CKE:1;
    unsigned SMP:1;
  };
  struct {
    unsigned :2;
    unsigned R_W:1;
    unsigned :2;
    unsigned D_A:1;
  };
  struct {
    unsigned :2;
    unsigned I2C_READ:1;
    unsigned I2C_START:1;
    unsigned I2C_STOP:1;
    unsigned I2C_DAT:1;
  };
  struct {
    unsigned :2;
    unsigned NOT_W:1;
    unsigned :2;
    unsigned NOT_A:1;
  };
  struct {
    unsigned :2;
    unsigned NOT_WRITE:1;
    unsigned :2;
    unsigned NOT_ADDRESS:1;
  };
  struct {
    unsigned :2;
    unsigned READ_WRITE:1;
    unsigned :2;
    unsigned DATA_ADDRESS:1;
  };
  struct {
    unsigned :2;
    unsigned R:1;
    unsigned :2;
    unsigned D:1;
  };
} SSPSTATbits;
extern volatile near unsigned char       SSP1ADD;
extern volatile near unsigned char       SSPADD;
extern volatile near unsigned char       SSP1BUF;
extern volatile near unsigned char       SSPBUF;
extern volatile near unsigned char       T2CON;
extern volatile near union {
  struct {
    unsigned T2CKPS:2;
    unsigned TMR2ON:1;
    unsigned TOUTPS:4;
  };
  struct {
    unsigned T2CKPS0:1;
    unsigned T2CKPS1:1;
    unsigned :1;
    unsigned T2OUTPS0:1;
    unsigned T2OUTPS1:1;
    unsigned T2OUTPS2:1;
    unsigned T2OUTPS3:1;
  };
} T2CONbits;
extern volatile near unsigned char       PR2;
extern volatile near unsigned char       TMR2;
extern volatile near unsigned char       T1CON;
extern volatile near union {
  struct {
    unsigned TMR1ON:1;
    unsigned TMR1CS:1;
    unsigned NOT_T1SYNC:1;
    unsigned T1OSCEN:1;
    unsigned T1CKPS:2;
    unsigned T1RUN:1;
    unsigned RD16:1;
  };
  struct {
    unsigned :2;
    unsigned T1SYNC:1;
    unsigned :1;
    unsigned T1CKPS0:1;
    unsigned T1CKPS1:1;
  };
  struct {
    unsigned :2;
    unsigned T1INSYNC:1;
  };
} T1CONbits;
extern volatile near unsigned char       TMR1L;
extern volatile near unsigned char       TMR1H;
extern volatile near unsigned char       RCON;
extern volatile near union {
  struct {
    unsigned NOT_BOR:1;
    unsigned NOT_POR:1;
    unsigned NOT_PD:1;
    unsigned NOT_TO:1;
    unsigned NOT_RI:1;
    unsigned :1;
    unsigned SBOREN:1;
    unsigned IPEN:1;
  };
  struct {
    unsigned BOR:1;
    unsigned POR:1;
    unsigned PD:1;
    unsigned TO:1;
    unsigned RI:1;
  };
} RCONbits;
extern volatile near unsigned char       WDTCON;
extern volatile near union {
  struct {
    unsigned SWDTEN:1;
  };
  struct {
    unsigned SWDTE:1;
  };
} WDTCONbits;
extern volatile near unsigned char       HLVDCON;
extern volatile near union {
  struct {
    unsigned HLVDL:4;
    unsigned HLVDEN:1;
    unsigned IRVST:1;
    unsigned :1;
    unsigned VDIRMAG:1;
  };
  struct {
    unsigned HLVDL0:1;
    unsigned HLVDL1:1;
    unsigned HLVDL2:1;
    unsigned HLVDL3:1;
    unsigned :1;
    unsigned IVRST:1;
  };
  struct {
    unsigned LVV0:1;
    unsigned LVV1:1;
    unsigned LVV2:1;
    unsigned LVV3:1;
    unsigned :1;
    unsigned BGST:1;
  };
  struct {
    unsigned LVDL0:1;
    unsigned LVDL1:1;
    unsigned LVDL2:1;
    unsigned LVDL3:1;
    unsigned LVDEN:1;
  };
} HLVDCONbits;
extern volatile near unsigned char       LVDCON;
extern volatile near union {
  struct {
    unsigned HLVDL:4;
    unsigned HLVDEN:1;
    unsigned IRVST:1;
    unsigned :1;
    unsigned VDIRMAG:1;
  };
  struct {
    unsigned HLVDL0:1;
    unsigned HLVDL1:1;
    unsigned HLVDL2:1;
    unsigned HLVDL3:1;
    unsigned :1;
    unsigned IVRST:1;
  };
  struct {
    unsigned LVV0:1;
    unsigned LVV1:1;
    unsigned LVV2:1;
    unsigned LVV3:1;
    unsigned :1;
    unsigned BGST:1;
  };
  struct {
    unsigned LVDL0:1;
    unsigned LVDL1:1;
    unsigned LVDL2:1;
    unsigned LVDL3:1;
    unsigned LVDEN:1;
  };
} LVDCONbits;
extern volatile near unsigned char       OSCCON;
extern volatile near union {
  struct {
    unsigned SCS:2;
    unsigned IOFS:1;
    unsigned OSTS:1;
    unsigned IRCF:3;
    unsigned IDLEN:1;
  };
  struct {
    unsigned SCS0:1;
    unsigned SCS1:1;
    unsigned FLTS:1;
    unsigned :1;
    unsigned IRCF0:1;
    unsigned IRCF1:1;
    unsigned IRCF2:1;
  };
} OSCCONbits;
extern volatile near unsigned char       T0CON;
extern volatile near union {
  struct {
    unsigned T0PS:3;
    unsigned PSA:1;
    unsigned T0SE:1;
    unsigned T0CS:1;
    unsigned T08BIT:1;
    unsigned TMR0ON:1;
  };
  struct {
    unsigned T0PS0:1;
    unsigned T0PS1:1;
    unsigned T0PS2:1;
    unsigned T0PS3:1;
  };
} T0CONbits;
extern volatile near unsigned char       TMR0L;
extern volatile near unsigned char       TMR0H;
extern          near unsigned char       STATUS;
extern          near struct {
  unsigned C:1;
  unsigned DC:1;
  unsigned Z:1;
  unsigned OV:1;
  unsigned N:1;
} STATUSbits;
extern          near unsigned            FSR2;
extern          near unsigned char       FSR2L;
extern          near unsigned char       FSR2H;
extern volatile near unsigned char       PLUSW2;
extern volatile near unsigned char       PREINC2;
extern volatile near unsigned char       POSTDEC2;
extern volatile near unsigned char       POSTINC2;
extern          near unsigned char       INDF2;
extern          near unsigned char       BSR;
extern          near unsigned            FSR1;
extern          near unsigned char       FSR1L;
extern          near unsigned char       FSR1H;
extern volatile near unsigned char       PLUSW1;
extern volatile near unsigned char       PREINC1;
extern volatile near unsigned char       POSTDEC1;
extern volatile near unsigned char       POSTINC1;
extern          near unsigned char       INDF1;
extern          near unsigned char       WREG;
extern          near unsigned            FSR0;
extern          near unsigned char       FSR0L;
extern          near unsigned char       FSR0H;
extern volatile near unsigned char       PLUSW0;
extern volatile near unsigned char       PREINC0;
extern volatile near unsigned char       POSTDEC0;
extern volatile near unsigned char       POSTINC0;
extern          near unsigned char       INDF0;
extern volatile near unsigned char       INTCON3;
extern volatile near union {
  struct {
    unsigned INT1IF:1;
    unsigned INT2IF:1;
    unsigned INT3IF:1;
    unsigned INT1IE:1;
    unsigned INT2IE:1;
    unsigned INT3IE:1;
    unsigned INT1IP:1;
    unsigned INT2IP:1;
  };
  struct {
    unsigned INT1F:1;
    unsigned INT2F:1;
    unsigned INT3F:1;
    unsigned INT1E:1;
    unsigned INT2E:1;
    unsigned INT3E:1;
    unsigned INT1P:1;
    unsigned INT2P:1;
  };
} INTCON3bits;
extern volatile near unsigned char       INTCON2;
extern volatile near union {
  struct {
    unsigned RBIP:1;
    unsigned INT3IP:1;
    unsigned TMR0IP:1;
    unsigned INTEDG3:1;
    unsigned INTEDG2:1;
    unsigned INTEDG1:1;
    unsigned INTEDG0:1;
    unsigned NOT_RBPU:1;
  };
  struct {
    unsigned :1;
    unsigned INT3P:1;
    unsigned T0IP:1;
    unsigned :4;
    unsigned RBPU:1;
  };
} INTCON2bits;
extern volatile near unsigned char       INTCON;
extern volatile near union {
  struct {
    unsigned RBIF:1;
    unsigned INT0IF:1;
    unsigned TMR0IF:1;
    unsigned RBIE:1;
    unsigned INT0IE:1;
    unsigned TMR0IE:1;
    unsigned PEIE_GIEL:1;
    unsigned GIE_GIEH:1;
  };
  struct {
    unsigned :1;
    unsigned INT0F:1;
    unsigned T0IF:1;
    unsigned :1;
    unsigned INT0E:1;
    unsigned T0IE:1;
    unsigned PEIE:1;
    unsigned GIE:1;
  };
  struct {
    unsigned :6;
    unsigned GIEL:1;
    unsigned GIEH:1;
  };
} INTCONbits;
extern          near unsigned            PROD;
extern          near unsigned char       PRODL;
extern          near unsigned char       PRODH;
extern volatile near unsigned char       TABLAT;
extern volatile near unsigned short long TBLPTR;
extern volatile near unsigned char       TBLPTRL;
extern volatile near unsigned char       TBLPTRH;
extern volatile near unsigned char       TBLPTRU;
extern volatile near unsigned short long PC;
extern volatile near unsigned char       PCL;
extern volatile near unsigned char       PCLATH;
extern volatile near unsigned char       PCLATU;
extern volatile near unsigned char       STKPTR;
extern volatile near union {
  struct {
    unsigned STKPTR:5;
    unsigned :1;
    unsigned STKUNF:1;
    unsigned STKFUL:1;
  };
  struct {
    unsigned STKPTR0:1;
    unsigned STKPTR1:1;
    unsigned STKPTR2:1;
    unsigned STKPTR3:1;
    unsigned STKPTR4:1;
    unsigned :2;
    unsigned STKOVF:1;
  };
  struct {
    unsigned SP0:1;
    unsigned SP1:1;
    unsigned SP2:1;
    unsigned SP3:1;
    unsigned SP4:1;
  };
} STKPTRbits;
extern          near unsigned short long TOS;
extern          near unsigned char       TOSL;
extern          near unsigned char       TOSH;
extern          near unsigned char       TOSU;



#line 2435 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"
 
#line 2437 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"
#line 2438 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"


#line 2441 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"
 
#line 2443 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"
#line 2444 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"
#line 2445 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"
#line 2446 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"

#line 2448 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"
#line 2449 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"
#line 2450 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"
#line 2451 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"
#line 2452 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"


#line 2456 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"
 
#line 2458 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"


#line 2461 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"
#line 8 "../project/Scrutation/RecupCapteur.c"



#line 1 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdio.h"

#line 3 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdio.h"

#line 1 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdarg.h"
 


#line 5 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdarg.h"

typedef void* va_list;
#line 8 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdarg.h"
#line 9 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdarg.h"
#line 10 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdarg.h"
#line 11 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdarg.h"
#line 12 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdarg.h"
#line 4 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdio.h"

#line 1 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stddef.h"
 

#line 4 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stddef.h"

typedef unsigned char wchar_t;


#line 10 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stddef.h"
 
typedef signed short int ptrdiff_t;
typedef signed short int ptrdiffram_t;
typedef signed short long int ptrdiffrom_t;


#line 20 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stddef.h"
 
typedef unsigned short int size_t;
typedef unsigned short int sizeram_t;
typedef unsigned short long int sizerom_t;


#line 34 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stddef.h"
 
#line 36 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stddef.h"


#line 41 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stddef.h"
 
#line 43 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stddef.h"

#line 45 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stddef.h"
#line 5 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdio.h"



#line 9 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdio.h"
 
#line 11 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdio.h"

#line 13 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdio.h"


typedef unsigned char FILE;

 
#line 19 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdio.h"
#line 20 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdio.h"

extern FILE *stderr;
extern FILE *stdout;


int putc (auto char c, auto FILE *f);
int vsprintf (auto char *buf, auto const far  rom char *fmt, auto va_list ap);
int vprintf (auto const far  rom char *fmt, auto va_list ap);
int sprintf (auto char *buf, auto const far  rom char *fmt, ...);
int printf (auto const far  rom char *fmt, ...);
int fprintf (auto FILE *f, auto const far  rom char *fmt, ...);
int vfprintf (auto FILE *f, auto const far  rom char *fmt, auto va_list ap);
int puts (auto const far  rom char *s);
int fputs (auto const far  rom char *s, auto FILE *f);

#line 36 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdio.h"
#line 11 "../project/Scrutation/RecupCapteur.c"

#line 1 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdlib.h"
 

#line 4 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdlib.h"

#line 6 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdlib.h"

#line 9 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdlib.h"
 
 

#line 16 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdlib.h"
 
double atof (const auto char *s);

#line 28 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdlib.h"
 
signed char atob (const auto char *s);


#line 39 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdlib.h"
 
int atoi (const auto char *s);

#line 47 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdlib.h"
 
long atol (const auto char *s);

#line 58 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdlib.h"
 
unsigned long atoul (const auto char *s);


#line 71 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdlib.h"
 
char *btoa (auto signed char value, auto char *s);

#line 83 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdlib.h"
 
char *itoa (auto int value, auto char *s);

#line 95 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdlib.h"
 
char *ltoa (auto long value, auto char *s);

#line 107 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdlib.h"
 
char *ultoa (auto unsigned long value, auto char *s);
 


#line 112 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdlib.h"
 
 

#line 116 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdlib.h"
 
#line 118 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdlib.h"


#line 124 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdlib.h"
 
int rand (void);

#line 136 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdlib.h"
 
void srand (auto unsigned int seed);
 
#line 140 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdlib.h"
#line 149 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdlib.h"

#line 151 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/stdlib.h"
#line 12 "../project/Scrutation/RecupCapteur.c"

#line 1 "../project/Scrutation/RecupCapteur.h"

#line 6 "../project/Scrutation/RecupCapteur.h"
 


#line 10 "../project/Scrutation/RecupCapteur.h"

#line 12 "../project/Scrutation/RecupCapteur.h"
#line 13 "../project/Scrutation/RecupCapteur.h"
#line 14 "../project/Scrutation/RecupCapteur.h"
#line 15 "../project/Scrutation/RecupCapteur.h"
#line 16 "../project/Scrutation/RecupCapteur.h"
#line 17 "../project/Scrutation/RecupCapteur.h"
#line 18 "../project/Scrutation/RecupCapteur.h"
#line 19 "../project/Scrutation/RecupCapteur.h"
#line 20 "../project/Scrutation/RecupCapteur.h"
#line 21 "../project/Scrutation/RecupCapteur.h"
#line 22 "../project/Scrutation/RecupCapteur.h"

#line 24 "../project/Scrutation/RecupCapteur.h"
#line 25 "../project/Scrutation/RecupCapteur.h"
#line 26 "../project/Scrutation/RecupCapteur.h"
#line 27 "../project/Scrutation/RecupCapteur.h"
#line 28 "../project/Scrutation/RecupCapteur.h"
#line 29 "../project/Scrutation/RecupCapteur.h"
#line 30 "../project/Scrutation/RecupCapteur.h"

#line 32 "../project/Scrutation/RecupCapteur.h"


#line 35 "../project/Scrutation/RecupCapteur.h"
#line 36 "../project/Scrutation/RecupCapteur.h"


#line 39 "../project/Scrutation/RecupCapteur.h"

#line 41 "../project/Scrutation/RecupCapteur.h"
#line 42 "../project/Scrutation/RecupCapteur.h"
#line 43 "../project/Scrutation/RecupCapteur.h"
#line 44 "../project/Scrutation/RecupCapteur.h"
#line 45 "../project/Scrutation/RecupCapteur.h"
#line 46 "../project/Scrutation/RecupCapteur.h"
#line 47 "../project/Scrutation/RecupCapteur.h"
#line 48 "../project/Scrutation/RecupCapteur.h"
#line 49 "../project/Scrutation/RecupCapteur.h"
#line 50 "../project/Scrutation/RecupCapteur.h"
#line 51 "../project/Scrutation/RecupCapteur.h"
#line 52 "../project/Scrutation/RecupCapteur.h"
#line 53 "../project/Scrutation/RecupCapteur.h"
#line 54 "../project/Scrutation/RecupCapteur.h"
#line 55 "../project/Scrutation/RecupCapteur.h"
#line 56 "../project/Scrutation/RecupCapteur.h"

#line 58 "../project/Scrutation/RecupCapteur.h"
#line 59 "../project/Scrutation/RecupCapteur.h"
#line 60 "../project/Scrutation/RecupCapteur.h"
#line 61 "../project/Scrutation/RecupCapteur.h"
#line 62 "../project/Scrutation/RecupCapteur.h"

#line 64 "../project/Scrutation/RecupCapteur.h"
#line 65 "../project/Scrutation/RecupCapteur.h"
#line 66 "../project/Scrutation/RecupCapteur.h"
#line 67 "../project/Scrutation/RecupCapteur.h"
#line 68 "../project/Scrutation/RecupCapteur.h"
#line 69 "../project/Scrutation/RecupCapteur.h"
#line 70 "../project/Scrutation/RecupCapteur.h"
#line 71 "../project/Scrutation/RecupCapteur.h"
#line 72 "../project/Scrutation/RecupCapteur.h"
#line 73 "../project/Scrutation/RecupCapteur.h"

#line 75 "../project/Scrutation/RecupCapteur.h"
#line 76 "../project/Scrutation/RecupCapteur.h"
#line 77 "../project/Scrutation/RecupCapteur.h"
#line 78 "../project/Scrutation/RecupCapteur.h"
#line 79 "../project/Scrutation/RecupCapteur.h"
#line 80 "../project/Scrutation/RecupCapteur.h"
#line 81 "../project/Scrutation/RecupCapteur.h"

#line 83 "../project/Scrutation/RecupCapteur.h"
#line 84 "../project/Scrutation/RecupCapteur.h"
#line 85 "../project/Scrutation/RecupCapteur.h"
#line 86 "../project/Scrutation/RecupCapteur.h"
#line 87 "../project/Scrutation/RecupCapteur.h"


#line 90 "../project/Scrutation/RecupCapteur.h"
#line 91 "../project/Scrutation/RecupCapteur.h"
#line 92 "../project/Scrutation/RecupCapteur.h"
#line 93 "../project/Scrutation/RecupCapteur.h"

#line 95 "../project/Scrutation/RecupCapteur.h"
#line 96 "../project/Scrutation/RecupCapteur.h"

#line 98 "../project/Scrutation/RecupCapteur.h"
#line 99 "../project/Scrutation/RecupCapteur.h"

#line 101 "../project/Scrutation/RecupCapteur.h"


void Delay10KTCYx(auto  unsigned char);
void DELAY_SW_250MS(void);
void DELAY_MS(int ms);


void ecriture_trame_i2c(unsigned char cVoie, unsigned int* iModul, unsigned int* iConso, unsigned int* iRepos, unsigned char* cEtat, unsigned int* iPression, unsigned int* iSeuil, unsigned char* cSortie);
unsigned int calcul_CRC(unsigned char far *txt, unsigned char lg_txt);
void TRANSFORMER_TRAME_MEM_EN_COM(unsigned char* cEntree, unsigned char* cSortie);
void RECUPERE_TRAME_ET_CAL_ETAT(unsigned char* cEtat, unsigned int* iPression, unsigned int* iSeuil, char cCapteur, char cTypeCarte);
void TableExistence(unsigned char cVoie, unsigned char* cSortie);
void EcritureTrameCapteurOeil(char cVoie, char cCapteur, unsigned int* iModul, unsigned int* iConso, unsigned int* iRepos, unsigned char* cEtat, unsigned int* iPression, unsigned int* iSeuil, unsigned char* cTrame);
void EcritureTrameCapteur(char cVoie, char cCapteur, unsigned char* cEtat, unsigned int* iPression, unsigned int* iSeuil);
void EcritureTrameVoie(char cVoie, unsigned char* cEtat, unsigned int* iPression, unsigned int* iSeuil);
void IntToChar5(unsigned int value, unsigned char *chaine, char Precision);
void EcritureTrameTot(char cVoie, char cCapteur, unsigned int* iPression, unsigned char* cEtat, unsigned int* iSeuil, unsigned char* cTrame);


signed char EESequRead(unsigned char cControl, unsigned int iAdresse, unsigned char *cTrame, unsigned char cLongueur);
signed char EEPaWrite(unsigned char cControl, unsigned int iAdresse, unsigned char *cTrame);
unsigned int calcul_addmemoire(unsigned char cVoie, unsigned char cCapteur);
unsigned char choix_memoire(unsigned char cVoie);


unsigned char envoyerHORLOGE_i2c(unsigned char cNumeroH, unsigned char cADDH, unsigned char cDataH0);
void MODIFICATION_TABLE_DEXISTENCE(unsigned char* cTrame, char cEtat);
void Prog_HC595(unsigned char cImpaire, unsigned char cPaire, unsigned char cAlarme, unsigned char cFusible);
void Mesure_DEB_TPA(char cCapteurmax, char cVoieImpaire, char cVoiePaire, unsigned char cAlarme, char cFusibleHS);
void ScrutationCapteur(char cVoie, char cCapteur, unsigned char* cTrameSortie);
void ScrutationVoies(void);



void init_uc(void);
void Init_I2C(void);
void Init_RS485(void);
void Init_Tab_tpa(void);


unsigned int Mesure_I_Conso(char cMesure, unsigned int iCoeff);
unsigned int Mesure_I_Modul(char cMesure, unsigned int iCoeff);
unsigned int Mesure_I_Repos(char cMesure, unsigned int iCoeff);
unsigned int Mesure_Freq(void);
signed int CalculFreq(char cSelecFreq);
void Calcul_Deb(unsigned int* iDebit);
unsigned int acqui_ana(unsigned char adcon);
unsigned int acqui_ana_16_plus(unsigned char adcon);
unsigned char Instable(signed int* iFreq);
void mesure_test_cc(unsigned char cImpaire, unsigned char cPaire, char cCourtCircuit);


void EnvoieTrame(unsigned char *cTableau, char cLongueur);
unsigned char RecoitTrame(unsigned char *cTableau, char cLongueur);


void DebutCommunication(void);
char Communication(unsigned char* cTrame);

#line 161 "../project/Scrutation/RecupCapteur.h"

#line 13 "../project/Scrutation/RecupCapteur.c"

#line 1 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 3 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 30 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
 
#line 1 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"



#line 5 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 11 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
 



#line 1 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"

#line 3 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"

#line 5 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 7 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 9 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 11 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 13 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 15 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 17 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 19 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 21 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 23 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 25 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 27 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 29 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 31 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 33 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 35 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 37 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 39 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 41 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 43 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 45 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 47 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 49 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 51 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 53 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 55 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 57 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 59 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 61 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 63 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 65 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 67 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 69 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 71 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 73 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 75 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 77 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 79 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 81 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 83 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 85 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 87 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 89 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 91 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 93 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 95 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 97 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 99 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 101 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 103 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 105 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 107 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 109 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 111 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 113 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 115 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 117 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 119 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 121 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 123 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 125 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 127 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 129 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 131 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 133 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 135 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 137 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 139 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 141 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 143 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 145 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 147 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 149 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 151 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 153 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 155 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 157 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 159 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 161 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 163 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 165 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 167 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 169 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 171 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 173 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 175 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 177 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 179 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 181 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 183 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 185 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 187 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 189 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 191 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 193 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 195 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 197 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 199 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 201 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 203 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 205 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 207 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 209 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 211 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 213 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 215 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 217 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 219 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 221 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 223 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 225 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 227 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 229 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 231 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 233 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 235 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 237 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 239 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 241 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 243 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 245 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 247 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 249 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 251 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 253 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 255 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 257 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 259 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 261 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 263 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 265 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 267 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 269 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 271 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 273 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 275 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 277 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 279 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 281 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 283 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 285 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 287 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 289 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 291 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 293 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 295 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 297 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 299 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 301 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 303 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 305 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 307 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 309 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 311 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 313 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 315 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 317 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 319 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 321 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 323 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 325 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 327 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 329 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 331 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 333 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 335 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 337 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 339 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 341 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 343 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 345 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 347 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 349 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 351 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 353 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 355 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 357 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 359 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 361 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 363 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 365 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 367 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 369 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 371 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 373 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 375 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 377 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 379 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 381 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 383 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 385 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 387 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 389 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 391 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 393 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 395 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 397 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 399 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 401 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 403 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 405 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 407 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 409 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 411 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 413 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 415 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 417 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 419 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 421 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 423 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 425 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 427 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 429 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 431 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 433 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 435 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 437 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 439 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 441 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 443 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 445 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 447 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 449 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 451 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 453 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 455 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 457 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 459 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 1 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"

#line 5 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"
 


#line 2435 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"

#line 2441 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"

#line 2456 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"
#line 2461 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18f8723.h"
#line 459 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"

#line 461 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 463 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 465 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 467 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 469 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 471 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 473 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 475 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 477 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 479 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 481 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 483 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 485 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 487 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 489 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 491 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 493 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 495 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 497 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 499 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 501 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 503 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 505 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 507 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 509 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 511 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 513 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 515 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 517 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 519 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 521 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 523 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 525 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 527 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 529 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 531 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 533 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 535 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 537 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 539 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 541 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 543 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 545 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 547 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 549 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 551 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 553 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 555 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 557 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 559 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 561 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 563 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 565 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 567 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 569 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 571 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 573 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 575 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 577 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 579 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 581 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 583 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 585 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"

#line 587 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/p18cxxx.h"
#line 15 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"



#line 84 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 150 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 216 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 282 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 348 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 414 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 480 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 546 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 612 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 678 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 744 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 810 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 876 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 942 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 1008 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 1074 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 1140 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 1206 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 1272 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 1338 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 1404 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 1470 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 1536 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 1602 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 1668 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 1734 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 1800 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 1866 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 1932 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 1998 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 2064 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 2130 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 2196 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 2262 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 2328 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 2394 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 2460 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 2526 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 2592 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 2658 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 2724 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 2790 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 2856 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 2922 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 2988 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 3054 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 3120 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 3186 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 3252 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 3318 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 3384 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 3450 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 3516 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 3582 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 3648 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 3714 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 3780 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 3846 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 3912 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 3978 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 4044 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 4110 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 4176 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 4242 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 4308 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 4374 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 4440 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 4506 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 4572 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 4638 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 4704 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 4770 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 4836 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 4902 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 4968 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 5034 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 5100 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 5166 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 5232 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 5298 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 5364 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 5430 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 5496 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 5562 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 5628 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 5694 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 5760 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 5826 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 5892 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 5958 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 6024 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 6090 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 6156 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 6222 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 6288 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 6354 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 6420 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 6486 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 6552 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 6618 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 6684 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 6750 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 6816 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 6882 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 6948 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 7014 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 7080 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 7146 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 7212 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 7278 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 7344 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 7410 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 7476 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 7542 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 7608 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 7674 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 7740 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 7806 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 7872 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 7938 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 8004 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 8070 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 8136 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 8202 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 8268 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 8334 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 8400 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 8466 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 8532 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 8598 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 8664 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 8730 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 8796 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 8862 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 8928 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 8994 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 9060 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 9126 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 9192 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 9258 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 9324 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 9390 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 9456 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 9522 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 9588 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 9654 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 9720 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 9786 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 9852 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 9918 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 9984 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 10050 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 10116 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 10182 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 10248 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 10314 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 10380 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 10446 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 10512 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 10578 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 10644 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 10710 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 10776 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

 
 
 

 
#line 10783 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

 
 

 
#line 10789 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

 
#line 10792 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

 
#line 10795 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

 
 

 
#line 10801 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

 
#line 10804 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

 
#line 10807 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

 
#line 10810 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

 
#line 10813 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

 
#line 10816 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

 
#line 10819 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

 
#line 10822 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

 
 

 
 

 
 

 
 

 
 

 
#line 10840 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 10842 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 10908 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 10974 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 11040 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 11106 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 11172 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 11238 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 11304 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 11370 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 11436 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 11502 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 11568 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 11634 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 11700 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 11766 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 11832 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 11898 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 11964 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 12030 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 12096 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 12162 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 12228 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 12294 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 12360 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 12426 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 12492 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 12558 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 12624 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 12690 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 12756 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 12822 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 12888 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 12954 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 13020 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 13086 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 13152 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 13218 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 13284 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 13350 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 13416 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 13482 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 13548 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 13614 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 13680 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 13746 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 13812 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 13878 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 13944 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 14010 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 14076 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 14142 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 14208 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 14274 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 14340 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 14406 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 14472 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 14538 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 14604 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 14670 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 14736 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 14802 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 14868 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 14934 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 15000 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 15066 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 15132 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 15198 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 15264 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 15330 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 15396 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 15462 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 15528 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 15594 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 15660 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 15726 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 15792 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 15858 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 15924 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 15990 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 16056 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 16122 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 16188 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 16254 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 16320 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 16386 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 16452 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 16518 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 16584 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 16650 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 16716 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 16782 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 16848 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 16914 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 16980 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 17046 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 17112 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 17178 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 17244 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 17310 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 17376 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 17442 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 17508 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 17574 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 17640 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 17706 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 17772 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 17838 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 17904 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 17970 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18036 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18102 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18168 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18234 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18300 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18366 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18432 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18498 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18564 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 18566 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"




 
 
 

#line 18600 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
 
 


#line 18605 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 18607 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18608 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18609 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 18621 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18622 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18623 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18624 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 18645 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18646 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 18652 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18653 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18655 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18656 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 18658 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18663 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 18665 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18668 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 18670 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18675 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 18677 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18681 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 18689 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18690 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18695 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 18697 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18698 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18702 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 18704 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18705 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18712 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 18714 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18719 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 18726 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18727 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 18730 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18731 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18733 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18734 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 18742 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18743 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 18750 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18751 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18753 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18754 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 18758 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18759 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18761 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 18765 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18766 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18768 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
	
#line 18770 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18772 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 18777 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18778 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18784 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 18786 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18787 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18795 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 18797 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18806 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 18808 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18815 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 18817 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18825 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 18827 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18828 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18838 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 18840 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18846 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 18848 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18853 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 18857 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18858 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18868 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18870 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 18871 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18872 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18874 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18875 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 18877 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18888 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 18890 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18891 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18894 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 18902 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18903 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18908 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 18910 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18911 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18912 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18913 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18914 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 18916 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18917 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18922 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18925 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18926 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 18928 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18929 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18936 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18938 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 18939 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18940 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18944 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18945 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 18950 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18951 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18961 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 18963 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18969 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 18971 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18978 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 18980 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 18988 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 18990 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 19000 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 19003 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 19004 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 19012 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 19013 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 19015 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 19016 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 19019 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 19020 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 19027 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 19033 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 19034 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 19039 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 19042 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 19043 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 19048 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 19052 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 19053 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 19056 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 19059 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 19060 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 19064 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"


#line 19068 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 19069 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 19079 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 19080 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 19083 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 19084 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 19087 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 19088 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"

#line 19090 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/pconfig.h"
#line 32 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


 
#line 36 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 37 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 38 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 39 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 40 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 41 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 42 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

 
#line 45 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 46 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 49 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

 


#line 62 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 64 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 75 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 77 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 89 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 91 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 102 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 104 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 115 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 117 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 129 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 131 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 133 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 145 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 147 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 149 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 161 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 163 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 165 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 178 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 180 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 182 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 195 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 197 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 199 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 216 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 218 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 220 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 233 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 235 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 237 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

void OpenI2C1(  unsigned char sync_mode,  unsigned char slew );
#line 240 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 253 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 255 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 257 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

unsigned char ReadI2C1( void );
#line 260 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 269 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 271 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 272 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

unsigned char WriteI2C1(  unsigned char data_out );
#line 275 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 284 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 286 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 287 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

unsigned char getsI2C1(  unsigned char *rdptr,  unsigned char length );
#line 290 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

unsigned char putsI2C1(  unsigned char *wrptr );
#line 293 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"






unsigned char EEAckPolling1(  unsigned char control );
#line 301 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

unsigned char EEByteWrite1(  unsigned char control,
                            unsigned char address,
                            unsigned char data );
#line 306 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

unsigned int  EECurrentAddRead1(  unsigned char control );
#line 309 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

unsigned char EEPageWrite1(  unsigned char control,
                            unsigned char address,
                            unsigned char *wrptr );
#line 314 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

unsigned int  EERandomRead1(  unsigned char control,  unsigned char address );
#line 317 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

unsigned char EESequentialRead1(  unsigned char control,
                                 unsigned char address,
                                 unsigned char *rdptr,
                                 unsigned char length );
#line 323 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 324 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"



#line 328 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 330 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 340 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 342 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 353 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 355 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 367 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 369 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 380 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 382 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 393 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 395 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 397 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 408 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 421 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 435 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 448 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 461 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 465 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 477 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 479 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 491 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 493 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 505 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 507 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 520 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 522 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 535 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 537 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 539 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 555 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 558 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 574 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 576 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 577 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"



#line 591 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 593 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

void OpenI2C2(  unsigned char sync_mode,  unsigned char slew );


#line 608 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 610 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

unsigned char ReadI2C2( void );


#line 621 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 623 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

unsigned char WriteI2C2(  unsigned char data_out );


#line 634 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
 
#line 636 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

unsigned char getsI2C2(  unsigned char *rdptr,  unsigned char length );

signed char putsI2C2(  unsigned char *wrptr );





unsigned char EEAckPolling2(  unsigned char control );

unsigned char EEByteWrite2(  unsigned char control,
                            unsigned char address,
                            unsigned char data );

unsigned int  EECurrentAddRead2(  unsigned char control );

unsigned char EEPageWrite2(  unsigned char control,
                            unsigned char address,
                            unsigned char *wrptr );

unsigned int  EERandomRead2(  unsigned char control,  unsigned char address );

unsigned char EESequentialRead2(  unsigned char control,
                                 unsigned char address,
                                 unsigned char *rdptr,
                                 unsigned char length );
#line 664 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"



#line 668 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 681 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 696 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 712 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 727 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 742 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 755 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 766 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 782 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 784 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 787 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 792 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 806 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 820 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 834 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 849 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 864 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 879 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 890 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 921 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"


#line 924 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 927 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 929 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 932 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 934 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 937 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 939 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 943 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 945 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 946 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 947 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 948 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 950 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 953 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 955 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 956 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 957 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"
#line 958 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 960 "C:/Program Files (x86)/Microchip/mplabc18/v3.42/bin/../h/i2c.h"

#line 14 "../project/Scrutation/RecupCapteur.c"

#line 16 "../project/Scrutation/RecupCapteur.c"

#pragma config OSC = HS         
#pragma config FCMEN = OFF      
#pragma config IESO = OFF       


#pragma config PWRT = OFF       
#pragma config BOREN = SBORDIS  
#pragma config BORV = 3         


#pragma config WDT = OFF        
#pragma config WDTPS = 32768    


#pragma config MODE = MC        
#pragma config ADDRBW = ADDR20BIT
#pragma config DATABW = DATA16BIT
#pragma config WAIT = OFF       


#pragma config CCP2MX = PORTC   
#pragma config ECCPMX = PORTE   
#pragma config LPT1OSC = OFF    
#pragma config MCLRE = ON       


#pragma config STVREN = ON      
#pragma config LVP = OFF        
#pragma config BBSIZ = BB2K     
#pragma config XINST = OFF      


#pragma config CP0 = OFF        
#pragma config CP1 = OFF        
#pragma config CP2 = OFF        
#pragma config CP3 = OFF        
#pragma config CP4 = OFF        
#pragma config CP5 = OFF        
#pragma config CP6 = OFF        
#pragma config CP7 = OFF        


#pragma config CPB = OFF        
#pragma config CPD = OFF        


#pragma config WRT0 = OFF       
#pragma config WRT1 = OFF       
#pragma config WRT2 = OFF       
#pragma config WRT3 = OFF       
#pragma config WRT4 = OFF       
#pragma config WRT5 = OFF       
#pragma config WRT6 = OFF       
#pragma config WRT7 = OFF       


#pragma config WRTC = OFF       
#pragma config WRTB = OFF       
#pragma config WRTD = OFF       


#pragma config EBTR0 = OFF      
#pragma config EBTR1 = OFF      
#pragma config EBTR2 = OFF      
#pragma config EBTR3 = OFF      
#pragma config EBTR4 = OFF      
#pragma config EBTR5 = OFF      
#pragma config EBTR6 = OFF      
#pragma config EBTR7 = OFF      


#pragma config EBTRB = OFF      




#line 94 "../project/Scrutation/RecupCapteur.c"
#line 95 "../project/Scrutation/RecupCapteur.c"
#line 96 "../project/Scrutation/RecupCapteur.c"
#line 97 "../project/Scrutation/RecupCapteur.c"
#line 98 "../project/Scrutation/RecupCapteur.c"

#pragma udata udata1

unsigned char Nb_TPA_ok;
unsigned char fin_mesures_1; 
unsigned char fin_mesures_2; 
unsigned char Nb_Capteurs_max; 
unsigned int Freq_TPA_1;
unsigned int Freq_TPA_2;

signed int Freq1_1;
signed int Freq2_1;
signed int Freq3_1;
signed int Freq4_1;
signed int Freq1_2;
signed int Freq2_2;
signed int Freq3_2;
signed int Freq4_2;
char COURTCIRCUIT;
unsigned char Memo_autorisee_1; 
unsigned char Memo_autorisee_2; 
unsigned int Freq_A;
float Cumul_Freq_1; 
float Cumul_Freq_2; 
unsigned char Tab_code_1[17]; 
unsigned char Tab_code_2[17]; 
unsigned char dernier_TPA_1; 
unsigned char dernier_TPA_2;
unsigned char voie_valide_1;
unsigned char voie_valide_2;

unsigned int Tab_Pression_1[17];
unsigned int Tab_Pression_2[17];
unsigned int debit_cable; 

unsigned int I_Conso_1; 
unsigned int I_Conso_2; 
unsigned int I_Modul_1; 
unsigned int I_Modul_2; 
unsigned int I_Repos_1;
unsigned int I_Repos_2;
#pragma udata udata2

int pointeur_out_in;
unsigned int Tab_I_Repos_1[17];
unsigned int Tab_I_Repos_2[17];
unsigned int Tab_I_Modul_1[17];
unsigned int Tab_I_Modul_2[17];
unsigned int Tab_I_Conso_1[17];
unsigned int Tab_I_Conso_2[17];
unsigned char Tab_Etat_1[17];
unsigned char Tab_Etat_2[17];
unsigned char BREAKSCRUTATION;

#pragma udata udata3
unsigned char Tab_Voie_1[1]; 
unsigned char Tab_Voie_2[1]; 

unsigned char Tab_Horodatage[10]; 

unsigned char alarme_caM;


float tempF1;
float tempF2;
float tempF3;
float tempF4;

unsigned int t1ov_cnt;
unsigned int cpt_int2;

unsigned int Timer1L;
unsigned int Timer1H;

unsigned char No_voie; 
unsigned char No_codage; 
unsigned char No_eeprom; 

unsigned char TRAMEOUT[125];
unsigned char recherche_TPA;

#pragma udata udata4
unsigned char TMP[125];

unsigned char Tab_Constitution[13];
unsigned char Tab_Commentaire_cable[18];
unsigned char Tab_Distance[10];
unsigned int TMPINT;

unsigned int tmp_I, tmp_I2;
#pragma udata udata5
unsigned char TRAMEIN[125];

unsigned int Tab_Seuil_1[17]; 

unsigned int Tab_Seuil_2[17]; 

unsigned char Sauvegarde_en_cours;
#pragma udata udata6
char NOSAVEOEILSCRUT;
char FUSIBLEHS, ALARMECABLE;

unsigned char Tab_existance1[24]; 
unsigned char Tab_existance2[24]; 
char typedecarte, numdecarte;
unsigned char recherche_TPR;
unsigned char SAVADDE;

char SAVMEMOEIL;
unsigned int SAVADOEIL;
char FUNCTIONUSE;
unsigned int SAVADDHL;
unsigned int SAVtab;
unsigned char SAVr;

char DEMANDE_INFOS_CAPTEURS[11];

char SAVcaracOEIL;

unsigned int Info_reset;

void EnvoieTrame(unsigned char *cTableau, char cLongueur);
unsigned char RecoitTrame(unsigned char *cTableau, char cLongueur);
unsigned int acqui_ana(unsigned char adcon);
void Init_Tab_tpa(void);
void Delay10KTCYx(auto  unsigned char);
void DELAY_125MS(int ms);
void DELAY_SW_250MS();
unsigned int Mesure_I_Conso(char cMesure, unsigned int Coeff);

unsigned int Mesure_I_Modul(char cMesure, unsigned int Coeff);

unsigned int Mesure_I_Repos(char cMesure, unsigned int Coeff);
void Prog_HC595(unsigned char rel_voies_impaires, unsigned char rel_voies_paires, unsigned char alarme_ca, unsigned char test_fus);
void DELAY_MS(int ms);
void Mesure_Freq();
unsigned char Instable(signed int iFreq1, signed int iFreq2, signed int iFreq3, signed int iFreq4);
void Calcul_Deb(unsigned int* deb_temp);
void delay_qms(char tempo);
void mesure_test_cc(unsigned char n, unsigned char i);
void InitTimer0();
unsigned int acqui_ana_16_plus(unsigned char adcon);
void IntToChar(signed int value, char *chaine, int Precision);
void MODIFICATION_TABLE_DEXISTENCE(char *v, char eff, char etat);
unsigned char LIRE_HORLOGE(unsigned char zone);
void remplir_tab_presence(char v, char pouimp, char co);
void creer_trame_horloge(void);
void ecriture_trame_i2c(char vv);
void RECUPERE_TRAME_ET_CAL_ETAT(char *hk, char t, char y);
unsigned char ECRIRE_EEPROMBYTE(unsigned char ADDE, unsigned int ADDHL, unsigned char *tab, char g);
unsigned char LIRE_EEPROM(unsigned char ADDE, unsigned int ADDHL, unsigned char *tab, char r);
unsigned char putstringI2C(unsigned char *wrptr);
unsigned int calcul_addmemoire(unsigned char *tab);
unsigned char choix_memoire(unsigned char *tab);
void ecrire_tab_presence_TPA(char p);
signed char litHORLOGE_i2c(unsigned char NumeroH, unsigned char AddH);
char VOIR_TABLE_EXISTENCE_POUR_OEIL(char *v);
unsigned int calcul_CRC(char far *txt, unsigned char lg_txt);
void IntToChar5(unsigned int value, char *chaine, char Precision);
void Mesure_DEB_TPA(void);
void TRANSFORMER_TRAME_MEM_EN_COM(void);
void TRAMEENVOIRS485DONNEES(void);
void RS485(unsigned char carac);
void Init_I2C(void);
void Init_RS485(void);
unsigned char ECRIRE_HORLOGE(unsigned char zone, unsigned char Time);
unsigned char envoyerHORLOGE_i2c(unsigned char NumeroH, unsigned char ADDH, unsigned char dataH0);
void init_uc(void);


void Interruption1(void);

#pragma code highVector=0x08			

void highVector(void) {
    _asm GOTO Interruption1 _endasm 
}

#pragma interrupt Interruption1 

void Interruption1(void) {
    unsigned char sauv1;
    unsigned char sauv2;
    char dummy;
    char buffer_test[2];

    sauv1 = PRODL; 
    sauv2 = PRODH;

    
    if (PIR1bits.TMR1IF) 
    {
        t1ov_cnt++; 
        PIR1bits.TMR1IF = 0; 
    }

    

    if (INTCONbits.INT0IF == 1) 
    {
        
        

        Info_reset = 5555; 
        
        INTCONbits.INT0IF = 0; 

        {_asm reset _endasm} ; 

    }



    

    
    


    
    if (INTCON3bits.INT2IF == 1) 
    {
        if (cpt_int2 == 0) {
            TMR1H = 0;
            TMR1L = 0;
            t1ov_cnt = 0;
            T1CONbits.TMR1ON = 1; 
            PIR1bits.TMR1IF = 0; 
        }

        
        if (cpt_int2 >= 20) {
            T1CONbits.TMR1ON = 0; 
            Timer1H = TMR1H;
            Timer1L = TMR1L;
            INTCON3bits.INT2IE = 0; 
        }
        cpt_int2++;
        INTCON3bits.INT2IF = 0; 
        
    }

    PRODL = sauv1; 
    PRODH = sauv2;
    
    

}

void main(void) {

    unsigned char cTrame[10];
    unsigned char cTestEnvoie;
    unsigned char cCara;
    unsigned char cFlag_OK;
    unsigned char cWatchDog;
    char g, k[20];
    int t;
    float h;
    unsigned int iii;
    unsigned int ttt;
    unsigned int rrr;


    TRISAbits.TRISA0 = 1; 
    TRISAbits.TRISA1 = 1; 
    TRISAbits.TRISA2 = 1; 
    TRISAbits.TRISA3 = 1; 
    TRISAbits.TRISA5 = 1; 
    TRISAbits.TRISA6 = 0; 
    TRISAbits.TRISA7 = 0; 

    TRISBbits.TRISB0 = 1; 
    TRISBbits.TRISB1 = 1; 
    TRISBbits.TRISB2 = 1; 
    TRISBbits.TRISB3 = 1; 
    TRISBbits.TRISB4 = 1; 
    TRISBbits.TRISB5 = 0; 
    TRISBbits.TRISB6 = 0; 
    TRISBbits.TRISB7 = 0; 

    TRISCbits.TRISC0 = 0; 
    TRISCbits.TRISC1 = 0; 
    TRISCbits.TRISC2 = 0; 
    TRISCbits.TRISC3 = 1; 
    TRISCbits.TRISC4 = 1; 

    TRISDbits.TRISD0 = 0; 
    TRISDbits.TRISD1 = 0; 
    TRISDbits.TRISD2 = 0; 
    TRISDbits.TRISD3 = 0; 
    TRISDbits.TRISD4 = 0; 
    TRISDbits.TRISD5 = 0; 
    TRISDbits.TRISD6 = 0; 
    TRISDbits.TRISD7 = 0; 
    
    TRISEbits.TRISE0 = 0; 
    TRISEbits.TRISE1 = 0; 
    TRISEbits.TRISE2 = 0; 
    TRISEbits.TRISE3 = 0; 
    TRISEbits.TRISE4 = 0; 
    TRISEbits.TRISE5 = 0; 
    TRISEbits.TRISE6 = 0; 
    TRISEbits.TRISE7 = 0; 


    TRISFbits.TRISF0 = 1; 
    TRISFbits.TRISF1 = 1; 
    TRISFbits.TRISF2 = 1; 
    TRISFbits.TRISF3 = 1; 
    TRISFbits.TRISF4 = 1; 
    TRISFbits.TRISF5 = 1; 
    TRISFbits.TRISF6 = 0; 
    TRISFbits.TRISF7 = 0; 

    TRISGbits.TRISG0 = 0; 
    TRISGbits.TRISG1 = 0; 
    TRISGbits.TRISG2 = 0; 
    TRISGbits.TRISG3 = 0; 
    TRISGbits.TRISG4 = 0; 
    

    TRISHbits.TRISH0 = 0; 
    TRISHbits.TRISH1 = 0; 
    TRISHbits.TRISH2 = 0; 
    TRISHbits.TRISH3 = 0; 
    TRISHbits.TRISH4 = 1; 
    TRISHbits.TRISH5 = 1; 
    TRISHbits.TRISH6 = 1; 
    TRISHbits.TRISH7 = 1; 

    TRISJbits.TRISJ0 = 0; 
    TRISJbits.TRISJ1 = 0; 
    TRISJbits.TRISJ2 = 0; 
    TRISJbits.TRISJ3 = 1; 
    TRISJbits.TRISJ4 = 1; 
    TRISJbits.TRISJ5 = 0; 
    TRISJbits.TRISJ6 = 0; 
    TRISJbits.TRISJ7 = 0; 

    PORTAbits.RA4 = 1;

    TRISCbits.TRISC5 = 0;
    BAUDCON1bits.BRG16 = 1;
    TXSTA1bits.BRGH = 1; 
    SPBRG1 = 51  & 0xFF; 
    SPBRGH1 = 51  >> 8; 
    TXSTA1bits.SYNC = 0; 
    RCSTA1bits.SPEN = 1; 

    IPR1bits.RCIP = 1; 
    PIE1bits.RCIE = 0; 

    RCSTA1bits.RX9 = 0; 
    TXSTA1bits.TX9 = 0; 
    RCONbits.IPEN = 1; 
    RCSTA1bits.CREN  = 0;
    TXSTA1bits.TXEN  = 0; 



    Init_I2C();
    Init_RS485();
    init_uc();
    PORTJbits.RJ5  = 0; 
    I2C_INTERNE = 0; 



    INTCONbits.GIE = 0;
    INTCONbits.PEIE = 0;
    INTCON2bits.RBIP = 0; 
    INTCONbits.RBIE = 0; 
    
    INTCONbits.TMR0IE = 0; 
    INTCONbits.INT0IE = 1; 
    INTCON2bits.INTEDG0 = 0; 
    
    INTCON2bits.INTEDG1 = 1; 

    INTCON3bits.INT1IF = 0; 

    INTCON3bits.INT1IE = 1; 



    INTCONbits.GIE = 1;

    TMP[2] = 'a';
    MODIFICATION_TABLE_DEXISTENCE(&TRAMEOUT, 2, 4);


    ECRIRE_HORLOGE(0x07, 0b00010000); 

    while (1) {




        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        

        


        


        INTCON3bits.INT1IE = 0; 


        TRAMEIN[0] = '#';
        TRAMEIN[1] = 'C';
        TRAMEIN[2] = 'M';
        TRAMEIN[3] = 'R';
        TRAMEIN[4] = '4';
        TRAMEIN[5] = 'O';
        TRAMEIN[6] = 'E';
        TRAMEIN[7] = 'I';
        TRAMEIN[8] = '0';
        TRAMEIN[9] = '2';
        TRAMEIN[10] = '0';
        TRAMEIN[11] = '0';
        TRAMEIN[12] = '1';
        TRAMEIN[13] = '2';
        TRAMEIN[14] = '2';
        TRAMEIN[15] = '2';
        TRAMEIN[16] = '2';
        TRAMEIN[17] = '2';
        TRAMEIN[18] = '*';


        DEMANDE_INFOS_CAPTEURS[1] = TRAMEIN[8];
        DEMANDE_INFOS_CAPTEURS[2] = TRAMEIN[9];
        DEMANDE_INFOS_CAPTEURS[3] = TRAMEIN[10];
        DEMANDE_INFOS_CAPTEURS[4] = TRAMEIN[11];
        DEMANDE_INFOS_CAPTEURS[5] = TRAMEIN[12]; 

        g = VOIR_TABLE_EXISTENCE_POUR_OEIL(&DEMANDE_INFOS_CAPTEURS); 


        for (t = 0; t < 21; t++) {
            k[t] = TMP[t];
        }

        if (TMP[1] % 2 == 1) 
        {
            NOSAVEOEILSCRUT = 1; 
            voie_valide_2 = 0;
            voie_valide_1 = 1;
            Tab_Voie_1[0] = TMP[1];
            Tab_Voie_2[0] = TMP[1] + 1;
        }
        if (TMP[1] % 2 == 0) 
        {
            NOSAVEOEILSCRUT = 2; 
            voie_valide_1 = 0;
            voie_valide_2 = 1;
            Tab_Voie_2[0] = TMP[1];
            Tab_Voie_1[0] = TMP[1] - 1;
        }

        



        DEMANDE_INFOS_CAPTEURS[0] = DEMANDE_INFOS_CAPTEURS[1];
        DEMANDE_INFOS_CAPTEURS[1] = DEMANDE_INFOS_CAPTEURS[2];
        DEMANDE_INFOS_CAPTEURS[2] = DEMANDE_INFOS_CAPTEURS[3];
        DEMANDE_INFOS_CAPTEURS[3] = DEMANDE_INFOS_CAPTEURS[4];
        DEMANDE_INFOS_CAPTEURS[4] = DEMANDE_INFOS_CAPTEURS[5];


        
        if (DEMANDE_INFOS_CAPTEURS[2] == '2')
            Tab_Voie_1[0] = 0;
        if (DEMANDE_INFOS_CAPTEURS[2] == '4')
            Tab_Voie_1[0] = 0;
        if (DEMANDE_INFOS_CAPTEURS[2] == '6')
            Tab_Voie_1[0] = 0;
        if (DEMANDE_INFOS_CAPTEURS[2] == '8')
            Tab_Voie_1[0] = 0;
        if (DEMANDE_INFOS_CAPTEURS[2] == '0')
            Tab_Voie_1[0] = 0;

        
        if (DEMANDE_INFOS_CAPTEURS[2] == '1')
            Tab_Voie_2[0] = 0;
        if (DEMANDE_INFOS_CAPTEURS[2] == '3')
            Tab_Voie_2[0] = 0;
        if (DEMANDE_INFOS_CAPTEURS[2] == '5')
            Tab_Voie_2[0] = 0;
        if (DEMANDE_INFOS_CAPTEURS[2] == '7')
            Tab_Voie_2[0] = 0;
        if (DEMANDE_INFOS_CAPTEURS[2] == '9')
            Tab_Voie_2[0] = 0;

        do {
            Prog_HC595(Tab_Voie_1[0], Tab_Voie_2[0], alarme_caM, FUSIBLEHS); 
            
            for (t = 3; t <= 19; t++) {
                if (k[t] == 'A') 
                {
                    dernier_TPA_2 = t - 2;
                    dernier_TPA_1 = t - 2;
                }
            }

            recherche_TPA = 0;
            Mesure_DEB_TPA();
            LIRE_EEPROM(2, 128, &TRAMEIN, 1);
            TMP[0] = '#';
            TMP[1] = DEMANDE_INFOS_CAPTEURS[0];
            TMP[2] = DEMANDE_INFOS_CAPTEURS[1];
            TMP[3] = DEMANDE_INFOS_CAPTEURS[2];
            TMP[4] = DEMANDE_INFOS_CAPTEURS[3];
            TMP[5] = DEMANDE_INFOS_CAPTEURS[4];
            
            TMP[6] = 0; 
            
            ttt = 0;
            iii = TMP[3];
            iii = iii - 48;
            ttt = ttt + 1 * iii;
            iii = TMP[2];
            iii = iii - 48;
            ttt = ttt + 10 * iii;
            iii = TMP[1];
            iii = iii - 48;
            ttt = ttt + 100 * iii;

            rrr = 2176 * ttt;

            ttt = 0;
            iii = TMP[5] - 48;
            ttt = 1 * iii;
            iii = TMP[4] - 48;
            ttt = ttt + 10 * iii;
            iii = ttt;
            ttt = rrr + 128 * iii;


            SAVMEMOEIL = 0; 

            SAVADOEIL = ttt;
            LIRE_EEPROM(2, 128, &TRAMEIN, 1); 
            FUNCTIONUSE = 'O';

            delay_qms(10);

            FUNCTIONUSE = 'N';

            TRAMEOUT[0] = '#';
            TRAMEOUT[1] = 'R';
            TRAMEOUT[2] = '4';
            TRAMEOUT[3] = 'C';
            TRAMEOUT[4] = 'M';
            TRAMEOUT[5] = 'O';
            TRAMEOUT[6] = 'E';
            TRAMEOUT[7] = 'I';

            TRANSFORMER_TRAME_MEM_EN_COM();
            pointeur_out_in = 92; 
            calcul_CRC(TRAMEOUT, pointeur_out_in - 1); 
            TRAMEOUT[pointeur_out_in + 5] = '*'; 

            if (TRAMEIN[0] == 0xFF) 
            {
                TRAMEOUT[8] = '*';
            }
            FUNCTIONUSE = 'O';
            TRAMEENVOIRS485DONNEES(); 
            FUNCTIONUSE = 'N';


        } while (PORTBbits.RB1);
        

        NOSAVEOEILSCRUT = 0;

        cTrame[0] = 0;
        cTrame[1] = 0;
        cTrame[2] = 0;
        cTrame[3] = 0;
        cTrame[4] = 0;
        cTrame[5] = 0;
        cTrame[6] = 0;
        cTrame[7] = 0;
        cTrame[8] = 0;
        cTrame[9] = 0;
    }

}

void EnvoieTrame(unsigned char *cTableau, char cLongueur) {
    int iBcl;
    TXSTA1bits.TXEN  = 1; 
    for (iBcl = 0; iBcl < cLongueur; iBcl++) {
        TXREG1 = *(cTableau + iBcl);
        while (TXSTA1bits.TRMT  == 0); 
    }
    TXSTA1bits.TXEN  = 0;
}

unsigned char RecoitTrame(unsigned char *cTableau, char cLongueur) {
    int iBcl;
    unsigned char cErreur;
    unsigned int iWatchDog;
    iWatchDog = 0;
    cErreur = 'a';
    RCSTA1bits.CREN  = 1; 
    for (iBcl = 0; iBcl < cLongueur; iBcl++) {
        if (iWatchDog < 20000) {
            iWatchDog = 0;
            while ((PIR1bits.RCIF  == 0) && (iWatchDog < 20000)) {
                
                iWatchDog++;
            }
            *(cTableau + iBcl) = RCREG1;
            if (RCSTAbits.OERR || iWatchDog == 20000) {
                
                RCSTA1bits.CREN  = 0;
                RCSTA1bits.CREN  = 1;
                cErreur = 'o';
                *(cTableau + iBcl) = 0; 
            } else if (RCSTAbits.FERR) {
                cErreur = 'f';
            }
        } else {
            RCREG1;
            *(cTableau + iBcl) = 0;
        }
    }
    RCSTA1bits.CREN  = 0;
    return cErreur;
}

unsigned int acqui_ana(unsigned char adcon) {


    
    ADCON0 = adcon; 
    ADCON0bits.ADON  = 1;
    ADCON0bits.GO  = 1;

    while (ADCON0bits.DONE ) {
        
    }

    return ADRES;
} 

unsigned int acqui_ana_16_plus(unsigned char adcon) {



    unsigned long II;
    int i; 



    II = 0;

    for (i = 0; i < 0x10; i++) 
    {
        II = II + acqui_ana(adcon);
        Delay10KTCYx(1);
    }
    II >>= 4; 

    return ((int) II); 
}

void Mesure_DEB_TPA(void) {
    unsigned int TEST; 

    int tpa;
    int scrut_TP; 
    int nb; 

    unsigned int I_Conso1_1; 
    unsigned int I_Conso2_1; 
    unsigned int I_Conso1_2; 
    unsigned int I_Conso2_2; 

    unsigned int I_Repos1_1; 
    unsigned int I_Repos2_1; 
    unsigned int I_Repos1_2; 
    unsigned int I_Repos2_2; 




    
    
    
    

    PORTHbits.RH3  = 0;
    Memo_autorisee_1 = 1;
    Memo_autorisee_2 = 1;
    
    COURTCIRCUIT = 0x00;
    Freq_A = 0x0000;
    Freq1_1 = 0x0000;
    Freq2_1 = 0x0000;
    Freq3_1 = 0x0000;
    Freq4_1 = 0x0000;
    Freq1_2 = 0x0000;
    Freq2_2 = 0x0000;
    Freq3_2 = 0x0000;
    Freq4_2 = 0x0000;

    Cumul_Freq_1 = 0x0000; 
    Cumul_Freq_2 = 0x0000; 

    I_Conso1_1 = 0x0000;
    I_Conso2_1 = 0x0000;
    I_Conso1_2 = 0x0000;
    I_Conso2_2 = 0x0000;
    I_Conso_1 = 0;
    I_Conso_2 = 0;
    Init_Tab_tpa();

    Nb_TPA_ok = 0; 
    fin_mesures_1 = 0; 
    fin_mesures_2 = 0; 



    if (recherche_TPA) 
    {
        Nb_Capteurs_max = 17; 
        for (nb = 0; nb <= 16; nb++) 
        {
            Tab_code_1[nb] = 1;
            Tab_code_2[nb] = 1;
        }
    } else {
        if (dernier_TPA_1 >= dernier_TPA_2) {
            Nb_Capteurs_max = dernier_TPA_1; 
        } else {
            Nb_Capteurs_max = dernier_TPA_2;
        }
    }


    tempF1 = 0x0000;
    tempF2 = 0x0000;
    tempF3 = 0x0000;
    tempF4 = 0x0000;







    
    PORTFbits.RF6  = 0;
    LATJbits.LATJ6  = 0;
    LATGbits.LATG2  = 0;
    LATGbits.LATG1  = 0;
    PORTFbits.RF7  = 0;
    PORTJbits.RJ7  = 0;
    LATGbits.LATG4  = 0;
    PORTGbits.RG3  = 0;





    
    LATGbits.LATG2  = 1; 
    LATJbits.LATJ6  = 0; 



    LATGbits.LATG1  = 0; 
    PORTFbits.RF6  = 0; 

    
    LATGbits.LATG4  = 1;
    PORTJbits.RJ7  = 0;

    PORTGbits.RG3  = 0; 
    PORTFbits.RF7  = 0; 




    tpa = 0;
    scrut_TP = 1; 


    PORTFbits.RF6  = 1;
    PORTFbits.RF7  = 1;



    creer_trame_horloge();
    envoyerHORLOGE_i2c(0xD0, 0, 0b00000000);

    while (PORTBbits.RB3  == 0); 


    while (PORTBbits.RB3  == 1); 


    PORTHbits.RH3  = !PORTHbits.RH3 ;
    
    if (voie_valide_1 == 1) { 
        LATGbits.LATG1  = 1; 
    }
    if (voie_valide_2 == 1) { 
        PORTGbits.RG3  = 1; 
    }



    PORTFbits.RF6  = 0; 
    PORTFbits.RF7  = 0; 

    Delay10KTCYx(10); 


    for (tpa = 0; tpa < Nb_Capteurs_max; tpa++) 
    {

        if (recherche_TPA)
            LATHbits.LATH0 = !LATHbits.LATH0; 



        PORTHbits.RH3  = !PORTHbits.RH3 ;
        Freq_A = 0;
        Freq1_1 = 0x0000;
        Freq2_1 = 0x0000;
        Freq3_1 = 0x0000;
        Freq4_1 = 0x0000;
        Freq1_2 = 0x0000;
        Freq2_2 = 0x0000;
        Freq3_2 = 0x0000;
        Freq4_2 = 0x0000;
        tempF1 = 0;
        tempF2 = 0;
        tempF3 = 0;
        tempF4 = 0;
        Cumul_Freq_1 = 0;
        Cumul_Freq_2 = 0;


        DELAY_125MS(2); 

        DELAY_SW_250MS(); 

        Delay10KTCYx(10); 


        if (voie_valide_1 == 1) { 
            I_Conso_1 = Mesure_I_Conso(0b00010000 , 458 );
        }
        if (voie_valide_2 == 1) { 
            I_Conso_2 = Mesure_I_Conso(0b00010100 , 458 );
        }

        Delay10KTCYx(10);

        if (voie_valide_1 == 1) { 
            I_Conso_1 = (I_Conso_1 + Mesure_I_Conso(0b00010000 , 458 )) / 2; 
            I_Modul_1 = Mesure_I_Modul(0b00011000 , 294 );
        }
        if (voie_valide_2 == 1) { 
            I_Conso_2 = (I_Conso_2 + Mesure_I_Conso(0b00010100 , 458 )) / 2; 
            I_Modul_2 = Mesure_I_Modul(0b00011100 , 294 );
        }

        
        
        if (I_Conso_1 >= 100 )
        {
            COURTCIRCUIT = 0xF1;
            Prog_HC595(0, Tab_Voie_2[0], alarme_caM, FUSIBLEHS); 
            if (I_Conso_2 >= 100 )
            {
                Prog_HC595(0, 0, alarme_caM, FUSIBLEHS); 
                COURTCIRCUIT = 0xFF;
            }
        }
        if (I_Conso_2 >= 100 )
        {
            COURTCIRCUIT = 0xF2;
            Prog_HC595(Tab_Voie_1[0], 0, alarme_caM, FUSIBLEHS); 
            if (I_Conso_1 >= 100 )
            {
                Prog_HC595(0, 0, alarme_caM, FUSIBLEHS); 
                COURTCIRCUIT = 0xFF;
            }
        }


        while (!(INTCONbits.TMR0IF)); 

        
        if (recherche_TPA)
            LATHbits.LATH0 = !LATHbits.LATH0; 

        PORTHbits.RH3  = !PORTHbits.RH3 ;

        DELAY_MS(60); 

        PORTHbits.RH3  = !PORTHbits.RH3 ;

        
        if (voie_valide_1 == 1) { 
            LATGbits.LATG0  = 0; 
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); 
                Freq1_1 += Freq_A; 

            }
        }
        
        if (voie_valide_2 == 1) { 
            LATGbits.LATG0  = 1; 
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); 
                Freq1_2 += Freq_A; 
            }
        }


        PORTHbits.RH3  = !PORTHbits.RH3 ;
        DELAY_MS(60); 
        PORTHbits.RH3  = !PORTHbits.RH3 ;
        
        if (voie_valide_1 == 1) { 
            LATGbits.LATG0  = 0; 
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); 
                Freq2_1 += Freq_A; 
            }
        }
        
        if (voie_valide_2 == 1) { 
            LATGbits.LATG0  = 1; 
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); 
                Freq2_2 += Freq_A; 
            }
        }

        PORTHbits.RH3  = !PORTHbits.RH3 ;
        DELAY_MS(60); 
        PORTHbits.RH3  = !PORTHbits.RH3 ;
        
        if (voie_valide_1 == 1) { 
            LATGbits.LATG0  = 0; 
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); 
                Freq3_1 += Freq_A; 
            }
        }
        
        if (voie_valide_2 == 1) { 
            LATGbits.LATG0  = 1; 
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); 
                Freq3_2 += Freq_A; 
            }
        }

        PORTHbits.RH3  = !PORTHbits.RH3 ;
        DELAY_MS(60); 
        PORTHbits.RH3  = !PORTHbits.RH3 ;
        
        if (voie_valide_1 == 1) { 
            LATGbits.LATG0  = 0; 
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); 
                Freq4_1 += Freq_A; 
            }
        }
        
        if (voie_valide_2 == 1) { 
            LATGbits.LATG0  = 1; 
            for (nb = 0; nb < 2; nb++) {
                Mesure_Freq(); 
                Freq4_2 += Freq_A; 
            }
        }

        PORTHbits.RH3  = !PORTHbits.RH3 ;

        if (tpa == 2) {

            PORTHbits.RH3  = !PORTHbits.RH3 ;

        }

        if (I_Conso_1 > 20 ) 
        {

            if (voie_valide_1 == 1) { 
                Tab_Etat_1[tpa] |= Instable(Freq1_1, Freq2_1, Freq3_1, Freq4_1); 
                Freq_TPA_1 = (int) (Cumul_Freq_1 / 8); 
            }


        } else {
            Freq_TPA_1 = 0;
        }

        if (I_Conso_2 > 20 ) 
        {

            if (voie_valide_2 == 1) { 
                Tab_Etat_2[tpa + 1] |= Instable(Freq1_2, Freq2_2, Freq3_2, Freq4_2);
                Freq_TPA_2 = (int) (Cumul_Freq_2 / 8); 
            }
        } else {
            Freq_TPA_2 = 0;
        }


        if (tpa > 0) {
            Tab_Pression_1[tpa] = Freq_TPA_1;
            Tab_Pression_2[tpa] = Freq_TPA_2; 
        }
        
        if (!tpa) {
            Calcul_Deb(Tab_Pression_1[0]);
            TEST = debit_cable;
            Tab_Pression_1[0] = debit_cable;

            Calcul_Deb(Tab_Pression_2[0]);
            TEST = debit_cable;
            Tab_Pression_2[0] = debit_cable;
        }
        
        

        if (PORTBbits.RB3  == 1) {
            while (PORTBbits.RB3  == 1);
            while (PORTBbits.RB3  == 0); 
        }

        while (PORTBbits.RB3  == 0); 

        PORTHbits.RH3  = !PORTHbits.RH3 ;


        
        DELAY_MS(150); 

        
        if (voie_valide_1) 
        {
            I_Repos_1 = Mesure_I_Repos(0b00001000 , 157 );
            DELAY_MS(5);
            I_Repos_1 = (I_Repos_1 + Mesure_I_Repos(0b00001000 , 157 )) / 2;
            if (I_Repos_1 <= 15) {
                I_Repos_1 = 0;
            } 
            

            Tab_I_Repos_1[tpa] = I_Repos_1; 
            Tab_I_Modul_1[tpa] = I_Modul_1; 

            
            I_Conso_1 *= 100; 
            if (I_Conso_1 > I_Repos_1) { 
                I_Conso_1 -= I_Repos_1; 
            } else {
                I_Conso_1 = 0;
            }
            I_Conso_1 /= 100; 
            
            I_Conso_1 += (I_Modul_1 / 2); 
            Tab_I_Conso_1[tpa] = I_Conso_1; 
        } 

        



        if (voie_valide_2 == 1) 
        {
            I_Repos_2 = Mesure_I_Repos(0b00001100 , 157 );
            DELAY_MS(5);
            I_Repos_2 = (I_Repos_2 + Mesure_I_Repos(0b00001100 , 157 )) / 2;
            if (I_Repos_2 <= 15) {
                I_Repos_2 = 0;
            } 
            

            Tab_I_Repos_2[tpa] = I_Repos_2; 
            Tab_I_Modul_2[tpa] = I_Modul_2; 


            
            I_Conso_2 *= 100; 
            if (I_Conso_2 > I_Repos_2) { 
                I_Conso_2 -= I_Repos_2; 
            } else {
                I_Conso_2 = 0;
            }
            I_Conso_2 = I_Conso_2 / 100; 
            
            I_Conso_2 += (I_Modul_2 / 2); 
            Tab_I_Conso_2[tpa] = I_Conso_2; 

        } 


        
        while (PORTBbits.RB3  == 1); 






        
        if (scrut_TP == 0) 
        {
            Nb_Capteurs_max = tpa; 
            fin_mesures_1 = 1; 
            fin_mesures_2 = 1; 
            break; 
        }
    } 


    LATGbits.LATG1  = 0; 
    PORTGbits.RG3  = 0; 

    delay_qms(100);

    PORTFbits.RF6  = 1; 
    PORTFbits.RF7  = 1; 

    delay_qms(100);
    PORTFbits.RF6  = 0; 
    PORTFbits.RF7  = 0; 

    LATJbits.LATJ6  = 0;
    LATGbits.LATG2  = 0;
    PORTJbits.RJ7  = 0;
    LATGbits.LATG4  = 0;

    Prog_HC595(0, 0, alarme_caM, FUSIBLEHS); 


    


    

    if (voie_valide_1 == 1) 
    {
        Memo_autorisee_1 = 1;
        Memo_autorisee_2 = 0;
        ecriture_trame_i2c(1);
    }

    if (BREAKSCRUTATION == 0xFF)
        goto BREAK;

    if (voie_valide_2 == 1) 
    {
        Memo_autorisee_1 = 0;
        Memo_autorisee_2 = 1;
        ecriture_trame_i2c(2);
    }

    if (BREAKSCRUTATION == 0xFF)
        goto BREAK;

BREAK:
    mesure_test_cc(Tab_Voie_1[0], Tab_Voie_2[0]); 

    LIRE_EEPROM(2, 128, &TRAMEIN, 1);
}

void Init_Tab_tpa(void) 
{
    int i, j;
    for (i = 0; i <= 16; i++) {
        
        Tab_Pression_1[i] = 0; 
        Tab_I_Repos_1[i] = 0; 
        Tab_I_Modul_1[i] = 0; 
        Tab_I_Conso_1[i] = 0; 
        Tab_Etat_1[i] = 0; 

        Tab_Pression_2[i] = 0; 
        Tab_I_Repos_2[i] = 0; 
        Tab_I_Modul_2[i] = 0; 
        Tab_I_Conso_2[i] = 0; 
        Tab_Etat_2[i] = 0; 

    }

    for (j = 0; j <= 9; j++) 
    {
        Tab_Horodatage[j] = 32; 
    }

}

void creer_trame_horloge(void) {

    char HEURE, MINUTE, SECONDE, MOIS, JOUR, ANNEE, DATE;


    ANNEE = LIRE_HORLOGE(6);
    delay_qms(10);
    MOIS = LIRE_HORLOGE(5);
    delay_qms(10);
    DATE = LIRE_HORLOGE(4);
    delay_qms(10);
    JOUR = LIRE_HORLOGE(3);
    delay_qms(10);
    HEURE = LIRE_HORLOGE(2);
    delay_qms(10);
    MINUTE = LIRE_HORLOGE(1);
    delay_qms(10);
    SECONDE = LIRE_HORLOGE(0);

    Tab_Horodatage[1] = (DATE & 0x0F) + 48;
    Tab_Horodatage[0] = ((DATE & 0x30) >> 4) + 48; 

    Tab_Horodatage[3] = (MOIS & 0x0F) + 48;
    Tab_Horodatage[2] = ((MOIS & 0x10) >> 4) + 48;

    Tab_Horodatage[5] = (SECONDE & 0x0F) + 48;
    Tab_Horodatage[4] = ((SECONDE & 0x70) >> 4) + 48;

    Tab_Horodatage[7] = (HEURE & 0x0F) + 48;
    Tab_Horodatage[6] = ((HEURE & 0x30) >> 4) + 48; 


    Tab_Horodatage[9] = (MINUTE & 0x0F) + 48;
    Tab_Horodatage[8] = ((MINUTE & 0x70) >> 4) + 48;



    Tab_Horodatage[11] = (ANNEE & 0x0F) + 48;
    Tab_Horodatage[10] = ((ANNEE & 0xF0) >> 4) + 48;
}

void DELAY_125MS(int ms) {
    int i;
    if (ms >= 1000) 
        ms = 2;

    T0CON = 0x83;  
    for (i = 0; i < ms; i++) {
        InitTimer0();
    }
    return;
}

void DELAY_SW_250MS() {

    T0CON = 0x84;  
    TMR0H = 0x48; 
    TMR0L = 0xE5;
    INTCONbits.TMR0IE = 0;
    INTCONbits.TMR0IF = 0;
    
    return;
}

unsigned int Mesure_I_Conso(char cMesure, unsigned int Coeff) 
{
    float Val_Temp_Conso2;
    Val_Temp_Conso2 = 10 * q * acqui_ana_16_plus(cMesure) * Coeff / 100; 
    return ((char) Val_Temp_Conso2);
}




unsigned int Mesure_I_Modul(char cMesure, unsigned int Coeff) 
{
    float Val_Temp_Modul2;
    Val_Temp_Modul2 = 10 * q * acqui_ana_16_plus(cMesure) * Coeff / 100; 
    return ((char) Val_Temp_Modul2);
}

void Prog_HC595(unsigned char rel_voies_impaires, unsigned char rel_voies_paires, unsigned char alarme_ca, unsigned char test_fus) {
    unsigned char i;
    unsigned char j;
    unsigned char val;
    unsigned char registre1; 
    unsigned char registre2;
    unsigned char registre3;
    registre1 = 0b00000000; 
    registre2 = 0b00000000; 
    registre3 = 0b00000000; 

    if (rel_voies_impaires > 0) {
        switch (rel_voies_impaires) {
            case 1:
                registre1 |= 0b00000001; 
                break;
            case 3:
                registre1 |= 0b00000100; 
                break;
            case 5:
                registre1 |= 0b00010000; 
                break;
            case 7:
                registre1 |= 0b01000000; 
                break;
            case 9:
                registre2 |= 0b00000001; 
                break;
            case 11:
                registre2 |= 0b00000100; 
                break;
            case 13:
                registre2 |= 0b00010000; 
                break;
            case 15:
                registre2 |= 0b01000000; 
                break;
            case 17:
                registre3 |= 0b00000001; 
                break;
            case 19:
                registre3 |= 0b00000100; 
                break;
        }
    }
    
    if (rel_voies_paires > 0) {
        switch (rel_voies_paires) {
            case 2:
                registre1 |= 0b00000010; 
                break;
            case 4:
                registre1 |= 0b00001000; 
                break;
            case 6:
                registre1 |= 0b00100000; 
                break;
            case 8:
                registre1 |= 0b10000000; 
                break;
            case 10:
                registre2 |= 0b00000010; 
                break;
            case 12:
                registre2 |= 0b00001000; 
                break;
            case 14:
                registre2 |= 0b00100000; 
                break;
            case 16:
                registre2 |= 0b10000000; 
                break;
            case 18:
                registre3 |= 0b00000010; 
                break;
            case 20:
                registre3 |= 0b00001000; 
                break;
        }
    }
    
    if (alarme_ca) {
        registre3 |= 0b00010000; 
    }
    
    if (test_fus) {
        registre3 |= 0b00100000; 
    }

    j = 24; 


    LATEbits.LATE6  = 0; 

    LATEbits.LATE6  = 1; 

    LATEbits.LATE5  = 0;
    delay_qms(1);
    LATEbits.LATE5  = 1;


    for (i = j; i > 0; i--) 
    {
        if (i > 16) {
            val = registre3 & 0x80;
            registre3 <<= 1; 
        }

        if (i > 8 && i < 17) {
            val = registre2 & 0x80;
            registre2 <<= 1; 
        }
        if (i < 9) {
            val = registre1 & 0x80;
            registre1 <<= 1; 
        }

        if (val == 0x80)
            val = 0xFF;
        LATEbits.LATE3  = val;
        LATEbits.LATE4  = 1;
        LATEbits.LATE4  = 0;

    }

    LATEbits.LATE5  = 0;
    LATEbits.LATE5  = 1;
    LATEbits.LATE6  = 1; 

}

void DELAY_MS(int ms) {
    int bb;

    
    if (ms >= 1000) 
        ms = 2;
    T0CON = 0x88;  
    for (bb = 0; bb < ms; bb++) {

        TMR0H = 0xE8; 
        TMR0L = 0x90;
        INTCONbits.TMR0IE = 0;
        INTCONbits.TMR0IF = 0;
        while (!(INTCONbits.TMR0IF));  

    }
    
    return;

}

void Mesure_Freq() 
{
    unsigned int cpt_to; 

    
    
    
    

    t1ov_cnt = 0x00; 
    cpt_int2 = 0x00;
    Freq_A = 0x0000; 
    tempF1 = 0x0000;
    tempF2 = 0x0000;
    tempF3 = 0x0000;
    tempF4 = 0x0000;
    TMR1H = 0x0000; 
    TMR1L = 0x0000; 
    Timer1L = 0x0000;
    Timer1H = 0x0000;


    PIR1bits.TMR1IF = 0; 
    T1CONbits.T1CKPS0 = 0; 
    T1CONbits.T1CKPS1 = 0; 
    T1CONbits.RD16 = 0; 
    T1CONbits.TMR1CS = 0; 
    T1CONbits.T1RUN = 0; 
    T1CONbits.T1OSCEN = 0; 
    IPR1 = 0x01; 
    PIE1 = 0x01; 
    cpt_int2 = 0;
    INTCONbits.RBIE = 0; 
    INTCON2bits.RBPU = 1; 
    INTCON2bits.INTEDG2 = 0; 
    INTCON2bits.RBIP = 0; 
    T1CONbits.TMR1ON = 0; 

    INTCONbits.GIEH = 1; 
    INTCONbits.PEIE = 1; 
    INTCON3bits.INT2IP = 1; 

    INTCON3bits.INT2IF = 0; 
    INTCON3bits.INT2IE = 1; 

    cpt_to = 0x0000; 

    while (cpt_to < 10000 && cpt_int2 < 21) {
        cpt_to++;
    }

    INTCONbits.GIE = 0; 
    INTCONbits.PEIE = 0; 
    INTCON3bits.INT2IE = 0; 

    tempF1 = (Timer1H * 256 + Timer1L);
    tempF1 = (tempF1 + t1ov_cnt * 65535) / 20;
    if (tempF1 > 0) {
        tempF1 += 0x0015;
        tempF2 = tempF1 * 0xA6; 
        tempF3 = 1000000 / tempF2;
        tempF4 = tempF3 * 1000; 
        Freq_A = (int) tempF4;

        if (!LATGbits.LATG0 ) { 
            Cumul_Freq_1 += tempF4;
        }
        if (LATGbits.LATG0 ) { 
            Cumul_Freq_2 += tempF4;
        }
    } else {
        Freq_A = 0x0000;
    }

    T1CONbits.RD16 = 0; 
    TMR1H = 0x0000; 
    TMR1L = 0x0000; 

}

unsigned char Instable(signed int iFreq1, signed int iFreq2, signed int iFreq3, signed int iFreq4) 
{
    float fDifF12, fDifF23, fDifF34;
    unsigned char cSortie;

    fDifF12 = iFreq1 - iFreq2;
    fDifF12 = ((fDifF12) > 0 ? (fDifF12) : (-fDifF12)) ;

    fDifF23 = iFreq2 - iFreq3;
    fDifF23 = ((fDifF23) > 0 ? (fDifF23) : (-fDifF23)) ;

    fDifF34 = iFreq3 - iFreq4;
    fDifF34 = ((fDifF34) > 0 ? (fDifF34) : (-fDifF34)) ;

    if (fDifF12 > 3  || fDifF23 > 3  || fDifF34 > 3 ) {
        cSortie = 0x80; 
    } else {
        cSortie = 0;
    }
    return (cSortie);

}

void Calcul_Deb(unsigned int* deb_temp) {


    

    if (*deb_temp >= 0x03E5 && *deb_temp <= 0x03E8) 
    {
        *deb_temp = 0;
    } else if ((*deb_temp < 0x03E5) || (*deb_temp >= 0x05DC)) 
    {
        *deb_temp = 0x270F; 
    } else {
        *deb_temp = (*deb_temp - 0x03E8); 
    }
}

unsigned int Mesure_I_Repos(char cMesure, unsigned int Coeff) 
{
    float Val_Temp_Repos2;
    Val_Temp_Repos2 = (q * acqui_ana_16_plus(cMesure) * Coeff / 1);
    return ((int) Val_Temp_Repos2);
} 

void delay_qms(char tempo) {
    char j, k, l;

    for (j = 0; j < tempo; j++) {
        for (k = 0; k < 10; k++) {
            for (l = 0; l < 40; l++) {
                
                
                
                
                
            }
        }
    }
}

void ecriture_trame_i2c(char vv) 
{


    unsigned char tpa;
    unsigned char i;
    unsigned char c; 
    
    float tmpf;
    unsigned int iAdresse;

    
    
    



    DELAY_MS(100); 
    for (i = 0; i <= 16; i++) 
    {
        BREAKSCRUTATION = 0x00;
        if ((vv != 1)&&(vv != 2)) 
        {




            BREAKSCRUTATION = 0xFF;
            goto finscrut;

        }

        if (vv == 1) 
        {
            tpa = Tab_code_1[i]; 
            if (tpa > 0) {
                
                No_voie = Tab_Voie_1[0];
                No_codage = Tab_code_1[i]; 
            }




            
            TRAMEOUT[0] = '#';
            IntToChar(No_voie, TMP, 3);
            TRAMEOUT[1] = TMP[0];
            TRAMEOUT[2] = TMP[1];
            TRAMEOUT[3] = TMP[2];
            IntToChar(i, TMP, 2);
            TRAMEOUT[4] = TMP[0];
            TRAMEOUT[5] = TMP[1];

            
            
            
            
            RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEOUT, 1, i); 


            TRAMEOUT[6] = 'A'; 

            IntToChar(Tab_I_Modul_1[i], TMP, 4);
            TRAMEOUT[7] = TMP[0];
            TRAMEOUT[8] = TMP[1];
            TRAMEOUT[9] = TMP[2];
            TRAMEOUT[10] = TMP[3];

            IntToChar(Tab_I_Conso_1[i], TMP, 4);
            TRAMEOUT[11] = TMP[0];
            TRAMEOUT[12] = TMP[1];
            TRAMEOUT[13] = TMP[2];
            TRAMEOUT[14] = TMP[3];


            IntToChar(Tab_I_Repos_1[i], TMP, 4);
            TRAMEOUT[15] = TMP[0];
            TRAMEOUT[16] = TMP[1];
            TRAMEOUT[17] = TMP[2];
            TRAMEOUT[18] = TMP[3];


            

            TRAMEOUT[19] = Tab_Constitution[0]; 
            TRAMEOUT[20] = Tab_Constitution[1];
            TRAMEOUT[21] = Tab_Constitution[2];
            TRAMEOUT[22] = Tab_Constitution[3];
            TRAMEOUT[23] = Tab_Constitution[4];
            TRAMEOUT[24] = Tab_Constitution[5];
            TRAMEOUT[25] = Tab_Constitution[6];
            TRAMEOUT[26] = Tab_Constitution[7];
            TRAMEOUT[27] = Tab_Constitution[8];
            TRAMEOUT[28] = Tab_Constitution[9];
            TRAMEOUT[29] = Tab_Constitution[10];
            TRAMEOUT[30] = Tab_Constitution[11];
            TRAMEOUT[31] = Tab_Constitution[12];

            if (recherche_TPA) {
                TRAMEOUT[19] = 'x';
                TRAMEOUT[20] = 'x';
                TRAMEOUT[21] = 'x';
                TRAMEOUT[22] = 'x';
                TRAMEOUT[23] = 'x';
                TRAMEOUT[24] = 'x';
                TRAMEOUT[25] = 'x';
                TRAMEOUT[26] = 'x';
                TRAMEOUT[27] = 'x';
                TRAMEOUT[28] = 'x';
                TRAMEOUT[29] = 'x';
                TRAMEOUT[30] = 'x';
                TRAMEOUT[31] = 'x';
            }

            


            TRAMEOUT[32] = Tab_Commentaire_cable[0]; 
            TRAMEOUT[33] = Tab_Commentaire_cable[1];
            TRAMEOUT[34] = Tab_Commentaire_cable[2];
            TRAMEOUT[35] = Tab_Commentaire_cable[3];
            TRAMEOUT[36] = Tab_Commentaire_cable[4];
            TRAMEOUT[37] = Tab_Commentaire_cable[5];
            TRAMEOUT[38] = Tab_Commentaire_cable[6];
            TRAMEOUT[39] = Tab_Commentaire_cable[7];
            TRAMEOUT[40] = Tab_Commentaire_cable[8];
            TRAMEOUT[41] = Tab_Commentaire_cable[9];
            TRAMEOUT[42] = Tab_Commentaire_cable[10];
            TRAMEOUT[43] = Tab_Commentaire_cable[11];
            TRAMEOUT[44] = Tab_Commentaire_cable[12];
            TRAMEOUT[45] = Tab_Commentaire_cable[13];
            TRAMEOUT[46] = Tab_Commentaire_cable[14];
            TRAMEOUT[47] = Tab_Commentaire_cable[15];
            TRAMEOUT[48] = Tab_Commentaire_cable[16];
            TRAMEOUT[49] = Tab_Commentaire_cable[17];

            if (recherche_TPA) {
                TRAMEOUT[32] = 'x';
                TRAMEOUT[33] = 'x';
                TRAMEOUT[34] = 'x';
                TRAMEOUT[35] = 'x';
                TRAMEOUT[36] = 'x';
                TRAMEOUT[37] = 'x';
                TRAMEOUT[38] = 'x';
                TRAMEOUT[39] = 'x';
                TRAMEOUT[40] = 'x';
                TRAMEOUT[41] = 'x';
                TRAMEOUT[42] = 'x';
                TRAMEOUT[43] = 'x';
                TRAMEOUT[44] = 'x';
                TRAMEOUT[45] = 'x';
                TRAMEOUT[46] = 'x';
                TRAMEOUT[47] = 'x';
                TRAMEOUT[48] = 'x';
                TRAMEOUT[49] = 'x';
            }






            IntToChar(No_voie, TMP, 3);
            TRAMEOUT[50] = TMP[0];
            TRAMEOUT[51] = TMP[1];
            TRAMEOUT[52] = TMP[2];



            TRAMEOUT[53] = Tab_Horodatage[0]; 
            TRAMEOUT[54] = Tab_Horodatage[1]; 
            TRAMEOUT[55] = Tab_Horodatage[2]; 
            TRAMEOUT[56] = Tab_Horodatage[3]; 
            TRAMEOUT[57] = Tab_Horodatage[6]; 
            TRAMEOUT[58] = Tab_Horodatage[7]; 
            TRAMEOUT[59] = Tab_Horodatage[8]; 
            TRAMEOUT[60] = Tab_Horodatage[9]; 



            TRAMEOUT[61] = '?';

            
            IntToChar(i, TMP, 2);
            TRAMEOUT[62] = TMP[0];
            TRAMEOUT[63] = TMP[1];

            
            if (i == 0) {
                TMP[0] = '0';
                TMP[1] = '1';
            } else {
                TMP[0] = '0';
                TMP[1] = '2';
            }
            
            TRAMEOUT[64] = TMP[0]; 
            TRAMEOUT[65] = TMP[1]; 




            TRAMEOUT[66] = '?';
            TRAMEOUT[67] = '?';
            TRAMEOUT[68] = '?';
            TRAMEOUT[69] = '?';







            
            TRAMEOUT[70] = Tab_Distance[0]; 
            TRAMEOUT[71] = Tab_Distance[1];
            TRAMEOUT[72] = Tab_Distance[2];
            TRAMEOUT[73] = Tab_Distance[3];
            TRAMEOUT[74] = Tab_Distance[4];

            if (recherche_TPA) {
                TRAMEOUT[70] = '?';
                TRAMEOUT[71] = '?';
                TRAMEOUT[72] = '?';
                TRAMEOUT[73] = '?';
                TRAMEOUT[74] = '?';
            }

            

            IntToChar(Tab_Etat_1[i], TMP, 2);
            TRAMEOUT[75] = TMP[0];
            TRAMEOUT[76] = TMP[1];



            
            
            
            
            tmp_I = 0;

            tmp_I2 = TRAMEIN[77];
            tmp_I = tmp_I + 1000 * (tmp_I2 - 48);
            tmp_I2 = TRAMEIN[78];
            tmp_I = tmp_I + 100 * (tmp_I2 - 48);
            tmp_I2 = TRAMEIN[79];
            tmp_I = tmp_I + 10 * (tmp_I2 - 48);
            tmp_I2 = TRAMEIN[80];
            tmp_I = tmp_I + (tmp_I2 - 48);
            if (Tab_Pression_1[i] != 0) {
                if ((tmp_I < (Tab_Pression_1[i] - 20)) || (tmp_I > (Tab_Pression_1[i] + 20)))
                {
                    
                    
                } 
            }
            


            IntToChar(Tab_Pression_1[i], TMP, 4);
            TRAMEOUT[77] = TMP[0];
            TRAMEOUT[78] = TMP[1];
            TRAMEOUT[79] = TMP[2];
            TRAMEOUT[80] = TMP[3];

            IntToChar(Tab_Seuil_1[i], TMP, 4);
            TRAMEOUT[81] = TMP[0];
            TRAMEOUT[82] = TMP[1];
            TRAMEOUT[83] = TMP[2];
            TRAMEOUT[84] = TMP[3];



            if (recherche_TPA) 
            {

                Tab_Seuil_1[i] = Tab_Pression_1[i] - 60;
                if (Tab_Seuil_1[i] <= 900)
                    Tab_Seuil_1[i] = 900; 

                IntToChar(Tab_Seuil_1[i], TMP, 4);
                TRAMEOUT[81] = TMP[0];
                TRAMEOUT[82] = TMP[1];
                TRAMEOUT[83] = TMP[2];
                TRAMEOUT[84] = TMP[3];

            }






            
            TRAMEOUT[85] = '2';
            TRAMEOUT[86] = '2';
            TRAMEOUT[87] = '2';
            TRAMEOUT[88] = '2';
            TRAMEOUT[89] = '2';
            TRAMEOUT[90] = '*';





            remplir_tab_presence(No_voie, 1, i);

        }

        if (vv == 2) 
        {

            tpa = Tab_code_2[i];
            if (tpa > 0) {
                
                No_voie = Tab_Voie_2[0];
                No_codage = Tab_code_2[i]; 
            }
            
            TRAMEOUT[0] = '#';
            IntToChar(No_voie, TMP, 3);
            TRAMEOUT[1] = TMP[0];
            TRAMEOUT[2] = TMP[1];
            TRAMEOUT[3] = TMP[2];
            IntToChar(i, TMP, 2);
            TRAMEOUT[4] = TMP[0];
            TRAMEOUT[5] = TMP[1];


            RECUPERE_TRAME_ET_CAL_ETAT(&TRAMEOUT, 2, i); 

            TRAMEOUT[6] = 'A'; 

            IntToChar(Tab_I_Modul_2[i], TMP, 4);
            TRAMEOUT[7] = TMP[0];
            TRAMEOUT[8] = TMP[1];
            TRAMEOUT[9] = TMP[2];
            TRAMEOUT[10] = TMP[3];

            IntToChar(Tab_I_Conso_2[i], TMP, 4);
            TRAMEOUT[11] = TMP[0];
            TRAMEOUT[12] = TMP[1];
            TRAMEOUT[13] = TMP[2];
            TRAMEOUT[14] = TMP[3];


            IntToChar(Tab_I_Repos_2[i], TMP, 4);
            TRAMEOUT[15] = TMP[0];
            TRAMEOUT[16] = TMP[1];
            TRAMEOUT[17] = TMP[2];
            TRAMEOUT[18] = TMP[3];



            TRAMEOUT[19] = Tab_Constitution[0]; 
            TRAMEOUT[20] = Tab_Constitution[1];
            TRAMEOUT[21] = Tab_Constitution[2];
            TRAMEOUT[22] = Tab_Constitution[3];
            TRAMEOUT[23] = Tab_Constitution[4];
            TRAMEOUT[24] = Tab_Constitution[5];
            TRAMEOUT[25] = Tab_Constitution[6];
            TRAMEOUT[26] = Tab_Constitution[7];
            TRAMEOUT[27] = Tab_Constitution[8];
            TRAMEOUT[28] = Tab_Constitution[9];
            TRAMEOUT[29] = Tab_Constitution[10];
            TRAMEOUT[30] = Tab_Constitution[11];
            TRAMEOUT[31] = Tab_Constitution[12];


            if (recherche_TPA) {
                TRAMEOUT[19] = 'x';
                TRAMEOUT[20] = 'x';
                TRAMEOUT[21] = 'x';
                TRAMEOUT[22] = 'x';
                TRAMEOUT[23] = 'x';
                TRAMEOUT[24] = 'x';
                TRAMEOUT[25] = 'x';
                TRAMEOUT[26] = 'x';
                TRAMEOUT[27] = 'x';
                TRAMEOUT[28] = 'x';
                TRAMEOUT[29] = 'x';
                TRAMEOUT[30] = 'x';
                TRAMEOUT[31] = 'x';
            }


            TRAMEOUT[32] = Tab_Commentaire_cable[0]; 
            TRAMEOUT[33] = Tab_Commentaire_cable[1];
            TRAMEOUT[34] = Tab_Commentaire_cable[2];
            TRAMEOUT[35] = Tab_Commentaire_cable[3];
            TRAMEOUT[36] = Tab_Commentaire_cable[4];
            TRAMEOUT[37] = Tab_Commentaire_cable[5];
            TRAMEOUT[38] = Tab_Commentaire_cable[6];
            TRAMEOUT[39] = Tab_Commentaire_cable[7];
            TRAMEOUT[40] = Tab_Commentaire_cable[8];
            TRAMEOUT[41] = Tab_Commentaire_cable[9];
            TRAMEOUT[42] = Tab_Commentaire_cable[10];
            TRAMEOUT[43] = Tab_Commentaire_cable[11];
            TRAMEOUT[44] = Tab_Commentaire_cable[12];
            TRAMEOUT[45] = Tab_Commentaire_cable[13];
            TRAMEOUT[46] = Tab_Commentaire_cable[14];
            TRAMEOUT[47] = Tab_Commentaire_cable[15];
            TRAMEOUT[48] = Tab_Commentaire_cable[16];
            TRAMEOUT[49] = Tab_Commentaire_cable[17];


            if (recherche_TPA) {
                TRAMEOUT[32] = 'x';
                TRAMEOUT[33] = 'x';
                TRAMEOUT[34] = 'x';
                TRAMEOUT[35] = 'x';
                TRAMEOUT[36] = 'x';
                TRAMEOUT[37] = 'x';
                TRAMEOUT[38] = 'x';
                TRAMEOUT[39] = 'x';
                TRAMEOUT[40] = 'x';
                TRAMEOUT[41] = 'x';
                TRAMEOUT[42] = 'x';
                TRAMEOUT[43] = 'x';
                TRAMEOUT[44] = 'x';
                TRAMEOUT[45] = 'x';
                TRAMEOUT[46] = 'x';
                TRAMEOUT[47] = 'x';
                TRAMEOUT[48] = 'x';
                TRAMEOUT[49] = 'x';
            }



            IntToChar(No_voie, TMP, 3);
            TRAMEOUT[50] = TMP[0];
            TRAMEOUT[51] = TMP[1];
            TRAMEOUT[52] = TMP[2];



            TRAMEOUT[53] = Tab_Horodatage[0]; 
            TRAMEOUT[54] = Tab_Horodatage[1]; 
            TRAMEOUT[55] = Tab_Horodatage[2]; 
            TRAMEOUT[56] = Tab_Horodatage[3]; 
            TRAMEOUT[57] = Tab_Horodatage[6]; 
            TRAMEOUT[58] = Tab_Horodatage[7]; 
            TRAMEOUT[59] = Tab_Horodatage[8]; 
            TRAMEOUT[60] = Tab_Horodatage[9]; 


            TRAMEOUT[61] = '?';
            IntToChar(i, TMP, 2);
            TRAMEOUT[62] = TMP[0];
            TRAMEOUT[63] = TMP[1];

            
            if (i == 0) {
                TMP[0] = '0';
                TMP[1] = '1';
            } else {
                TMP[0] = '0';
                TMP[1] = '2';
            }
            
            TRAMEOUT[64] = TMP[0]; 
            TRAMEOUT[65] = TMP[1]; 



            TRAMEOUT[66] = '?';
            TRAMEOUT[67] = '?';
            TRAMEOUT[68] = '?';
            TRAMEOUT[69] = '?';

            
            TRAMEOUT[70] = Tab_Distance[0]; 
            TRAMEOUT[71] = Tab_Distance[1];
            TRAMEOUT[72] = Tab_Distance[2];
            TRAMEOUT[73] = Tab_Distance[3];
            TRAMEOUT[74] = Tab_Distance[4];


            if (recherche_TPA) {
                TRAMEOUT[70] = '?';
                TRAMEOUT[71] = '?';
                TRAMEOUT[72] = '?';
                TRAMEOUT[73] = '?';
                TRAMEOUT[74] = '?';
            }

            
            IntToChar(Tab_Etat_2[i], TMP, 2); 
            TRAMEOUT[75] = TMP[0];
            TRAMEOUT[76] = TMP[1];

            


            tmp_I = 0;

            tmp_I2 = TRAMEIN[77];
            tmp_I = tmp_I + 1000 * (tmp_I2 - 48);
            tmp_I2 = TRAMEIN[78];
            tmp_I = tmp_I + 100 * (tmp_I2 - 48);
            tmp_I2 = TRAMEIN[79];
            tmp_I = tmp_I + 10 * (tmp_I2 - 48);
            tmp_I2 = TRAMEIN[80];
            tmp_I = tmp_I + (tmp_I2 - 48);
            if (Tab_Pression_2[i] != 0) {
                if ((tmp_I < (Tab_Pression_2[i] - 20)) || (tmp_I > (Tab_Pression_2[i] + 20)))
                {
                    
                    
                } 
            }
            


            IntToChar(Tab_Pression_2[i], TMP, 4);
            TRAMEOUT[77] = TMP[0];
            TRAMEOUT[78] = TMP[1];
            TRAMEOUT[79] = TMP[2];
            TRAMEOUT[80] = TMP[3];

            IntToChar(Tab_Seuil_2[i], TMP, 4);
            TRAMEOUT[81] = TMP[0];
            TRAMEOUT[82] = TMP[1];
            TRAMEOUT[83] = TMP[2];
            TRAMEOUT[84] = TMP[3];

            if (recherche_TPA) {


                Tab_Seuil_2[i] = Tab_Pression_2[i] - 60;
                if (Tab_Seuil_2[i] <= 900)
                    Tab_Seuil_2[i] = 900; 
                IntToChar(Tab_Seuil_2[i], TMP, 4);
                TRAMEOUT[81] = TMP[0];
                TRAMEOUT[82] = TMP[1];
                TRAMEOUT[83] = TMP[2];
                TRAMEOUT[84] = TMP[3];

            }
            
            TRAMEOUT[85] = '2';
            TRAMEOUT[86] = '2';
            TRAMEOUT[87] = '2';
            TRAMEOUT[88] = '2';
            TRAMEOUT[89] = '2';
            TRAMEOUT[90] = '*';

            remplir_tab_presence(No_voie, 2, i);

        }
        Sauvegarde_en_cours = 1;

        INTCON3bits.INT1IE = 0; 

        
        if (Memo_autorisee_1 || Memo_autorisee_2) 
        {
            if ((NOSAVEOEILSCRUT == 0) || (NOSAVEOEILSCRUT == 2)) 
            {
                if (vv == 2) 
                    iAdresse = 128 * i;
                ECRIRE_EEPROMBYTE(2, iAdresse, &TRAMEOUT, 1); 
            } else {
                DELAY_MS(100);
            }
            if ((NOSAVEOEILSCRUT == 0) || (NOSAVEOEILSCRUT == 1)) 
            {
                if (vv == 1) 
                    ECRIRE_EEPROMBYTE(choix_memoire(&TRAMEOUT), calcul_addmemoire(&TRAMEOUT), &TRAMEOUT, 1); 
            } else {
                DELAY_MS(100);
            }
        }
        DELAY_MS(10);
        LIRE_EEPROM(2, iAdresse, &TRAMEIN, 1); 
        DELAY_MS(10);
    } 

    
    if (vv == 1 && recherche_TPA == 1) {
        if ((NOSAVEOEILSCRUT == 0) || (NOSAVEOEILSCRUT == 1)) 
        {
            ecrire_tab_presence_TPA(1);
        } else {
            DELAY_MS(100);
        }
    }
    if (vv == 2 && recherche_TPA == 1) {
        if ((NOSAVEOEILSCRUT == 0) || (NOSAVEOEILSCRUT == 2)) 
        {
            ecrire_tab_presence_TPA(2);
        } else {
            DELAY_MS(100);
        }

    }

finscrut:


    LIRE_EEPROM(2, 128, &TRAMEIN, 1);
    Sauvegarde_en_cours = 0; 
    INTCON3bits.INT1IE = 1; 

}

void mesure_test_cc(unsigned char n, unsigned char i) 
{
    if ((COURTCIRCUIT == 0xF1) || (COURTCIRCUIT == 0xFF)) {
        
        TRAMEOUT[1] = '0';
        TRAMEOUT[2] = '0';
        TRAMEOUT[3] = n + 48;
        if (n >= 10) {
            TRAMEOUT[2] = '1';
            TRAMEOUT[3] = (n - 10) + 48;
        }
        if (n >= 20) {
            TRAMEOUT[2] = '2';
            TRAMEOUT[3] = '0';
        }

        TRAMEOUT[4] = '0';
        TRAMEOUT[5] = '0';

        MODIFICATION_TABLE_DEXISTENCE(&TRAMEOUT, 2, 6); 
    }


    if ((COURTCIRCUIT == 0xF2) || (COURTCIRCUIT == 0xFF)) {



        TRAMEOUT[1] = '0';
        TRAMEOUT[2] = '0';
        TRAMEOUT[3] = i + 48;
        if (i >= 10) {
            TRAMEOUT[2] = '1';
            TRAMEOUT[3] = (i - 10) + 48;
        }
        if (i >= 20) {
            TRAMEOUT[2] = '2';
            TRAMEOUT[3] = '0';
        }
        TRAMEOUT[4] = '0';
        TRAMEOUT[5] = '0';

        MODIFICATION_TABLE_DEXISTENCE(&TRAMEOUT, 2, 7); 
    }
}

void InitTimer0() {
    TMR0H = 0x48; 
    TMR0L = 0xE5;
    INTCONbits.TMR0IE = 0;
    INTCONbits.TMR0IF = 0;
    while (!(INTCONbits.TMR0IF));  
}

void IntToChar(signed int value, char *chaine, int Precision) {
    signed int Mil, Cent, Diz, Unit;
    int count = 0;

    
    Mil = 0;
    Cent = 0;
    Diz = 0;
    Unit = 0;
    
    if (value != 0) {
        if (Precision >= 4) 
        {
            
            Mil = value / 1000;
            if (Mil != 0) {
                *(chaine + count) = Mil + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }

        if (Precision >= 3) 
        {
            
            Cent = value - (Mil * 1000);
            Cent = Cent / 100;
            if (Cent != 0) {
                *(chaine + count) = Cent + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }

        if (Precision >= 2) 
        {
            
            Diz = value - (Mil * 1000) - (Cent * 100);
            Diz = Diz / 10;
            if (Diz != 0) {
                *(chaine + count) = Diz + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }

        
        Unit = value - (Mil * 1000) - (Cent * 100) - (Diz * 10);
        *(chaine + count) = Unit + 48;
        if (*(chaine + count) < 48) 
            *(chaine + count) = 48;
        if (*(chaine + count) > 57)
            *(chaine + count) = 57;
    } else 
    {
        
        for (Mil = 0; Mil < Precision; Mil++) 
            *(chaine + Mil) = 48;
    }

} 

void RECUPERE_TRAME_ET_CAL_ETAT(char *hk, char t, char y) 
{
    unsigned int jp[4];
    char j;


    LIRE_EEPROM(choix_memoire(hk), calcul_addmemoire(hk), &TRAMEIN, 1); 



    ALARMECABLE = 0; 

    
    TMPINT = 0;

    
    if (t == 1) {

        if (y == 2) 
        {
            jp[0] = (TRAMEIN[81] - 48); 
        }

        
        jp[0] = (TRAMEIN[81] - 48);
        jp[1] = (TRAMEIN[82] - 48);
        jp[2] = (TRAMEIN[83] - 48);
        jp[3] = (TRAMEIN[84] - 48);

        TMPINT = jp[3] + 10 * jp[2] + 100 * jp[1] + 1000 * jp[0];

        Tab_Seuil_1[y] = TMPINT;

        if (Tab_Etat_1[y] == 0x80)
        {
            Tab_Etat_1[y] = 50; 
        }

        
        if (Tab_Etat_1[y] != 0x80)
        {
            if (Tab_Pression_1[y] >= Tab_Seuil_1[y]) 
                Tab_Etat_1[y] = 0x00; 

            if (Tab_Pression_1[y] < Tab_Seuil_1[y]) 
            {
                Tab_Etat_1[y] = 0x1E; 
                
                
            }


            if (Tab_Pression_1[y] == 0) 
                Tab_Etat_1[y] = 50; 




            if ((Tab_Pression_1[y] < 900)&&(Tab_Pression_1[y] > 5)) 
                Tab_Etat_1[y] = 40; 





            if (Tab_Pression_1[y] > 1700) 
            {
                Tab_Etat_1[y] = 40; 
                if (typedecarte == 'M') 
                {
                    Tab_Etat_1[y] = 50; 
                }

            }

            if ((y == 0)&&(Tab_Pression_1[0] >= 9000)) 
                Tab_Etat_1[y] = 0;



        }


        if ((!recherche_TPA)&&(!recherche_TPR)) {
            if (TRAMEIN[76] == '1') 
                Tab_Etat_1[y] = Tab_Etat_1[y] + 1;
        } else {
            if (TRAMEIN[76] == '1') 
                Tab_Etat_1[y] = Tab_Etat_1[y] - 1;
        }



    }

    if (t == 2) {

        if (y == 10) {

        }



        jp[0] = (TRAMEIN[81] - 48);
        jp[1] = (TRAMEIN[82] - 48);
        jp[2] = (TRAMEIN[83] - 48);
        jp[3] = (TRAMEIN[84] - 48);

        TMPINT = jp[3] + 10 * jp[2] + 100 * jp[1] + 1000 * jp[0];



        Tab_Seuil_2[y] = TMPINT;

        if (Tab_Etat_2[y] == 0x80)
        {
            Tab_Etat_2[y] == 50; 
        }


        if (Tab_Etat_2[y] != 0x80)
        {



            if (Tab_Pression_2[y] >= Tab_Seuil_2[y]) 
                Tab_Etat_2[y] = 0x00; 

            if (Tab_Pression_2[y] < Tab_Seuil_2[y]) 
            {
                Tab_Etat_2[y] = 0x1E; 
            }

            if (Tab_Pression_2[y] == 0) 
                Tab_Etat_2[y] = 50; 



            if ((Tab_Pression_2[y] < 900)&&(Tab_Pression_2[y] > 5)) 
                Tab_Etat_2[y] = 40; 




            if (Tab_Pression_2[y] > 1700) {
                Tab_Etat_2[y] = 40; 
                if (typedecarte == 'M') 
                {
                    Tab_Etat_2[y] = 50; 
                }
            }




            if ((y == 0)&&(Tab_Pression_2[0] >= 9000)) 
                Tab_Etat_2[y] = 0;


        }



        if ((!recherche_TPA)&&(!recherche_TPR)) {
            if (TRAMEIN[76] == '1') 
                Tab_Etat_2[y] = Tab_Etat_2[y] + 1;
        } else {
            if (TRAMEIN[76] == '1') 
                Tab_Etat_2[y] = Tab_Etat_2[y] - 1;
        }




    }



    for (j = 0; j < 13; j++) {
        Tab_Constitution[j] = TRAMEIN[19 + j];
    }

    for (j = 0; j < 18; j++) {
        Tab_Commentaire_cable[j] = TRAMEIN[32 + j];
    }

    for (j = 0; j < 5; j++) {
        Tab_Distance[j] = TRAMEIN[70 + j];
    }
}

void remplir_tab_presence(char v, char pouimp, char co) 
{


    Tab_existance2[0] = '#';
    Tab_existance1[0] = '#';

    if (pouimp == 1) {
        Tab_existance1[1] = v;
        Tab_existance1[2] = 'a'; 

        if (v == 5) {




            v = 5;
        }

        if (Tab_Pression_1[co] == 0) 
            Tab_existance1[co + 3] = 'F';
        if (Tab_Pression_1[co] != 0) 
            Tab_existance1[co + 3] = 'A';
        if (Tab_Pression_1[0] == 9999) 
            Tab_existance1[3] = 'F';

        if (Tab_existance1[3] == 'A') 
        {


            Tab_existance2[0] = '#';
        }




        if (co == 16) {
            Tab_existance1[20] = numdecarte + 48;
            Tab_existance1[21] = Tab_existance1[2];
            Tab_existance1[22] = 'x';
            Tab_existance1[23] = '*';



        }


    }

    if (pouimp == 2) {

        Tab_existance2[1] = v;
        Tab_existance2[2] = 'a'; 



        if (Tab_Pression_2[co] == 0) 
            Tab_existance2[co + 3] = 'F';
        if (Tab_Pression_2[co] != 0) 
            Tab_existance2[co + 3] = 'A';
        if (Tab_Pression_2[0] == 9999) 
            Tab_existance2[3] = 'F';
        if (co == 16) {
            Tab_existance2[20] = numdecarte + 48;
            Tab_existance2[21] = Tab_existance2[2];
            Tab_existance2[22] = typedecarte; 
            Tab_existance2[23] = '*';
        }
    }
}

unsigned char ECRIRE_EEPROMBYTE(unsigned char ADDE, unsigned int ADDHL, unsigned char *tab, char g) {
    unsigned char TMP2;
    unsigned char AddH, AddL;



    if ((ADDE != SAVADDE)&&(FUNCTIONUSE == 'R'))
        ADDE = SAVADDE;
    if ((ADDHL != SAVADDHL)&&(FUNCTIONUSE == 'R')) {
        g = SAVr;
        ADDHL = SAVADDHL;
        tab = TMP;
    }


    
    if (ADDE == 3)
        ADDE = 6;
    if (ADDE == 2)
        ADDE = 4;
    if (ADDE == 1)
        ADDE = 2;




    TMP2 = ADDE;
    TMP2 = TMP2 << 1;
    TMP2 = 0XA0 | TMP2;


    AddL = (ADDHL);
    AddH = (ADDHL) >> 8;

    if (g == 1) 
        tab[85] = '*'; 
    if (g == 3) 
        tab[115] = '*'; 
    if (g == 4) 
        tab[23] = '*'; 




    while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W)) ; 
    SSP1CON2bits.SEN=1;while(SSP1CON2bits.SEN) ; 
    while (SSPCON2bits.SEN); 
    WriteI2C1 (TMP2); 
    while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W)) ; 
    WriteI2C1 (AddH); 
    while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W)) ; 
    WriteI2C1 (AddL); 
    while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W)) ; 
    putstringI2C(tab); 
    while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W)) ; 
    SSP1CON2bits.PEN=1;while(SSP1CON2bits.PEN) ; 
    while (SSPCON2bits.PEN); 
    return ( 0); 

}

unsigned char putstringI2C(unsigned char *wrptr) {



    unsigned char x;

    unsigned int PageSize;
    PageSize = 128;


    for (x = 0; x < PageSize; x++) 
    {
        if (SSPCON1bits.SSPM3) 
        { 


            if (WriteI2C1  (*wrptr)) 
            {
                return ( -3); 
            }


            while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W)) ; 
            if (SSPCON2bits.ACKSTAT) 
            {
                return ( -2); 
            } 
        } else 
        {
            PIR1bits.SSPIF = 0; 


            SSPBUF = *wrptr; 


            SSPCON1bits.CKP = 1; 
            while (!PIR1bits.SSPIF); 

            if ((!SSPSTATbits.R_W) && (!SSPSTATbits.BF))
            {
                return ( -2); 
            }
        }



        wrptr++; 



    } 
    return ( 0);
}

unsigned int calcul_addmemoire(unsigned char *tab) {
    unsigned int t;
    unsigned int i;
    unsigned int r;



    t = 0;
    i = tab[3];
    i = i - 48;
    t = t + 1 * i;
    i = tab[2];
    i = i - 48;
    t = t + 10 * i;
    i = tab[1];
    i = i - 48;
    t = t + 100 * i;


    r = 2176 * (t);
    if (t > 29)
        r = 2176 * (t - 30);
    if (t > 59)
        r = 2176 * (t - 60);
    if (t > 89)
        r = 2176 * (t - 90);


    t = 0;
    i = tab[5] - 48;
    t = 1 * i;
    i = tab[4] - 48;
    t = t + 10 * i;
    i = t;

    t = r + 128 * i;



    delay_qms(10);



    return t;


}



unsigned char choix_memoire(unsigned char *tab) {
    unsigned int i;
    unsigned int t;
    unsigned char r;




    t = 0;
    i = tab[3] - 48;
    t = t + 1 * i;
    i = tab[2] - 48;
    t = t + 10 * i;
    i = tab[1] - 48;
    t = t + 100 * i;

    r = 0;
    if (t > 29)
        r = 1;
    if (t > 59)
        r = 2;
    if (t > 89)
        r = 3;




    delay_qms(10);
    return r;

}

unsigned char LIRE_EEPROM(unsigned char ADDE, unsigned int ADDHL, unsigned char *tab, char r) {
    char h;
    unsigned char t;
    unsigned char TMP2;
    unsigned char TMP4;
    unsigned char AddH, AddL;

    if ((ADDE != SAVMEMOEIL)&&(FUNCTIONUSE == 'O'))
        ADDE = SAVMEMOEIL;

    if ((ADDHL != SAVADOEIL)&&(FUNCTIONUSE == 'O')) {
        ADDHL = SAVADOEIL;
        tab = TRAMEIN;
    }


    
    


    if ((ADDE != SAVADDE)&&(FUNCTIONUSE == 'R'))
        ADDE = SAVADDE;
    if ((ADDHL != SAVADDHL)&&(FUNCTIONUSE == 'R')) {
        r = SAVr;
        ADDHL = SAVADDHL;
        tab = TMP;
    }

    if ((ADDE != SAVADDE)&&(FUNCTIONUSE == 'r')) {
        ADDE = SAVADDE;
        r = SAVr;
        ADDHL = SAVADDHL;
        tab = TRAMEIN;
    }

    if ((ADDHL != SAVADDHL)&&(FUNCTIONUSE == 'r')) {
        r = SAVr;
        ADDHL = SAVADDHL;
        tab = TRAMEIN;
    }

    if ((ADDE != SAVADDE)&&(FUNCTIONUSE == 'T'))
        ADDE = SAVADDE;
    if ((ADDHL != SAVADDHL)&&(FUNCTIONUSE == 'T')) {
        r = SAVr;
        ADDHL = SAVADDHL;
        tab = TRAMEIN;
    }




    
    if (ADDE == 3)
        ADDE = 6;
    if (ADDE == 2)
        ADDE = 4;
    if (ADDE == 1)
        ADDE = 2;





    AddL = ADDHL;
    AddH = ADDHL >> 8;



    TMP2 = ADDE;
    TMP2 = TMP2 << 1;
    TMP2 = TMP2 | 0xA0;


    while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W)) ;
    SSP1CON2bits.SEN=1;while(SSP1CON2bits.SEN) ;
    
    while (SSPCON2bits.SEN);

    WriteI2C1 (TMP2); 
    while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W)) ;




    WriteI2C1 (AddH); 
    while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W)) ;

    while (SSPCON2bits.RSEN);

    WriteI2C1 (AddL); 
    while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W)) ;


    SSP1CON2bits.RSEN=1;while(SSP1CON2bits.RSEN) ;
    while (SSPCON2bits.RSEN);


    
    TMP2 = TMP2 | 0x01;
    WriteI2C1 (TMP2); 
    while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W)) ;



    
    if (r == 1) 
        getsI2C1 (tab, 86);
    if (r == 0)
        getsI2C1 (tab, 108);
    if (r == 3)
        getsI2C1 (tab, 116);
    if (r == 4)
        getsI2C1 (tab, 24);


    SSP1CON2bits.ACKDT=1, SSP1CON2bits.ACKEN=1;while(SSP1CON2bits.ACKEN) ;
    while (SSPCON2bits.ACKEN);



    SSP1CON2bits.PEN=1;while(SSP1CON2bits.PEN) ;
    while (SSPCON2bits.PEN);
testEEW:
    return (1);
}

void ecrire_tab_presence_TPA(char p) 
{
    unsigned int l;
    char i;



    if (p == 1) {
        l = Tab_existance1[1];
        l = 128 * l + 10000;



        if (Memo_autorisee_1)
            ECRIRE_EEPROMBYTE(3, l, &Tab_existance1, 4);
    }


    if (p == 2) {
        l = Tab_existance2[1];
        l = 128 * l + 10000;


        if (Memo_autorisee_2)
            ECRIRE_EEPROMBYTE(3, l, &Tab_existance2, 4);
    }


    p = 0;
    delay_qms(10);
    LIRE_EEPROM(3, l, &TRAMEIN, 4); 
    p = 0;
}

void MODIFICATION_TABLE_DEXISTENCE(char *v, char eff, char etat) 
{

    unsigned int y, z;
    char fa, nbrcapteurs, jjj;

    nbrcapteurs = 0;
    v[0] = '#';
    y = 100 * (v[1] - 48) + 10 * (v[2] - 48)+(v[3] - 48); 

    y = 128 * y + 10000;

    LIRE_EEPROM(3, y, &TMP, 4); 

    z = 10 * (v[4] - 48)+(v[5] - 48); 

    if (eff != 2) 
    {
        if (eff == 0) 
        {
            TMP[z + 3] = v[6]; 
            fa = TMP[z + 3];
        }

        if (eff == 1) 
        {

            
            for (jjj = 2; jjj < 20; jjj++) 
            {
                if (TMP[jjj] == 'A') {

                    delay_qms(1);
                    nbrcapteurs = nbrcapteurs + 1; 
                }
            }
            TMP[z + 3] = 'F'; 


            if (nbrcapteurs < 2) {
                TMP[2] = 'v';
                TMP[21] = 'v';
            }

        }
        TMP[2] = fa + 0x20; 
        TMP[21] = fa + 0x20;
    }

    if ((etat == 1) || (etat == 2)) {
        TMP[2] = 'h';
    }

    if ((((etat == 4) || (etat == 5))&&(TMP[2] != 'c')) || (etat == 8)) {
        TMP[2] = TMP[21];
    }

    if ((etat == 6) || (etat == 7)) {
        TMP[2] = 'c';
    }

    
    ECRIRE_EEPROMBYTE(3, y, &TMP, 4);

    delay_qms(10);
    LIRE_EEPROM(3, y, &TMP, 4); 

    delay_qms(10);

}

unsigned char LIRE_HORLOGE(unsigned char zone) {
    unsigned char TMP;
    TMP = litHORLOGE_i2c(0xD1, zone);


    if (zone == 0)
        TMP = TMP & 0x7F; 
    if (zone == 1)
        TMP = TMP & 0x7F; 
    if (zone == 2)
        TMP = TMP & 0x3F; 
    if (zone == 3)
        TMP = TMP & 0x07; 
    if (zone == 4)
        TMP = TMP & 0x3F; 
    if (zone == 5)
        TMP = TMP & 0x1F; 
    if (zone == 6)
        TMP = TMP;

    return (TMP);
}

signed char litHORLOGE_i2c(unsigned char NumeroH, unsigned char AddH) {
    char Data;

    SSP1CON2bits.SEN=1;while(SSP1CON2bits.SEN) ;
    WriteI2C1 (NumeroH & 0xFE); 
    WriteI2C1 (AddH); 
    SSP1CON2bits.RSEN=1;while(SSP1CON2bits.RSEN) ;
    WriteI2C1 (NumeroH); 
    Data = ReadI2C1 (); 
    SSP1CON2bits.PEN=1;while(SSP1CON2bits.PEN) ;
    return (Data);
}

char VOIR_TABLE_EXISTENCE_POUR_OEIL(char *v) {

    unsigned int y, z;
    char fa;

    v[0] = '#';

    y = 100 * (v[1] - 48) + 10 * (v[2] - 48)+(v[3] - 48); 

    y = 128 * y + 10000;

    LIRE_EEPROM(3, y, &TMP, 4); 
    TMP[2] = 'a';
    fa = TMP[2];




    return (fa); 
}

void TRANSFORMER_TRAME_MEM_EN_COM(void) {

    int t;
    int u;
    u = 8;
    TRAMEOUT[0] = '#';

    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    

    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;



    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;





    t = 0;
    for (t = 0; t < 13; t++) {
        TRAMEOUT[u] = TRAMEIN[u - 7];
        u = u + 1;

    }

    t = 0;
    for (t = 0; t < 18; t++) {
        TRAMEOUT[u] = TRAMEIN[u - 7];
        u = u + 1;
    }



    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;



    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;



    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;



    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;




    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;








    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;




    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;



    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;


    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;
    TRAMEOUT[u] = TRAMEIN[u - 7];
    u = u + 1;

    TRAMEOUT[u] = '*';

    pointeur_out_in = u;


}

unsigned int calcul_CRC(char far *txt, unsigned char lg_txt) {
    unsigned char ii, nn;
    unsigned int crc;
    char CRC[5];

    char far *p;
    crc = 0xFFFF ;
    p = txt;

    
    ii = 0;
    nn = 0;
    
    do {
        crc ^= (unsigned int) *p;
        
        do {
            if (crc & 0x8000) {
                crc <<= 1;
                
                crc ^= 0x01021 ;
            } else {
                crc <<= 1;
            }
            nn++;
        } while (nn < 8);
        ii = ii + 1;
        p++;
        if (nn == 8) {
            nn = 0;
        }
    } while (ii < lg_txt);
    ii = 0;


    IntToChar5(crc, CRC, 5);


    TRAMEOUT[pointeur_out_in] = CRC[0];
    TRAMEOUT[pointeur_out_in + 1] = CRC[1];
    TRAMEOUT[pointeur_out_in + 2] = CRC[2];
    TRAMEOUT[pointeur_out_in + 3] = CRC[3];
    TRAMEOUT[pointeur_out_in + 4] = CRC[4];
    TRAMEOUT[pointeur_out_in + 5] = '*';

    return crc;

}

void IntToChar5(unsigned int value, char *chaine, char Precision) {
    unsigned int Mil, Cent, Diz, Unit, DMil;
    int count = 0;

    
    Mil = 0;
    Cent = 0;
    Diz = 0;
    Unit = 0;
    DMil = 0;

    
    if (value != 0) {
        if (Precision >= 5) 
        {
            
            DMil = value / 10000;
            if (DMil != 0) {
                *(chaine + count) = DMil + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }
        if (Precision >= 4) 
        {
            
            Mil = value - (DMil * 10000);
            Mil = Mil / 1000;
            if (Mil != 0) {
                *(chaine + count) = Mil + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }

        if (Precision >= 3) 
        {
            
            Cent = value - (Mil * 1000)-(DMil * 10000);
            Cent = Cent / 100;
            if (Cent != 0) {
                *(chaine + count) = Cent + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }

        if (Precision >= 2) 
        {
            
            Diz = value - (Mil * 1000) - (Cent * 100)-(DMil * 10000);
            Diz = Diz / 10;
            if (Diz != 0) {
                *(chaine + count) = Diz + 48;
                if (*(chaine + count) < 48)
                    *(chaine + count) = 48;
                if (*(chaine + count) > 57)
                    *(chaine + count) = 57;
            } else
                *(chaine + count) = 48;
            count++;
        }

        
        Unit = value - (Mil * 1000) - (Cent * 100) - (Diz * 10)-(DMil * 10000);
        *(chaine + count) = Unit + 48;
        if (*(chaine + count) < 48) 
            *(chaine + count) = 48;
        if (*(chaine + count) > 57)
            *(chaine + count) = 57;
    } else 
    {
        
        for (Mil = 0; Mil < Precision; Mil++) 
            *(chaine + Mil) = 48;
    }

} 

void TRAMEENVOIRS485DONNEES(void) {
    unsigned char u;
    unsigned char tmp;
    tmp = 0;



    do {

        u = TRAMEOUT[tmp];
        SAVcaracOEIL = u;
        RS485(u);


        PORTHbits.RH2  = !PORTHbits.RH2 ;
        tmp = tmp + 1;
    } while ((u != '*') && (tmp < 200));


}

void RS485(unsigned char carac) {
    
    if ((carac != SAVcaracOEIL)&&(FUNCTIONUSE == 'O'))
        carac = SAVcaracOEIL;

    if ((carac != SAVcaracOEIL)&&(FUNCTIONUSE == 'E')) 
        carac = SAVcaracOEIL;

    LATAbits.LATA4  = 1; 


    TXSTA1bits.TXEN = 0;

    RCSTA1bits.CREN = 0; 

    TXSTA1bits.TXEN = 1; 


    TXREG1 = carac;

    
    
    
    
    while (TXSTA1bits.TRMT == 0); 
    
    TXSTA1bits.TXEN = 0;

    LATAbits.LATA4  = 0; 
    
}

void Init_I2C(void) {

    
    DDRCbits.RC3 = 1; 
    DDRCbits.RC4 = 1; 
    SSP1STAT = 0x00; 
    SSP1CON1 = 0x28; 
    SSP1CON2 = 0x00; 
    SSP1ADD = 0x3B; 
}

void Init_RS485(void) {



    TRISCbits.TRISC5 = 0;
    TRISCbits.TRISC7 = 1;
    TRISCbits.TRISC6 = 0;
    TRISCbits.TRISC5 = 0; 

    TRISAbits.TRISA4 = 0; 



    RCSTA1bits.SPEN = 1; 
    TXSTA1bits.SYNC = 0; 
    BAUDCON1bits.BRG16 = 1;

    TXSTA1bits.BRGH = 1; 
    
    SPBRG1 = 51 ; 
    SPBRGH1 = 51  >> 8; 





    IPR1bits.RCIP = 1; 
    PIR1bits.RCIF = 0; 
    PIE1bits.RCIE = 0; 

    TXSTA1bits.TXEN = 0; 
    RCSTA1bits.RX9 = 0; 
    TXSTA1bits.TX9 = 0; 
    RCSTA1bits.CREN = 0; 
    RCONbits.IPEN = 1; 

}

unsigned char ECRIRE_HORLOGE(unsigned char zone, unsigned char Time) {
    unsigned char TMP;

    if (zone == 0)
        zone = zone & 0x7F; 
    if (zone == 1)
        zone = zone & 0x7F; 
    if (zone == 2)
        zone = zone & 0x3F; 
    if (zone == 3)
        zone = zone & 0x07; 
    if (zone == 4)
        zone = zone & 0x3F; 
    if (zone == 5)
        zone = zone & 0x1F; 
    if (zone == 6)
        zone = zone;
    if (zone == 7)
        zone = zone; 





    TMP = envoyerHORLOGE_i2c(0xD0, zone, Time);
    return (TMP);
}

unsigned char envoyerHORLOGE_i2c(unsigned char NumeroH, unsigned char ADDH, unsigned char dataH0) {

    SSP1CON2bits.SEN=1;while(SSP1CON2bits.SEN) ;
    WriteI2C1 (NumeroH); 
    WriteI2C1 (ADDH);
    WriteI2C1 (dataH0);
    SSP1CON2bits.PEN=1;while(SSP1CON2bits.PEN) ;
    delay_qms(11);

}

void init_uc(void) {
    
    
    TRISAbits.TRISA0 = 1; 
    TRISAbits.TRISA1 = 1; 
    TRISAbits.TRISA2 = 1; 
    TRISAbits.TRISA3 = 1; 
    TRISAbits.TRISA4 = 0; 
    TRISAbits.TRISA5 = 1; 
    TRISAbits.TRISA6 = 0; 
    TRISAbits.TRISA7 = 0; 

    TRISBbits.TRISB0 = 1; 
    TRISBbits.TRISB1 = 1; 
    TRISBbits.TRISB2 = 1; 
    TRISBbits.TRISB3 = 1; 
    TRISBbits.TRISB4 = 1; 
    TRISBbits.TRISB5 = 0; 
    TRISBbits.TRISB6 = 0; 
    TRISBbits.TRISB7 = 0; 

    TRISCbits.TRISC0 = 0; 
    TRISCbits.TRISC1 = 0; 
    TRISCbits.TRISC2 = 0; 
    TRISCbits.TRISC3 = 1; 
    TRISCbits.TRISC4 = 1; 
    
    
    

    TRISDbits.TRISD0 = 0; 
    TRISDbits.TRISD1 = 0; 
    TRISDbits.TRISD2 = 0; 
    TRISDbits.TRISD3 = 0; 
    TRISDbits.TRISD4 = 0; 
    TRISDbits.TRISD5 = 0; 
    TRISDbits.TRISD6 = 0; 
    TRISDbits.TRISD7 = 0; 
    
    TRISEbits.TRISE0 = 0; 
    TRISEbits.TRISE1 = 0; 
    TRISEbits.TRISE2 = 0; 
    TRISEbits.TRISE3 = 0; 
    TRISEbits.TRISE4 = 0; 
    TRISEbits.TRISE5 = 0; 
    TRISEbits.TRISE6 = 0; 
    TRISEbits.TRISE7 = 0; 


    TRISFbits.TRISF0 = 1; 
    TRISFbits.TRISF1 = 1; 
    TRISFbits.TRISF2 = 1; 
    TRISFbits.TRISF3 = 1; 
    TRISFbits.TRISF4 = 1; 
    TRISFbits.TRISF5 = 1; 
    TRISFbits.TRISF6 = 0; 
    TRISFbits.TRISF7 = 0; 

    TRISGbits.TRISG0 = 0; 
    TRISGbits.TRISG1 = 0; 
    TRISGbits.TRISG2 = 0; 
    TRISGbits.TRISG3 = 0; 
    TRISGbits.TRISG4 = 0; 
    

    TRISHbits.TRISH0 = 0; 
    TRISHbits.TRISH1 = 0; 
    TRISHbits.TRISH2 = 0; 
    TRISHbits.TRISH3 = 0; 
    TRISHbits.TRISH4 = 1; 
    TRISHbits.TRISH5 = 1; 
    TRISHbits.TRISH6 = 1; 
    TRISHbits.TRISH7 = 1; 

    TRISJbits.TRISJ0 = 0; 
    TRISJbits.TRISJ1 = 0; 
    TRISJbits.TRISJ2 = 0; 
    TRISJbits.TRISJ3 = 1; 
    TRISJbits.TRISJ4 = 1; 
    TRISJbits.TRISJ5 = 0; 
    TRISJbits.TRISJ6 = 0; 
    TRISJbits.TRISJ7 = 0; 




    
    ADCON1 = 0b00000111; 

    ADCON0 = 0; 

    
    

    
    
    
    

    ADCON2bits.ADFM = 1; 
    ADCON2bits.ACQT2 = 1; 
    ADCON2bits.ACQT1 = 0;
    ADCON2bits.ACQT0 = 1;

    ADCON2bits.ADCS2 = 0; 
    ADCON2bits.ADCS1 = 1; 
    ADCON2bits.ADCS0 = 0;
}