#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=cof
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/CMSCarteRelais.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/CMSCarteRelais.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../project/Scrutation/FonctionsScrutation.c ../project/Scrutation/InitScrut.c ../project/Scrutation/Mesure.c ../project/Scrutation/Delay.c ../project/Scrutation/EcritureTrame.c ../project/Scrutation/RS485.c ../project/Scrutation/Eeprom.c ../project/Scrutation/FonctionCom.c ../project/Scrutation/ScrutRes.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/2074710311/FonctionsScrutation.o ${OBJECTDIR}/_ext/2074710311/InitScrut.o ${OBJECTDIR}/_ext/2074710311/Mesure.o ${OBJECTDIR}/_ext/2074710311/Delay.o ${OBJECTDIR}/_ext/2074710311/EcritureTrame.o ${OBJECTDIR}/_ext/2074710311/RS485.o ${OBJECTDIR}/_ext/2074710311/Eeprom.o ${OBJECTDIR}/_ext/2074710311/FonctionCom.o ${OBJECTDIR}/_ext/2074710311/ScrutRes.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/2074710311/FonctionsScrutation.o.d ${OBJECTDIR}/_ext/2074710311/InitScrut.o.d ${OBJECTDIR}/_ext/2074710311/Mesure.o.d ${OBJECTDIR}/_ext/2074710311/Delay.o.d ${OBJECTDIR}/_ext/2074710311/EcritureTrame.o.d ${OBJECTDIR}/_ext/2074710311/RS485.o.d ${OBJECTDIR}/_ext/2074710311/Eeprom.o.d ${OBJECTDIR}/_ext/2074710311/FonctionCom.o.d ${OBJECTDIR}/_ext/2074710311/ScrutRes.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/2074710311/FonctionsScrutation.o ${OBJECTDIR}/_ext/2074710311/InitScrut.o ${OBJECTDIR}/_ext/2074710311/Mesure.o ${OBJECTDIR}/_ext/2074710311/Delay.o ${OBJECTDIR}/_ext/2074710311/EcritureTrame.o ${OBJECTDIR}/_ext/2074710311/RS485.o ${OBJECTDIR}/_ext/2074710311/Eeprom.o ${OBJECTDIR}/_ext/2074710311/FonctionCom.o ${OBJECTDIR}/_ext/2074710311/ScrutRes.o

# Source Files
SOURCEFILES=../project/Scrutation/FonctionsScrutation.c ../project/Scrutation/InitScrut.c ../project/Scrutation/Mesure.c ../project/Scrutation/Delay.c ../project/Scrutation/EcritureTrame.c ../project/Scrutation/RS485.c ../project/Scrutation/Eeprom.c ../project/Scrutation/FonctionCom.c ../project/Scrutation/ScrutRes.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/CMSCarteRelais.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18F8723
MP_PROCESSOR_OPTION_LD=18f8723
MP_LINKER_DEBUG_OPTION= -u_DEBUGCODESTART=0x1fd40 -u_DEBUGCODELEN=0x2c0 -u_DEBUGDATASTART=0xeef -u_DEBUGDATALEN=0x10
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/2074710311/FonctionsScrutation.o: ../project/Scrutation/FonctionsScrutation.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2074710311" 
	@${RM} ${OBJECTDIR}/_ext/2074710311/FonctionsScrutation.o.d 
	@${RM} ${OBJECTDIR}/_ext/2074710311/FonctionsScrutation.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/2074710311/FonctionsScrutation.o   ../project/Scrutation/FonctionsScrutation.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2074710311/FonctionsScrutation.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2074710311/FonctionsScrutation.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2074710311/InitScrut.o: ../project/Scrutation/InitScrut.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2074710311" 
	@${RM} ${OBJECTDIR}/_ext/2074710311/InitScrut.o.d 
	@${RM} ${OBJECTDIR}/_ext/2074710311/InitScrut.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/2074710311/InitScrut.o   ../project/Scrutation/InitScrut.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2074710311/InitScrut.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2074710311/InitScrut.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2074710311/Mesure.o: ../project/Scrutation/Mesure.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2074710311" 
	@${RM} ${OBJECTDIR}/_ext/2074710311/Mesure.o.d 
	@${RM} ${OBJECTDIR}/_ext/2074710311/Mesure.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/2074710311/Mesure.o   ../project/Scrutation/Mesure.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2074710311/Mesure.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2074710311/Mesure.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2074710311/Delay.o: ../project/Scrutation/Delay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2074710311" 
	@${RM} ${OBJECTDIR}/_ext/2074710311/Delay.o.d 
	@${RM} ${OBJECTDIR}/_ext/2074710311/Delay.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/2074710311/Delay.o   ../project/Scrutation/Delay.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2074710311/Delay.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2074710311/Delay.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2074710311/EcritureTrame.o: ../project/Scrutation/EcritureTrame.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2074710311" 
	@${RM} ${OBJECTDIR}/_ext/2074710311/EcritureTrame.o.d 
	@${RM} ${OBJECTDIR}/_ext/2074710311/EcritureTrame.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/2074710311/EcritureTrame.o   ../project/Scrutation/EcritureTrame.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2074710311/EcritureTrame.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2074710311/EcritureTrame.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2074710311/RS485.o: ../project/Scrutation/RS485.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2074710311" 
	@${RM} ${OBJECTDIR}/_ext/2074710311/RS485.o.d 
	@${RM} ${OBJECTDIR}/_ext/2074710311/RS485.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/2074710311/RS485.o   ../project/Scrutation/RS485.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2074710311/RS485.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2074710311/RS485.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2074710311/Eeprom.o: ../project/Scrutation/Eeprom.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2074710311" 
	@${RM} ${OBJECTDIR}/_ext/2074710311/Eeprom.o.d 
	@${RM} ${OBJECTDIR}/_ext/2074710311/Eeprom.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/2074710311/Eeprom.o   ../project/Scrutation/Eeprom.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2074710311/Eeprom.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2074710311/Eeprom.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2074710311/FonctionCom.o: ../project/Scrutation/FonctionCom.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2074710311" 
	@${RM} ${OBJECTDIR}/_ext/2074710311/FonctionCom.o.d 
	@${RM} ${OBJECTDIR}/_ext/2074710311/FonctionCom.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/2074710311/FonctionCom.o   ../project/Scrutation/FonctionCom.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2074710311/FonctionCom.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2074710311/FonctionCom.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2074710311/ScrutRes.o: ../project/Scrutation/ScrutRes.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2074710311" 
	@${RM} ${OBJECTDIR}/_ext/2074710311/ScrutRes.o.d 
	@${RM} ${OBJECTDIR}/_ext/2074710311/ScrutRes.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/2074710311/ScrutRes.o   ../project/Scrutation/ScrutRes.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2074710311/ScrutRes.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2074710311/ScrutRes.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
else
${OBJECTDIR}/_ext/2074710311/FonctionsScrutation.o: ../project/Scrutation/FonctionsScrutation.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2074710311" 
	@${RM} ${OBJECTDIR}/_ext/2074710311/FonctionsScrutation.o.d 
	@${RM} ${OBJECTDIR}/_ext/2074710311/FonctionsScrutation.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/2074710311/FonctionsScrutation.o   ../project/Scrutation/FonctionsScrutation.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2074710311/FonctionsScrutation.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2074710311/FonctionsScrutation.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2074710311/InitScrut.o: ../project/Scrutation/InitScrut.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2074710311" 
	@${RM} ${OBJECTDIR}/_ext/2074710311/InitScrut.o.d 
	@${RM} ${OBJECTDIR}/_ext/2074710311/InitScrut.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/2074710311/InitScrut.o   ../project/Scrutation/InitScrut.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2074710311/InitScrut.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2074710311/InitScrut.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2074710311/Mesure.o: ../project/Scrutation/Mesure.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2074710311" 
	@${RM} ${OBJECTDIR}/_ext/2074710311/Mesure.o.d 
	@${RM} ${OBJECTDIR}/_ext/2074710311/Mesure.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/2074710311/Mesure.o   ../project/Scrutation/Mesure.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2074710311/Mesure.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2074710311/Mesure.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2074710311/Delay.o: ../project/Scrutation/Delay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2074710311" 
	@${RM} ${OBJECTDIR}/_ext/2074710311/Delay.o.d 
	@${RM} ${OBJECTDIR}/_ext/2074710311/Delay.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/2074710311/Delay.o   ../project/Scrutation/Delay.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2074710311/Delay.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2074710311/Delay.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2074710311/EcritureTrame.o: ../project/Scrutation/EcritureTrame.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2074710311" 
	@${RM} ${OBJECTDIR}/_ext/2074710311/EcritureTrame.o.d 
	@${RM} ${OBJECTDIR}/_ext/2074710311/EcritureTrame.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/2074710311/EcritureTrame.o   ../project/Scrutation/EcritureTrame.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2074710311/EcritureTrame.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2074710311/EcritureTrame.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2074710311/RS485.o: ../project/Scrutation/RS485.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2074710311" 
	@${RM} ${OBJECTDIR}/_ext/2074710311/RS485.o.d 
	@${RM} ${OBJECTDIR}/_ext/2074710311/RS485.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/2074710311/RS485.o   ../project/Scrutation/RS485.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2074710311/RS485.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2074710311/RS485.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2074710311/Eeprom.o: ../project/Scrutation/Eeprom.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2074710311" 
	@${RM} ${OBJECTDIR}/_ext/2074710311/Eeprom.o.d 
	@${RM} ${OBJECTDIR}/_ext/2074710311/Eeprom.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/2074710311/Eeprom.o   ../project/Scrutation/Eeprom.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2074710311/Eeprom.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2074710311/Eeprom.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2074710311/FonctionCom.o: ../project/Scrutation/FonctionCom.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2074710311" 
	@${RM} ${OBJECTDIR}/_ext/2074710311/FonctionCom.o.d 
	@${RM} ${OBJECTDIR}/_ext/2074710311/FonctionCom.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/2074710311/FonctionCom.o   ../project/Scrutation/FonctionCom.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2074710311/FonctionCom.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2074710311/FonctionCom.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2074710311/ScrutRes.o: ../project/Scrutation/ScrutRes.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2074710311" 
	@${RM} ${OBJECTDIR}/_ext/2074710311/ScrutRes.o.d 
	@${RM} ${OBJECTDIR}/_ext/2074710311/ScrutRes.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) --verbose -I"../project/h" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/2074710311/ScrutRes.o   ../project/Scrutation/ScrutRes.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2074710311/ScrutRes.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2074710311/ScrutRes.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/CMSCarteRelais.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    ../project/18f8723_bigdata.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "..\project\18f8723_bigdata.lkr"  -p$(MP_PROCESSOR_OPTION_LD)  -w -x -u_DEBUG -m"$(BINDIR_)$(TARGETBASE).map" -l"../../../../MCC18/src/traditional/math" -l"../../../../mcc18/lib" -l"."  -z__MPLAB_BUILD=1  -u_CRUNTIME -z__MPLAB_DEBUG=1 $(MP_LINKER_DEBUG_OPTION) -l ${MP_CC_DIR}\\..\\lib  -o dist/${CND_CONF}/${IMAGE_TYPE}/CMSCarteRelais.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}   
else
dist/${CND_CONF}/${IMAGE_TYPE}/CMSCarteRelais.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   ../project/18f8723_bigdata.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "..\project\18f8723_bigdata.lkr"  -p$(MP_PROCESSOR_OPTION_LD)  -w  -m"$(BINDIR_)$(TARGETBASE).map" -l"../../../../MCC18/src/traditional/math" -l"../../../../mcc18/lib" -l"."  -z__MPLAB_BUILD=1  -u_CRUNTIME -l ${MP_CC_DIR}\\..\\lib  -o dist/${CND_CONF}/${IMAGE_TYPE}/CMSCarteRelais.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}   
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
