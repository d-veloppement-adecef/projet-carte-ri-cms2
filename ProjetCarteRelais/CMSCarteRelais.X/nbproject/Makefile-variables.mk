#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
# default configuration
CND_ARTIFACT_DIR_default=dist/default/production
CND_ARTIFACT_NAME_default=CMSCarteRelais.X.production.hex
CND_ARTIFACT_PATH_default=dist/default/production/CMSCarteRelais.X.production.hex
CND_PACKAGE_DIR_default=${CND_DISTDIR}/default/package
CND_PACKAGE_NAME_default=cmscarterelais.x.tar
CND_PACKAGE_PATH_default=${CND_DISTDIR}/default/package/cmscarterelais.x.tar
# Simulatorisation configuration
CND_ARTIFACT_DIR_Simulatorisation=dist/Simulatorisation/production
CND_ARTIFACT_NAME_Simulatorisation=CMSCarteRelais.X.production.hex
CND_ARTIFACT_PATH_Simulatorisation=dist/Simulatorisation/production/CMSCarteRelais.X.production.hex
CND_PACKAGE_DIR_Simulatorisation=${CND_DISTDIR}/Simulatorisation/package
CND_PACKAGE_NAME_Simulatorisation=cmscarterelais.x.tar
CND_PACKAGE_PATH_Simulatorisation=${CND_DISTDIR}/Simulatorisation/package/cmscarterelais.x.tar
